<?php
defined('_JEXEC') or die;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;
$doc->addStyleSheet('templates/'.$this->template.'/css/template.css');
$lang = $app->input->get('lang');

?>

<!DOCTYPE html>
<html>
<head>    
    <jdoc:include type="head" />
    <?php JHTML::_('behavior.modal'); ?>
</head>
<body>
	<div class="header">
		<div class="logo">
			<jdoc:include type="modules" name="logo" style="no"/>
		</div>
		<div class="top-menu">
	    	<jdoc:include type="modules" name="top-menu" style="no"/>
	    </div>
	    <div class="telephone">
	    	<jdoc:include type="modules" name="telephone" style="no"/>
	    </div>
	    <div class="search">
	    	<jdoc:include type="modules" name="search" style="no"/>
	    </div>
	    <div class="banner">
			<jdoc:include type="modules" name="banner" style="no"/>
		</div>
	</div>
	<div class="page">
		<div class="content">
			<jdoc:include type="modules" name="content" style="no"/>
		    <jdoc:include type="message" />
		    <jdoc:include type="component" />
		</div>
		<div class="news">
			<jdoc:include type="modules" name="news" style="xhtml"/>
			<jdoc:include type="message" />
		    <jdoc:include type="component" />
		</div>
		<div class="article">
			<jdoc:include type="modules" name="article" style="no"/>
		</div>
	</div>
	<div class="footer">
		<jdoc:include type="modules" name="footer" style="no"/>
	</div>
</body>
</html>







