-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Сен 25 2015 г., 16:53
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `lavka`
--

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_assets`
--

CREATE TABLE IF NOT EXISTS `yawnc_assets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set parent.',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `level` int(10) unsigned NOT NULL COMMENT 'The cached level in the nested tree.',
  `name` varchar(50) NOT NULL COMMENT 'The unique name for the asset.\n',
  `title` varchar(100) NOT NULL COMMENT 'The descriptive title for the asset.',
  `rules` varchar(5120) NOT NULL COMMENT 'JSON encoded access control.',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_asset_name` (`name`),
  KEY `idx_lft_rgt` (`lft`,`rgt`),
  KEY `idx_parent_id` (`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=74 ;

--
-- Дамп данных таблицы `yawnc_assets`
--

INSERT INTO `yawnc_assets` (`id`, `parent_id`, `lft`, `rgt`, `level`, `name`, `title`, `rules`) VALUES
(1, 0, 0, 141, 0, 'root.1', 'Root Asset', '{"core.login.site":{"6":1,"2":1},"core.login.admin":{"6":1},"core.login.offline":{"6":1},"core.admin":{"8":1},"core.manage":{"7":1},"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(2, 1, 1, 2, 1, 'com_admin', 'com_admin', '{}'),
(3, 1, 3, 6, 1, 'com_banners', 'com_banners', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(4, 1, 7, 8, 1, 'com_cache', 'com_cache', '{"core.admin":{"7":1},"core.manage":{"7":1}}'),
(5, 1, 9, 10, 1, 'com_checkin', 'com_checkin', '{"core.admin":{"7":1},"core.manage":{"7":1}}'),
(6, 1, 11, 12, 1, 'com_config', 'com_config', '{}'),
(7, 1, 13, 16, 1, 'com_contact', 'com_contact', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(8, 1, 17, 40, 1, 'com_content', 'com_content', '{"core.admin":{"7":1},"core.options":[],"core.manage":{"6":1},"core.create":{"3":1},"core.delete":[],"core.edit":{"4":1},"core.edit.state":{"5":1},"core.edit.own":[]}'),
(9, 1, 41, 42, 1, 'com_cpanel', 'com_cpanel', '{}'),
(10, 1, 43, 44, 1, 'com_installer', 'com_installer', '{"core.admin":[],"core.manage":{"7":0},"core.delete":{"7":0},"core.edit.state":{"7":0}}'),
(11, 1, 45, 46, 1, 'com_languages', 'com_languages', '{"core.admin":{"7":1},"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(12, 1, 47, 48, 1, 'com_login', 'com_login', '{}'),
(13, 1, 49, 50, 1, 'com_mailto', 'com_mailto', '{}'),
(14, 1, 51, 52, 1, 'com_massmail', 'com_massmail', '{}'),
(15, 1, 53, 54, 1, 'com_media', 'com_media', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":{"3":1},"core.delete":{"5":1}}'),
(16, 1, 55, 56, 1, 'com_menus', 'com_menus', '{"core.admin":{"7":1},"core.options":[],"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(17, 1, 57, 58, 1, 'com_messages', 'com_messages', '{"core.admin":{"7":1},"core.manage":{"7":1}}'),
(18, 1, 59, 110, 1, 'com_modules', 'com_modules', '{"core.admin":{"7":1},"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(19, 1, 111, 114, 1, 'com_newsfeeds', 'com_newsfeeds', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(20, 1, 115, 116, 1, 'com_plugins', 'com_plugins', '{"core.admin":{"7":1},"core.manage":[],"core.edit":[],"core.edit.state":[]}'),
(21, 1, 117, 118, 1, 'com_redirect', 'com_redirect', '{"core.admin":{"7":1},"core.manage":[]}'),
(22, 1, 119, 120, 1, 'com_search', 'com_search', '{"core.admin":{"7":1},"core.manage":{"6":1}}'),
(23, 1, 121, 122, 1, 'com_templates', 'com_templates', '{"core.admin":{"7":1},"core.options":[],"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(24, 1, 123, 126, 1, 'com_users', 'com_users', '{"core.admin":{"7":1},"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(26, 1, 127, 128, 1, 'com_wrapper', 'com_wrapper', '{}'),
(27, 8, 18, 25, 2, 'com_content.category.2', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(28, 3, 4, 5, 2, 'com_banners.category.3', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(29, 7, 14, 15, 2, 'com_contact.category.4', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(30, 19, 112, 113, 2, 'com_newsfeeds.category.5', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(32, 24, 124, 125, 1, 'com_users.category.7', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(33, 1, 129, 130, 1, 'com_finder', 'com_finder', '{"core.admin":{"7":1},"core.options":[],"core.manage":{"6":1},"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(34, 1, 131, 132, 1, 'com_joomlaupdate', 'com_joomlaupdate', '{"core.admin":[],"core.manage":[],"core.delete":[],"core.edit.state":[]}'),
(35, 1, 133, 134, 1, 'com_tags', 'com_tags', '{"core.admin":[],"core.manage":[],"core.manage":[],"core.delete":[],"core.edit.state":[]}'),
(36, 1, 135, 136, 1, 'com_contenthistory', 'com_contenthistory', '{}'),
(37, 1, 137, 138, 1, 'com_ajax', 'com_ajax', '{}'),
(38, 1, 139, 140, 1, 'com_postinstall', 'com_postinstall', '{}'),
(39, 18, 60, 61, 2, 'com_modules.module.1', 'Главное Меню', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(40, 18, 62, 63, 2, 'com_modules.module.2', 'Login', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(41, 18, 64, 65, 2, 'com_modules.module.3', 'Popular Articles', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(42, 18, 66, 67, 2, 'com_modules.module.4', 'Recently Added Articles', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(43, 18, 68, 69, 2, 'com_modules.module.8', 'Toolbar', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(44, 18, 70, 71, 2, 'com_modules.module.9', 'Quick Icons', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(45, 18, 72, 73, 2, 'com_modules.module.10', 'Logged-in Users', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(46, 18, 74, 75, 2, 'com_modules.module.12', 'Admin Menu', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(47, 18, 76, 77, 2, 'com_modules.module.13', 'Admin Submenu', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(48, 18, 78, 79, 2, 'com_modules.module.14', 'User Status', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(49, 18, 80, 81, 2, 'com_modules.module.15', 'Title', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(50, 18, 82, 83, 2, 'com_modules.module.16', 'Login Form', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(51, 18, 84, 85, 2, 'com_modules.module.17', 'Breadcrumbs', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(52, 18, 86, 87, 2, 'com_modules.module.79', 'Multilanguage status', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(53, 18, 88, 89, 2, 'com_modules.module.86', 'Joomla Version', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(54, 27, 23, 24, 3, 'com_content.article.1', 'Иконы Храмовые', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(55, 18, 90, 91, 2, 'com_modules.module.87', 'Верхнее меню', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}'),
(56, 18, 92, 93, 2, 'com_modules.module.88', 'Поиск', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}'),
(57, 18, 94, 95, 2, 'com_modules.module.89', 'Логотип', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}'),
(58, 18, 96, 97, 2, 'com_modules.module.90', 'Телефон', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}'),
(59, 18, 98, 99, 2, 'com_modules.module.91', 'Текст на баннере', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}'),
(60, 18, 100, 101, 2, 'com_modules.module.92', 'Баннер', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}'),
(61, 27, 21, 22, 3, 'com_content.article.2', 'Свечи восковые', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(62, 27, 19, 20, 3, 'com_content.article.3', 'Книги, полиграфия, молитвы на бересте', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(63, 8, 26, 27, 2, 'com_content.category.8', 'Лавка', '{"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(64, 18, 102, 103, 2, 'com_modules.module.93', 'Контент', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}'),
(65, 8, 28, 39, 2, 'com_content.category.9', 'Новости', '{"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(66, 65, 29, 30, 3, 'com_content.article.4', '"Воды, текущие в жизнь вечную"', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(67, 65, 31, 32, 3, 'com_content.article.5', 'Празднование Пасхи Христовой в Александровке', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(68, 65, 33, 34, 3, 'com_content.article.6', 'Масленица в Александровке', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(69, 65, 35, 36, 3, 'com_content.article.7', 'XIII Сретенский фестиваль духовной и народной музыки', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(70, 65, 37, 38, 3, 'com_content.article.8', 'XIII Сретенский фестиваль духовной и народной музыки', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(71, 18, 104, 105, 2, 'com_modules.module.94', 'Новости', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}'),
(72, 18, 106, 107, 2, 'com_modules.module.95', 'Статья', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}'),
(73, 18, 108, 109, 2, 'com_modules.module.96', 'Footer', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}');

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_associations`
--

CREATE TABLE IF NOT EXISTS `yawnc_associations` (
  `id` int(11) NOT NULL COMMENT 'A reference to the associated item.',
  `context` varchar(50) NOT NULL COMMENT 'The context of the associated item.',
  `key` char(32) NOT NULL COMMENT 'The key for the association computed from an md5 on associated ids.',
  PRIMARY KEY (`context`,`id`),
  KEY `idx_key` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_banners`
--

CREATE TABLE IF NOT EXISTS `yawnc_banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `imptotal` int(11) NOT NULL DEFAULT '0',
  `impmade` int(11) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `clickurl` varchar(200) NOT NULL DEFAULT '',
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `custombannercode` varchar(2048) NOT NULL,
  `sticky` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `params` text NOT NULL,
  `own_prefix` tinyint(1) NOT NULL DEFAULT '0',
  `metakey_prefix` varchar(255) NOT NULL DEFAULT '',
  `purchase_type` tinyint(4) NOT NULL DEFAULT '-1',
  `track_clicks` tinyint(4) NOT NULL DEFAULT '-1',
  `track_impressions` tinyint(4) NOT NULL DEFAULT '-1',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `reset` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `language` char(7) NOT NULL DEFAULT '',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_state` (`state`),
  KEY `idx_own_prefix` (`own_prefix`),
  KEY `idx_metakey_prefix` (`metakey_prefix`),
  KEY `idx_banner_catid` (`catid`),
  KEY `idx_language` (`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_banner_clients`
--

CREATE TABLE IF NOT EXISTS `yawnc_banner_clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `contact` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `extrainfo` text NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `metakey` text NOT NULL,
  `own_prefix` tinyint(4) NOT NULL DEFAULT '0',
  `metakey_prefix` varchar(255) NOT NULL DEFAULT '',
  `purchase_type` tinyint(4) NOT NULL DEFAULT '-1',
  `track_clicks` tinyint(4) NOT NULL DEFAULT '-1',
  `track_impressions` tinyint(4) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`),
  KEY `idx_own_prefix` (`own_prefix`),
  KEY `idx_metakey_prefix` (`metakey_prefix`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_banner_tracks`
--

CREATE TABLE IF NOT EXISTS `yawnc_banner_tracks` (
  `track_date` datetime NOT NULL,
  `track_type` int(10) unsigned NOT NULL,
  `banner_id` int(10) unsigned NOT NULL,
  `count` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`track_date`,`track_type`,`banner_id`),
  KEY `idx_track_date` (`track_date`),
  KEY `idx_track_type` (`track_type`),
  KEY `idx_banner_id` (`banner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_categories`
--

CREATE TABLE IF NOT EXISTS `yawnc_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `level` int(10) unsigned NOT NULL DEFAULT '0',
  `path` varchar(255) NOT NULL DEFAULT '',
  `extension` varchar(50) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `metadesc` varchar(1024) NOT NULL COMMENT 'The meta description for the page.',
  `metakey` varchar(1024) NOT NULL COMMENT 'The meta keywords for the page.',
  `metadata` varchar(2048) NOT NULL COMMENT 'JSON encoded metadata properties.',
  `created_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL,
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `cat_idx` (`extension`,`published`,`access`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_path` (`path`),
  KEY `idx_left_right` (`lft`,`rgt`),
  KEY `idx_alias` (`alias`),
  KEY `idx_language` (`language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `yawnc_categories`
--

INSERT INTO `yawnc_categories` (`id`, `asset_id`, `parent_id`, `lft`, `rgt`, `level`, `path`, `extension`, `title`, `alias`, `note`, `description`, `published`, `checked_out`, `checked_out_time`, `access`, `params`, `metadesc`, `metakey`, `metadata`, `created_user_id`, `created_time`, `modified_user_id`, `modified_time`, `hits`, `language`, `version`) VALUES
(1, 0, 0, 0, 15, 0, '', 'system', 'ROOT', 'root', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{}', '', '', '{}', 42, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(2, 27, 1, 1, 2, 1, 'uncategorised', 'com_content', 'Uncategorised', 'uncategorised', '', '', -2, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 42, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(3, 28, 1, 3, 4, 1, 'uncategorised', 'com_banners', 'Uncategorised', 'uncategorised', '', '', 1, 595, '2015-09-14 11:39:57', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 42, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(4, 29, 1, 5, 6, 1, 'uncategorised', 'com_contact', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 42, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(5, 30, 1, 7, 8, 1, 'uncategorised', 'com_newsfeeds', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 42, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(7, 32, 1, 9, 10, 1, 'uncategorised', 'com_users', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 42, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(8, 63, 1, 11, 12, 1, 'lavka', 'com_content', 'Лавка', 'lavka', '', '', -2, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":"","image_alt":""}', '', '', '{"author":"","robots":""}', 595, '2015-09-16 14:40:19', 0, '2015-09-16 14:40:19', 0, '*', 1),
(9, 65, 1, 13, 14, 1, 'novosti', 'com_content', 'Новости', 'novosti', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":"","image_alt":""}', '', '', '{"author":"","robots":""}', 595, '2015-09-18 12:15:24', 0, '2015-09-18 12:15:24', 0, '*', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_contact_details`
--

CREATE TABLE IF NOT EXISTS `yawnc_contact_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `con_position` varchar(255) DEFAULT NULL,
  `address` text,
  `suburb` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `postcode` varchar(100) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `misc` mediumtext,
  `image` varchar(255) DEFAULT NULL,
  `email_to` varchar(255) DEFAULT NULL,
  `default_con` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `catid` int(11) NOT NULL DEFAULT '0',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `mobile` varchar(255) NOT NULL DEFAULT '',
  `webpage` varchar(255) NOT NULL DEFAULT '',
  `sortname1` varchar(255) NOT NULL,
  `sortname2` varchar(255) NOT NULL,
  `sortname3` varchar(255) NOT NULL,
  `language` char(7) NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `metadata` text NOT NULL,
  `featured` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Set if article is featured.',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`published`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`),
  KEY `idx_featured_catid` (`featured`,`catid`),
  KEY `idx_language` (`language`),
  KEY `idx_xreference` (`xreference`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_content`
--

CREATE TABLE IF NOT EXISTS `yawnc_content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `title` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `introtext` mediumtext NOT NULL,
  `fulltext` mediumtext NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` text NOT NULL,
  `urls` text NOT NULL,
  `attribs` varchar(5120) NOT NULL,
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `metadata` text NOT NULL,
  `featured` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Set if article is featured.',
  `language` char(7) NOT NULL COMMENT 'The language code for the article.',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  PRIMARY KEY (`id`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`state`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`),
  KEY `idx_featured_catid` (`featured`,`catid`),
  KEY `idx_language` (`language`),
  KEY `idx_xreference` (`xreference`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Дамп данных таблицы `yawnc_content`
--

INSERT INTO `yawnc_content` (`id`, `asset_id`, `title`, `alias`, `introtext`, `fulltext`, `state`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`, `featured`, `language`, `xreference`) VALUES
(1, 54, 'Иконы Храмовые', 'ikony-khramovye', '<p>варпцрапуолафцралфуоадлфцладжфцладжфцлджафцладжфцладжйфца</p>\r\n<p>арвфцгшафцгшфцшарфцгшарфцгшарвфцгшарфцгшарфцгшарфцгшарфцг</p>', '', -2, 2, '2015-09-10 16:18:34', 595, '', '2015-09-16 14:41:48', 595, 0, '0000-00-00 00:00:00', '2015-09-10 16:18:34', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 2, 2, '', '', 1, 0, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(2, 61, 'Свечи восковые', 'svechi-voskovye', 'пуывпмфуапфцафца', '', -2, 2, '2015-09-16 13:53:12', 595, '', '2015-09-16 14:43:22', 595, 0, '0000-00-00 00:00:00', '2015-09-16 13:53:12', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 3, 1, '', '', 1, 0, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(3, 62, 'Книги, полиграфия, молитвы на бересте', 'knigi-poligrafiya-molitvy-na-bereste', 'пыкпфыцпяпяяпяпякпкпкп', '', -2, 2, '2015-09-16 13:54:09', 595, '', '2015-09-16 15:04:38', 595, 0, '0000-00-00 00:00:00', '2015-09-16 13:54:09', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 5, 0, '', '', 1, 0, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(4, 66, '"Воды, текущие в жизнь вечную"', 'vody-tekushchie-v-zhizn-vechnuyu', '', '\r\nлмпапри', 1, 9, '2015-09-18 12:16:59', 595, '', '2015-09-19 13:56:18', 595, 0, '0000-00-00 00:00:00', '2015-09-18 12:16:59', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 8, 4, '', '', 1, 0, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(5, 67, 'Празднование Пасхи Христовой в Александровке', 'prazdnovanie-paskhi-khristovoj-v-aleksandrovke', '', '\r\nмпапрапри р', 1, 9, '2015-09-18 12:17:42', 595, '', '2015-09-19 13:56:02', 595, 0, '0000-00-00 00:00:00', '2015-09-18 12:17:42', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 2, 3, '', '', 1, 0, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(6, 68, 'Масленица в Александровке', 'maslenitsa-v-aleksandrovke', '', '\r\nотормрпарп', 1, 9, '2015-09-18 12:18:05', 595, '', '2015-09-19 13:55:42', 595, 0, '0000-00-00 00:00:00', '2015-09-18 12:18:05', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 2, 2, '', '', 1, 0, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(7, 69, 'XIII Сретенский фестиваль духовной и народной музыки', 'xiii-sretenskij-festival-dukhovnoj-i-narodnoj-muzyki', '', '\r\nрпрапрари', 1, 9, '2015-09-18 12:19:02', 595, '', '2015-09-19 13:55:20', 595, 0, '0000-00-00 00:00:00', '2015-09-18 12:19:02', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 6, 1, '', '', 1, 0, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(8, 70, 'XIII Сретенский фестиваль духовной и народной музыки', 'xiii-sretenskij-festival-dukhovnoj-i-narodnoj-muzyki-2', '', '\r\nghbdnsjhkjlkj', 1, 9, '2015-09-18 12:19:39', 595, '', '2015-09-19 13:55:01', 595, 0, '0000-00-00 00:00:00', '2015-09-18 12:19:39', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"0","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 34, 0, '', '', 1, 0, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', '');

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_contentitem_tag_map`
--

CREATE TABLE IF NOT EXISTS `yawnc_contentitem_tag_map` (
  `type_alias` varchar(255) NOT NULL DEFAULT '',
  `core_content_id` int(10) unsigned NOT NULL COMMENT 'PK from the core content table',
  `content_item_id` int(11) NOT NULL COMMENT 'PK from the content type table',
  `tag_id` int(10) unsigned NOT NULL COMMENT 'PK from the tag table',
  `tag_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date of most recent save for this tag-item',
  `type_id` mediumint(8) NOT NULL COMMENT 'PK from the content_type table',
  UNIQUE KEY `uc_ItemnameTagid` (`type_id`,`content_item_id`,`tag_id`),
  KEY `idx_tag_type` (`tag_id`,`type_id`),
  KEY `idx_date_id` (`tag_date`,`tag_id`),
  KEY `idx_tag` (`tag_id`),
  KEY `idx_type` (`type_id`),
  KEY `idx_core_content_id` (`core_content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Maps items from content tables to tags';

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_content_frontpage`
--

CREATE TABLE IF NOT EXISTS `yawnc_content_frontpage` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_content_rating`
--

CREATE TABLE IF NOT EXISTS `yawnc_content_rating` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `rating_sum` int(10) unsigned NOT NULL DEFAULT '0',
  `rating_count` int(10) unsigned NOT NULL DEFAULT '0',
  `lastip` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_content_types`
--

CREATE TABLE IF NOT EXISTS `yawnc_content_types` (
  `type_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_title` varchar(255) NOT NULL DEFAULT '',
  `type_alias` varchar(255) NOT NULL DEFAULT '',
  `table` varchar(255) NOT NULL DEFAULT '',
  `rules` text NOT NULL,
  `field_mappings` text NOT NULL,
  `router` varchar(255) NOT NULL DEFAULT '',
  `content_history_options` varchar(5120) DEFAULT NULL COMMENT 'JSON string for com_contenthistory options',
  PRIMARY KEY (`type_id`),
  KEY `idx_alias` (`type_alias`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Дамп данных таблицы `yawnc_content_types`
--

INSERT INTO `yawnc_content_types` (`type_id`, `type_title`, `type_alias`, `table`, `rules`, `field_mappings`, `router`, `content_history_options`) VALUES
(1, 'Article', 'com_content.article', '{"special":{"dbtable":"#__content","key":"id","type":"Content","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"state","core_alias":"alias","core_created_time":"created","core_modified_time":"modified","core_body":"introtext", "core_hits":"hits","core_publish_up":"publish_up","core_publish_down":"publish_down","core_access":"access", "core_params":"attribs", "core_featured":"featured", "core_metadata":"metadata", "core_language":"language", "core_images":"images", "core_urls":"urls", "core_version":"version", "core_ordering":"ordering", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"catid", "core_xreference":"xreference", "asset_id":"asset_id"}, "special":{"fulltext":"fulltext"}}', 'ContentHelperRoute::getArticleRoute', '{"formFile":"administrator\\/components\\/com_content\\/models\\/forms\\/article.xml", "hideFields":["asset_id","checked_out","checked_out_time","version"],"ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time", "version", "hits"],"convertToInt":["publish_up", "publish_down", "featured", "ordering"],"displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"} ]}'),
(2, 'Contact', 'com_contact.contact', '{"special":{"dbtable":"#__contact_details","key":"id","type":"Contact","prefix":"ContactTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"name","core_state":"published","core_alias":"alias","core_created_time":"created","core_modified_time":"modified","core_body":"address", "core_hits":"hits","core_publish_up":"publish_up","core_publish_down":"publish_down","core_access":"access", "core_params":"params", "core_featured":"featured", "core_metadata":"metadata", "core_language":"language", "core_images":"image", "core_urls":"webpage", "core_version":"version", "core_ordering":"ordering", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"catid", "core_xreference":"xreference", "asset_id":"null"}, "special":{"con_position":"con_position","suburb":"suburb","state":"state","country":"country","postcode":"postcode","telephone":"telephone","fax":"fax","misc":"misc","email_to":"email_to","default_con":"default_con","user_id":"user_id","mobile":"mobile","sortname1":"sortname1","sortname2":"sortname2","sortname3":"sortname3"}}', 'ContactHelperRoute::getContactRoute', '{"formFile":"administrator\\/components\\/com_contact\\/models\\/forms\\/contact.xml","hideFields":["default_con","checked_out","checked_out_time","version","xreference"],"ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time", "version", "hits"],"convertToInt":["publish_up", "publish_down", "featured", "ordering"], "displayLookup":[ {"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"} ] }'),
(3, 'Newsfeed', 'com_newsfeeds.newsfeed', '{"special":{"dbtable":"#__newsfeeds","key":"id","type":"Newsfeed","prefix":"NewsfeedsTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"name","core_state":"published","core_alias":"alias","core_created_time":"created","core_modified_time":"modified","core_body":"description", "core_hits":"hits","core_publish_up":"publish_up","core_publish_down":"publish_down","core_access":"access", "core_params":"params", "core_featured":"featured", "core_metadata":"metadata", "core_language":"language", "core_images":"images", "core_urls":"link", "core_version":"version", "core_ordering":"ordering", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"catid", "core_xreference":"xreference", "asset_id":"null"}, "special":{"numarticles":"numarticles","cache_time":"cache_time","rtl":"rtl"}}', 'NewsfeedsHelperRoute::getNewsfeedRoute', '{"formFile":"administrator\\/components\\/com_newsfeeds\\/models\\/forms\\/newsfeed.xml","hideFields":["asset_id","checked_out","checked_out_time","version"],"ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time", "version", "hits"],"convertToInt":["publish_up", "publish_down", "featured", "ordering"],"displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"} ]}'),
(4, 'User', 'com_users.user', '{"special":{"dbtable":"#__users","key":"id","type":"User","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"name","core_state":"null","core_alias":"username","core_created_time":"registerdate","core_modified_time":"lastvisitDate","core_body":"null", "core_hits":"null","core_publish_up":"null","core_publish_down":"null","access":"null", "core_params":"params", "core_featured":"null", "core_metadata":"null", "core_language":"null", "core_images":"null", "core_urls":"null", "core_version":"null", "core_ordering":"null", "core_metakey":"null", "core_metadesc":"null", "core_catid":"null", "core_xreference":"null", "asset_id":"null"}, "special":{}}', 'UsersHelperRoute::getUserRoute', ''),
(5, 'Article Category', 'com_content.category', '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special":{"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}', 'ContentHelperRoute::getCategoryRoute', '{"formFile":"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml", "hideFields":["asset_id","checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"],"convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}'),
(6, 'Contact Category', 'com_contact.category', '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special":{"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}', 'ContactHelperRoute::getCategoryRoute', '{"formFile":"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml", "hideFields":["asset_id","checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"],"convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}'),
(7, 'Newsfeeds Category', 'com_newsfeeds.category', '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special":{"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}', 'NewsfeedsHelperRoute::getCategoryRoute', '{"formFile":"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml", "hideFields":["asset_id","checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"],"convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}'),
(8, 'Tag', 'com_tags.tag', '{"special":{"dbtable":"#__tags","key":"tag_id","type":"Tag","prefix":"TagsTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"featured", "core_metadata":"metadata", "core_language":"language", "core_images":"images", "core_urls":"urls", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"null", "core_xreference":"null", "asset_id":"null"}, "special":{"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path"}}', 'TagsHelperRoute::getTagRoute', '{"formFile":"administrator\\/components\\/com_tags\\/models\\/forms\\/tag.xml", "hideFields":["checked_out","checked_out_time","version", "lft", "rgt", "level", "path", "urls", "publish_up", "publish_down"],"ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"],"convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}, {"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"}, {"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}'),
(9, 'Banner', 'com_banners.banner', '{"special":{"dbtable":"#__banners","key":"id","type":"Banner","prefix":"BannersTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"name","core_state":"published","core_alias":"alias","core_created_time":"created","core_modified_time":"modified","core_body":"description", "core_hits":"null","core_publish_up":"publish_up","core_publish_down":"publish_down","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"images", "core_urls":"link", "core_version":"version", "core_ordering":"ordering", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"catid", "core_xreference":"null", "asset_id":"null"}, "special":{"imptotal":"imptotal", "impmade":"impmade", "clicks":"clicks", "clickurl":"clickurl", "custombannercode":"custombannercode", "cid":"cid", "purchase_type":"purchase_type", "track_impressions":"track_impressions", "track_clicks":"track_clicks"}}', '', '{"formFile":"administrator\\/components\\/com_banners\\/models\\/forms\\/banner.xml", "hideFields":["checked_out","checked_out_time","version", "reset"],"ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time", "version", "imptotal", "impmade", "reset"], "convertToInt":["publish_up", "publish_down", "ordering"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}, {"sourceColumn":"cid","targetTable":"#__banner_clients","targetColumn":"id","displayColumn":"name"}, {"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"} ]}'),
(10, 'Banners Category', 'com_banners.category', '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special": {"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}', '', '{"formFile":"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml", "hideFields":["asset_id","checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}'),
(11, 'Banner Client', 'com_banners.client', '{"special":{"dbtable":"#__banner_clients","key":"id","type":"Client","prefix":"BannersTable"}}', '', '', '', '{"formFile":"administrator\\/components\\/com_banners\\/models\\/forms\\/client.xml", "hideFields":["checked_out","checked_out_time"], "ignoreChanges":["checked_out", "checked_out_time"], "convertToInt":[], "displayLookup":[]}'),
(12, 'User Notes', 'com_users.note', '{"special":{"dbtable":"#__user_notes","key":"id","type":"Note","prefix":"UsersTable"}}', '', '', '', '{"formFile":"administrator\\/components\\/com_users\\/models\\/forms\\/note.xml", "hideFields":["checked_out","checked_out_time", "publish_up", "publish_down"],"ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"],"displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}, {"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}, {"sourceColumn":"user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}, {"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}'),
(13, 'User Notes Category', 'com_users.category', '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special":{"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}', '', '{"formFile":"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml", "hideFields":["checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}, {"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}');

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_core_log_searches`
--

CREATE TABLE IF NOT EXISTS `yawnc_core_log_searches` (
  `search_term` varchar(128) NOT NULL DEFAULT '',
  `hits` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_extensions`
--

CREATE TABLE IF NOT EXISTS `yawnc_extensions` (
  `extension_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `type` varchar(20) NOT NULL,
  `element` varchar(100) NOT NULL,
  `folder` varchar(100) NOT NULL,
  `client_id` tinyint(3) NOT NULL,
  `enabled` tinyint(3) NOT NULL DEFAULT '1',
  `access` int(10) unsigned NOT NULL DEFAULT '1',
  `protected` tinyint(3) NOT NULL DEFAULT '0',
  `manifest_cache` text NOT NULL,
  `params` text NOT NULL,
  `custom_data` text NOT NULL,
  `system_data` text NOT NULL,
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) DEFAULT '0',
  `state` int(11) DEFAULT '0',
  PRIMARY KEY (`extension_id`),
  KEY `element_clientid` (`element`,`client_id`),
  KEY `element_folder_clientid` (`element`,`folder`,`client_id`),
  KEY `extension` (`type`,`element`,`folder`,`client_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10005 ;

--
-- Дамп данных таблицы `yawnc_extensions`
--

INSERT INTO `yawnc_extensions` (`extension_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(1, 'com_mailto', 'component', 'com_mailto', '', 0, 1, 1, 1, '{"name":"com_mailto","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_MAILTO_XML_DESCRIPTION","group":"","filename":"mailto"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(2, 'com_wrapper', 'component', 'com_wrapper', '', 0, 1, 1, 1, '{"name":"com_wrapper","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.\\n\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_WRAPPER_XML_DESCRIPTION","group":"","filename":"wrapper"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(3, 'com_admin', 'component', 'com_admin', '', 1, 1, 1, 1, '{"name":"com_admin","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_ADMIN_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(4, 'com_banners', 'component', 'com_banners', '', 1, 1, 1, 0, '{"name":"com_banners","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_BANNERS_XML_DESCRIPTION","group":"","filename":"banners"}', '{"purchase_type":"3","track_impressions":"0","track_clicks":"0","metakey_prefix":"","save_history":"1","history_limit":10}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(5, 'com_cache', 'component', 'com_cache', '', 1, 1, 1, 1, '{"name":"com_cache","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CACHE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(6, 'com_categories', 'component', 'com_categories', '', 1, 1, 1, 1, '{"name":"com_categories","type":"component","creationDate":"December 2007","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CATEGORIES_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(7, 'com_checkin', 'component', 'com_checkin', '', 1, 1, 1, 1, '{"name":"com_checkin","type":"component","creationDate":"Unknown","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CHECKIN_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(8, 'com_contact', 'component', 'com_contact', '', 1, 1, 1, 0, '{"name":"com_contact","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CONTACT_XML_DESCRIPTION","group":"","filename":"contact"}', '{"show_contact_category":"hide","save_history":"1","history_limit":10,"show_contact_list":"0","presentation_style":"sliders","show_name":"1","show_position":"1","show_email":"0","show_street_address":"1","show_suburb":"1","show_state":"1","show_postcode":"1","show_country":"1","show_telephone":"1","show_mobile":"1","show_fax":"1","show_webpage":"1","show_misc":"1","show_image":"1","image":"","allow_vcard":"0","show_articles":"0","show_profile":"0","show_links":"0","linka_name":"","linkb_name":"","linkc_name":"","linkd_name":"","linke_name":"","contact_icons":"0","icon_address":"","icon_email":"","icon_telephone":"","icon_mobile":"","icon_fax":"","icon_misc":"","show_headings":"1","show_position_headings":"1","show_email_headings":"0","show_telephone_headings":"1","show_mobile_headings":"0","show_fax_headings":"0","allow_vcard_headings":"0","show_suburb_headings":"1","show_state_headings":"1","show_country_headings":"1","show_email_form":"1","show_email_copy":"1","banned_email":"","banned_subject":"","banned_text":"","validate_session":"1","custom_reply":"0","redirect":"","show_category_crumb":"0","metakey":"","metadesc":"","robots":"","author":"","rights":"","xreference":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(9, 'com_cpanel', 'component', 'com_cpanel', '', 1, 1, 1, 1, '{"name":"com_cpanel","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CPANEL_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10, 'com_installer', 'component', 'com_installer', '', 1, 1, 1, 1, '{"name":"com_installer","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_INSTALLER_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(11, 'com_languages', 'component', 'com_languages', '', 1, 1, 1, 1, '{"name":"com_languages","type":"component","creationDate":"2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_LANGUAGES_XML_DESCRIPTION","group":""}', '{"administrator":"ru-RU","site":"ru-RU"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(12, 'com_login', 'component', 'com_login', '', 1, 1, 1, 1, '{"name":"com_login","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_LOGIN_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(13, 'com_media', 'component', 'com_media', '', 1, 1, 0, 1, '{"name":"com_media","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_MEDIA_XML_DESCRIPTION","group":"","filename":"media"}', '{"upload_extensions":"bmp,csv,doc,gif,ico,jpg,jpeg,odg,odp,ods,odt,pdf,png,ppt,swf,txt,xcf,xls,BMP,CSV,DOC,GIF,ICO,JPG,JPEG,ODG,ODP,ODS,ODT,PDF,PNG,PPT,SWF,TXT,XCF,XLS","upload_maxsize":"10","file_path":"images","image_path":"images","restrict_uploads":"1","allowed_media_usergroup":"3","check_mime":"1","image_extensions":"bmp,gif,jpg,png","ignore_extensions":"","upload_mime":"image\\/jpeg,image\\/gif,image\\/png,image\\/bmp,application\\/x-shockwave-flash,application\\/msword,application\\/excel,application\\/pdf,application\\/powerpoint,text\\/plain,application\\/x-zip","upload_mime_illegal":"text\\/html"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(14, 'com_menus', 'component', 'com_menus', '', 1, 1, 1, 1, '{"name":"com_menus","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_MENUS_XML_DESCRIPTION","group":""}', '{"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(15, 'com_messages', 'component', 'com_messages', '', 1, 1, 1, 1, '{"name":"com_messages","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_MESSAGES_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(16, 'com_modules', 'component', 'com_modules', '', 1, 1, 1, 1, '{"name":"com_modules","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_MODULES_XML_DESCRIPTION","group":""}', '{"redirect_edit":"site"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(17, 'com_newsfeeds', 'component', 'com_newsfeeds', '', 1, 1, 1, 0, '{"name":"com_newsfeeds","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_NEWSFEEDS_XML_DESCRIPTION","group":"","filename":"newsfeeds"}', '{"newsfeed_layout":"_:default","save_history":"1","history_limit":5,"show_feed_image":"1","show_feed_description":"1","show_item_description":"1","feed_character_count":"0","feed_display_order":"des","float_first":"right","float_second":"right","show_tags":"1","category_layout":"_:default","show_category_title":"1","show_description":"1","show_description_image":"1","maxLevel":"-1","show_empty_categories":"0","show_subcat_desc":"1","show_cat_items":"1","show_cat_tags":"1","show_base_description":"1","maxLevelcat":"-1","show_empty_categories_cat":"0","show_subcat_desc_cat":"1","show_cat_items_cat":"1","filter_field":"1","show_pagination_limit":"1","show_headings":"1","show_articles":"0","show_link":"1","show_pagination":"1","show_pagination_results":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(18, 'com_plugins', 'component', 'com_plugins', '', 1, 1, 1, 1, '{"name":"com_plugins","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_PLUGINS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(19, 'com_search', 'component', 'com_search', '', 1, 1, 1, 0, '{"name":"com_search","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_SEARCH_XML_DESCRIPTION","group":"","filename":"search"}', '{"enabled":"0","show_date":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(20, 'com_templates', 'component', 'com_templates', '', 1, 1, 1, 1, '{"name":"com_templates","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_TEMPLATES_XML_DESCRIPTION","group":""}', '{"template_positions_display":"1","upload_limit":"2","image_formats":"gif,bmp,jpg,jpeg,png","source_formats":"txt,less,ini,xml,js,php,css","font_formats":"woff,ttf,otf","compressed_formats":"zip"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(22, 'com_content', 'component', 'com_content', '', 1, 1, 0, 1, '{"name":"com_content","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CONTENT_XML_DESCRIPTION","group":"","filename":"content"}', '{"article_layout":"_:default","show_title":"1","link_titles":"0","show_intro":"1","info_block_position":"0","show_category":"1","link_category":"1","show_parent_category":"1","link_parent_category":"0","show_author":"0","link_author":"0","show_create_date":"1","show_modify_date":"1","show_publish_date":"1","show_item_navigation":"1","show_vote":"0","show_readmore":"1","show_readmore_title":"1","readmore_limit":"100","show_tags":"1","show_icons":"1","show_print_icon":"0","show_email_icon":"0","show_hits":"0","show_noauth":"0","urls_position":"0","show_publishing_options":"1","show_article_options":"1","save_history":"1","history_limit":10,"show_urls_images_frontend":"0","show_urls_images_backend":"1","targeta":0,"targetb":0,"targetc":0,"float_intro":"left","float_fulltext":"left","category_layout":"_:blog","show_category_heading_title_text":"1","show_category_title":"1","show_description":"0","show_description_image":"0","maxLevel":"1","show_empty_categories":"0","show_no_articles":"1","show_subcat_desc":"1","show_cat_num_articles":"0","show_cat_tags":"1","show_base_description":"1","maxLevelcat":"-1","show_empty_categories_cat":"0","show_subcat_desc_cat":"0","show_cat_num_articles_cat":"1","num_leading_articles":"0","num_intro_articles":"6","num_columns":"3","num_links":"4","multi_column_order":"0","show_subcategory_content":"-1","show_pagination_limit":"1","filter_field":"hide","show_headings":"1","list_show_date":"0","date_format":"","list_show_hits":"1","list_show_author":"1","orderby_pri":"order","orderby_sec":"rdate","order_date":"published","show_pagination":"2","show_pagination_results":"1","show_featured":"show","show_feed_link":"1","feed_summary":"0","feed_show_readmore":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(23, 'com_config', 'component', 'com_config', '', 1, 1, 0, 1, '{"name":"com_config","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CONFIG_XML_DESCRIPTION","group":""}', '{"filters":{"1":{"filter_type":"NH","filter_tags":"","filter_attributes":""},"9":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"6":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"7":{"filter_type":"NONE","filter_tags":"","filter_attributes":""},"2":{"filter_type":"NH","filter_tags":"","filter_attributes":""},"3":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"4":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"5":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"8":{"filter_type":"NONE","filter_tags":"","filter_attributes":""}}}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(24, 'com_redirect', 'component', 'com_redirect', '', 1, 1, 0, 1, '{"name":"com_redirect","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_REDIRECT_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(25, 'com_users', 'component', 'com_users', '', 1, 1, 0, 1, '{"name":"com_users","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_USERS_XML_DESCRIPTION","group":"","filename":"users"}', '{"allowUserRegistration":"0","new_usertype":"2","guest_usergroup":"9","sendpassword":"1","useractivation":"1","mail_to_admin":"0","captcha":"","frontend_userparams":"1","site_language":"0","change_login_name":"0","reset_count":"10","reset_time":"1","minimum_length":"4","minimum_integers":"0","minimum_symbols":"0","minimum_uppercase":"0","save_history":"1","history_limit":5,"mailSubjectPrefix":"","mailBodySuffix":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(27, 'com_finder', 'component', 'com_finder', '', 1, 1, 0, 0, '{"name":"com_finder","type":"component","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_FINDER_XML_DESCRIPTION","group":"","filename":"finder"}', '{"enabled":"0","show_description":"0","description_length":255,"allow_empty_query":"0","show_url":"1","show_autosuggest":"1","show_suggested_query":"1","show_explained_query":"1","show_advanced":"1","show_advanced_tips":"1","expand_advanced":"0","show_date_filters":"0","sort_order":"relevance","sort_direction":"desc","highlight_terms":"1","opensearch_name":"","opensearch_description":"","batch_size":"50","memory_table_limit":30000,"title_multiplier":"1.7","text_multiplier":"0.7","meta_multiplier":"1.2","path_multiplier":"2.0","misc_multiplier":"0.3","stem":"1","stemmer":"snowball","enable_logging":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(28, 'com_joomlaupdate', 'component', 'com_joomlaupdate', '', 1, 1, 0, 1, '{"name":"com_joomlaupdate","type":"component","creationDate":"February 2012","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_JOOMLAUPDATE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(29, 'com_tags', 'component', 'com_tags', '', 1, 1, 1, 1, '{"name":"com_tags","type":"component","creationDate":"December 2013","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.1.0","description":"COM_TAGS_XML_DESCRIPTION","group":"","filename":"tags"}', '{"tag_layout":"_:default","save_history":"1","history_limit":5,"show_tag_title":"0","tag_list_show_tag_image":"0","tag_list_show_tag_description":"0","tag_list_image":"","show_tag_num_items":"0","tag_list_orderby":"title","tag_list_orderby_direction":"ASC","show_headings":"0","tag_list_show_date":"0","tag_list_show_item_image":"0","tag_list_show_item_description":"0","tag_list_item_maximum_characters":0,"return_any_or_all":"1","include_children":"0","maximum":200,"tag_list_language_filter":"all","tags_layout":"_:default","all_tags_orderby":"title","all_tags_orderby_direction":"ASC","all_tags_show_tag_image":"0","all_tags_show_tag_descripion":"0","all_tags_tag_maximum_characters":20,"all_tags_show_tag_hits":"0","filter_field":"1","show_pagination_limit":"1","show_pagination":"2","show_pagination_results":"1","tag_field_ajax_mode":"1","show_feed_link":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(30, 'com_contenthistory', 'component', 'com_contenthistory', '', 1, 1, 1, 0, '{"name":"com_contenthistory","type":"component","creationDate":"May 2013","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.0","description":"COM_CONTENTHISTORY_XML_DESCRIPTION","group":"","filename":"contenthistory"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(31, 'com_ajax', 'component', 'com_ajax', '', 1, 1, 1, 0, '{"name":"com_ajax","type":"component","creationDate":"August 2013","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.0","description":"COM_AJAX_XML_DESCRIPTION","group":"","filename":"ajax"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(32, 'com_postinstall', 'component', 'com_postinstall', '', 1, 1, 1, 1, '{"name":"com_postinstall","type":"component","creationDate":"September 2013","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.0","description":"COM_POSTINSTALL_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(101, 'SimplePie', 'library', 'simplepie', '', 0, 1, 1, 1, '{"name":"SimplePie","type":"library","creationDate":"2004","author":"SimplePie","copyright":"Copyright (c) 2004-2009, Ryan Parman and Geoffrey Sneddon","authorEmail":"","authorUrl":"http:\\/\\/simplepie.org\\/","version":"1.2","description":"LIB_SIMPLEPIE_XML_DESCRIPTION","group":"","filename":"simplepie"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(102, 'phputf8', 'library', 'phputf8', '', 0, 1, 1, 1, '{"name":"phputf8","type":"library","creationDate":"2006","author":"Harry Fuecks","copyright":"Copyright various authors","authorEmail":"hfuecks@gmail.com","authorUrl":"http:\\/\\/sourceforge.net\\/projects\\/phputf8","version":"0.5","description":"LIB_PHPUTF8_XML_DESCRIPTION","group":"","filename":"phputf8"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(103, 'Joomla! Platform', 'library', 'joomla', '', 0, 1, 1, 1, '{"name":"Joomla! Platform","type":"library","creationDate":"2008","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"http:\\/\\/www.joomla.org","version":"13.1","description":"LIB_JOOMLA_XML_DESCRIPTION","group":"","filename":"joomla"}', '{"mediaversion":"175d33d380cbff800d335f4c56ef43a6"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(104, 'IDNA Convert', 'library', 'idna_convert', '', 0, 1, 1, 1, '{"name":"IDNA Convert","type":"library","creationDate":"2004","author":"phlyLabs","copyright":"2004-2011 phlyLabs Berlin, http:\\/\\/phlylabs.de","authorEmail":"phlymail@phlylabs.de","authorUrl":"http:\\/\\/phlylabs.de","version":"0.8.0","description":"LIB_IDNA_XML_DESCRIPTION","group":"","filename":"idna_convert"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(105, 'FOF', 'library', 'fof', '', 0, 1, 1, 1, '{"name":"FOF","type":"library","creationDate":"2015-04-22 13:15:32","author":"Nicholas K. Dionysopoulos \\/ Akeeba Ltd","copyright":"(C)2011-2015 Nicholas K. Dionysopoulos","authorEmail":"nicholas@akeebabackup.com","authorUrl":"https:\\/\\/www.akeebabackup.com","version":"2.4.3","description":"LIB_FOF_XML_DESCRIPTION","group":"","filename":"fof"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(106, 'PHPass', 'library', 'phpass', '', 0, 1, 1, 1, '{"name":"PHPass","type":"library","creationDate":"2004-2006","author":"Solar Designer","copyright":"","authorEmail":"solar@openwall.com","authorUrl":"http:\\/\\/www.openwall.com\\/phpass\\/","version":"0.3","description":"LIB_PHPASS_XML_DESCRIPTION","group":"","filename":"phpass"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(200, 'mod_articles_archive', 'module', 'mod_articles_archive', '', 0, 1, 1, 0, '{"name":"mod_articles_archive","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_ARTICLES_ARCHIVE_XML_DESCRIPTION","group":"","filename":"mod_articles_archive"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(201, 'mod_articles_latest', 'module', 'mod_articles_latest', '', 0, 1, 1, 0, '{"name":"mod_articles_latest","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LATEST_NEWS_XML_DESCRIPTION","group":"","filename":"mod_articles_latest"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(202, 'mod_articles_popular', 'module', 'mod_articles_popular', '', 0, 1, 1, 0, '{"name":"mod_articles_popular","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_POPULAR_XML_DESCRIPTION","group":"","filename":"mod_articles_popular"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(203, 'mod_banners', 'module', 'mod_banners', '', 0, 1, 1, 0, '{"name":"mod_banners","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_BANNERS_XML_DESCRIPTION","group":"","filename":"mod_banners"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(204, 'mod_breadcrumbs', 'module', 'mod_breadcrumbs', '', 0, 1, 1, 1, '{"name":"mod_breadcrumbs","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_BREADCRUMBS_XML_DESCRIPTION","group":"","filename":"mod_breadcrumbs"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(205, 'mod_custom', 'module', 'mod_custom', '', 0, 1, 1, 1, '{"name":"mod_custom","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_CUSTOM_XML_DESCRIPTION","group":"","filename":"mod_custom"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(206, 'mod_feed', 'module', 'mod_feed', '', 0, 1, 1, 0, '{"name":"mod_feed","type":"module","creationDate":"July 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_FEED_XML_DESCRIPTION","group":"","filename":"mod_feed"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(207, 'mod_footer', 'module', 'mod_footer', '', 0, 1, 1, 0, '{"name":"mod_footer","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_FOOTER_XML_DESCRIPTION","group":"","filename":"mod_footer"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(208, 'mod_login', 'module', 'mod_login', '', 0, 1, 1, 1, '{"name":"mod_login","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LOGIN_XML_DESCRIPTION","group":"","filename":"mod_login"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(209, 'mod_menu', 'module', 'mod_menu', '', 0, 1, 1, 1, '{"name":"mod_menu","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_MENU_XML_DESCRIPTION","group":"","filename":"mod_menu"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(210, 'mod_articles_news', 'module', 'mod_articles_news', '', 0, 1, 1, 0, '{"name":"mod_articles_news","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_ARTICLES_NEWS_XML_DESCRIPTION","group":"","filename":"mod_articles_news"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(211, 'mod_random_image', 'module', 'mod_random_image', '', 0, 1, 1, 0, '{"name":"mod_random_image","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_RANDOM_IMAGE_XML_DESCRIPTION","group":"","filename":"mod_random_image"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(212, 'mod_related_items', 'module', 'mod_related_items', '', 0, 1, 1, 0, '{"name":"mod_related_items","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_RELATED_XML_DESCRIPTION","group":"","filename":"mod_related_items"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(213, 'mod_search', 'module', 'mod_search', '', 0, 1, 1, 0, '{"name":"mod_search","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_SEARCH_XML_DESCRIPTION","group":"","filename":"mod_search"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(214, 'mod_stats', 'module', 'mod_stats', '', 0, 1, 1, 0, '{"name":"mod_stats","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_STATS_XML_DESCRIPTION","group":"","filename":"mod_stats"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(215, 'mod_syndicate', 'module', 'mod_syndicate', '', 0, 1, 1, 1, '{"name":"mod_syndicate","type":"module","creationDate":"May 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_SYNDICATE_XML_DESCRIPTION","group":"","filename":"mod_syndicate"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(216, 'mod_users_latest', 'module', 'mod_users_latest', '', 0, 1, 1, 0, '{"name":"mod_users_latest","type":"module","creationDate":"December 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_USERS_LATEST_XML_DESCRIPTION","group":"","filename":"mod_users_latest"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(218, 'mod_whosonline', 'module', 'mod_whosonline', '', 0, 1, 1, 0, '{"name":"mod_whosonline","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_WHOSONLINE_XML_DESCRIPTION","group":"","filename":"mod_whosonline"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(219, 'mod_wrapper', 'module', 'mod_wrapper', '', 0, 1, 1, 0, '{"name":"mod_wrapper","type":"module","creationDate":"October 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_WRAPPER_XML_DESCRIPTION","group":"","filename":"mod_wrapper"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(220, 'mod_articles_category', 'module', 'mod_articles_category', '', 0, 1, 1, 0, '{"name":"mod_articles_category","type":"module","creationDate":"February 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_ARTICLES_CATEGORY_XML_DESCRIPTION","group":"","filename":"mod_articles_category"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(221, 'mod_articles_categories', 'module', 'mod_articles_categories', '', 0, 1, 1, 0, '{"name":"mod_articles_categories","type":"module","creationDate":"February 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_ARTICLES_CATEGORIES_XML_DESCRIPTION","group":"","filename":"mod_articles_categories"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(222, 'mod_languages', 'module', 'mod_languages', '', 0, 1, 1, 1, '{"name":"mod_languages","type":"module","creationDate":"February 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LANGUAGES_XML_DESCRIPTION","group":"","filename":"mod_languages"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(223, 'mod_finder', 'module', 'mod_finder', '', 0, 1, 0, 0, '{"name":"mod_finder","type":"module","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_FINDER_XML_DESCRIPTION","group":"","filename":"mod_finder"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(300, 'mod_custom', 'module', 'mod_custom', '', 1, 1, 1, 1, '{"name":"mod_custom","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_CUSTOM_XML_DESCRIPTION","group":"","filename":"mod_custom"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(301, 'mod_feed', 'module', 'mod_feed', '', 1, 1, 1, 0, '{"name":"mod_feed","type":"module","creationDate":"July 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_FEED_XML_DESCRIPTION","group":"","filename":"mod_feed"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(302, 'mod_latest', 'module', 'mod_latest', '', 1, 1, 1, 0, '{"name":"mod_latest","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LATEST_XML_DESCRIPTION","group":"","filename":"mod_latest"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(303, 'mod_logged', 'module', 'mod_logged', '', 1, 1, 1, 0, '{"name":"mod_logged","type":"module","creationDate":"January 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LOGGED_XML_DESCRIPTION","group":"","filename":"mod_logged"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(304, 'mod_login', 'module', 'mod_login', '', 1, 1, 1, 1, '{"name":"mod_login","type":"module","creationDate":"March 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LOGIN_XML_DESCRIPTION","group":"","filename":"mod_login"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(305, 'mod_menu', 'module', 'mod_menu', '', 1, 1, 1, 0, '{"name":"mod_menu","type":"module","creationDate":"March 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_MENU_XML_DESCRIPTION","group":"","filename":"mod_menu"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(307, 'mod_popular', 'module', 'mod_popular', '', 1, 1, 1, 0, '{"name":"mod_popular","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_POPULAR_XML_DESCRIPTION","group":"","filename":"mod_popular"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(308, 'mod_quickicon', 'module', 'mod_quickicon', '', 1, 1, 1, 1, '{"name":"mod_quickicon","type":"module","creationDate":"Nov 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_QUICKICON_XML_DESCRIPTION","group":"","filename":"mod_quickicon"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(309, 'mod_status', 'module', 'mod_status', '', 1, 1, 1, 0, '{"name":"mod_status","type":"module","creationDate":"Feb 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_STATUS_XML_DESCRIPTION","group":"","filename":"mod_status"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(310, 'mod_submenu', 'module', 'mod_submenu', '', 1, 1, 1, 0, '{"name":"mod_submenu","type":"module","creationDate":"Feb 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_SUBMENU_XML_DESCRIPTION","group":"","filename":"mod_submenu"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(311, 'mod_title', 'module', 'mod_title', '', 1, 1, 1, 0, '{"name":"mod_title","type":"module","creationDate":"Nov 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_TITLE_XML_DESCRIPTION","group":"","filename":"mod_title"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(312, 'mod_toolbar', 'module', 'mod_toolbar', '', 1, 1, 1, 1, '{"name":"mod_toolbar","type":"module","creationDate":"Nov 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_TOOLBAR_XML_DESCRIPTION","group":"","filename":"mod_toolbar"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(313, 'mod_multilangstatus', 'module', 'mod_multilangstatus', '', 1, 1, 1, 0, '{"name":"mod_multilangstatus","type":"module","creationDate":"September 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_MULTILANGSTATUS_XML_DESCRIPTION","group":"","filename":"mod_multilangstatus"}', '{"cache":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(314, 'mod_version', 'module', 'mod_version', '', 1, 1, 1, 0, '{"name":"mod_version","type":"module","creationDate":"January 2012","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_VERSION_XML_DESCRIPTION","group":"","filename":"mod_version"}', '{"format":"short","product":"1","cache":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(315, 'mod_stats_admin', 'module', 'mod_stats_admin', '', 1, 1, 1, 0, '{"name":"mod_stats_admin","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_STATS_XML_DESCRIPTION","group":"","filename":"mod_stats_admin"}', '{"serverinfo":"0","siteinfo":"0","counter":"0","increase":"0","cache":"1","cache_time":"900","cachemode":"static"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(316, 'mod_tags_popular', 'module', 'mod_tags_popular', '', 0, 1, 1, 0, '{"name":"mod_tags_popular","type":"module","creationDate":"January 2013","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.1.0","description":"MOD_TAGS_POPULAR_XML_DESCRIPTION","group":"","filename":"mod_tags_popular"}', '{"maximum":"5","timeframe":"alltime","owncache":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(317, 'mod_tags_similar', 'module', 'mod_tags_similar', '', 0, 1, 1, 0, '{"name":"mod_tags_similar","type":"module","creationDate":"January 2013","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.1.0","description":"MOD_TAGS_SIMILAR_XML_DESCRIPTION","group":"","filename":"mod_tags_similar"}', '{"maximum":"5","matchtype":"any","owncache":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(400, 'plg_authentication_gmail', 'plugin', 'gmail', 'authentication', 0, 0, 1, 0, '{"name":"plg_authentication_gmail","type":"plugin","creationDate":"February 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_GMAIL_XML_DESCRIPTION","group":"","filename":"gmail"}', '{"applysuffix":"0","suffix":"","verifypeer":"1","user_blacklist":""}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(401, 'plg_authentication_joomla', 'plugin', 'joomla', 'authentication', 0, 1, 1, 1, '{"name":"plg_authentication_joomla","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_AUTH_JOOMLA_XML_DESCRIPTION","group":"","filename":"joomla"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(402, 'plg_authentication_ldap', 'plugin', 'ldap', 'authentication', 0, 0, 1, 0, '{"name":"plg_authentication_ldap","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_LDAP_XML_DESCRIPTION","group":"","filename":"ldap"}', '{"host":"","port":"389","use_ldapV3":"0","negotiate_tls":"0","no_referrals":"0","auth_method":"bind","base_dn":"","search_string":"","users_dn":"","username":"admin","password":"bobby7","ldap_fullname":"fullName","ldap_email":"mail","ldap_uid":"uid"}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(403, 'plg_content_contact', 'plugin', 'contact', 'content', 0, 1, 1, 0, '{"name":"plg_content_contact","type":"plugin","creationDate":"January 2014","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.2","description":"PLG_CONTENT_CONTACT_XML_DESCRIPTION","group":"","filename":"contact"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(404, 'plg_content_emailcloak', 'plugin', 'emailcloak', 'content', 0, 1, 1, 0, '{"name":"plg_content_emailcloak","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CONTENT_EMAILCLOAK_XML_DESCRIPTION","group":"","filename":"emailcloak"}', '{"mode":"1"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(406, 'plg_content_loadmodule', 'plugin', 'loadmodule', 'content', 0, 1, 1, 0, '{"name":"plg_content_loadmodule","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_LOADMODULE_XML_DESCRIPTION","group":"","filename":"loadmodule"}', '{"style":"xhtml"}', '', '', 0, '2011-09-18 15:22:50', 0, 0),
(407, 'plg_content_pagebreak', 'plugin', 'pagebreak', 'content', 0, 1, 1, 0, '{"name":"plg_content_pagebreak","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CONTENT_PAGEBREAK_XML_DESCRIPTION","group":"","filename":"pagebreak"}', '{"title":"1","multipage_toc":"1","showall":"1"}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(408, 'plg_content_pagenavigation', 'plugin', 'pagenavigation', 'content', 0, 1, 1, 0, '{"name":"plg_content_pagenavigation","type":"plugin","creationDate":"January 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_PAGENAVIGATION_XML_DESCRIPTION","group":"","filename":"pagenavigation"}', '{"position":"1"}', '', '', 0, '0000-00-00 00:00:00', 5, 0),
(409, 'plg_content_vote', 'plugin', 'vote', 'content', 0, 1, 1, 0, '{"name":"plg_content_vote","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_VOTE_XML_DESCRIPTION","group":"","filename":"vote"}', '', '', '', 0, '0000-00-00 00:00:00', 6, 0),
(410, 'plg_editors_codemirror', 'plugin', 'codemirror', 'editors', 0, 1, 1, 1, '{"name":"plg_editors_codemirror","type":"plugin","creationDate":"28 March 2011","author":"Marijn Haverbeke","copyright":"Copyright (C) 2014 by Marijn Haverbeke <marijnh@gmail.com> and others","authorEmail":"marijnh@gmail.com","authorUrl":"http:\\/\\/codemirror.net\\/","version":"5.6","description":"PLG_CODEMIRROR_XML_DESCRIPTION","group":"","filename":"codemirror"}', '{"lineNumbers":"1","lineWrapping":"1","matchTags":"1","matchBrackets":"1","marker-gutter":"1","autoCloseTags":"1","autoCloseBrackets":"1","autoFocus":"1","theme":"default","tabmode":"indent"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(411, 'plg_editors_none', 'plugin', 'none', 'editors', 0, 1, 1, 1, '{"name":"plg_editors_none","type":"plugin","creationDate":"September 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_NONE_XML_DESCRIPTION","group":"","filename":"none"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(412, 'plg_editors_tinymce', 'plugin', 'tinymce', 'editors', 0, 1, 1, 0, '{"name":"plg_editors_tinymce","type":"plugin","creationDate":"2005-2014","author":"Moxiecode Systems AB","copyright":"Moxiecode Systems AB","authorEmail":"N\\/A","authorUrl":"tinymce.moxiecode.com","version":"4.1.7","description":"PLG_TINY_XML_DESCRIPTION","group":"","filename":"tinymce"}', '{"mode":"1","skin":"0","mobile":"0","entity_encoding":"raw","lang_mode":"1","text_direction":"ltr","content_css":"1","content_css_custom":"","relative_urls":"1","newlines":"0","invalid_elements":"script,applet,iframe","extended_elements":"","html_height":"550","html_width":"750","resizing":"1","element_path":"1","fonts":"1","paste":"1","searchreplace":"1","insertdate":"1","colors":"1","table":"1","smilies":"1","hr":"1","link":"1","media":"1","print":"1","directionality":"1","fullscreen":"1","alignment":"1","visualchars":"1","visualblocks":"1","nonbreaking":"1","template":"1","blockquote":"1","wordcount":"1","advlist":"1","autosave":"1","contextmenu":"1","inlinepopups":"1","custom_plugin":"","custom_button":""}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(413, 'plg_editors-xtd_article', 'plugin', 'article', 'editors-xtd', 0, 1, 1, 0, '{"name":"plg_editors-xtd_article","type":"plugin","creationDate":"October 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_ARTICLE_XML_DESCRIPTION","group":"","filename":"article"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(414, 'plg_editors-xtd_image', 'plugin', 'image', 'editors-xtd', 0, 1, 1, 0, '{"name":"plg_editors-xtd_image","type":"plugin","creationDate":"August 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_IMAGE_XML_DESCRIPTION","group":"","filename":"image"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(415, 'plg_editors-xtd_pagebreak', 'plugin', 'pagebreak', 'editors-xtd', 0, 1, 1, 0, '{"name":"plg_editors-xtd_pagebreak","type":"plugin","creationDate":"August 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_EDITORSXTD_PAGEBREAK_XML_DESCRIPTION","group":"","filename":"pagebreak"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0);
INSERT INTO `yawnc_extensions` (`extension_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(416, 'plg_editors-xtd_readmore', 'plugin', 'readmore', 'editors-xtd', 0, 1, 1, 0, '{"name":"plg_editors-xtd_readmore","type":"plugin","creationDate":"March 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_READMORE_XML_DESCRIPTION","group":"","filename":"readmore"}', '', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(417, 'plg_search_categories', 'plugin', 'categories', 'search', 0, 1, 1, 0, '{"name":"plg_search_categories","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEARCH_CATEGORIES_XML_DESCRIPTION","group":"","filename":"categories"}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(418, 'plg_search_contacts', 'plugin', 'contacts', 'search', 0, 1, 1, 0, '{"name":"plg_search_contacts","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEARCH_CONTACTS_XML_DESCRIPTION","group":"","filename":"contacts"}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(419, 'plg_search_content', 'plugin', 'content', 'search', 0, 1, 1, 0, '{"name":"plg_search_content","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEARCH_CONTENT_XML_DESCRIPTION","group":"","filename":"content"}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(420, 'plg_search_newsfeeds', 'plugin', 'newsfeeds', 'search', 0, 1, 1, 0, '{"name":"plg_search_newsfeeds","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEARCH_NEWSFEEDS_XML_DESCRIPTION","group":"","filename":"newsfeeds"}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(422, 'plg_system_languagefilter', 'plugin', 'languagefilter', 'system', 0, 0, 1, 1, '{"name":"plg_system_languagefilter","type":"plugin","creationDate":"July 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SYSTEM_LANGUAGEFILTER_XML_DESCRIPTION","group":"","filename":"languagefilter"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(423, 'plg_system_p3p', 'plugin', 'p3p', 'system', 0, 0, 1, 0, '{"name":"plg_system_p3p","type":"plugin","creationDate":"September 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_P3P_XML_DESCRIPTION","group":"","filename":"p3p"}', '{"headers":"NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM"}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(424, 'plg_system_cache', 'plugin', 'cache', 'system', 0, 0, 1, 1, '{"name":"plg_system_cache","type":"plugin","creationDate":"February 2007","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CACHE_XML_DESCRIPTION","group":"","filename":"cache"}', '{"browsercache":"0","cachetime":"15"}', '', '', 0, '0000-00-00 00:00:00', 9, 0),
(425, 'plg_system_debug', 'plugin', 'debug', 'system', 0, 1, 1, 0, '{"name":"plg_system_debug","type":"plugin","creationDate":"December 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_DEBUG_XML_DESCRIPTION","group":"","filename":"debug"}', '{"profile":"1","queries":"1","memory":"1","language_files":"1","language_strings":"1","strip-first":"1","strip-prefix":"","strip-suffix":""}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(426, 'plg_system_log', 'plugin', 'log', 'system', 0, 1, 1, 1, '{"name":"plg_system_log","type":"plugin","creationDate":"April 2007","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_LOG_XML_DESCRIPTION","group":"","filename":"log"}', '', '', '', 0, '0000-00-00 00:00:00', 5, 0),
(427, 'plg_system_redirect', 'plugin', 'redirect', 'system', 0, 0, 1, 1, '{"name":"plg_system_redirect","type":"plugin","creationDate":"April 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SYSTEM_REDIRECT_XML_DESCRIPTION","group":"","filename":"redirect"}', '', '', '', 0, '0000-00-00 00:00:00', 6, 0),
(428, 'plg_system_remember', 'plugin', 'remember', 'system', 0, 1, 1, 1, '{"name":"plg_system_remember","type":"plugin","creationDate":"April 2007","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_REMEMBER_XML_DESCRIPTION","group":"","filename":"remember"}', '', '', '', 0, '0000-00-00 00:00:00', 7, 0),
(429, 'plg_system_sef', 'plugin', 'sef', 'system', 0, 1, 1, 0, '{"name":"plg_system_sef","type":"plugin","creationDate":"December 2007","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEF_XML_DESCRIPTION","group":"","filename":"sef"}', '', '', '', 0, '0000-00-00 00:00:00', 8, 0),
(430, 'plg_system_logout', 'plugin', 'logout', 'system', 0, 1, 1, 1, '{"name":"plg_system_logout","type":"plugin","creationDate":"April 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SYSTEM_LOGOUT_XML_DESCRIPTION","group":"","filename":"logout"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(431, 'plg_user_contactcreator', 'plugin', 'contactcreator', 'user', 0, 0, 1, 0, '{"name":"plg_user_contactcreator","type":"plugin","creationDate":"August 2009","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CONTACTCREATOR_XML_DESCRIPTION","group":"","filename":"contactcreator"}', '{"autowebpage":"","category":"34","autopublish":"0"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(432, 'plg_user_joomla', 'plugin', 'joomla', 'user', 0, 1, 1, 0, '{"name":"plg_user_joomla","type":"plugin","creationDate":"December 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_USER_JOOMLA_XML_DESCRIPTION","group":"","filename":"joomla"}', '{"autoregister":"1","mail_to_user":"1","forceLogout":"1"}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(433, 'plg_user_profile', 'plugin', 'profile', 'user', 0, 0, 1, 0, '{"name":"plg_user_profile","type":"plugin","creationDate":"January 2008","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_USER_PROFILE_XML_DESCRIPTION","group":"","filename":"profile"}', '{"register-require_address1":"1","register-require_address2":"1","register-require_city":"1","register-require_region":"1","register-require_country":"1","register-require_postal_code":"1","register-require_phone":"1","register-require_website":"1","register-require_favoritebook":"1","register-require_aboutme":"1","register-require_tos":"1","register-require_dob":"1","profile-require_address1":"1","profile-require_address2":"1","profile-require_city":"1","profile-require_region":"1","profile-require_country":"1","profile-require_postal_code":"1","profile-require_phone":"1","profile-require_website":"1","profile-require_favoritebook":"1","profile-require_aboutme":"1","profile-require_tos":"1","profile-require_dob":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(434, 'plg_extension_joomla', 'plugin', 'joomla', 'extension', 0, 1, 1, 1, '{"name":"plg_extension_joomla","type":"plugin","creationDate":"May 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_EXTENSION_JOOMLA_XML_DESCRIPTION","group":"","filename":"joomla"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(435, 'plg_content_joomla', 'plugin', 'joomla', 'content', 0, 1, 1, 0, '{"name":"plg_content_joomla","type":"plugin","creationDate":"November 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CONTENT_JOOMLA_XML_DESCRIPTION","group":"","filename":"joomla"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(436, 'plg_system_languagecode', 'plugin', 'languagecode', 'system', 0, 0, 1, 0, '{"name":"plg_system_languagecode","type":"plugin","creationDate":"November 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SYSTEM_LANGUAGECODE_XML_DESCRIPTION","group":"","filename":"languagecode"}', '', '', '', 0, '0000-00-00 00:00:00', 10, 0),
(437, 'plg_quickicon_joomlaupdate', 'plugin', 'joomlaupdate', 'quickicon', 0, 1, 1, 1, '{"name":"plg_quickicon_joomlaupdate","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_QUICKICON_JOOMLAUPDATE_XML_DESCRIPTION","group":"","filename":"joomlaupdate"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(438, 'plg_quickicon_extensionupdate', 'plugin', 'extensionupdate', 'quickicon', 0, 1, 1, 1, '{"name":"plg_quickicon_extensionupdate","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_QUICKICON_EXTENSIONUPDATE_XML_DESCRIPTION","group":"","filename":"extensionupdate"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(439, 'plg_captcha_recaptcha', 'plugin', 'recaptcha', 'captcha', 0, 0, 1, 0, '{"name":"plg_captcha_recaptcha","type":"plugin","creationDate":"December 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.4.0","description":"PLG_CAPTCHA_RECAPTCHA_XML_DESCRIPTION","group":"","filename":"recaptcha"}', '{"public_key":"","private_key":"","theme":"clean"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(440, 'plg_system_highlight', 'plugin', 'highlight', 'system', 0, 1, 1, 0, '{"name":"plg_system_highlight","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SYSTEM_HIGHLIGHT_XML_DESCRIPTION","group":"","filename":"highlight"}', '', '', '', 0, '0000-00-00 00:00:00', 7, 0),
(441, 'plg_content_finder', 'plugin', 'finder', 'content', 0, 1, 1, 0, '{"name":"plg_content_finder","type":"plugin","creationDate":"December 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CONTENT_FINDER_XML_DESCRIPTION","group":"","filename":"finder"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(442, 'plg_finder_categories', 'plugin', 'categories', 'finder', 0, 0, 1, 0, '{"name":"plg_finder_categories","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_FINDER_CATEGORIES_XML_DESCRIPTION","group":"","filename":"categories"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(443, 'plg_finder_contacts', 'plugin', 'contacts', 'finder', 0, 0, 1, 0, '{"name":"plg_finder_contacts","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_FINDER_CONTACTS_XML_DESCRIPTION","group":"","filename":"contacts"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(444, 'plg_finder_content', 'plugin', 'content', 'finder', 0, 1, 1, 0, '{"name":"plg_finder_content","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_FINDER_CONTENT_XML_DESCRIPTION","group":"","filename":"content"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(445, 'plg_finder_newsfeeds', 'plugin', 'newsfeeds', 'finder', 0, 0, 1, 0, '{"name":"plg_finder_newsfeeds","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_FINDER_NEWSFEEDS_XML_DESCRIPTION","group":"","filename":"newsfeeds"}', '', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(447, 'plg_finder_tags', 'plugin', 'tags', 'finder', 0, 0, 1, 0, '{"name":"plg_finder_tags","type":"plugin","creationDate":"February 2013","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_FINDER_TAGS_XML_DESCRIPTION","group":"","filename":"tags"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(448, 'plg_twofactorauth_totp', 'plugin', 'totp', 'twofactorauth', 0, 0, 1, 0, '{"name":"plg_twofactorauth_totp","type":"plugin","creationDate":"August 2013","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.0","description":"PLG_TWOFACTORAUTH_TOTP_XML_DESCRIPTION","group":"","filename":"totp"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(449, 'plg_authentication_cookie', 'plugin', 'cookie', 'authentication', 0, 1, 1, 0, '{"name":"plg_authentication_cookie","type":"plugin","creationDate":"July 2013","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_AUTH_COOKIE_XML_DESCRIPTION","group":"","filename":"cookie"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(450, 'plg_twofactorauth_yubikey', 'plugin', 'yubikey', 'twofactorauth', 0, 0, 1, 0, '{"name":"plg_twofactorauth_yubikey","type":"plugin","creationDate":"September 2013","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.0","description":"PLG_TWOFACTORAUTH_YUBIKEY_XML_DESCRIPTION","group":"","filename":"yubikey"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(451, 'plg_search_tags', 'plugin', 'tags', 'search', 0, 1, 1, 0, '{"name":"plg_search_tags","type":"plugin","creationDate":"March 2014","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEARCH_TAGS_XML_DESCRIPTION","group":"","filename":"tags"}', '{"search_limit":"50","show_tagged_items":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(503, 'beez3', 'template', 'beez3', '', 0, 1, 1, 0, '{"name":"beez3","type":"template","creationDate":"25 November 2009","author":"Angie Radtke","copyright":"Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.","authorEmail":"a.radtke@derauftritt.de","authorUrl":"http:\\/\\/www.der-auftritt.de","version":"3.1.0","description":"TPL_BEEZ3_XML_DESCRIPTION","group":"","filename":"templateDetails"}', '{"wrapperSmall":"53","wrapperLarge":"72","sitetitle":"","sitedescription":"","navposition":"center","templatecolor":"nature"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(504, 'hathor', 'template', 'hathor', '', 1, 1, 1, 0, '{"name":"hathor","type":"template","creationDate":"May 2010","author":"Andrea Tarr","copyright":"Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.","authorEmail":"hathor@tarrconsulting.com","authorUrl":"http:\\/\\/www.tarrconsulting.com","version":"3.0.0","description":"TPL_HATHOR_XML_DESCRIPTION","group":"","filename":"templateDetails"}', '{"showSiteName":"0","colourChoice":"0","boldText":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(506, 'protostar', 'template', 'protostar', '', 0, 1, 1, 0, '{"name":"protostar","type":"template","creationDate":"4\\/30\\/2012","author":"Kyle Ledbetter","copyright":"Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"","version":"1.0","description":"TPL_PROTOSTAR_XML_DESCRIPTION","group":"","filename":"templateDetails"}', '{"templateColor":"","logoFile":"","googleFont":"1","googleFontName":"Open+Sans","fluidContainer":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(507, 'isis', 'template', 'isis', '', 1, 1, 1, 0, '{"name":"isis","type":"template","creationDate":"3\\/30\\/2012","author":"Kyle Ledbetter","copyright":"Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"","version":"1.0","description":"TPL_ISIS_XML_DESCRIPTION","group":"","filename":"templateDetails"}', '{"templateColor":"","logoFile":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(600, 'English (en-GB)', 'language', 'en-GB', '', 0, 1, 1, 1, '{"name":"English (en-GB)","type":"language","creationDate":"2013-03-07","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.4.3","description":"en-GB site language","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(601, 'English (en-GB)', 'language', 'en-GB', '', 1, 1, 1, 1, '{"name":"English (en-GB)","type":"language","creationDate":"2013-03-07","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.4.3","description":"en-GB administrator language","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(700, 'files_joomla', 'file', 'joomla', '', 0, 1, 1, 1, '{"name":"files_joomla","type":"file","creationDate":"September 2015","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.4.4","description":"FILES_JOOMLA_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10000, 'Russian', 'language', 'ru-RU', '', 0, 1, 0, 0, '{"name":"Russian","type":"language","creationDate":"2015-05-29","author":"Russian Translation Team","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved","authorEmail":"smart@joomlaportal.ru","authorUrl":"www.joomlaportal.ru","version":"3.4.1.3","description":"Russian language pack (site) for Joomla! 3.4.1","group":"","filename":"install"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10001, 'Russian', 'language', 'ru-RU', '', 1, 1, 0, 0, '{"name":"Russian","type":"language","creationDate":"2015-05-29","author":"Russian Translation Team","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved","authorEmail":"smart@joomlaportal.ru","authorUrl":"www.joomlaportal.ru","version":"3.4.1.3","description":"Russian language pack (administrator) for Joomla! 3.4.1","group":"","filename":"install"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10002, 'Russian Language Pack', 'package', 'pkg_ru-RU', '', 0, 1, 1, 0, '{"name":"Russian Language Pack","type":"package","creationDate":"Unknown","author":"Unknown","copyright":"","authorEmail":"","authorUrl":"","version":"3.4.1.3","description":"Joomla 3.4 Russian Language Package","group":"","filename":"pkg_ru-RU"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10003, 'jshopping', 'component', 'com_jshopping', '', 1, 1, 0, 0, '{"name":"jshopping","type":"component","creationDate":"07.08.2015","author":"MAXXmarketing GmbH","copyright":"","authorEmail":"marketing@maxx-marketing.net","authorUrl":"http:\\/\\/www.webdesigner-profi.de","version":"4.10.4","description":"Joomshopping - shop component. Note: JoomShopping code files are named as jshopping","group":"","filename":"jshopping"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10004, 'lavka', 'template', 'lavka', '', 0, 1, 1, 0, '{"name":"lavka","type":"template","creationDate":"22/06/2015","author":"Malevich","copyright":"Copyright (C) Malevich. All rights reserved.","authorEmail":"info@malevich.com.ua","authorUrl":"","version":"1.0","description":"optotechnic. NOTE: It is not free or public.","group":""}', '{"config":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_finder_filters`
--

CREATE TABLE IF NOT EXISTS `yawnc_finder_filters` (
  `filter_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL,
  `created_by_alias` varchar(255) NOT NULL,
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `map_count` int(10) unsigned NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  `params` mediumtext,
  PRIMARY KEY (`filter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_finder_links`
--

CREATE TABLE IF NOT EXISTS `yawnc_finder_links` (
  `link_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `route` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `indexdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `md5sum` varchar(32) DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `state` int(5) DEFAULT '1',
  `access` int(5) DEFAULT '0',
  `language` varchar(8) NOT NULL,
  `publish_start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `list_price` double unsigned NOT NULL DEFAULT '0',
  `sale_price` double unsigned NOT NULL DEFAULT '0',
  `type_id` int(11) NOT NULL,
  `object` mediumblob NOT NULL,
  PRIMARY KEY (`link_id`),
  KEY `idx_type` (`type_id`),
  KEY `idx_title` (`title`),
  KEY `idx_md5` (`md5sum`),
  KEY `idx_url` (`url`(75)),
  KEY `idx_published_list` (`published`,`state`,`access`,`publish_start_date`,`publish_end_date`,`list_price`),
  KEY `idx_published_sale` (`published`,`state`,`access`,`publish_start_date`,`publish_end_date`,`sale_price`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=74 ;

--
-- Дамп данных таблицы `yawnc_finder_links`
--

INSERT INTO `yawnc_finder_links` (`link_id`, `url`, `route`, `title`, `description`, `indexdate`, `md5sum`, `published`, `state`, `access`, `language`, `publish_start_date`, `publish_end_date`, `start_date`, `end_date`, `list_price`, `sale_price`, `type_id`, `object`) VALUES
(19, 'index.php?option=com_content&view=article&id=1', 'index.php?option=com_content&view=article&id=1:ikony-khramovye&Itemid=101', 'Иконы Храмовые', ' варпцрапуолафцралфуоадлфцладжфцладжфцлджафцладжфцладжйфца арвфцгшафцгшфцшарфцгшарфцгшарвфцгшарфцгшарфцгшарфцгшарфцг ', '2015-09-18 15:14:42', '9cdd3bc641a30c722a7d3a38aca14153', 1, 0, 1, '*', '2015-09-10 16:18:34', '0000-00-00 00:00:00', '2015-09-10 16:18:34', '0000-00-00 00:00:00', 0, 0, 4, 0x4f3a31393a2246696e646572496e6465786572526573756c74223a31393a7b733a31313a22002a00656c656d656e7473223b613a32343a7b733a323a226964223b733a313a2231223b733a353a22616c696173223b733a31353a22696b6f6e792d6b6872616d6f767965223b733a373a2273756d6d617279223b733a3234343a223c703ed0b2d0b0d180d0bfd186d180d0b0d0bfd183d0bed0bbd0b0d184d186d180d0b0d0bbd184d183d0bed0b0d0b4d0bbd184d186d0bbd0b0d0b4d0b6d184d186d0bbd0b0d0b4d0b6d184d186d0bbd0b4d0b6d0b0d184d186d0bbd0b0d0b4d0b6d184d186d0bbd0b0d0b4d0b6d0b9d184d186d0b03c2f703e0d0a3c703ed0b0d180d0b2d184d186d0b3d188d0b0d184d186d0b3d188d184d186d188d0b0d180d184d186d0b3d188d0b0d180d184d186d0b3d188d0b0d180d0b2d184d186d0b3d188d0b0d180d184d186d0b3d188d0b0d180d184d186d0b3d188d0b0d180d184d186d0b3d188d0b0d180d184d186d0b33c2f703e223b733a343a22626f6479223b733a303a22223b733a353a226361746964223b733a313a2232223b733a31303a22637265617465645f6279223b733a333a22353935223b733a31363a22637265617465645f62795f616c696173223b733a303a22223b733a383a226d6f646966696564223b733a31393a22323031352d30392d31362031343a34313a3438223b733a31313a226d6f6469666965645f6279223b733a333a22353935223b733a363a22706172616d73223b4f3a32343a224a6f6f6d6c615c52656769737472795c5265676973747279223a323a7b733a373a22002a0064617461223b4f3a383a22737464436c617373223a37353a7b733a31343a2261727469636c655f6c61796f7574223b733a393a225f3a64656661756c74223b733a31303a2273686f775f7469746c65223b733a313a2230223b733a31313a226c696e6b5f7469746c6573223b733a313a2231223b733a31303a2273686f775f696e74726f223b733a313a2231223b733a31393a22696e666f5f626c6f636b5f706f736974696f6e223b733a313a2230223b733a31333a2273686f775f63617465676f7279223b733a313a2230223b733a31333a226c696e6b5f63617465676f7279223b733a313a2231223b733a32303a2273686f775f706172656e745f63617465676f7279223b733a313a2230223b733a32303a226c696e6b5f706172656e745f63617465676f7279223b733a313a2230223b733a31313a2273686f775f617574686f72223b733a313a2230223b733a31313a226c696e6b5f617574686f72223b733a313a2230223b733a31363a2273686f775f6372656174655f64617465223b733a313a2230223b733a31363a2273686f775f6d6f646966795f64617465223b733a313a2230223b733a31373a2273686f775f7075626c6973685f64617465223b733a313a2230223b733a32303a2273686f775f6974656d5f6e617669676174696f6e223b733a313a2231223b733a393a2273686f775f766f7465223b733a313a2230223b733a31333a2273686f775f726561646d6f7265223b733a313a2231223b733a31393a2273686f775f726561646d6f72655f7469746c65223b733a313a2231223b733a31343a22726561646d6f72655f6c696d6974223b733a333a22313030223b733a393a2273686f775f74616773223b733a313a2231223b733a31303a2273686f775f69636f6e73223b733a313a2231223b733a31353a2273686f775f7072696e745f69636f6e223b733a313a2230223b733a31353a2273686f775f656d61696c5f69636f6e223b733a313a2230223b733a393a2273686f775f68697473223b733a313a2230223b733a31313a2273686f775f6e6f61757468223b733a313a2230223b733a31333a2275726c735f706f736974696f6e223b733a313a2230223b733a32333a2273686f775f7075626c697368696e675f6f7074696f6e73223b733a313a2231223b733a32303a2273686f775f61727469636c655f6f7074696f6e73223b733a313a2231223b733a31323a22736176655f686973746f7279223b733a313a2231223b733a31333a22686973746f72795f6c696d6974223b693a31303b733a32353a2273686f775f75726c735f696d616765735f66726f6e74656e64223b733a313a2230223b733a32343a2273686f775f75726c735f696d616765735f6261636b656e64223b733a313a2231223b733a373a2274617267657461223b693a303b733a373a2274617267657462223b693a303b733a373a2274617267657463223b693a303b733a31313a22666c6f61745f696e74726f223b733a343a226c656674223b733a31343a22666c6f61745f66756c6c74657874223b733a343a226c656674223b733a31353a2263617465676f72795f6c61796f7574223b733a363a225f3a626c6f67223b733a33323a2273686f775f63617465676f72795f68656164696e675f7469746c655f74657874223b733a313a2231223b733a31393a2273686f775f63617465676f72795f7469746c65223b733a313a2230223b733a31363a2273686f775f6465736372697074696f6e223b733a313a2230223b733a32323a2273686f775f6465736372697074696f6e5f696d616765223b733a313a2230223b733a383a226d61784c6576656c223b733a313a2231223b733a32313a2273686f775f656d7074795f63617465676f72696573223b733a313a2230223b733a31363a2273686f775f6e6f5f61727469636c6573223b733a313a2231223b733a31363a2273686f775f7375626361745f64657363223b733a313a2231223b733a32313a2273686f775f6361745f6e756d5f61727469636c6573223b733a313a2230223b733a31333a2273686f775f6361745f74616773223b733a313a2231223b733a32313a2273686f775f626173655f6465736372697074696f6e223b733a313a2231223b733a31313a226d61784c6576656c636174223b733a323a222d31223b733a32353a2273686f775f656d7074795f63617465676f726965735f636174223b733a313a2230223b733a32303a2273686f775f7375626361745f646573635f636174223b733a313a2231223b733a32353a2273686f775f6361745f6e756d5f61727469636c65735f636174223b733a313a2231223b733a32303a226e756d5f6c656164696e675f61727469636c6573223b733a313a2230223b733a31383a226e756d5f696e74726f5f61727469636c6573223b733a313a2236223b733a31313a226e756d5f636f6c756d6e73223b733a313a2233223b733a393a226e756d5f6c696e6b73223b733a313a2234223b733a31383a226d756c74695f636f6c756d6e5f6f72646572223b733a313a2230223b733a32343a2273686f775f73756263617465676f72795f636f6e74656e74223b733a323a222d31223b733a32313a2273686f775f706167696e6174696f6e5f6c696d6974223b733a313a2231223b733a31323a2266696c7465725f6669656c64223b733a343a2268696465223b733a31333a2273686f775f68656164696e6773223b733a313a2231223b733a31343a226c6973745f73686f775f64617465223b733a313a2230223b733a31313a22646174655f666f726d6174223b733a303a22223b733a31343a226c6973745f73686f775f68697473223b733a313a2231223b733a31363a226c6973745f73686f775f617574686f72223b733a313a2231223b733a31313a226f7264657262795f707269223b733a353a226f72646572223b733a31313a226f7264657262795f736563223b733a353a227264617465223b733a31303a226f726465725f64617465223b733a393a227075626c6973686564223b733a31353a2273686f775f706167696e6174696f6e223b733a313a2232223b733a32333a2273686f775f706167696e6174696f6e5f726573756c7473223b733a313a2231223b733a31333a2273686f775f6665617475726564223b733a343a2273686f77223b733a31343a2273686f775f666565645f6c696e6b223b733a313a2231223b733a31323a22666565645f73756d6d617279223b733a313a2230223b733a31383a22666565645f73686f775f726561646d6f7265223b733a313a2230223b7d733a393a22736570617261746f72223b733a313a222e223b7d733a373a226d6574616b6579223b733a303a22223b733a383a226d65746164657363223b733a303a22223b733a383a226d65746164617461223b4f3a32343a224a6f6f6d6c615c52656769737472795c5265676973747279223a323a7b733a373a22002a0064617461223b4f3a383a22737464436c617373223a343a7b733a363a22726f626f7473223b733a303a22223b733a363a22617574686f72223b733a303a22223b733a363a22726967687473223b733a303a22223b733a31303a22787265666572656e6365223b733a303a22223b7d733a393a22736570617261746f72223b733a313a222e223b7d733a373a2276657273696f6e223b733a313a2232223b733a383a226f72646572696e67223b733a313a2232223b733a383a2263617465676f7279223b733a31333a22556e63617465676f7269736564223b733a393a226361745f7374617465223b733a323a222d32223b733a31303a226361745f616363657373223b733a313a2231223b733a343a22736c7567223b733a31373a22313a696b6f6e792d6b6872616d6f767965223b733a373a22636174736c7567223b733a31353a22323a756e63617465676f7269736564223b733a363a22617574686f72223b733a31303a2253757065722055736572223b733a363a226c61796f7574223b733a373a2261727469636c65223b733a343a2270617468223b733a32313a223f69643d313a696b6f6e792d6b6872616d6f767965223b733a31303a226d657461617574686f72223b4e3b7d733a31353a22002a00696e737472756374696f6e73223b613a353a7b693a313b613a333a7b693a303b733a353a227469746c65223b693a313b733a383a227375627469746c65223b693a323b733a323a226964223b7d693a323b613a323a7b693a303b733a373a2273756d6d617279223b693a313b733a343a22626f6479223b7d693a333b613a383a7b693a303b733a343a226d657461223b693a313b733a31303a226c6973745f7072696365223b693a323b733a31303a2273616c655f7072696365223b693a333b733a373a226d6574616b6579223b693a343b733a383a226d65746164657363223b693a353b733a31303a226d657461617574686f72223b693a363b733a363a22617574686f72223b693a373b733a31363a22637265617465645f62795f616c696173223b7d693a343b613a323a7b693a303b733a343a2270617468223b693a313b733a353a22616c696173223b7d693a353b613a313a7b693a303b733a383a22636f6d6d656e7473223b7d7d733a31313a22002a007461786f6e6f6d79223b613a343a7b733a343a2254797065223b613a313a7b733a373a2241727469636c65223b4f3a373a224a4f626a656374223a343a7b733a31303a22002a005f6572726f7273223b613a303a7b7d733a353a227469746c65223b733a373a2241727469636c65223b733a353a227374617465223b693a313b733a363a22616363657373223b693a313b7d7d733a363a22417574686f72223b613a313a7b733a31303a2253757065722055736572223b4f3a373a224a4f626a656374223a343a7b733a31303a22002a005f6572726f7273223b613a303a7b7d733a353a227469746c65223b733a31303a2253757065722055736572223b733a353a227374617465223b693a313b733a363a22616363657373223b693a313b7d7d733a383a2243617465676f7279223b613a313a7b733a31333a22556e63617465676f7269736564223b4f3a373a224a4f626a656374223a343a7b733a31303a22002a005f6572726f7273223b613a303a7b7d733a353a227469746c65223b733a31333a22556e63617465676f7269736564223b733a353a227374617465223b693a2d323b733a363a22616363657373223b693a313b7d7d733a383a224c616e6775616765223b613a313a7b733a313a222a223b4f3a373a224a4f626a656374223a343a7b733a31303a22002a005f6572726f7273223b613a303a7b7d733a353a227469746c65223b733a313a222a223b733a353a227374617465223b693a313b733a363a22616363657373223b693a313b7d7d7d733a333a2275726c223b733a34363a22696e6465782e7068703f6f7074696f6e3d636f6d5f636f6e74656e7426766965773d61727469636c652669643d31223b733a353a22726f757465223b733a37333a22696e6465782e7068703f6f7074696f6e3d636f6d5f636f6e74656e7426766965773d61727469636c652669643d313a696b6f6e792d6b6872616d6f767965264974656d69643d313031223b733a353a227469746c65223b733a32373a22d098d0bad0bed0bdd18b20d0a5d180d0b0d0bcd0bed0b2d18bd0b5223b733a31313a226465736372697074696f6e223b733a3233313a2220d0b2d0b0d180d0bfd186d180d0b0d0bfd183d0bed0bbd0b0d184d186d180d0b0d0bbd184d183d0bed0b0d0b4d0bbd184d186d0bbd0b0d0b4d0b6d184d186d0bbd0b0d0b4d0b6d184d186d0bbd0b4d0b6d0b0d184d186d0bbd0b0d0b4d0b6d184d186d0bbd0b0d0b4d0b6d0b9d184d186d0b020d0b0d180d0b2d184d186d0b3d188d0b0d184d186d0b3d188d184d186d188d0b0d180d184d186d0b3d188d0b0d180d184d186d0b3d188d0b0d180d0b2d184d186d0b3d188d0b0d180d184d186d0b3d188d0b0d180d184d186d0b3d188d0b0d180d184d186d0b3d188d0b0d180d184d186d0b320223b733a393a227075626c6973686564223b4e3b733a353a227374617465223b693a303b733a363a22616363657373223b733a313a2231223b733a383a226c616e6775616765223b733a313a222a223b733a31383a227075626c6973685f73746172745f64617465223b733a31393a22323031352d30392d31302031363a31383a3334223b733a31363a227075626c6973685f656e645f64617465223b733a31393a22303030302d30302d30302030303a30303a3030223b733a31303a2273746172745f64617465223b733a31393a22323031352d30392d31302031363a31383a3334223b733a383a22656e645f64617465223b733a31393a22303030302d30302d30302030303a30303a3030223b733a31303a226c6973745f7072696365223b4e3b733a31303a2273616c655f7072696365223b4e3b733a373a22747970655f6964223b693a343b733a31353a2264656661756c744c616e6775616765223b733a353a2272752d5255223b7d),
(20, 'index.php?option=com_content&view=article&id=2', 'index.php?option=com_content&view=article&id=2:svechi-voskovye&Itemid=101', 'Свечи восковые', 'пуывпмфуапфцафца', '2015-09-18 15:14:44', 'b80d88fdd92394098fdc160fa5615c1b', 1, 0, 1, '*', '2015-09-16 13:53:12', '0000-00-00 00:00:00', '2015-09-16 13:53:12', '0000-00-00 00:00:00', 0, 0, 4, 0x4f3a31393a2246696e646572496e6465786572526573756c74223a31393a7b733a31313a22002a00656c656d656e7473223b613a32343a7b733a323a226964223b733a313a2232223b733a353a22616c696173223b733a31353a227376656368692d766f736b6f767965223b733a373a2273756d6d617279223b733a33323a22d0bfd183d18bd0b2d0bfd0bcd184d183d0b0d0bfd184d186d0b0d184d186d0b0223b733a343a22626f6479223b733a303a22223b733a353a226361746964223b733a313a2232223b733a31303a22637265617465645f6279223b733a333a22353935223b733a31363a22637265617465645f62795f616c696173223b733a303a22223b733a383a226d6f646966696564223b733a31393a22323031352d30392d31362031343a34333a3232223b733a31313a226d6f6469666965645f6279223b733a333a22353935223b733a363a22706172616d73223b4f3a32343a224a6f6f6d6c615c52656769737472795c5265676973747279223a323a7b733a373a22002a0064617461223b4f3a383a22737464436c617373223a37353a7b733a31343a2261727469636c655f6c61796f7574223b733a393a225f3a64656661756c74223b733a31303a2273686f775f7469746c65223b733a313a2230223b733a31313a226c696e6b5f7469746c6573223b733a313a2231223b733a31303a2273686f775f696e74726f223b733a313a2231223b733a31393a22696e666f5f626c6f636b5f706f736974696f6e223b733a313a2230223b733a31333a2273686f775f63617465676f7279223b733a313a2230223b733a31333a226c696e6b5f63617465676f7279223b733a313a2231223b733a32303a2273686f775f706172656e745f63617465676f7279223b733a313a2230223b733a32303a226c696e6b5f706172656e745f63617465676f7279223b733a313a2230223b733a31313a2273686f775f617574686f72223b733a313a2230223b733a31313a226c696e6b5f617574686f72223b733a313a2230223b733a31363a2273686f775f6372656174655f64617465223b733a313a2230223b733a31363a2273686f775f6d6f646966795f64617465223b733a313a2230223b733a31373a2273686f775f7075626c6973685f64617465223b733a313a2230223b733a32303a2273686f775f6974656d5f6e617669676174696f6e223b733a313a2231223b733a393a2273686f775f766f7465223b733a313a2230223b733a31333a2273686f775f726561646d6f7265223b733a313a2231223b733a31393a2273686f775f726561646d6f72655f7469746c65223b733a313a2231223b733a31343a22726561646d6f72655f6c696d6974223b733a333a22313030223b733a393a2273686f775f74616773223b733a313a2231223b733a31303a2273686f775f69636f6e73223b733a313a2231223b733a31353a2273686f775f7072696e745f69636f6e223b733a313a2230223b733a31353a2273686f775f656d61696c5f69636f6e223b733a313a2230223b733a393a2273686f775f68697473223b733a313a2230223b733a31313a2273686f775f6e6f61757468223b733a313a2230223b733a31333a2275726c735f706f736974696f6e223b733a313a2230223b733a32333a2273686f775f7075626c697368696e675f6f7074696f6e73223b733a313a2231223b733a32303a2273686f775f61727469636c655f6f7074696f6e73223b733a313a2231223b733a31323a22736176655f686973746f7279223b733a313a2231223b733a31333a22686973746f72795f6c696d6974223b693a31303b733a32353a2273686f775f75726c735f696d616765735f66726f6e74656e64223b733a313a2230223b733a32343a2273686f775f75726c735f696d616765735f6261636b656e64223b733a313a2231223b733a373a2274617267657461223b693a303b733a373a2274617267657462223b693a303b733a373a2274617267657463223b693a303b733a31313a22666c6f61745f696e74726f223b733a343a226c656674223b733a31343a22666c6f61745f66756c6c74657874223b733a343a226c656674223b733a31353a2263617465676f72795f6c61796f7574223b733a363a225f3a626c6f67223b733a33323a2273686f775f63617465676f72795f68656164696e675f7469746c655f74657874223b733a313a2231223b733a31393a2273686f775f63617465676f72795f7469746c65223b733a313a2230223b733a31363a2273686f775f6465736372697074696f6e223b733a313a2230223b733a32323a2273686f775f6465736372697074696f6e5f696d616765223b733a313a2230223b733a383a226d61784c6576656c223b733a313a2231223b733a32313a2273686f775f656d7074795f63617465676f72696573223b733a313a2230223b733a31363a2273686f775f6e6f5f61727469636c6573223b733a313a2231223b733a31363a2273686f775f7375626361745f64657363223b733a313a2231223b733a32313a2273686f775f6361745f6e756d5f61727469636c6573223b733a313a2230223b733a31333a2273686f775f6361745f74616773223b733a313a2231223b733a32313a2273686f775f626173655f6465736372697074696f6e223b733a313a2231223b733a31313a226d61784c6576656c636174223b733a323a222d31223b733a32353a2273686f775f656d7074795f63617465676f726965735f636174223b733a313a2230223b733a32303a2273686f775f7375626361745f646573635f636174223b733a313a2231223b733a32353a2273686f775f6361745f6e756d5f61727469636c65735f636174223b733a313a2231223b733a32303a226e756d5f6c656164696e675f61727469636c6573223b733a313a2230223b733a31383a226e756d5f696e74726f5f61727469636c6573223b733a313a2236223b733a31313a226e756d5f636f6c756d6e73223b733a313a2233223b733a393a226e756d5f6c696e6b73223b733a313a2234223b733a31383a226d756c74695f636f6c756d6e5f6f72646572223b733a313a2230223b733a32343a2273686f775f73756263617465676f72795f636f6e74656e74223b733a323a222d31223b733a32313a2273686f775f706167696e6174696f6e5f6c696d6974223b733a313a2231223b733a31323a2266696c7465725f6669656c64223b733a343a2268696465223b733a31333a2273686f775f68656164696e6773223b733a313a2231223b733a31343a226c6973745f73686f775f64617465223b733a313a2230223b733a31313a22646174655f666f726d6174223b733a303a22223b733a31343a226c6973745f73686f775f68697473223b733a313a2231223b733a31363a226c6973745f73686f775f617574686f72223b733a313a2231223b733a31313a226f7264657262795f707269223b733a353a226f72646572223b733a31313a226f7264657262795f736563223b733a353a227264617465223b733a31303a226f726465725f64617465223b733a393a227075626c6973686564223b733a31353a2273686f775f706167696e6174696f6e223b733a313a2232223b733a32333a2273686f775f706167696e6174696f6e5f726573756c7473223b733a313a2231223b733a31333a2273686f775f6665617475726564223b733a343a2273686f77223b733a31343a2273686f775f666565645f6c696e6b223b733a313a2231223b733a31323a22666565645f73756d6d617279223b733a313a2230223b733a31383a22666565645f73686f775f726561646d6f7265223b733a313a2230223b7d733a393a22736570617261746f72223b733a313a222e223b7d733a373a226d6574616b6579223b733a303a22223b733a383a226d65746164657363223b733a303a22223b733a383a226d65746164617461223b4f3a32343a224a6f6f6d6c615c52656769737472795c5265676973747279223a323a7b733a373a22002a0064617461223b4f3a383a22737464436c617373223a343a7b733a363a22726f626f7473223b733a303a22223b733a363a22617574686f72223b733a303a22223b733a363a22726967687473223b733a303a22223b733a31303a22787265666572656e6365223b733a303a22223b7d733a393a22736570617261746f72223b733a313a222e223b7d733a373a2276657273696f6e223b733a313a2233223b733a383a226f72646572696e67223b733a313a2231223b733a383a2263617465676f7279223b733a31333a22556e63617465676f7269736564223b733a393a226361745f7374617465223b733a323a222d32223b733a31303a226361745f616363657373223b733a313a2231223b733a343a22736c7567223b733a31373a22323a7376656368692d766f736b6f767965223b733a373a22636174736c7567223b733a31353a22323a756e63617465676f7269736564223b733a363a22617574686f72223b733a31303a2253757065722055736572223b733a363a226c61796f7574223b733a373a2261727469636c65223b733a343a2270617468223b733a32313a223f69643d323a7376656368692d766f736b6f767965223b733a31303a226d657461617574686f72223b4e3b7d733a31353a22002a00696e737472756374696f6e73223b613a353a7b693a313b613a333a7b693a303b733a353a227469746c65223b693a313b733a383a227375627469746c65223b693a323b733a323a226964223b7d693a323b613a323a7b693a303b733a373a2273756d6d617279223b693a313b733a343a22626f6479223b7d693a333b613a383a7b693a303b733a343a226d657461223b693a313b733a31303a226c6973745f7072696365223b693a323b733a31303a2273616c655f7072696365223b693a333b733a373a226d6574616b6579223b693a343b733a383a226d65746164657363223b693a353b733a31303a226d657461617574686f72223b693a363b733a363a22617574686f72223b693a373b733a31363a22637265617465645f62795f616c696173223b7d693a343b613a323a7b693a303b733a343a2270617468223b693a313b733a353a22616c696173223b7d693a353b613a313a7b693a303b733a383a22636f6d6d656e7473223b7d7d733a31313a22002a007461786f6e6f6d79223b613a343a7b733a343a2254797065223b613a313a7b733a373a2241727469636c65223b4f3a373a224a4f626a656374223a343a7b733a31303a22002a005f6572726f7273223b613a303a7b7d733a353a227469746c65223b733a373a2241727469636c65223b733a353a227374617465223b693a313b733a363a22616363657373223b693a313b7d7d733a363a22417574686f72223b613a313a7b733a31303a2253757065722055736572223b4f3a373a224a4f626a656374223a343a7b733a31303a22002a005f6572726f7273223b613a303a7b7d733a353a227469746c65223b733a31303a2253757065722055736572223b733a353a227374617465223b693a313b733a363a22616363657373223b693a313b7d7d733a383a2243617465676f7279223b613a313a7b733a31333a22556e63617465676f7269736564223b4f3a373a224a4f626a656374223a343a7b733a31303a22002a005f6572726f7273223b613a303a7b7d733a353a227469746c65223b733a31333a22556e63617465676f7269736564223b733a353a227374617465223b693a2d323b733a363a22616363657373223b693a313b7d7d733a383a224c616e6775616765223b613a313a7b733a313a222a223b4f3a373a224a4f626a656374223a343a7b733a31303a22002a005f6572726f7273223b613a303a7b7d733a353a227469746c65223b733a313a222a223b733a353a227374617465223b693a313b733a363a22616363657373223b693a313b7d7d7d733a333a2275726c223b733a34363a22696e6465782e7068703f6f7074696f6e3d636f6d5f636f6e74656e7426766965773d61727469636c652669643d32223b733a353a22726f757465223b733a37333a22696e6465782e7068703f6f7074696f6e3d636f6d5f636f6e74656e7426766965773d61727469636c652669643d323a7376656368692d766f736b6f767965264974656d69643d313031223b733a353a227469746c65223b733a32373a22d0a1d0b2d0b5d187d0b820d0b2d0bed181d0bad0bed0b2d18bd0b5223b733a31313a226465736372697074696f6e223b733a33323a22d0bfd183d18bd0b2d0bfd0bcd184d183d0b0d0bfd184d186d0b0d184d186d0b0223b733a393a227075626c6973686564223b4e3b733a353a227374617465223b693a303b733a363a22616363657373223b733a313a2231223b733a383a226c616e6775616765223b733a313a222a223b733a31383a227075626c6973685f73746172745f64617465223b733a31393a22323031352d30392d31362031333a35333a3132223b733a31363a227075626c6973685f656e645f64617465223b733a31393a22303030302d30302d30302030303a30303a3030223b733a31303a2273746172745f64617465223b733a31393a22323031352d30392d31362031333a35333a3132223b733a383a22656e645f64617465223b733a31393a22303030302d30302d30302030303a30303a3030223b733a31303a226c6973745f7072696365223b4e3b733a31303a2273616c655f7072696365223b4e3b733a373a22747970655f6964223b693a343b733a31353a2264656661756c744c616e6775616765223b733a353a2272752d5255223b7d),
(21, 'index.php?option=com_content&view=article&id=3', 'index.php?option=com_content&view=article&id=3:knigi-poligrafiya-molitvy-na-bereste&Itemid=101', 'Книги, полиграфия, молитвы на бересте', 'пыкпфыцпяпяяпяпякпкпкп', '2015-09-18 15:14:46', 'ce33cd99dc5a2c780a3e1293e2a1648a', 1, 0, 1, '*', '2015-09-16 13:54:09', '0000-00-00 00:00:00', '2015-09-16 13:54:09', '0000-00-00 00:00:00', 0, 0, 4, 0x4f3a31393a2246696e646572496e6465786572526573756c74223a31393a7b733a31313a22002a00656c656d656e7473223b613a32343a7b733a323a226964223b733a313a2233223b733a353a22616c696173223b733a33363a226b6e6967692d706f6c69677261666979612d6d6f6c697476792d6e612d62657265737465223b733a373a2273756d6d617279223b733a34343a22d0bfd18bd0bad0bfd184d18bd186d0bfd18fd0bfd18fd18fd0bfd18fd0bfd18fd0bad0bfd0bad0bfd0bad0bf223b733a343a22626f6479223b733a303a22223b733a353a226361746964223b733a313a2232223b733a31303a22637265617465645f6279223b733a333a22353935223b733a31363a22637265617465645f62795f616c696173223b733a303a22223b733a383a226d6f646966696564223b733a31393a22323031352d30392d31362031353a30343a3338223b733a31313a226d6f6469666965645f6279223b733a333a22353935223b733a363a22706172616d73223b4f3a32343a224a6f6f6d6c615c52656769737472795c5265676973747279223a323a7b733a373a22002a0064617461223b4f3a383a22737464436c617373223a37353a7b733a31343a2261727469636c655f6c61796f7574223b733a393a225f3a64656661756c74223b733a31303a2273686f775f7469746c65223b733a313a2230223b733a31313a226c696e6b5f7469746c6573223b733a313a2231223b733a31303a2273686f775f696e74726f223b733a313a2231223b733a31393a22696e666f5f626c6f636b5f706f736974696f6e223b733a313a2230223b733a31333a2273686f775f63617465676f7279223b733a313a2230223b733a31333a226c696e6b5f63617465676f7279223b733a313a2231223b733a32303a2273686f775f706172656e745f63617465676f7279223b733a313a2230223b733a32303a226c696e6b5f706172656e745f63617465676f7279223b733a313a2230223b733a31313a2273686f775f617574686f72223b733a313a2230223b733a31313a226c696e6b5f617574686f72223b733a313a2230223b733a31363a2273686f775f6372656174655f64617465223b733a313a2230223b733a31363a2273686f775f6d6f646966795f64617465223b733a313a2230223b733a31373a2273686f775f7075626c6973685f64617465223b733a313a2230223b733a32303a2273686f775f6974656d5f6e617669676174696f6e223b733a313a2231223b733a393a2273686f775f766f7465223b733a313a2230223b733a31333a2273686f775f726561646d6f7265223b733a313a2231223b733a31393a2273686f775f726561646d6f72655f7469746c65223b733a313a2231223b733a31343a22726561646d6f72655f6c696d6974223b733a333a22313030223b733a393a2273686f775f74616773223b733a313a2231223b733a31303a2273686f775f69636f6e73223b733a313a2231223b733a31353a2273686f775f7072696e745f69636f6e223b733a313a2230223b733a31353a2273686f775f656d61696c5f69636f6e223b733a313a2230223b733a393a2273686f775f68697473223b733a313a2230223b733a31313a2273686f775f6e6f61757468223b733a313a2230223b733a31333a2275726c735f706f736974696f6e223b733a313a2230223b733a32333a2273686f775f7075626c697368696e675f6f7074696f6e73223b733a313a2231223b733a32303a2273686f775f61727469636c655f6f7074696f6e73223b733a313a2231223b733a31323a22736176655f686973746f7279223b733a313a2231223b733a31333a22686973746f72795f6c696d6974223b693a31303b733a32353a2273686f775f75726c735f696d616765735f66726f6e74656e64223b733a313a2230223b733a32343a2273686f775f75726c735f696d616765735f6261636b656e64223b733a313a2231223b733a373a2274617267657461223b693a303b733a373a2274617267657462223b693a303b733a373a2274617267657463223b693a303b733a31313a22666c6f61745f696e74726f223b733a343a226c656674223b733a31343a22666c6f61745f66756c6c74657874223b733a343a226c656674223b733a31353a2263617465676f72795f6c61796f7574223b733a363a225f3a626c6f67223b733a33323a2273686f775f63617465676f72795f68656164696e675f7469746c655f74657874223b733a313a2231223b733a31393a2273686f775f63617465676f72795f7469746c65223b733a313a2230223b733a31363a2273686f775f6465736372697074696f6e223b733a313a2230223b733a32323a2273686f775f6465736372697074696f6e5f696d616765223b733a313a2230223b733a383a226d61784c6576656c223b733a313a2231223b733a32313a2273686f775f656d7074795f63617465676f72696573223b733a313a2230223b733a31363a2273686f775f6e6f5f61727469636c6573223b733a313a2231223b733a31363a2273686f775f7375626361745f64657363223b733a313a2231223b733a32313a2273686f775f6361745f6e756d5f61727469636c6573223b733a313a2230223b733a31333a2273686f775f6361745f74616773223b733a313a2231223b733a32313a2273686f775f626173655f6465736372697074696f6e223b733a313a2231223b733a31313a226d61784c6576656c636174223b733a323a222d31223b733a32353a2273686f775f656d7074795f63617465676f726965735f636174223b733a313a2230223b733a32303a2273686f775f7375626361745f646573635f636174223b733a313a2231223b733a32353a2273686f775f6361745f6e756d5f61727469636c65735f636174223b733a313a2231223b733a32303a226e756d5f6c656164696e675f61727469636c6573223b733a313a2230223b733a31383a226e756d5f696e74726f5f61727469636c6573223b733a313a2236223b733a31313a226e756d5f636f6c756d6e73223b733a313a2233223b733a393a226e756d5f6c696e6b73223b733a313a2234223b733a31383a226d756c74695f636f6c756d6e5f6f72646572223b733a313a2230223b733a32343a2273686f775f73756263617465676f72795f636f6e74656e74223b733a323a222d31223b733a32313a2273686f775f706167696e6174696f6e5f6c696d6974223b733a313a2231223b733a31323a2266696c7465725f6669656c64223b733a343a2268696465223b733a31333a2273686f775f68656164696e6773223b733a313a2231223b733a31343a226c6973745f73686f775f64617465223b733a313a2230223b733a31313a22646174655f666f726d6174223b733a303a22223b733a31343a226c6973745f73686f775f68697473223b733a313a2231223b733a31363a226c6973745f73686f775f617574686f72223b733a313a2231223b733a31313a226f7264657262795f707269223b733a353a226f72646572223b733a31313a226f7264657262795f736563223b733a353a227264617465223b733a31303a226f726465725f64617465223b733a393a227075626c6973686564223b733a31353a2273686f775f706167696e6174696f6e223b733a313a2232223b733a32333a2273686f775f706167696e6174696f6e5f726573756c7473223b733a313a2231223b733a31333a2273686f775f6665617475726564223b733a343a2273686f77223b733a31343a2273686f775f666565645f6c696e6b223b733a313a2231223b733a31323a22666565645f73756d6d617279223b733a313a2230223b733a31383a22666565645f73686f775f726561646d6f7265223b733a313a2230223b7d733a393a22736570617261746f72223b733a313a222e223b7d733a373a226d6574616b6579223b733a303a22223b733a383a226d65746164657363223b733a303a22223b733a383a226d65746164617461223b4f3a32343a224a6f6f6d6c615c52656769737472795c5265676973747279223a323a7b733a373a22002a0064617461223b4f3a383a22737464436c617373223a343a7b733a363a22726f626f7473223b733a303a22223b733a363a22617574686f72223b733a303a22223b733a363a22726967687473223b733a303a22223b733a31303a22787265666572656e6365223b733a303a22223b7d733a393a22736570617261746f72223b733a313a222e223b7d733a373a2276657273696f6e223b733a313a2235223b733a383a226f72646572696e67223b733a313a2230223b733a383a2263617465676f7279223b733a31333a22556e63617465676f7269736564223b733a393a226361745f7374617465223b733a323a222d32223b733a31303a226361745f616363657373223b733a313a2231223b733a343a22736c7567223b733a33383a22333a6b6e6967692d706f6c69677261666979612d6d6f6c697476792d6e612d62657265737465223b733a373a22636174736c7567223b733a31353a22323a756e63617465676f7269736564223b733a363a22617574686f72223b733a31303a2253757065722055736572223b733a363a226c61796f7574223b733a373a2261727469636c65223b733a343a2270617468223b733a34323a223f69643d333a6b6e6967692d706f6c69677261666979612d6d6f6c697476792d6e612d62657265737465223b733a31303a226d657461617574686f72223b4e3b7d733a31353a22002a00696e737472756374696f6e73223b613a353a7b693a313b613a333a7b693a303b733a353a227469746c65223b693a313b733a383a227375627469746c65223b693a323b733a323a226964223b7d693a323b613a323a7b693a303b733a373a2273756d6d617279223b693a313b733a343a22626f6479223b7d693a333b613a383a7b693a303b733a343a226d657461223b693a313b733a31303a226c6973745f7072696365223b693a323b733a31303a2273616c655f7072696365223b693a333b733a373a226d6574616b6579223b693a343b733a383a226d65746164657363223b693a353b733a31303a226d657461617574686f72223b693a363b733a363a22617574686f72223b693a373b733a31363a22637265617465645f62795f616c696173223b7d693a343b613a323a7b693a303b733a343a2270617468223b693a313b733a353a22616c696173223b7d693a353b613a313a7b693a303b733a383a22636f6d6d656e7473223b7d7d733a31313a22002a007461786f6e6f6d79223b613a343a7b733a343a2254797065223b613a313a7b733a373a2241727469636c65223b4f3a373a224a4f626a656374223a343a7b733a31303a22002a005f6572726f7273223b613a303a7b7d733a353a227469746c65223b733a373a2241727469636c65223b733a353a227374617465223b693a313b733a363a22616363657373223b693a313b7d7d733a363a22417574686f72223b613a313a7b733a31303a2253757065722055736572223b4f3a373a224a4f626a656374223a343a7b733a31303a22002a005f6572726f7273223b613a303a7b7d733a353a227469746c65223b733a31303a2253757065722055736572223b733a353a227374617465223b693a313b733a363a22616363657373223b693a313b7d7d733a383a2243617465676f7279223b613a313a7b733a31333a22556e63617465676f7269736564223b4f3a373a224a4f626a656374223a343a7b733a31303a22002a005f6572726f7273223b613a303a7b7d733a353a227469746c65223b733a31333a22556e63617465676f7269736564223b733a353a227374617465223b693a2d323b733a363a22616363657373223b693a313b7d7d733a383a224c616e6775616765223b613a313a7b733a313a222a223b4f3a373a224a4f626a656374223a343a7b733a31303a22002a005f6572726f7273223b613a303a7b7d733a353a227469746c65223b733a313a222a223b733a353a227374617465223b693a313b733a363a22616363657373223b693a313b7d7d7d733a333a2275726c223b733a34363a22696e6465782e7068703f6f7074696f6e3d636f6d5f636f6e74656e7426766965773d61727469636c652669643d33223b733a353a22726f757465223b733a39343a22696e6465782e7068703f6f7074696f6e3d636f6d5f636f6e74656e7426766965773d61727469636c652669643d333a6b6e6967692d706f6c69677261666979612d6d6f6c697476792d6e612d62657265737465264974656d69643d313031223b733a353a227469746c65223b733a36383a22d09ad0bdd0b8d0b3d0b82c20d0bfd0bed0bbd0b8d0b3d180d0b0d184d0b8d18f2c20d0bcd0bed0bbd0b8d182d0b2d18b20d0bdd0b020d0b1d0b5d180d0b5d181d182d0b5223b733a31313a226465736372697074696f6e223b733a34343a22d0bfd18bd0bad0bfd184d18bd186d0bfd18fd0bfd18fd18fd0bfd18fd0bfd18fd0bad0bfd0bad0bfd0bad0bf223b733a393a227075626c6973686564223b4e3b733a353a227374617465223b693a303b733a363a22616363657373223b733a313a2231223b733a383a226c616e6775616765223b733a313a222a223b733a31383a227075626c6973685f73746172745f64617465223b733a31393a22323031352d30392d31362031333a35343a3039223b733a31363a227075626c6973685f656e645f64617465223b733a31393a22303030302d30302d30302030303a30303a3030223b733a31303a2273746172745f64617465223b733a31393a22323031352d30392d31362031333a35343a3039223b733a383a22656e645f64617465223b733a31393a22303030302d30302d30302030303a30303a3030223b733a31303a226c6973745f7072696365223b4e3b733a31303a2273616c655f7072696365223b4e3b733a373a22747970655f6964223b693a343b733a31353a2264656661756c744c616e6775616765223b733a353a2272752d5255223b7d),
(69, 'index.php?option=com_content&view=article&id=8', 'index.php?option=com_content&view=article&id=8:xiii-sretenskij-festival-dukhovnoj-i-narodnoj-muzyki-2&catid=9&Itemid=101', 'XIII Сретенский фестиваль духовной и народной музыки', '', '2015-09-19 16:55:02', '5652b2988db7f7ad9a76ac2e78035ad9', 1, 1, 1, '*', '2015-09-18 12:19:39', '0000-00-00 00:00:00', '2015-09-18 12:19:39', '0000-00-00 00:00:00', 0, 0, 4, 0x4f3a31393a2246696e646572496e6465786572526573756c74223a31393a7b733a31313a22002a00656c656d656e7473223b613a32343a7b733a323a226964223b733a313a2238223b733a353a22616c696173223b733a35343a22786969692d73726574656e736b696a2d666573746976616c2d64756b686f766e6f6a2d692d6e61726f646e6f6a2d6d757a796b692d32223b733a373a2273756d6d617279223b733a303a22223b733a343a22626f6479223b733a31353a220d0a676862646e736a686b6a6c6b6a223b733a353a226361746964223b733a313a2239223b733a31303a22637265617465645f6279223b733a333a22353935223b733a31363a22637265617465645f62795f616c696173223b733a303a22223b733a383a226d6f646966696564223b733a31393a22323031352d30392d31392031333a35353a3031223b733a31313a226d6f6469666965645f6279223b733a333a22353935223b733a363a22706172616d73223b4f3a32343a224a6f6f6d6c615c52656769737472795c5265676973747279223a323a7b733a373a22002a0064617461223b4f3a383a22737464436c617373223a37353a7b733a31343a2261727469636c655f6c61796f7574223b733a393a225f3a64656661756c74223b733a31303a2273686f775f7469746c65223b733a313a2230223b733a31313a226c696e6b5f7469746c6573223b733a313a2230223b733a31303a2273686f775f696e74726f223b733a313a2231223b733a31393a22696e666f5f626c6f636b5f706f736974696f6e223b733a313a2230223b733a31333a2273686f775f63617465676f7279223b733a313a2231223b733a31333a226c696e6b5f63617465676f7279223b733a313a2230223b733a32303a2273686f775f706172656e745f63617465676f7279223b733a313a2231223b733a32303a226c696e6b5f706172656e745f63617465676f7279223b733a313a2230223b733a31313a2273686f775f617574686f72223b733a313a2230223b733a31313a226c696e6b5f617574686f72223b733a313a2230223b733a31363a2273686f775f6372656174655f64617465223b733a313a2231223b733a31363a2273686f775f6d6f646966795f64617465223b733a313a2231223b733a31373a2273686f775f7075626c6973685f64617465223b733a313a2231223b733a32303a2273686f775f6974656d5f6e617669676174696f6e223b733a313a2231223b733a393a2273686f775f766f7465223b733a313a2230223b733a31333a2273686f775f726561646d6f7265223b733a313a2231223b733a31393a2273686f775f726561646d6f72655f7469746c65223b733a313a2231223b733a31343a22726561646d6f72655f6c696d6974223b733a333a22313030223b733a393a2273686f775f74616773223b733a313a2231223b733a31303a2273686f775f69636f6e73223b733a313a2231223b733a31353a2273686f775f7072696e745f69636f6e223b733a313a2230223b733a31353a2273686f775f656d61696c5f69636f6e223b733a313a2230223b733a393a2273686f775f68697473223b733a313a2230223b733a31313a2273686f775f6e6f61757468223b733a313a2230223b733a31333a2275726c735f706f736974696f6e223b733a313a2230223b733a32333a2273686f775f7075626c697368696e675f6f7074696f6e73223b733a313a2231223b733a32303a2273686f775f61727469636c655f6f7074696f6e73223b733a313a2231223b733a31323a22736176655f686973746f7279223b733a313a2231223b733a31333a22686973746f72795f6c696d6974223b693a31303b733a32353a2273686f775f75726c735f696d616765735f66726f6e74656e64223b733a313a2230223b733a32343a2273686f775f75726c735f696d616765735f6261636b656e64223b733a313a2231223b733a373a2274617267657461223b693a303b733a373a2274617267657462223b693a303b733a373a2274617267657463223b693a303b733a31313a22666c6f61745f696e74726f223b733a343a226c656674223b733a31343a22666c6f61745f66756c6c74657874223b733a343a226c656674223b733a31353a2263617465676f72795f6c61796f7574223b733a363a225f3a626c6f67223b733a33323a2273686f775f63617465676f72795f68656164696e675f7469746c655f74657874223b733a313a2230223b733a31393a2273686f775f63617465676f72795f7469746c65223b733a313a2231223b733a31363a2273686f775f6465736372697074696f6e223b733a313a2230223b733a32323a2273686f775f6465736372697074696f6e5f696d616765223b733a313a2230223b733a383a226d61784c6576656c223b733a313a2231223b733a32313a2273686f775f656d7074795f63617465676f72696573223b733a313a2230223b733a31363a2273686f775f6e6f5f61727469636c6573223b733a313a2231223b733a31363a2273686f775f7375626361745f64657363223b733a313a2231223b733a32313a2273686f775f6361745f6e756d5f61727469636c6573223b733a313a2230223b733a31333a2273686f775f6361745f74616773223b733a313a2231223b733a32313a2273686f775f626173655f6465736372697074696f6e223b733a313a2231223b733a31313a226d61784c6576656c636174223b733a323a222d31223b733a32353a2273686f775f656d7074795f63617465676f726965735f636174223b733a313a2230223b733a32303a2273686f775f7375626361745f646573635f636174223b733a313a2231223b733a32353a2273686f775f6361745f6e756d5f61727469636c65735f636174223b733a313a2231223b733a32303a226e756d5f6c656164696e675f61727469636c6573223b733a313a2230223b733a31383a226e756d5f696e74726f5f61727469636c6573223b733a313a2236223b733a31313a226e756d5f636f6c756d6e73223b733a313a2233223b733a393a226e756d5f6c696e6b73223b733a313a2234223b733a31383a226d756c74695f636f6c756d6e5f6f72646572223b733a313a2230223b733a32343a2273686f775f73756263617465676f72795f636f6e74656e74223b733a323a222d31223b733a32313a2273686f775f706167696e6174696f6e5f6c696d6974223b733a313a2231223b733a31323a2266696c7465725f6669656c64223b733a343a2268696465223b733a31333a2273686f775f68656164696e6773223b733a313a2231223b733a31343a226c6973745f73686f775f64617465223b733a313a2230223b733a31313a22646174655f666f726d6174223b733a303a22223b733a31343a226c6973745f73686f775f68697473223b733a313a2231223b733a31363a226c6973745f73686f775f617574686f72223b733a313a2231223b733a31313a226f7264657262795f707269223b733a353a226f72646572223b733a31313a226f7264657262795f736563223b733a353a227264617465223b733a31303a226f726465725f64617465223b733a393a227075626c6973686564223b733a31353a2273686f775f706167696e6174696f6e223b733a313a2232223b733a32333a2273686f775f706167696e6174696f6e5f726573756c7473223b733a313a2231223b733a31333a2273686f775f6665617475726564223b733a343a2273686f77223b733a31343a2273686f775f666565645f6c696e6b223b733a313a2231223b733a31323a22666565645f73756d6d617279223b733a313a2230223b733a31383a22666565645f73686f775f726561646d6f7265223b733a313a2230223b7d733a393a22736570617261746f72223b733a313a222e223b7d733a373a226d6574616b6579223b733a303a22223b733a383a226d65746164657363223b733a303a22223b733a383a226d65746164617461223b4f3a32343a224a6f6f6d6c615c52656769737472795c5265676973747279223a323a7b733a373a22002a0064617461223b4f3a383a22737464436c617373223a343a7b733a363a22726f626f7473223b733a303a22223b733a363a22617574686f72223b733a303a22223b733a363a22726967687473223b733a303a22223b733a31303a22787265666572656e6365223b733a303a22223b7d733a393a22736570617261746f72223b733a313a222e223b7d733a373a2276657273696f6e223b733a323a223334223b733a383a226f72646572696e67223b733a313a2230223b733a383a2263617465676f7279223b733a31343a22d09dd0bed0b2d0bed181d182d0b8223b733a393a226361745f7374617465223b733a313a2231223b733a31303a226361745f616363657373223b733a313a2231223b733a343a22736c7567223b733a35363a22383a786969692d73726574656e736b696a2d666573746976616c2d64756b686f766e6f6a2d692d6e61726f646e6f6a2d6d757a796b692d32223b733a373a22636174736c7567223b733a393a22393a6e6f766f737469223b733a363a22617574686f72223b733a31303a2253757065722055736572223b733a363a226c61796f7574223b733a373a2261727469636c65223b733a343a2270617468223b733a36363a22392d6e6f766f7374692f382d786969692d73726574656e736b696a2d666573746976616c2d64756b686f766e6f6a2d692d6e61726f646e6f6a2d6d757a796b692d32223b733a31303a226d657461617574686f72223b4e3b7d733a31353a22002a00696e737472756374696f6e73223b613a353a7b693a313b613a333a7b693a303b733a353a227469746c65223b693a313b733a383a227375627469746c65223b693a323b733a323a226964223b7d693a323b613a323a7b693a303b733a373a2273756d6d617279223b693a313b733a343a22626f6479223b7d693a333b613a383a7b693a303b733a343a226d657461223b693a313b733a31303a226c6973745f7072696365223b693a323b733a31303a2273616c655f7072696365223b693a333b733a373a226d6574616b6579223b693a343b733a383a226d65746164657363223b693a353b733a31303a226d657461617574686f72223b693a363b733a363a22617574686f72223b693a373b733a31363a22637265617465645f62795f616c696173223b7d693a343b613a323a7b693a303b733a343a2270617468223b693a313b733a353a22616c696173223b7d693a353b613a313a7b693a303b733a383a22636f6d6d656e7473223b7d7d733a31313a22002a007461786f6e6f6d79223b613a343a7b733a343a2254797065223b613a313a7b733a373a2241727469636c65223b4f3a373a224a4f626a656374223a343a7b733a31303a22002a005f6572726f7273223b613a303a7b7d733a353a227469746c65223b733a373a2241727469636c65223b733a353a227374617465223b693a313b733a363a22616363657373223b693a313b7d7d733a363a22417574686f72223b613a313a7b733a31303a2253757065722055736572223b4f3a373a224a4f626a656374223a343a7b733a31303a22002a005f6572726f7273223b613a303a7b7d733a353a227469746c65223b733a31303a2253757065722055736572223b733a353a227374617465223b693a313b733a363a22616363657373223b693a313b7d7d733a383a2243617465676f7279223b613a313a7b733a31343a22d09dd0bed0b2d0bed181d182d0b8223b4f3a373a224a4f626a656374223a343a7b733a31303a22002a005f6572726f7273223b613a303a7b7d733a353a227469746c65223b733a31343a22d09dd0bed0b2d0bed181d182d0b8223b733a353a227374617465223b693a313b733a363a22616363657373223b693a313b7d7d733a383a224c616e6775616765223b613a313a7b733a313a222a223b4f3a373a224a4f626a656374223a343a7b733a31303a22002a005f6572726f7273223b613a303a7b7d733a353a227469746c65223b733a313a222a223b733a353a227374617465223b693a313b733a363a22616363657373223b693a313b7d7d7d733a333a2275726c223b733a34363a22696e6465782e7068703f6f7074696f6e3d636f6d5f636f6e74656e7426766965773d61727469636c652669643d38223b733a353a22726f757465223b733a3132303a22696e6465782e7068703f6f7074696f6e3d636f6d5f636f6e74656e7426766965773d61727469636c652669643d383a786969692d73726574656e736b696a2d666573746976616c2d64756b686f766e6f6a2d692d6e61726f646e6f6a2d6d757a796b692d322663617469643d39264974656d69643d313031223b733a353a227469746c65223b733a39343a225849494920d0a1d180d0b5d182d0b5d0bdd181d0bad0b8d0b920d184d0b5d181d182d0b8d0b2d0b0d0bbd18c20d0b4d183d185d0bed0b2d0bdd0bed0b920d0b820d0bdd0b0d180d0bed0b4d0bdd0bed0b920d0bcd183d0b7d18bd0bad0b8223b733a31313a226465736372697074696f6e223b733a303a22223b733a393a227075626c6973686564223b4e3b733a353a227374617465223b693a313b733a363a22616363657373223b733a313a2231223b733a383a226c616e6775616765223b733a313a222a223b733a31383a227075626c6973685f73746172745f64617465223b733a31393a22323031352d30392d31382031323a31393a3339223b733a31363a227075626c6973685f656e645f64617465223b733a31393a22303030302d30302d30302030303a30303a3030223b733a31303a2273746172745f64617465223b733a31393a22323031352d30392d31382031323a31393a3339223b733a383a22656e645f64617465223b733a31393a22303030302d30302d30302030303a30303a3030223b733a31303a226c6973745f7072696365223b4e3b733a31303a2273616c655f7072696365223b4e3b733a373a22747970655f6964223b693a343b733a31353a2264656661756c744c616e6775616765223b733a353a2272752d5255223b7d);
INSERT INTO `yawnc_finder_links` (`link_id`, `url`, `route`, `title`, `description`, `indexdate`, `md5sum`, `published`, `state`, `access`, `language`, `publish_start_date`, `publish_end_date`, `start_date`, `end_date`, `list_price`, `sale_price`, `type_id`, `object`) VALUES
(70, 'index.php?option=com_content&view=article&id=7', 'index.php?option=com_content&view=article&id=7:xiii-sretenskij-festival-dukhovnoj-i-narodnoj-muzyki&catid=9&Itemid=101', 'XIII Сретенский фестиваль духовной и народной музыки', '', '2015-09-19 16:55:21', 'e66319bf2de32ce2cdd9b100a6ce7122', 1, 1, 1, '*', '2015-09-18 12:19:02', '0000-00-00 00:00:00', '2015-09-18 12:19:02', '0000-00-00 00:00:00', 0, 0, 4, 0x4f3a31393a2246696e646572496e6465786572526573756c74223a31393a7b733a31313a22002a00656c656d656e7473223b613a32343a7b733a323a226964223b733a313a2237223b733a353a22616c696173223b733a35323a22786969692d73726574656e736b696a2d666573746976616c2d64756b686f766e6f6a2d692d6e61726f646e6f6a2d6d757a796b69223b733a373a2273756d6d617279223b733a303a22223b733a343a22626f6479223b733a32303a220d0ad180d0bfd180d0b0d0bfd180d0b0d180d0b8223b733a353a226361746964223b733a313a2239223b733a31303a22637265617465645f6279223b733a333a22353935223b733a31363a22637265617465645f62795f616c696173223b733a303a22223b733a383a226d6f646966696564223b733a31393a22323031352d30392d31392031333a35353a3230223b733a31313a226d6f6469666965645f6279223b733a333a22353935223b733a363a22706172616d73223b4f3a32343a224a6f6f6d6c615c52656769737472795c5265676973747279223a323a7b733a373a22002a0064617461223b4f3a383a22737464436c617373223a37353a7b733a31343a2261727469636c655f6c61796f7574223b733a393a225f3a64656661756c74223b733a31303a2273686f775f7469746c65223b733a313a2231223b733a31313a226c696e6b5f7469746c6573223b733a313a2230223b733a31303a2273686f775f696e74726f223b733a313a2231223b733a31393a22696e666f5f626c6f636b5f706f736974696f6e223b733a313a2230223b733a31333a2273686f775f63617465676f7279223b733a313a2231223b733a31333a226c696e6b5f63617465676f7279223b733a313a2230223b733a32303a2273686f775f706172656e745f63617465676f7279223b733a313a2231223b733a32303a226c696e6b5f706172656e745f63617465676f7279223b733a313a2230223b733a31313a2273686f775f617574686f72223b733a313a2230223b733a31313a226c696e6b5f617574686f72223b733a313a2230223b733a31363a2273686f775f6372656174655f64617465223b733a313a2231223b733a31363a2273686f775f6d6f646966795f64617465223b733a313a2231223b733a31373a2273686f775f7075626c6973685f64617465223b733a313a2231223b733a32303a2273686f775f6974656d5f6e617669676174696f6e223b733a313a2231223b733a393a2273686f775f766f7465223b733a313a2230223b733a31333a2273686f775f726561646d6f7265223b733a313a2231223b733a31393a2273686f775f726561646d6f72655f7469746c65223b733a313a2231223b733a31343a22726561646d6f72655f6c696d6974223b733a333a22313030223b733a393a2273686f775f74616773223b733a313a2231223b733a31303a2273686f775f69636f6e73223b733a313a2231223b733a31353a2273686f775f7072696e745f69636f6e223b733a313a2230223b733a31353a2273686f775f656d61696c5f69636f6e223b733a313a2230223b733a393a2273686f775f68697473223b733a313a2230223b733a31313a2273686f775f6e6f61757468223b733a313a2230223b733a31333a2275726c735f706f736974696f6e223b733a313a2230223b733a32333a2273686f775f7075626c697368696e675f6f7074696f6e73223b733a313a2231223b733a32303a2273686f775f61727469636c655f6f7074696f6e73223b733a313a2231223b733a31323a22736176655f686973746f7279223b733a313a2231223b733a31333a22686973746f72795f6c696d6974223b693a31303b733a32353a2273686f775f75726c735f696d616765735f66726f6e74656e64223b733a313a2230223b733a32343a2273686f775f75726c735f696d616765735f6261636b656e64223b733a313a2231223b733a373a2274617267657461223b693a303b733a373a2274617267657462223b693a303b733a373a2274617267657463223b693a303b733a31313a22666c6f61745f696e74726f223b733a343a226c656674223b733a31343a22666c6f61745f66756c6c74657874223b733a343a226c656674223b733a31353a2263617465676f72795f6c61796f7574223b733a363a225f3a626c6f67223b733a33323a2273686f775f63617465676f72795f68656164696e675f7469746c655f74657874223b733a313a2230223b733a31393a2273686f775f63617465676f72795f7469746c65223b733a313a2231223b733a31363a2273686f775f6465736372697074696f6e223b733a313a2230223b733a32323a2273686f775f6465736372697074696f6e5f696d616765223b733a313a2230223b733a383a226d61784c6576656c223b733a313a2231223b733a32313a2273686f775f656d7074795f63617465676f72696573223b733a313a2230223b733a31363a2273686f775f6e6f5f61727469636c6573223b733a313a2231223b733a31363a2273686f775f7375626361745f64657363223b733a313a2231223b733a32313a2273686f775f6361745f6e756d5f61727469636c6573223b733a313a2230223b733a31333a2273686f775f6361745f74616773223b733a313a2231223b733a32313a2273686f775f626173655f6465736372697074696f6e223b733a313a2231223b733a31313a226d61784c6576656c636174223b733a323a222d31223b733a32353a2273686f775f656d7074795f63617465676f726965735f636174223b733a313a2230223b733a32303a2273686f775f7375626361745f646573635f636174223b733a313a2231223b733a32353a2273686f775f6361745f6e756d5f61727469636c65735f636174223b733a313a2231223b733a32303a226e756d5f6c656164696e675f61727469636c6573223b733a313a2230223b733a31383a226e756d5f696e74726f5f61727469636c6573223b733a313a2236223b733a31313a226e756d5f636f6c756d6e73223b733a313a2233223b733a393a226e756d5f6c696e6b73223b733a313a2234223b733a31383a226d756c74695f636f6c756d6e5f6f72646572223b733a313a2230223b733a32343a2273686f775f73756263617465676f72795f636f6e74656e74223b733a323a222d31223b733a32313a2273686f775f706167696e6174696f6e5f6c696d6974223b733a313a2231223b733a31323a2266696c7465725f6669656c64223b733a343a2268696465223b733a31333a2273686f775f68656164696e6773223b733a313a2231223b733a31343a226c6973745f73686f775f64617465223b733a313a2230223b733a31313a22646174655f666f726d6174223b733a303a22223b733a31343a226c6973745f73686f775f68697473223b733a313a2231223b733a31363a226c6973745f73686f775f617574686f72223b733a313a2231223b733a31313a226f7264657262795f707269223b733a353a226f72646572223b733a31313a226f7264657262795f736563223b733a353a227264617465223b733a31303a226f726465725f64617465223b733a393a227075626c6973686564223b733a31353a2273686f775f706167696e6174696f6e223b733a313a2232223b733a32333a2273686f775f706167696e6174696f6e5f726573756c7473223b733a313a2231223b733a31333a2273686f775f6665617475726564223b733a343a2273686f77223b733a31343a2273686f775f666565645f6c696e6b223b733a313a2231223b733a31323a22666565645f73756d6d617279223b733a313a2230223b733a31383a22666565645f73686f775f726561646d6f7265223b733a313a2230223b7d733a393a22736570617261746f72223b733a313a222e223b7d733a373a226d6574616b6579223b733a303a22223b733a383a226d65746164657363223b733a303a22223b733a383a226d65746164617461223b4f3a32343a224a6f6f6d6c615c52656769737472795c5265676973747279223a323a7b733a373a22002a0064617461223b4f3a383a22737464436c617373223a343a7b733a363a22726f626f7473223b733a303a22223b733a363a22617574686f72223b733a303a22223b733a363a22726967687473223b733a303a22223b733a31303a22787265666572656e6365223b733a303a22223b7d733a393a22736570617261746f72223b733a313a222e223b7d733a373a2276657273696f6e223b733a313a2236223b733a383a226f72646572696e67223b733a313a2231223b733a383a2263617465676f7279223b733a31343a22d09dd0bed0b2d0bed181d182d0b8223b733a393a226361745f7374617465223b733a313a2231223b733a31303a226361745f616363657373223b733a313a2231223b733a343a22736c7567223b733a35343a22373a786969692d73726574656e736b696a2d666573746976616c2d64756b686f766e6f6a2d692d6e61726f646e6f6a2d6d757a796b69223b733a373a22636174736c7567223b733a393a22393a6e6f766f737469223b733a363a22617574686f72223b733a31303a2253757065722055736572223b733a363a226c61796f7574223b733a373a2261727469636c65223b733a343a2270617468223b733a36343a22392d6e6f766f7374692f372d786969692d73726574656e736b696a2d666573746976616c2d64756b686f766e6f6a2d692d6e61726f646e6f6a2d6d757a796b69223b733a31303a226d657461617574686f72223b4e3b7d733a31353a22002a00696e737472756374696f6e73223b613a353a7b693a313b613a333a7b693a303b733a353a227469746c65223b693a313b733a383a227375627469746c65223b693a323b733a323a226964223b7d693a323b613a323a7b693a303b733a373a2273756d6d617279223b693a313b733a343a22626f6479223b7d693a333b613a383a7b693a303b733a343a226d657461223b693a313b733a31303a226c6973745f7072696365223b693a323b733a31303a2273616c655f7072696365223b693a333b733a373a226d6574616b6579223b693a343b733a383a226d65746164657363223b693a353b733a31303a226d657461617574686f72223b693a363b733a363a22617574686f72223b693a373b733a31363a22637265617465645f62795f616c696173223b7d693a343b613a323a7b693a303b733a343a2270617468223b693a313b733a353a22616c696173223b7d693a353b613a313a7b693a303b733a383a22636f6d6d656e7473223b7d7d733a31313a22002a007461786f6e6f6d79223b613a343a7b733a343a2254797065223b613a313a7b733a373a2241727469636c65223b4f3a373a224a4f626a656374223a343a7b733a31303a22002a005f6572726f7273223b613a303a7b7d733a353a227469746c65223b733a373a2241727469636c65223b733a353a227374617465223b693a313b733a363a22616363657373223b693a313b7d7d733a363a22417574686f72223b613a313a7b733a31303a2253757065722055736572223b4f3a373a224a4f626a656374223a343a7b733a31303a22002a005f6572726f7273223b613a303a7b7d733a353a227469746c65223b733a31303a2253757065722055736572223b733a353a227374617465223b693a313b733a363a22616363657373223b693a313b7d7d733a383a2243617465676f7279223b613a313a7b733a31343a22d09dd0bed0b2d0bed181d182d0b8223b4f3a373a224a4f626a656374223a343a7b733a31303a22002a005f6572726f7273223b613a303a7b7d733a353a227469746c65223b733a31343a22d09dd0bed0b2d0bed181d182d0b8223b733a353a227374617465223b693a313b733a363a22616363657373223b693a313b7d7d733a383a224c616e6775616765223b613a313a7b733a313a222a223b4f3a373a224a4f626a656374223a343a7b733a31303a22002a005f6572726f7273223b613a303a7b7d733a353a227469746c65223b733a313a222a223b733a353a227374617465223b693a313b733a363a22616363657373223b693a313b7d7d7d733a333a2275726c223b733a34363a22696e6465782e7068703f6f7074696f6e3d636f6d5f636f6e74656e7426766965773d61727469636c652669643d37223b733a353a22726f757465223b733a3131383a22696e6465782e7068703f6f7074696f6e3d636f6d5f636f6e74656e7426766965773d61727469636c652669643d373a786969692d73726574656e736b696a2d666573746976616c2d64756b686f766e6f6a2d692d6e61726f646e6f6a2d6d757a796b692663617469643d39264974656d69643d313031223b733a353a227469746c65223b733a39343a225849494920d0a1d180d0b5d182d0b5d0bdd181d0bad0b8d0b920d184d0b5d181d182d0b8d0b2d0b0d0bbd18c20d0b4d183d185d0bed0b2d0bdd0bed0b920d0b820d0bdd0b0d180d0bed0b4d0bdd0bed0b920d0bcd183d0b7d18bd0bad0b8223b733a31313a226465736372697074696f6e223b733a303a22223b733a393a227075626c6973686564223b4e3b733a353a227374617465223b693a313b733a363a22616363657373223b733a313a2231223b733a383a226c616e6775616765223b733a313a222a223b733a31383a227075626c6973685f73746172745f64617465223b733a31393a22323031352d30392d31382031323a31393a3032223b733a31363a227075626c6973685f656e645f64617465223b733a31393a22303030302d30302d30302030303a30303a3030223b733a31303a2273746172745f64617465223b733a31393a22323031352d30392d31382031323a31393a3032223b733a383a22656e645f64617465223b733a31393a22303030302d30302d30302030303a30303a3030223b733a31303a226c6973745f7072696365223b4e3b733a31303a2273616c655f7072696365223b4e3b733a373a22747970655f6964223b693a343b733a31353a2264656661756c744c616e6775616765223b733a353a2272752d5255223b7d),
(71, 'index.php?option=com_content&view=article&id=6', 'index.php?option=com_content&view=article&id=6:maslenitsa-v-aleksandrovke&catid=9&Itemid=101', 'Масленица в Александровке', '', '2015-09-19 16:55:43', '49d01e7128b1be1f8d1d7a0778521b4e', 1, 1, 1, '*', '2015-09-18 12:18:05', '0000-00-00 00:00:00', '2015-09-18 12:18:05', '0000-00-00 00:00:00', 0, 0, 4, 0x4f3a31393a2246696e646572496e6465786572526573756c74223a31393a7b733a31313a22002a00656c656d656e7473223b613a32343a7b733a323a226964223b733a313a2236223b733a353a22616c696173223b733a32363a226d61736c656e697473612d762d616c656b73616e64726f766b65223b733a373a2273756d6d617279223b733a303a22223b733a343a22626f6479223b733a32323a220d0ad0bed182d0bed180d0bcd180d0bfd0b0d180d0bf223b733a353a226361746964223b733a313a2239223b733a31303a22637265617465645f6279223b733a333a22353935223b733a31363a22637265617465645f62795f616c696173223b733a303a22223b733a383a226d6f646966696564223b733a31393a22323031352d30392d31392031333a35353a3432223b733a31313a226d6f6469666965645f6279223b733a333a22353935223b733a363a22706172616d73223b4f3a32343a224a6f6f6d6c615c52656769737472795c5265676973747279223a323a7b733a373a22002a0064617461223b4f3a383a22737464436c617373223a37353a7b733a31343a2261727469636c655f6c61796f7574223b733a393a225f3a64656661756c74223b733a31303a2273686f775f7469746c65223b733a313a2231223b733a31313a226c696e6b5f7469746c6573223b733a313a2230223b733a31303a2273686f775f696e74726f223b733a313a2231223b733a31393a22696e666f5f626c6f636b5f706f736974696f6e223b733a313a2230223b733a31333a2273686f775f63617465676f7279223b733a313a2231223b733a31333a226c696e6b5f63617465676f7279223b733a313a2230223b733a32303a2273686f775f706172656e745f63617465676f7279223b733a313a2231223b733a32303a226c696e6b5f706172656e745f63617465676f7279223b733a313a2230223b733a31313a2273686f775f617574686f72223b733a313a2230223b733a31313a226c696e6b5f617574686f72223b733a313a2230223b733a31363a2273686f775f6372656174655f64617465223b733a313a2231223b733a31363a2273686f775f6d6f646966795f64617465223b733a313a2231223b733a31373a2273686f775f7075626c6973685f64617465223b733a313a2231223b733a32303a2273686f775f6974656d5f6e617669676174696f6e223b733a313a2231223b733a393a2273686f775f766f7465223b733a313a2230223b733a31333a2273686f775f726561646d6f7265223b733a313a2231223b733a31393a2273686f775f726561646d6f72655f7469746c65223b733a313a2231223b733a31343a22726561646d6f72655f6c696d6974223b733a333a22313030223b733a393a2273686f775f74616773223b733a313a2231223b733a31303a2273686f775f69636f6e73223b733a313a2231223b733a31353a2273686f775f7072696e745f69636f6e223b733a313a2230223b733a31353a2273686f775f656d61696c5f69636f6e223b733a313a2230223b733a393a2273686f775f68697473223b733a313a2230223b733a31313a2273686f775f6e6f61757468223b733a313a2230223b733a31333a2275726c735f706f736974696f6e223b733a313a2230223b733a32333a2273686f775f7075626c697368696e675f6f7074696f6e73223b733a313a2231223b733a32303a2273686f775f61727469636c655f6f7074696f6e73223b733a313a2231223b733a31323a22736176655f686973746f7279223b733a313a2231223b733a31333a22686973746f72795f6c696d6974223b693a31303b733a32353a2273686f775f75726c735f696d616765735f66726f6e74656e64223b733a313a2230223b733a32343a2273686f775f75726c735f696d616765735f6261636b656e64223b733a313a2231223b733a373a2274617267657461223b693a303b733a373a2274617267657462223b693a303b733a373a2274617267657463223b693a303b733a31313a22666c6f61745f696e74726f223b733a343a226c656674223b733a31343a22666c6f61745f66756c6c74657874223b733a343a226c656674223b733a31353a2263617465676f72795f6c61796f7574223b733a363a225f3a626c6f67223b733a33323a2273686f775f63617465676f72795f68656164696e675f7469746c655f74657874223b733a313a2230223b733a31393a2273686f775f63617465676f72795f7469746c65223b733a313a2231223b733a31363a2273686f775f6465736372697074696f6e223b733a313a2230223b733a32323a2273686f775f6465736372697074696f6e5f696d616765223b733a313a2230223b733a383a226d61784c6576656c223b733a313a2231223b733a32313a2273686f775f656d7074795f63617465676f72696573223b733a313a2230223b733a31363a2273686f775f6e6f5f61727469636c6573223b733a313a2231223b733a31363a2273686f775f7375626361745f64657363223b733a313a2231223b733a32313a2273686f775f6361745f6e756d5f61727469636c6573223b733a313a2230223b733a31333a2273686f775f6361745f74616773223b733a313a2231223b733a32313a2273686f775f626173655f6465736372697074696f6e223b733a313a2231223b733a31313a226d61784c6576656c636174223b733a323a222d31223b733a32353a2273686f775f656d7074795f63617465676f726965735f636174223b733a313a2230223b733a32303a2273686f775f7375626361745f646573635f636174223b733a313a2231223b733a32353a2273686f775f6361745f6e756d5f61727469636c65735f636174223b733a313a2231223b733a32303a226e756d5f6c656164696e675f61727469636c6573223b733a313a2230223b733a31383a226e756d5f696e74726f5f61727469636c6573223b733a313a2236223b733a31313a226e756d5f636f6c756d6e73223b733a313a2233223b733a393a226e756d5f6c696e6b73223b733a313a2234223b733a31383a226d756c74695f636f6c756d6e5f6f72646572223b733a313a2230223b733a32343a2273686f775f73756263617465676f72795f636f6e74656e74223b733a323a222d31223b733a32313a2273686f775f706167696e6174696f6e5f6c696d6974223b733a313a2231223b733a31323a2266696c7465725f6669656c64223b733a343a2268696465223b733a31333a2273686f775f68656164696e6773223b733a313a2231223b733a31343a226c6973745f73686f775f64617465223b733a313a2230223b733a31313a22646174655f666f726d6174223b733a303a22223b733a31343a226c6973745f73686f775f68697473223b733a313a2231223b733a31363a226c6973745f73686f775f617574686f72223b733a313a2231223b733a31313a226f7264657262795f707269223b733a353a226f72646572223b733a31313a226f7264657262795f736563223b733a353a227264617465223b733a31303a226f726465725f64617465223b733a393a227075626c6973686564223b733a31353a2273686f775f706167696e6174696f6e223b733a313a2232223b733a32333a2273686f775f706167696e6174696f6e5f726573756c7473223b733a313a2231223b733a31333a2273686f775f6665617475726564223b733a343a2273686f77223b733a31343a2273686f775f666565645f6c696e6b223b733a313a2231223b733a31323a22666565645f73756d6d617279223b733a313a2230223b733a31383a22666565645f73686f775f726561646d6f7265223b733a313a2230223b7d733a393a22736570617261746f72223b733a313a222e223b7d733a373a226d6574616b6579223b733a303a22223b733a383a226d65746164657363223b733a303a22223b733a383a226d65746164617461223b4f3a32343a224a6f6f6d6c615c52656769737472795c5265676973747279223a323a7b733a373a22002a0064617461223b4f3a383a22737464436c617373223a343a7b733a363a22726f626f7473223b733a303a22223b733a363a22617574686f72223b733a303a22223b733a363a22726967687473223b733a303a22223b733a31303a22787265666572656e6365223b733a303a22223b7d733a393a22736570617261746f72223b733a313a222e223b7d733a373a2276657273696f6e223b733a313a2232223b733a383a226f72646572696e67223b733a313a2232223b733a383a2263617465676f7279223b733a31343a22d09dd0bed0b2d0bed181d182d0b8223b733a393a226361745f7374617465223b733a313a2231223b733a31303a226361745f616363657373223b733a313a2231223b733a343a22736c7567223b733a32383a22363a6d61736c656e697473612d762d616c656b73616e64726f766b65223b733a373a22636174736c7567223b733a393a22393a6e6f766f737469223b733a363a22617574686f72223b733a31303a2253757065722055736572223b733a363a226c61796f7574223b733a373a2261727469636c65223b733a343a2270617468223b733a33383a22392d6e6f766f7374692f362d6d61736c656e697473612d762d616c656b73616e64726f766b65223b733a31303a226d657461617574686f72223b4e3b7d733a31353a22002a00696e737472756374696f6e73223b613a353a7b693a313b613a333a7b693a303b733a353a227469746c65223b693a313b733a383a227375627469746c65223b693a323b733a323a226964223b7d693a323b613a323a7b693a303b733a373a2273756d6d617279223b693a313b733a343a22626f6479223b7d693a333b613a383a7b693a303b733a343a226d657461223b693a313b733a31303a226c6973745f7072696365223b693a323b733a31303a2273616c655f7072696365223b693a333b733a373a226d6574616b6579223b693a343b733a383a226d65746164657363223b693a353b733a31303a226d657461617574686f72223b693a363b733a363a22617574686f72223b693a373b733a31363a22637265617465645f62795f616c696173223b7d693a343b613a323a7b693a303b733a343a2270617468223b693a313b733a353a22616c696173223b7d693a353b613a313a7b693a303b733a383a22636f6d6d656e7473223b7d7d733a31313a22002a007461786f6e6f6d79223b613a343a7b733a343a2254797065223b613a313a7b733a373a2241727469636c65223b4f3a373a224a4f626a656374223a343a7b733a31303a22002a005f6572726f7273223b613a303a7b7d733a353a227469746c65223b733a373a2241727469636c65223b733a353a227374617465223b693a313b733a363a22616363657373223b693a313b7d7d733a363a22417574686f72223b613a313a7b733a31303a2253757065722055736572223b4f3a373a224a4f626a656374223a343a7b733a31303a22002a005f6572726f7273223b613a303a7b7d733a353a227469746c65223b733a31303a2253757065722055736572223b733a353a227374617465223b693a313b733a363a22616363657373223b693a313b7d7d733a383a2243617465676f7279223b613a313a7b733a31343a22d09dd0bed0b2d0bed181d182d0b8223b4f3a373a224a4f626a656374223a343a7b733a31303a22002a005f6572726f7273223b613a303a7b7d733a353a227469746c65223b733a31343a22d09dd0bed0b2d0bed181d182d0b8223b733a353a227374617465223b693a313b733a363a22616363657373223b693a313b7d7d733a383a224c616e6775616765223b613a313a7b733a313a222a223b4f3a373a224a4f626a656374223a343a7b733a31303a22002a005f6572726f7273223b613a303a7b7d733a353a227469746c65223b733a313a222a223b733a353a227374617465223b693a313b733a363a22616363657373223b693a313b7d7d7d733a333a2275726c223b733a34363a22696e6465782e7068703f6f7074696f6e3d636f6d5f636f6e74656e7426766965773d61727469636c652669643d36223b733a353a22726f757465223b733a39323a22696e6465782e7068703f6f7074696f6e3d636f6d5f636f6e74656e7426766965773d61727469636c652669643d363a6d61736c656e697473612d762d616c656b73616e64726f766b652663617469643d39264974656d69643d313031223b733a353a227469746c65223b733a34383a22d09cd0b0d181d0bbd0b5d0bdd0b8d186d0b020d0b220d090d0bbd0b5d0bad181d0b0d0bdd0b4d180d0bed0b2d0bad0b5223b733a31313a226465736372697074696f6e223b733a303a22223b733a393a227075626c6973686564223b4e3b733a353a227374617465223b693a313b733a363a22616363657373223b733a313a2231223b733a383a226c616e6775616765223b733a313a222a223b733a31383a227075626c6973685f73746172745f64617465223b733a31393a22323031352d30392d31382031323a31383a3035223b733a31363a227075626c6973685f656e645f64617465223b733a31393a22303030302d30302d30302030303a30303a3030223b733a31303a2273746172745f64617465223b733a31393a22323031352d30392d31382031323a31383a3035223b733a383a22656e645f64617465223b733a31393a22303030302d30302d30302030303a30303a3030223b733a31303a226c6973745f7072696365223b4e3b733a31303a2273616c655f7072696365223b4e3b733a373a22747970655f6964223b693a343b733a31353a2264656661756c744c616e6775616765223b733a353a2272752d5255223b7d),
(72, 'index.php?option=com_content&view=article&id=5', 'index.php?option=com_content&view=article&id=5:prazdnovanie-paskhi-khristovoj-v-aleksandrovke&catid=9&Itemid=101', 'Празднование Пасхи Христовой в Александровке', '', '2015-09-19 16:56:03', '80e8a7602206865bdccd742a1480b317', 1, 1, 1, '*', '2015-09-18 12:17:42', '0000-00-00 00:00:00', '2015-09-18 12:17:42', '0000-00-00 00:00:00', 0, 0, 4, 0x4f3a31393a2246696e646572496e6465786572526573756c74223a31393a7b733a31313a22002a00656c656d656e7473223b613a32343a7b733a323a226964223b733a313a2235223b733a353a22616c696173223b733a34363a227072617a646e6f76616e69652d7061736b68692d6b68726973746f766f6a2d762d616c656b73616e64726f766b65223b733a373a2273756d6d617279223b733a303a22223b733a343a22626f6479223b733a32333a220d0ad0bcd0bfd0b0d0bfd180d0b0d0bfd180d0b820d180223b733a353a226361746964223b733a313a2239223b733a31303a22637265617465645f6279223b733a333a22353935223b733a31363a22637265617465645f62795f616c696173223b733a303a22223b733a383a226d6f646966696564223b733a31393a22323031352d30392d31392031333a35363a3032223b733a31313a226d6f6469666965645f6279223b733a333a22353935223b733a363a22706172616d73223b4f3a32343a224a6f6f6d6c615c52656769737472795c5265676973747279223a323a7b733a373a22002a0064617461223b4f3a383a22737464436c617373223a37353a7b733a31343a2261727469636c655f6c61796f7574223b733a393a225f3a64656661756c74223b733a31303a2273686f775f7469746c65223b733a313a2231223b733a31313a226c696e6b5f7469746c6573223b733a313a2230223b733a31303a2273686f775f696e74726f223b733a313a2231223b733a31393a22696e666f5f626c6f636b5f706f736974696f6e223b733a313a2230223b733a31333a2273686f775f63617465676f7279223b733a313a2231223b733a31333a226c696e6b5f63617465676f7279223b733a313a2230223b733a32303a2273686f775f706172656e745f63617465676f7279223b733a313a2231223b733a32303a226c696e6b5f706172656e745f63617465676f7279223b733a313a2230223b733a31313a2273686f775f617574686f72223b733a313a2230223b733a31313a226c696e6b5f617574686f72223b733a313a2230223b733a31363a2273686f775f6372656174655f64617465223b733a313a2231223b733a31363a2273686f775f6d6f646966795f64617465223b733a313a2231223b733a31373a2273686f775f7075626c6973685f64617465223b733a313a2231223b733a32303a2273686f775f6974656d5f6e617669676174696f6e223b733a313a2231223b733a393a2273686f775f766f7465223b733a313a2230223b733a31333a2273686f775f726561646d6f7265223b733a313a2231223b733a31393a2273686f775f726561646d6f72655f7469746c65223b733a313a2231223b733a31343a22726561646d6f72655f6c696d6974223b733a333a22313030223b733a393a2273686f775f74616773223b733a313a2231223b733a31303a2273686f775f69636f6e73223b733a313a2231223b733a31353a2273686f775f7072696e745f69636f6e223b733a313a2230223b733a31353a2273686f775f656d61696c5f69636f6e223b733a313a2230223b733a393a2273686f775f68697473223b733a313a2230223b733a31313a2273686f775f6e6f61757468223b733a313a2230223b733a31333a2275726c735f706f736974696f6e223b733a313a2230223b733a32333a2273686f775f7075626c697368696e675f6f7074696f6e73223b733a313a2231223b733a32303a2273686f775f61727469636c655f6f7074696f6e73223b733a313a2231223b733a31323a22736176655f686973746f7279223b733a313a2231223b733a31333a22686973746f72795f6c696d6974223b693a31303b733a32353a2273686f775f75726c735f696d616765735f66726f6e74656e64223b733a313a2230223b733a32343a2273686f775f75726c735f696d616765735f6261636b656e64223b733a313a2231223b733a373a2274617267657461223b693a303b733a373a2274617267657462223b693a303b733a373a2274617267657463223b693a303b733a31313a22666c6f61745f696e74726f223b733a343a226c656674223b733a31343a22666c6f61745f66756c6c74657874223b733a343a226c656674223b733a31353a2263617465676f72795f6c61796f7574223b733a363a225f3a626c6f67223b733a33323a2273686f775f63617465676f72795f68656164696e675f7469746c655f74657874223b733a313a2230223b733a31393a2273686f775f63617465676f72795f7469746c65223b733a313a2231223b733a31363a2273686f775f6465736372697074696f6e223b733a313a2230223b733a32323a2273686f775f6465736372697074696f6e5f696d616765223b733a313a2230223b733a383a226d61784c6576656c223b733a313a2231223b733a32313a2273686f775f656d7074795f63617465676f72696573223b733a313a2230223b733a31363a2273686f775f6e6f5f61727469636c6573223b733a313a2231223b733a31363a2273686f775f7375626361745f64657363223b733a313a2231223b733a32313a2273686f775f6361745f6e756d5f61727469636c6573223b733a313a2230223b733a31333a2273686f775f6361745f74616773223b733a313a2231223b733a32313a2273686f775f626173655f6465736372697074696f6e223b733a313a2231223b733a31313a226d61784c6576656c636174223b733a323a222d31223b733a32353a2273686f775f656d7074795f63617465676f726965735f636174223b733a313a2230223b733a32303a2273686f775f7375626361745f646573635f636174223b733a313a2231223b733a32353a2273686f775f6361745f6e756d5f61727469636c65735f636174223b733a313a2231223b733a32303a226e756d5f6c656164696e675f61727469636c6573223b733a313a2230223b733a31383a226e756d5f696e74726f5f61727469636c6573223b733a313a2236223b733a31313a226e756d5f636f6c756d6e73223b733a313a2233223b733a393a226e756d5f6c696e6b73223b733a313a2234223b733a31383a226d756c74695f636f6c756d6e5f6f72646572223b733a313a2230223b733a32343a2273686f775f73756263617465676f72795f636f6e74656e74223b733a323a222d31223b733a32313a2273686f775f706167696e6174696f6e5f6c696d6974223b733a313a2231223b733a31323a2266696c7465725f6669656c64223b733a343a2268696465223b733a31333a2273686f775f68656164696e6773223b733a313a2231223b733a31343a226c6973745f73686f775f64617465223b733a313a2230223b733a31313a22646174655f666f726d6174223b733a303a22223b733a31343a226c6973745f73686f775f68697473223b733a313a2231223b733a31363a226c6973745f73686f775f617574686f72223b733a313a2231223b733a31313a226f7264657262795f707269223b733a353a226f72646572223b733a31313a226f7264657262795f736563223b733a353a227264617465223b733a31303a226f726465725f64617465223b733a393a227075626c6973686564223b733a31353a2273686f775f706167696e6174696f6e223b733a313a2232223b733a32333a2273686f775f706167696e6174696f6e5f726573756c7473223b733a313a2231223b733a31333a2273686f775f6665617475726564223b733a343a2273686f77223b733a31343a2273686f775f666565645f6c696e6b223b733a313a2231223b733a31323a22666565645f73756d6d617279223b733a313a2230223b733a31383a22666565645f73686f775f726561646d6f7265223b733a313a2230223b7d733a393a22736570617261746f72223b733a313a222e223b7d733a373a226d6574616b6579223b733a303a22223b733a383a226d65746164657363223b733a303a22223b733a383a226d65746164617461223b4f3a32343a224a6f6f6d6c615c52656769737472795c5265676973747279223a323a7b733a373a22002a0064617461223b4f3a383a22737464436c617373223a343a7b733a363a22726f626f7473223b733a303a22223b733a363a22617574686f72223b733a303a22223b733a363a22726967687473223b733a303a22223b733a31303a22787265666572656e6365223b733a303a22223b7d733a393a22736570617261746f72223b733a313a222e223b7d733a373a2276657273696f6e223b733a313a2232223b733a383a226f72646572696e67223b733a313a2233223b733a383a2263617465676f7279223b733a31343a22d09dd0bed0b2d0bed181d182d0b8223b733a393a226361745f7374617465223b733a313a2231223b733a31303a226361745f616363657373223b733a313a2231223b733a343a22736c7567223b733a34383a22353a7072617a646e6f76616e69652d7061736b68692d6b68726973746f766f6a2d762d616c656b73616e64726f766b65223b733a373a22636174736c7567223b733a393a22393a6e6f766f737469223b733a363a22617574686f72223b733a31303a2253757065722055736572223b733a363a226c61796f7574223b733a373a2261727469636c65223b733a343a2270617468223b733a35383a22392d6e6f766f7374692f352d7072617a646e6f76616e69652d7061736b68692d6b68726973746f766f6a2d762d616c656b73616e64726f766b65223b733a31303a226d657461617574686f72223b4e3b7d733a31353a22002a00696e737472756374696f6e73223b613a353a7b693a313b613a333a7b693a303b733a353a227469746c65223b693a313b733a383a227375627469746c65223b693a323b733a323a226964223b7d693a323b613a323a7b693a303b733a373a2273756d6d617279223b693a313b733a343a22626f6479223b7d693a333b613a383a7b693a303b733a343a226d657461223b693a313b733a31303a226c6973745f7072696365223b693a323b733a31303a2273616c655f7072696365223b693a333b733a373a226d6574616b6579223b693a343b733a383a226d65746164657363223b693a353b733a31303a226d657461617574686f72223b693a363b733a363a22617574686f72223b693a373b733a31363a22637265617465645f62795f616c696173223b7d693a343b613a323a7b693a303b733a343a2270617468223b693a313b733a353a22616c696173223b7d693a353b613a313a7b693a303b733a383a22636f6d6d656e7473223b7d7d733a31313a22002a007461786f6e6f6d79223b613a343a7b733a343a2254797065223b613a313a7b733a373a2241727469636c65223b4f3a373a224a4f626a656374223a343a7b733a31303a22002a005f6572726f7273223b613a303a7b7d733a353a227469746c65223b733a373a2241727469636c65223b733a353a227374617465223b693a313b733a363a22616363657373223b693a313b7d7d733a363a22417574686f72223b613a313a7b733a31303a2253757065722055736572223b4f3a373a224a4f626a656374223a343a7b733a31303a22002a005f6572726f7273223b613a303a7b7d733a353a227469746c65223b733a31303a2253757065722055736572223b733a353a227374617465223b693a313b733a363a22616363657373223b693a313b7d7d733a383a2243617465676f7279223b613a313a7b733a31343a22d09dd0bed0b2d0bed181d182d0b8223b4f3a373a224a4f626a656374223a343a7b733a31303a22002a005f6572726f7273223b613a303a7b7d733a353a227469746c65223b733a31343a22d09dd0bed0b2d0bed181d182d0b8223b733a353a227374617465223b693a313b733a363a22616363657373223b693a313b7d7d733a383a224c616e6775616765223b613a313a7b733a313a222a223b4f3a373a224a4f626a656374223a343a7b733a31303a22002a005f6572726f7273223b613a303a7b7d733a353a227469746c65223b733a313a222a223b733a353a227374617465223b693a313b733a363a22616363657373223b693a313b7d7d7d733a333a2275726c223b733a34363a22696e6465782e7068703f6f7074696f6e3d636f6d5f636f6e74656e7426766965773d61727469636c652669643d35223b733a353a22726f757465223b733a3131323a22696e6465782e7068703f6f7074696f6e3d636f6d5f636f6e74656e7426766965773d61727469636c652669643d353a7072617a646e6f76616e69652d7061736b68692d6b68726973746f766f6a2d762d616c656b73616e64726f766b652663617469643d39264974656d69643d313031223b733a353a227469746c65223b733a38343a22d09fd180d0b0d0b7d0b4d0bdd0bed0b2d0b0d0bdd0b8d0b520d09fd0b0d181d185d0b820d0a5d180d0b8d181d182d0bed0b2d0bed0b920d0b220d090d0bbd0b5d0bad181d0b0d0bdd0b4d180d0bed0b2d0bad0b5223b733a31313a226465736372697074696f6e223b733a303a22223b733a393a227075626c6973686564223b4e3b733a353a227374617465223b693a313b733a363a22616363657373223b733a313a2231223b733a383a226c616e6775616765223b733a313a222a223b733a31383a227075626c6973685f73746172745f64617465223b733a31393a22323031352d30392d31382031323a31373a3432223b733a31363a227075626c6973685f656e645f64617465223b733a31393a22303030302d30302d30302030303a30303a3030223b733a31303a2273746172745f64617465223b733a31393a22323031352d30392d31382031323a31373a3432223b733a383a22656e645f64617465223b733a31393a22303030302d30302d30302030303a30303a3030223b733a31303a226c6973745f7072696365223b4e3b733a31303a2273616c655f7072696365223b4e3b733a373a22747970655f6964223b693a343b733a31353a2264656661756c744c616e6775616765223b733a353a2272752d5255223b7d),
(73, 'index.php?option=com_content&view=article&id=4', 'index.php?option=com_content&view=article&id=4:vody-tekushchie-v-zhizn-vechnuyu&catid=9&Itemid=101', '"Воды, текущие в жизнь вечную"', '', '2015-09-19 16:56:19', 'd57a86b43a3fe94c9f6e3a4ab1b72a23', 1, 1, 1, '*', '2015-09-18 12:16:59', '0000-00-00 00:00:00', '2015-09-18 12:16:59', '0000-00-00 00:00:00', 0, 0, 4, 0x4f3a31393a2246696e646572496e6465786572526573756c74223a31393a7b733a31313a22002a00656c656d656e7473223b613a32343a7b733a323a226964223b733a313a2234223b733a353a22616c696173223b733a33323a22766f64792d74656b757368636869652d762d7a68697a6e2d766563686e757975223b733a373a2273756d6d617279223b733a303a22223b733a343a22626f6479223b733a31363a220d0ad0bbd0bcd0bfd0b0d0bfd180d0b8223b733a353a226361746964223b733a313a2239223b733a31303a22637265617465645f6279223b733a333a22353935223b733a31363a22637265617465645f62795f616c696173223b733a303a22223b733a383a226d6f646966696564223b733a31393a22323031352d30392d31392031333a35363a3138223b733a31313a226d6f6469666965645f6279223b733a333a22353935223b733a363a22706172616d73223b4f3a32343a224a6f6f6d6c615c52656769737472795c5265676973747279223a323a7b733a373a22002a0064617461223b4f3a383a22737464436c617373223a37353a7b733a31343a2261727469636c655f6c61796f7574223b733a393a225f3a64656661756c74223b733a31303a2273686f775f7469746c65223b733a313a2231223b733a31313a226c696e6b5f7469746c6573223b733a313a2230223b733a31303a2273686f775f696e74726f223b733a313a2231223b733a31393a22696e666f5f626c6f636b5f706f736974696f6e223b733a313a2230223b733a31333a2273686f775f63617465676f7279223b733a313a2231223b733a31333a226c696e6b5f63617465676f7279223b733a313a2230223b733a32303a2273686f775f706172656e745f63617465676f7279223b733a313a2231223b733a32303a226c696e6b5f706172656e745f63617465676f7279223b733a313a2230223b733a31313a2273686f775f617574686f72223b733a313a2230223b733a31313a226c696e6b5f617574686f72223b733a313a2230223b733a31363a2273686f775f6372656174655f64617465223b733a313a2231223b733a31363a2273686f775f6d6f646966795f64617465223b733a313a2231223b733a31373a2273686f775f7075626c6973685f64617465223b733a313a2231223b733a32303a2273686f775f6974656d5f6e617669676174696f6e223b733a313a2231223b733a393a2273686f775f766f7465223b733a313a2230223b733a31333a2273686f775f726561646d6f7265223b733a313a2231223b733a31393a2273686f775f726561646d6f72655f7469746c65223b733a313a2231223b733a31343a22726561646d6f72655f6c696d6974223b733a333a22313030223b733a393a2273686f775f74616773223b733a313a2231223b733a31303a2273686f775f69636f6e73223b733a313a2231223b733a31353a2273686f775f7072696e745f69636f6e223b733a313a2230223b733a31353a2273686f775f656d61696c5f69636f6e223b733a313a2230223b733a393a2273686f775f68697473223b733a313a2230223b733a31313a2273686f775f6e6f61757468223b733a313a2230223b733a31333a2275726c735f706f736974696f6e223b733a313a2230223b733a32333a2273686f775f7075626c697368696e675f6f7074696f6e73223b733a313a2231223b733a32303a2273686f775f61727469636c655f6f7074696f6e73223b733a313a2231223b733a31323a22736176655f686973746f7279223b733a313a2231223b733a31333a22686973746f72795f6c696d6974223b693a31303b733a32353a2273686f775f75726c735f696d616765735f66726f6e74656e64223b733a313a2230223b733a32343a2273686f775f75726c735f696d616765735f6261636b656e64223b733a313a2231223b733a373a2274617267657461223b693a303b733a373a2274617267657462223b693a303b733a373a2274617267657463223b693a303b733a31313a22666c6f61745f696e74726f223b733a343a226c656674223b733a31343a22666c6f61745f66756c6c74657874223b733a343a226c656674223b733a31353a2263617465676f72795f6c61796f7574223b733a363a225f3a626c6f67223b733a33323a2273686f775f63617465676f72795f68656164696e675f7469746c655f74657874223b733a313a2230223b733a31393a2273686f775f63617465676f72795f7469746c65223b733a313a2231223b733a31363a2273686f775f6465736372697074696f6e223b733a313a2230223b733a32323a2273686f775f6465736372697074696f6e5f696d616765223b733a313a2230223b733a383a226d61784c6576656c223b733a313a2231223b733a32313a2273686f775f656d7074795f63617465676f72696573223b733a313a2230223b733a31363a2273686f775f6e6f5f61727469636c6573223b733a313a2231223b733a31363a2273686f775f7375626361745f64657363223b733a313a2231223b733a32313a2273686f775f6361745f6e756d5f61727469636c6573223b733a313a2230223b733a31333a2273686f775f6361745f74616773223b733a313a2231223b733a32313a2273686f775f626173655f6465736372697074696f6e223b733a313a2231223b733a31313a226d61784c6576656c636174223b733a323a222d31223b733a32353a2273686f775f656d7074795f63617465676f726965735f636174223b733a313a2230223b733a32303a2273686f775f7375626361745f646573635f636174223b733a313a2231223b733a32353a2273686f775f6361745f6e756d5f61727469636c65735f636174223b733a313a2231223b733a32303a226e756d5f6c656164696e675f61727469636c6573223b733a313a2230223b733a31383a226e756d5f696e74726f5f61727469636c6573223b733a313a2236223b733a31313a226e756d5f636f6c756d6e73223b733a313a2233223b733a393a226e756d5f6c696e6b73223b733a313a2234223b733a31383a226d756c74695f636f6c756d6e5f6f72646572223b733a313a2230223b733a32343a2273686f775f73756263617465676f72795f636f6e74656e74223b733a323a222d31223b733a32313a2273686f775f706167696e6174696f6e5f6c696d6974223b733a313a2231223b733a31323a2266696c7465725f6669656c64223b733a343a2268696465223b733a31333a2273686f775f68656164696e6773223b733a313a2231223b733a31343a226c6973745f73686f775f64617465223b733a313a2230223b733a31313a22646174655f666f726d6174223b733a303a22223b733a31343a226c6973745f73686f775f68697473223b733a313a2231223b733a31363a226c6973745f73686f775f617574686f72223b733a313a2231223b733a31313a226f7264657262795f707269223b733a353a226f72646572223b733a31313a226f7264657262795f736563223b733a353a227264617465223b733a31303a226f726465725f64617465223b733a393a227075626c6973686564223b733a31353a2273686f775f706167696e6174696f6e223b733a313a2232223b733a32333a2273686f775f706167696e6174696f6e5f726573756c7473223b733a313a2231223b733a31333a2273686f775f6665617475726564223b733a343a2273686f77223b733a31343a2273686f775f666565645f6c696e6b223b733a313a2231223b733a31323a22666565645f73756d6d617279223b733a313a2230223b733a31383a22666565645f73686f775f726561646d6f7265223b733a313a2230223b7d733a393a22736570617261746f72223b733a313a222e223b7d733a373a226d6574616b6579223b733a303a22223b733a383a226d65746164657363223b733a303a22223b733a383a226d65746164617461223b4f3a32343a224a6f6f6d6c615c52656769737472795c5265676973747279223a323a7b733a373a22002a0064617461223b4f3a383a22737464436c617373223a343a7b733a363a22726f626f7473223b733a303a22223b733a363a22617574686f72223b733a303a22223b733a363a22726967687473223b733a303a22223b733a31303a22787265666572656e6365223b733a303a22223b7d733a393a22736570617261746f72223b733a313a222e223b7d733a373a2276657273696f6e223b733a313a2238223b733a383a226f72646572696e67223b733a313a2234223b733a383a2263617465676f7279223b733a31343a22d09dd0bed0b2d0bed181d182d0b8223b733a393a226361745f7374617465223b733a313a2231223b733a31303a226361745f616363657373223b733a313a2231223b733a343a22736c7567223b733a33343a22343a766f64792d74656b757368636869652d762d7a68697a6e2d766563686e757975223b733a373a22636174736c7567223b733a393a22393a6e6f766f737469223b733a363a22617574686f72223b733a31303a2253757065722055736572223b733a363a226c61796f7574223b733a373a2261727469636c65223b733a343a2270617468223b733a34343a22392d6e6f766f7374692f342d766f64792d74656b757368636869652d762d7a68697a6e2d766563686e757975223b733a31303a226d657461617574686f72223b4e3b7d733a31353a22002a00696e737472756374696f6e73223b613a353a7b693a313b613a333a7b693a303b733a353a227469746c65223b693a313b733a383a227375627469746c65223b693a323b733a323a226964223b7d693a323b613a323a7b693a303b733a373a2273756d6d617279223b693a313b733a343a22626f6479223b7d693a333b613a383a7b693a303b733a343a226d657461223b693a313b733a31303a226c6973745f7072696365223b693a323b733a31303a2273616c655f7072696365223b693a333b733a373a226d6574616b6579223b693a343b733a383a226d65746164657363223b693a353b733a31303a226d657461617574686f72223b693a363b733a363a22617574686f72223b693a373b733a31363a22637265617465645f62795f616c696173223b7d693a343b613a323a7b693a303b733a343a2270617468223b693a313b733a353a22616c696173223b7d693a353b613a313a7b693a303b733a383a22636f6d6d656e7473223b7d7d733a31313a22002a007461786f6e6f6d79223b613a343a7b733a343a2254797065223b613a313a7b733a373a2241727469636c65223b4f3a373a224a4f626a656374223a343a7b733a31303a22002a005f6572726f7273223b613a303a7b7d733a353a227469746c65223b733a373a2241727469636c65223b733a353a227374617465223b693a313b733a363a22616363657373223b693a313b7d7d733a363a22417574686f72223b613a313a7b733a31303a2253757065722055736572223b4f3a373a224a4f626a656374223a343a7b733a31303a22002a005f6572726f7273223b613a303a7b7d733a353a227469746c65223b733a31303a2253757065722055736572223b733a353a227374617465223b693a313b733a363a22616363657373223b693a313b7d7d733a383a2243617465676f7279223b613a313a7b733a31343a22d09dd0bed0b2d0bed181d182d0b8223b4f3a373a224a4f626a656374223a343a7b733a31303a22002a005f6572726f7273223b613a303a7b7d733a353a227469746c65223b733a31343a22d09dd0bed0b2d0bed181d182d0b8223b733a353a227374617465223b693a313b733a363a22616363657373223b693a313b7d7d733a383a224c616e6775616765223b613a313a7b733a313a222a223b4f3a373a224a4f626a656374223a343a7b733a31303a22002a005f6572726f7273223b613a303a7b7d733a353a227469746c65223b733a313a222a223b733a353a227374617465223b693a313b733a363a22616363657373223b693a313b7d7d7d733a333a2275726c223b733a34363a22696e6465782e7068703f6f7074696f6e3d636f6d5f636f6e74656e7426766965773d61727469636c652669643d34223b733a353a22726f757465223b733a39383a22696e6465782e7068703f6f7074696f6e3d636f6d5f636f6e74656e7426766965773d61727469636c652669643d343a766f64792d74656b757368636869652d762d7a68697a6e2d766563686e7579752663617469643d39264974656d69643d313031223b733a353a227469746c65223b733a35333a2222d092d0bed0b4d18b2c20d182d0b5d0bad183d189d0b8d0b520d0b220d0b6d0b8d0b7d0bdd18c20d0b2d0b5d187d0bdd183d18e22223b733a31313a226465736372697074696f6e223b733a303a22223b733a393a227075626c6973686564223b4e3b733a353a227374617465223b693a313b733a363a22616363657373223b733a313a2231223b733a383a226c616e6775616765223b733a313a222a223b733a31383a227075626c6973685f73746172745f64617465223b733a31393a22323031352d30392d31382031323a31363a3539223b733a31363a227075626c6973685f656e645f64617465223b733a31393a22303030302d30302d30302030303a30303a3030223b733a31303a2273746172745f64617465223b733a31393a22323031352d30392d31382031323a31363a3539223b733a383a22656e645f64617465223b733a31393a22303030302d30302d30302030303a30303a3030223b733a31303a226c6973745f7072696365223b4e3b733a31303a2273616c655f7072696365223b4e3b733a373a22747970655f6964223b693a343b733a31353a2264656661756c744c616e6775616765223b733a353a2272752d5255223b7d);

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_finder_links_terms0`
--

CREATE TABLE IF NOT EXISTS `yawnc_finder_links_terms0` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yawnc_finder_links_terms0`
--

INSERT INTO `yawnc_finder_links_terms0` (`link_id`, `term_id`, `weight`) VALUES
(6, 60, 0.56004),
(19, 60, 0.56004),
(20, 60, 0.56004),
(21, 60, 0.56004),
(69, 60, 0.56004),
(70, 60, 0.56004),
(71, 60, 0.56004),
(72, 60, 0.56004),
(73, 60, 0.56004),
(6, 64, 0.79992),
(19, 64, 0.79992),
(20, 64, 0.79992),
(21, 64, 0.79992),
(69, 64, 0.79992),
(70, 64, 0.79992),
(71, 64, 0.79992),
(72, 64, 0.79992),
(73, 64, 0.79992),
(6, 65, 3.19992),
(19, 65, 3.19992),
(20, 65, 3.19992),
(21, 65, 3.19992),
(69, 65, 3.19992),
(70, 65, 3.19992),
(71, 65, 3.19992),
(72, 65, 3.19992),
(73, 65, 3.19992),
(20, 270, 1.6),
(20, 271, 6),
(71, 350, 3.4668),
(72, 350, 3.4668),
(69, 473, 2.6668),
(70, 473, 2.6668),
(69, 474, 6.5332),
(70, 474, 6.5332),
(69, 475, 7.8668),
(70, 475, 7.8668);

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_finder_links_terms1`
--

CREATE TABLE IF NOT EXISTS `yawnc_finder_links_terms1` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yawnc_finder_links_terms1`
--

INSERT INTO `yawnc_finder_links_terms1` (`link_id`, `term_id`, `weight`) VALUES
(71, 1091, 0.37),
(71, 1092, 2.8),
(71, 1093, 2.9334),
(73, 1161, 0.32669);

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_finder_links_terms2`
--

CREATE TABLE IF NOT EXISTS `yawnc_finder_links_terms2` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yawnc_finder_links_terms2`
--

INSERT INTO `yawnc_finder_links_terms2` (`link_id`, `term_id`, `weight`) VALUES
(69, 481, 0.90661),
(70, 481, 0.90661),
(69, 482, 2.26661),
(70, 482, 2.26661),
(69, 483, 2.77661),
(70, 483, 2.77661),
(69, 493, 1.02),
(70, 493, 1.02),
(69, 494, 2.72),
(70, 494, 2.72),
(69, 495, 2.83339),
(70, 495, 2.83339);

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_finder_links_terms3`
--

CREATE TABLE IF NOT EXISTS `yawnc_finder_links_terms3` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_finder_links_terms4`
--

CREATE TABLE IF NOT EXISTS `yawnc_finder_links_terms4` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yawnc_finder_links_terms4`
--

INSERT INTO `yawnc_finder_links_terms4` (`link_id`, `term_id`, `weight`) VALUES
(1, 4, 0.63996),
(2, 4, 0.63996),
(5, 4, 0.63996),
(3, 4, 1.70656),
(4, 4, 1.70656),
(3, 13, 2.6666),
(3, 14, 3.6),
(3, 15, 1.2),
(4, 15, 1.2),
(3, 16, 3.1334),
(3, 17, 3.7334),
(3, 18, 0.9334),
(3, 19, 3.0666),
(3, 20, 3.2),
(4, 43, 2.6666),
(4, 44, 3.6),
(4, 45, 3.2666),
(4, 46, 3.8666),
(6, 72, 0.56661),
(19, 72, 0.56661),
(6, 73, 2.49339),
(19, 73, 2.49339),
(21, 188, 0.79339),
(69, 286, 0.2),
(70, 286, 0.2),
(71, 286, 0.2),
(72, 286, 0.2),
(73, 286, 0.2),
(69, 287, 2.6),
(70, 287, 2.6),
(71, 287, 2.6),
(72, 287, 2.6),
(73, 287, 2.6),
(69, 484, 0.11339),
(70, 484, 0.11339),
(69, 485, 2.26661),
(70, 485, 2.26661),
(69, 486, 2.66339),
(70, 486, 2.66339),
(69, 1071, 2.7334),
(70, 1086, 2.7334),
(71, 1094, 2.7334),
(72, 1108, 2.7334),
(73, 1139, 2.7334);

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_finder_links_terms5`
--

CREATE TABLE IF NOT EXISTS `yawnc_finder_links_terms5` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yawnc_finder_links_terms5`
--

INSERT INTO `yawnc_finder_links_terms5` (`link_id`, `term_id`, `weight`) VALUES
(21, 189, 0.56661),
(21, 190, 2.60661),
(21, 191, 3.06),
(73, 1162, 0.79339),
(73, 1163, 2.21),
(73, 1164, 2.55);

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_finder_links_terms6`
--

CREATE TABLE IF NOT EXISTS `yawnc_finder_links_terms6` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yawnc_finder_links_terms6`
--

INSERT INTO `yawnc_finder_links_terms6` (`link_id`, `term_id`, `weight`) VALUES
(21, 180, 1.8668),
(21, 181, 5.3332),
(21, 182, 6.4),
(69, 468, 1.6),
(70, 468, 1.6),
(69, 1073, 5.0668),
(71, 1095, 2.6668),
(71, 1096, 5.6),
(71, 1097, 7.4668);

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_finder_links_terms7`
--

CREATE TABLE IF NOT EXISTS `yawnc_finder_links_terms7` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yawnc_finder_links_terms7`
--

INSERT INTO `yawnc_finder_links_terms7` (`link_id`, `term_id`, `weight`) VALUES
(19, 5, 1.04004),
(20, 5, 1.04004),
(21, 5, 1.04004),
(6, 5, 2.77344),
(2, 5, 3.20679),
(5, 5, 3.20679),
(1, 5, 4.94019),
(3, 5, 4.94019),
(4, 5, 4.94019),
(3, 23, 3.3334),
(4, 23, 3.3334),
(3, 24, 3.6),
(4, 24, 3.6),
(4, 47, 1.2),
(4, 48, 3.2),
(4, 49, 3.3334),
(6, 66, 3),
(6, 67, 3.4),
(6, 68, 0.64008),
(19, 68, 0.64008),
(20, 68, 0.64008),
(21, 68, 0.64008),
(69, 68, 0.64008),
(70, 68, 0.64008),
(71, 68, 0.64008),
(72, 68, 0.64008),
(73, 68, 0.64008),
(21, 183, 0.5332),
(21, 184, 5.3332),
(21, 198, 1.13339),
(21, 199, 2.72),
(21, 200, 2.89),
(21, 201, 0.7),
(20, 274, 0.7),
(69, 289, 0.9334),
(70, 289, 0.9334),
(71, 289, 0.9334),
(72, 289, 0.9334),
(73, 289, 0.9334),
(69, 469, 2.1332),
(70, 469, 2.1332),
(69, 470, 6),
(70, 470, 6),
(69, 1074, 6.2668),
(69, 1075, 2.6),
(69, 1076, 2.9334),
(70, 1087, 2.6),
(70, 1088, 2.9334),
(71, 1098, 2.6),
(71, 1099, 3.3334),
(72, 1112, 2.6),
(72, 1113, 3.4666),
(72, 1122, 0.56661),
(72, 1123, 2.55),
(72, 1124, 2.66339),
(72, 1125, 1.36),
(72, 1126, 2.72),
(72, 1127, 3.28661),
(73, 1140, 2.6),
(73, 1141, 2.9334);

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_finder_links_terms8`
--

CREATE TABLE IF NOT EXISTS `yawnc_finder_links_terms8` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yawnc_finder_links_terms8`
--

INSERT INTO `yawnc_finder_links_terms8` (`link_id`, `term_id`, `weight`) VALUES
(3, 21, 0.8),
(4, 21, 0.8),
(3, 22, 2.6666),
(4, 22, 2.6666),
(5, 55, 0.17),
(70, 55, 0.37),
(6, 61, 1.3332),
(19, 61, 1.3332),
(6, 62, 6),
(19, 62, 6),
(6, 63, 2.4),
(19, 63, 2.4),
(21, 174, 1.3332),
(21, 175, 6.2668),
(21, 176, 7.3332),
(21, 185, 2.9332),
(21, 186, 6.5332),
(21, 187, 6.9332),
(19, 263, 0.2666),
(20, 263, 0.2666),
(21, 263, 0.2666),
(19, 264, 2.2666),
(19, 265, 2.6666),
(20, 268, 2.2666),
(20, 269, 2.7334),
(21, 281, 2.2666),
(21, 282, 2.6666),
(69, 459, 2.4),
(70, 459, 2.4),
(69, 460, 5.4668),
(70, 460, 5.4668),
(69, 461, 6.6668),
(70, 461, 6.6668),
(69, 462, 2.1332),
(70, 462, 2.1332),
(69, 463, 6.4),
(70, 463, 6.4),
(69, 464, 6.6668),
(70, 464, 6.6668),
(69, 465, 0.2668),
(70, 465, 0.2668),
(69, 466, 5.3332),
(70, 466, 5.3332),
(69, 467, 6.2668),
(70, 467, 6.2668),
(70, 1084, 2.4),
(70, 1085, 3.1334),
(72, 1109, 2.6668),
(72, 1110, 5.6),
(72, 1111, 7.4668),
(72, 1114, 1.6),
(72, 1115, 6.2668),
(72, 1116, 6.5332),
(72, 1117, 3.2),
(72, 1118, 6.5332),
(72, 1119, 8);

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_finder_links_terms9`
--

CREATE TABLE IF NOT EXISTS `yawnc_finder_links_terms9` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yawnc_finder_links_terms9`
--

INSERT INTO `yawnc_finder_links_terms9` (`link_id`, `term_id`, `weight`) VALUES
(21, 173, 1.8668),
(20, 272, 2.1332),
(20, 275, 0.56661),
(20, 276, 2.49339),
(71, 295, 0.2668),
(72, 295, 0.2668),
(73, 295, 0.2668),
(71, 362, 6),
(72, 362, 6),
(69, 476, 1.52019),
(70, 476, 1.52019),
(69, 477, 6),
(70, 477, 6),
(69, 478, 7.2),
(70, 478, 7.2),
(69, 479, 2.55),
(70, 479, 2.55),
(69, 480, 3.11661),
(70, 480, 3.11661),
(69, 490, 1.13339),
(70, 490, 1.13339),
(69, 491, 2.83339),
(70, 491, 2.83339),
(69, 492, 3.34339),
(70, 492, 3.34339),
(71, 1103, 0.46669),
(73, 1145, 4.9332),
(73, 1146, 6.1332),
(73, 1147, 2.1332),
(73, 1148, 1.0668),
(73, 1149, 6),
(73, 1150, 6.2668);

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_finder_links_termsa`
--

CREATE TABLE IF NOT EXISTS `yawnc_finder_links_termsa` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yawnc_finder_links_termsa`
--

INSERT INTO `yawnc_finder_links_termsa` (`link_id`, `term_id`, `weight`) VALUES
(3, 10, 0.37),
(73, 10, 0.37),
(3, 11, 3),
(3, 12, 3.4666),
(21, 193, 0.79339),
(21, 194, 2.26661),
(21, 195, 2.72),
(69, 487, 0.68),
(70, 487, 0.68),
(71, 1100, 1.02),
(71, 1101, 2.32339),
(71, 1102, 3.11661),
(72, 1120, 0.42),
(72, 1121, 0.95669),
(73, 1137, 2.4),
(73, 1138, 3.1334);

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_finder_links_termsb`
--

CREATE TABLE IF NOT EXISTS `yawnc_finder_links_termsb` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yawnc_finder_links_termsb`
--

INSERT INTO `yawnc_finder_links_termsb` (`link_id`, `term_id`, `weight`) VALUES
(6, 70, 0.7),
(19, 70, 0.7),
(6, 71, 1.4),
(19, 71, 1.4),
(6, 74, 0.90661),
(19, 74, 0.90661),
(20, 273, 0.90661),
(71, 304, 0.11339),
(72, 304, 0.11339),
(73, 304, 0.11339),
(71, 364, 2.55),
(72, 364, 2.55),
(69, 1072, 0.60669),
(72, 1129, 1.02),
(72, 1130, 2.32339),
(72, 1131, 3.11661),
(73, 1153, 2.09661),
(73, 1154, 2.49339),
(73, 1155, 0.68),
(73, 1156, 0.45339),
(73, 1157, 2.38),
(73, 1158, 2.49339);

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_finder_links_termsc`
--

CREATE TABLE IF NOT EXISTS `yawnc_finder_links_termsc` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yawnc_finder_links_termsc`
--

INSERT INTO `yawnc_finder_links_termsc` (`link_id`, `term_id`, `weight`) VALUES
(6, 2, 0.2),
(1, 2, 0.37),
(20, 2, 0.37),
(69, 2, 0.4),
(1, 3, 3),
(6, 3, 3),
(3, 9, 0.6),
(4, 9, 0.6),
(6, 56, 0.37),
(19, 56, 0.37),
(6, 57, 2.4666),
(19, 57, 2.4666),
(6, 58, 3.1334),
(19, 58, 3.1334),
(6, 59, 3.1334),
(6, 69, 0.7),
(19, 69, 0.7),
(69, 170, 0.37),
(20, 266, 2.5334),
(20, 267, 3.1334),
(71, 363, 1.47339),
(72, 363, 1.47339),
(69, 1069, 2.4),
(69, 1070, 3.1334);

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_finder_links_termsd`
--

CREATE TABLE IF NOT EXISTS `yawnc_finder_links_termsd` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yawnc_finder_links_termsd`
--

INSERT INTO `yawnc_finder_links_termsd` (`link_id`, `term_id`, `weight`) VALUES
(1, 1, 0),
(2, 1, 0),
(3, 1, 0),
(4, 1, 0),
(5, 1, 0),
(6, 1, 0),
(19, 1, 0),
(20, 1, 0),
(21, 1, 0),
(69, 1, 0),
(70, 1, 0),
(71, 1, 0),
(72, 1, 0),
(73, 1, 0),
(21, 196, 0.22661),
(21, 197, 2.26661),
(69, 313, 0.56004),
(70, 313, 0.56004),
(71, 313, 0.56004),
(72, 313, 0.56004),
(73, 313, 0.56004),
(69, 488, 0.90661),
(70, 488, 0.90661),
(69, 489, 2.55),
(70, 489, 2.55),
(73, 1159, 0.56661),
(73, 1160, 2.38);

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_finder_links_termse`
--

CREATE TABLE IF NOT EXISTS `yawnc_finder_links_termse` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yawnc_finder_links_termse`
--

INSERT INTO `yawnc_finder_links_termse` (`link_id`, `term_id`, `weight`) VALUES
(2, 8, 0.17),
(4, 40, 0.37),
(72, 40, 0.37),
(4, 41, 3),
(4, 42, 3.4666),
(21, 167, 0.37),
(21, 168, 2.4666),
(21, 169, 3.2666),
(70, 1089, 0.42),
(72, 1106, 2.9334),
(72, 1107, 3.4),
(72, 1128, 0.04669),
(73, 1142, 2.6668),
(73, 1143, 5.6),
(73, 1144, 6.4);

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_finder_links_termsf`
--

CREATE TABLE IF NOT EXISTS `yawnc_finder_links_termsf` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yawnc_finder_links_termsf`
--

INSERT INTO `yawnc_finder_links_termsf` (`link_id`, `term_id`, `weight`) VALUES
(73, 1151, 1.3332),
(73, 1152, 5.8668);

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_finder_taxonomy`
--

CREATE TABLE IF NOT EXISTS `yawnc_finder_taxonomy` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `state` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `access` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ordering` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  KEY `state` (`state`),
  KEY `ordering` (`ordering`),
  KEY `access` (`access`),
  KEY `idx_parent_published` (`parent_id`,`state`,`access`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Дамп данных таблицы `yawnc_finder_taxonomy`
--

INSERT INTO `yawnc_finder_taxonomy` (`id`, `parent_id`, `title`, `state`, `access`, `ordering`) VALUES
(1, 0, 'ROOT', 0, 0, 0),
(2, 1, 'Type', 1, 1, 0),
(3, 2, 'Category', 1, 1, 0),
(4, 1, 'Language', 1, 1, 0),
(5, 4, '*', 1, 1, 0),
(6, 2, 'Article', 1, 1, 0),
(7, 1, 'Author', 1, 1, 0),
(8, 7, 'Super User', 1, 1, 0),
(9, 1, 'Category', 1, 1, 0),
(10, 9, 'Uncategorised', 0, 1, 0),
(11, 9, 'Новости', 1, 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_finder_taxonomy_map`
--

CREATE TABLE IF NOT EXISTS `yawnc_finder_taxonomy_map` (
  `link_id` int(10) unsigned NOT NULL,
  `node_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`node_id`),
  KEY `link_id` (`link_id`),
  KEY `node_id` (`node_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yawnc_finder_taxonomy_map`
--

INSERT INTO `yawnc_finder_taxonomy_map` (`link_id`, `node_id`) VALUES
(1, 3),
(1, 5),
(2, 3),
(2, 5),
(3, 3),
(3, 5),
(4, 3),
(4, 5),
(5, 3),
(5, 5),
(6, 5),
(6, 6),
(6, 8),
(6, 10),
(19, 5),
(19, 6),
(19, 8),
(19, 10),
(20, 5),
(20, 6),
(20, 8),
(20, 10),
(21, 5),
(21, 6),
(21, 8),
(21, 10),
(69, 5),
(69, 6),
(69, 8),
(69, 11),
(70, 5),
(70, 6),
(70, 8),
(70, 11),
(71, 5),
(71, 6),
(71, 8),
(71, 11),
(72, 5),
(72, 6),
(72, 8),
(72, 11),
(73, 5),
(73, 6),
(73, 8),
(73, 11);

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_finder_terms`
--

CREATE TABLE IF NOT EXISTS `yawnc_finder_terms` (
  `term_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `phrase` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `weight` float unsigned NOT NULL DEFAULT '0',
  `soundex` varchar(75) NOT NULL,
  `links` int(10) NOT NULL DEFAULT '0',
  `language` char(3) NOT NULL DEFAULT '',
  PRIMARY KEY (`term_id`),
  UNIQUE KEY `idx_term` (`term`),
  KEY `idx_term_phrase` (`term`,`phrase`),
  KEY `idx_stem_phrase` (`stem`,`phrase`),
  KEY `idx_soundex_phrase` (`soundex`,`phrase`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1165 ;

--
-- Дамп данных таблицы `yawnc_finder_terms`
--

INSERT INTO `yawnc_finder_terms` (`term_id`, `term`, `stem`, `common`, `phrase`, `weight`, `soundex`, `links`, `language`) VALUES
(1, '', '', 0, 0, 0, '', 14, '*'),
(2, '2', '2', 0, 0, 0.1, '', 3, '*'),
(3, '2 uncategorised', '2 uncategorised', 0, 1, 1.5, 'U5232623', 1, '*'),
(4, 'category', 'category', 0, 0, 0.5333, 'C326', 5, '*'),
(5, 'uncategorised', 'uncategorised', 0, 0, 0.8667, 'U5232623', 9, '*'),
(9, '101', '101', 0, 0, 0.3, '', 2, '*'),
(10, '4', '4', 0, 0, 0.1, '', 2, '*'),
(11, '4 uncategorised', '4 uncategorised', 0, 1, 1.5, 'U5232623', 1, '*'),
(12, '4 uncategorised itemid', '4 uncategorised itemid', 0, 1, 1.7333, 'U523262353', 1, '*'),
(13, 'category 4', 'category 4', 0, 1, 1.3333, 'C326', 1, '*'),
(14, 'category 4 uncategorised', 'category 4 uncategorised', 0, 1, 1.8, 'C3265232623', 1, '*'),
(15, 'component', 'component', 0, 0, 0.6, 'C5153', 2, '*'),
(16, 'component contact', 'component contact', 0, 1, 1.5667, 'C515325323', 1, '*'),
(17, 'component contact category', 'component contact category', 0, 1, 1.8667, 'C5153253232326', 1, '*'),
(18, 'contact', 'contact', 0, 0, 0.4667, 'C5323', 1, '*'),
(19, 'contact category', 'contact category', 0, 1, 1.5333, 'C53232326', 1, '*'),
(20, 'contact category 4', 'contact category 4', 0, 1, 1.6, 'C53232326', 1, '*'),
(21, 'itemid', 'itemid', 0, 0, 0.4, 'I353', 2, '*'),
(22, 'itemid 101', 'itemid 101', 0, 1, 1.3333, 'I353', 2, '*'),
(23, 'uncategorised itemid', 'uncategorised itemid', 0, 1, 1.6667, 'U523262353', 2, '*'),
(24, 'uncategorised itemid 101', 'uncategorised itemid 101', 0, 1, 1.8, 'U523262353', 2, '*'),
(40, '5', '5', 0, 0, 0.1, '', 2, '*'),
(41, '5 uncategorised', '5 uncategorised', 0, 1, 1.5, 'U5232623', 1, '*'),
(42, '5 uncategorised itemid', '5 uncategorised itemid', 0, 1, 1.7333, 'U523262353', 1, '*'),
(43, 'category 5', 'category 5', 0, 1, 1.3333, 'C326', 1, '*'),
(44, 'category 5 uncategorised', 'category 5 uncategorised', 0, 1, 1.8, 'C3265232623', 1, '*'),
(45, 'component newsfeeds', 'component newsfeeds', 0, 1, 1.6333, 'C515352132', 1, '*'),
(46, 'component newsfeeds category', 'component newsfeeds category', 0, 1, 1.9333, 'C515352132326', 1, '*'),
(47, 'newsfeeds', 'newsfeeds', 0, 0, 0.6, 'N2132', 1, '*'),
(48, 'newsfeeds category', 'newsfeeds category', 0, 1, 1.6, 'N2132326', 1, '*'),
(49, 'newsfeeds category 5', 'newsfeeds category 5', 0, 1, 1.6667, 'N2132326', 1, '*'),
(55, '7', '7', 0, 0, 0.1, '', 2, '*'),
(56, '1', '1', 0, 0, 0.1, '', 2, '*'),
(57, '1 ikony', '1 ikony', 0, 1, 1.2333, 'I250', 2, '*'),
(58, '1 ikony khramovye', '1 ikony khramovye', 0, 1, 1.5667, 'I252651', 2, '*'),
(59, '2 uncategorised 1', '2 uncategorised 1', 0, 1, 1.5667, 'U5232623', 1, '*'),
(60, 'article', 'article', 0, 0, 0.4667, 'A6324', 10, '*'),
(61, 'ikony', 'ikony', 0, 0, 0.3333, 'I250', 2, '*'),
(62, 'ikony khramovye', 'ikony khramovye', 0, 1, 1.5, 'I252651', 2, '*'),
(63, 'khramovye', 'khramovye', 0, 0, 0.6, 'K651', 2, '*'),
(64, 'super', 'super', 0, 0, 0.3333, 'S160', 10, '*'),
(65, 'super user', 'super user', 0, 1, 1.3333, 'S1626', 10, '*'),
(66, 'uncategorised 1', 'uncategorised 1', 0, 1, 1.5, 'U5232623', 1, '*'),
(67, 'uncategorised 1 ikony', 'uncategorised 1 ikony', 0, 1, 1.7, 'U523262325', 1, '*'),
(68, 'user', 'user', 0, 0, 0.2667, 'U260', 10, '*'),
(69, 'арвфцгшафцгшфцшарфцгшарфцгшарвфцгшарфцгшарфцгшарфцгшарфцг', 'арвфцгшафцгшфцшарфцгшарфцгшарвфцгшарфцгшарфцгшарфцгшарфцг', 0, 0, 1, 'а000', 2, '*'),
(70, 'варпцрапуолафцралфуоадлфцладжфцладжфцлджафцладжфцладжйфца', 'варпцрапуолафцралфуоадлфцладжфцладжфцлджафцладжфцладжйфца', 0, 0, 1, 'в000', 2, '*'),
(71, 'варпцрапуолафцралфуоадлфцладжфцладжфцлджафцладжфцладжйфца арвфцгшафцгшфцшар', 'варпцрапуолафцралфуоадлфцладжфцладжфцлджафцладжфцладжйфца арвфцгшафцгшфцшар', 0, 1, 2, 'в000', 2, '*'),
(72, 'иконы', 'иконы', 0, 0, 0.3333, 'и000', 2, '*'),
(73, 'иконы храмовые', 'иконы храмовые', 0, 1, 1.4667, 'и000', 2, '*'),
(74, 'храмовые', 'храмовые', 0, 0, 0.5333, 'х000', 2, '*'),
(167, '3', '3', 0, 0, 0.1, '', 2, '*'),
(168, '3 knigi', '3 knigi', 0, 1, 1.2333, 'K520', 2, '*'),
(169, '3 knigi poligrafiya', '3 knigi poligrafiya', 0, 1, 1.6333, 'K5214261', 2, '*'),
(170, '8', '8', 0, 0, 0.1, '', 2, '*'),
(171, '8 lavka', '8 lavka', 0, 1, 1.2333, 'L120', 1, '*'),
(172, '8 lavka 3', '8 lavka 3', 0, 1, 1.3, 'L120', 1, '*'),
(173, 'bereste', 'bereste', 0, 0, 0.4667, 'B623', 2, '*'),
(174, 'knigi', 'knigi', 0, 0, 0.3333, 'K520', 2, '*'),
(175, 'knigi poligrafiya', 'knigi poligrafiya', 0, 1, 1.5667, 'K5214261', 2, '*'),
(176, 'knigi poligrafiya molitvy', 'knigi poligrafiya molitvy', 0, 1, 1.8333, 'K52142615431', 2, '*'),
(177, 'lavka', 'lavka', 0, 0, 0.3333, 'L120', 1, '*'),
(178, 'lavka 3', 'lavka 3', 0, 1, 1.2333, 'L120', 1, '*'),
(179, 'lavka 3 knigi', 'lavka 3 knigi', 0, 1, 1.4333, 'L1252', 1, '*'),
(180, 'molitvy', 'molitvy', 0, 0, 0.4667, 'M431', 2, '*'),
(181, 'molitvy na', 'molitvy na', 0, 1, 1.3333, 'M4315', 2, '*'),
(182, 'molitvy na bereste', 'molitvy na bereste', 0, 1, 1.6, 'M43151623', 2, '*'),
(183, 'na', 'na', 0, 0, 0.1333, 'N000', 2, '*'),
(184, 'na bereste', 'na bereste', 0, 1, 1.3333, 'N1623', 2, '*'),
(185, 'poligrafiya', 'poligrafiya', 0, 0, 0.7333, 'P4261', 2, '*'),
(186, 'poligrafiya molitvy', 'poligrafiya molitvy', 0, 1, 1.6333, 'P42615431', 2, '*'),
(187, 'poligrafiya molitvy na', 'poligrafiya molitvy na', 0, 1, 1.7333, 'P426154315', 2, '*'),
(188, 'бересте', 'бересте', 0, 0, 0.4667, 'б000', 2, '*'),
(189, 'книги', 'книги', 0, 0, 0.3333, 'к000', 2, '*'),
(190, 'книги полиграфия', 'книги полиграфия', 0, 1, 1.5333, 'к000', 2, '*'),
(191, 'книги полиграфия молитвы', 'книги полиграфия молитвы', 0, 1, 1.8, 'к000', 2, '*'),
(192, 'лавка', 'лавка', 0, 0, 0.3333, 'л000', 1, '*'),
(193, 'молитвы', 'молитвы', 0, 0, 0.4667, 'м000', 2, '*'),
(194, 'молитвы на', 'молитвы на', 0, 1, 1.3333, 'м000', 2, '*'),
(195, 'молитвы на бересте', 'молитвы на бересте', 0, 1, 1.6, 'м000', 2, '*'),
(196, 'на', 'на', 0, 0, 0.1333, 'н000', 2, '*'),
(197, 'на бересте', 'на бересте', 0, 1, 1.3333, 'н000', 2, '*'),
(198, 'полиграфия', 'полиграфия', 0, 0, 0.6667, 'п000', 2, '*'),
(199, 'полиграфия молитвы', 'полиграфия молитвы', 0, 1, 1.6, 'п000', 2, '*'),
(200, 'полиграфия молитвы на', 'полиграфия молитвы на', 0, 1, 1.7, 'п000', 2, '*'),
(201, 'пыкпфыцпяпяяпяпякпкпкп', 'пыкпфыцпяпяяпяпякпкпкп', 0, 0, 1, 'п000', 2, '*'),
(263, 'id', 'id', 0, 0, 0.1333, 'I300', 3, '*'),
(264, 'id 1', 'id 1', 0, 1, 1.1333, 'I300', 1, '*'),
(265, 'id 1 ikony', 'id 1 ikony', 0, 1, 1.3333, 'I325', 1, '*'),
(266, '2 svechi', '2 svechi', 0, 1, 1.2667, 'S120', 1, '*'),
(267, '2 svechi voskovye', '2 svechi voskovye', 0, 1, 1.5667, 'S12121', 1, '*'),
(268, 'id 2', 'id 2', 0, 1, 1.1333, 'I300', 1, '*'),
(269, 'id 2 svechi', 'id 2 svechi', 0, 1, 1.3667, 'I3212', 1, '*'),
(270, 'svechi', 'svechi', 0, 0, 0.4, 'S120', 1, '*'),
(271, 'svechi voskovye', 'svechi voskovye', 0, 1, 1.5, 'S12121', 1, '*'),
(272, 'voskovye', 'voskovye', 0, 0, 0.5333, 'V210', 1, '*'),
(273, 'восковые', 'восковые', 0, 0, 0.5333, 'в000', 1, '*'),
(274, 'пуывпмфуапфцафца', 'пуывпмфуапфцафца', 0, 0, 1, 'п000', 1, '*'),
(275, 'свечи', 'свечи', 0, 0, 0.3333, 'с000', 1, '*'),
(276, 'свечи восковые', 'свечи восковые', 0, 1, 1.4667, 'с000', 1, '*'),
(281, 'id 3', 'id 3', 0, 1, 1.1333, 'I300', 1, '*'),
(282, 'id 3 knigi', 'id 3 knigi', 0, 1, 1.3333, 'I3252', 1, '*'),
(286, '9', '9', 0, 0, 0.1, '', 5, '*'),
(287, '9 novosti', '9 novosti', 0, 1, 1.3, 'N123', 5, '*'),
(289, 'novosti', 'novosti', 0, 0, 0.4667, 'N123', 5, '*'),
(295, 'v', 'v', 0, 0, 0.0667, 'V000', 3, '*'),
(304, 'в', 'в', 0, 0, 0.0667, 'в000', 3, '*'),
(313, 'новости', 'новости', 0, 0, 0.4667, 'н000', 5, '*'),
(350, 'aleksandrovke', 'aleksandrovke', 0, 0, 0.8667, 'A4253612', 2, '*'),
(362, 'v aleksandrovke', 'v aleksandrovke', 0, 1, 1.5, 'V4253612', 2, '*'),
(363, 'александровке', 'александровке', 0, 0, 0.8667, 'а000', 2, '*'),
(364, 'в александровке', 'в александровке', 0, 1, 1.5, 'в000', 2, '*'),
(459, 'dukhovnoj', 'dukhovnoj', 0, 0, 0.6, 'D2152', 2, '*'),
(460, 'dukhovnoj i', 'dukhovnoj i', 0, 1, 1.3667, 'D2152', 2, '*'),
(461, 'dukhovnoj i narodnoj', 'dukhovnoj i narodnoj', 0, 1, 1.6667, 'D215256352', 2, '*'),
(462, 'festival', 'festival', 0, 0, 0.5333, 'F2314', 2, '*'),
(463, 'festival dukhovnoj', 'festival dukhovnoj', 0, 1, 1.6, 'F231432152', 2, '*'),
(464, 'festival dukhovnoj i', 'festival dukhovnoj i', 0, 1, 1.6667, 'F231432152', 2, '*'),
(465, 'i', 'i', 0, 0, 0.0667, 'I000', 2, '*'),
(466, 'i narodnoj', 'i narodnoj', 0, 1, 1.3333, 'I56352', 2, '*'),
(467, 'i narodnoj muzyki', 'i narodnoj muzyki', 0, 1, 1.5667, 'I5635252', 2, '*'),
(468, 'muzyki', 'muzyki', 0, 0, 0.4, 'M200', 2, '*'),
(469, 'narodnoj', 'narodnoj', 0, 0, 0.5333, 'N6352', 2, '*'),
(470, 'narodnoj muzyki', 'narodnoj muzyki', 0, 1, 1.5, 'N635252', 2, '*'),
(473, 'sretenskij', 'sretenskij', 0, 0, 0.6667, 'S6352', 2, '*'),
(474, 'sretenskij festival', 'sretenskij festival', 0, 1, 1.6333, 'S635212314', 2, '*'),
(475, 'sretenskij festival dukhovnoj', 'sretenskij festival dukhovnoj', 0, 1, 1.9667, 'S63521231432152', 2, '*'),
(476, 'xiii', 'xiii', 0, 0, 0.2667, 'X000', 2, '*'),
(477, 'xiii sretenskij', 'xiii sretenskij', 0, 1, 1.5, 'X6352', 2, '*'),
(478, 'xiii sretenskij festival', 'xiii sretenskij festival', 0, 1, 1.8, 'X635212314', 2, '*'),
(479, 'xiii сретенский', 'xiii сретенский', 0, 1, 1.5, 'X000', 2, '*'),
(480, 'xiii сретенский фестиваль', 'xiii сретенский фестиваль', 0, 1, 1.8333, 'X000', 2, '*'),
(481, 'духовной', 'духовной', 0, 0, 0.5333, 'д000', 2, '*'),
(482, 'духовной и', 'духовной и', 0, 1, 1.3333, 'д000', 2, '*'),
(483, 'духовной и народной', 'духовной и народной', 0, 1, 1.6333, 'д000', 2, '*'),
(484, 'и', 'и', 0, 0, 0.0667, 'и000', 2, '*'),
(485, 'и народной', 'и народной', 0, 1, 1.3333, 'и000', 2, '*'),
(486, 'и народной музыки', 'и народной музыки', 0, 1, 1.5667, 'и000', 2, '*'),
(487, 'музыки', 'музыки', 0, 0, 0.4, 'м000', 2, '*'),
(488, 'народной', 'народной', 0, 0, 0.5333, 'н000', 2, '*'),
(489, 'народной музыки', 'народной музыки', 0, 1, 1.5, 'н000', 2, '*'),
(490, 'сретенский', 'сретенский', 0, 0, 0.6667, 'с000', 2, '*'),
(491, 'сретенский фестиваль', 'сретенский фестиваль', 0, 1, 1.6667, 'с000', 2, '*'),
(492, 'сретенский фестиваль духовной', 'сретенский фестиваль духовной', 0, 1, 1.9667, 'с000', 2, '*'),
(493, 'фестиваль', 'фестиваль', 0, 0, 0.6, 'ф000', 2, '*'),
(494, 'фестиваль духовной', 'фестиваль духовной', 0, 1, 1.6, 'ф000', 2, '*'),
(495, 'фестиваль духовной и', 'фестиваль духовной и', 0, 1, 1.6667, 'ф000', 2, '*'),
(1069, '8 xiii', '8 xiii', 0, 1, 1.2, 'X000', 1, '*'),
(1070, '8 xiii sretenskij', '8 xiii sretenskij', 0, 1, 1.5667, 'X6352', 1, '*'),
(1071, '9 novosti 8', '9 novosti 8', 0, 1, 1.3667, 'N123', 1, '*'),
(1072, 'ghbdnsjhkjlkj', 'ghbdnsjhkjlkj', 0, 0, 0.8667, 'G135242', 1, '*'),
(1073, 'muzyki 2', 'muzyki 2', 0, 1, 1.2667, 'M200', 1, '*'),
(1074, 'narodnoj muzyki 2', 'narodnoj muzyki 2', 0, 1, 1.5667, 'N635252', 1, '*'),
(1075, 'novosti 8', 'novosti 8', 0, 1, 1.3, 'N123', 1, '*'),
(1076, 'novosti 8 xiii', 'novosti 8 xiii', 0, 1, 1.4667, 'N1232', 1, '*'),
(1084, '7 xiii', '7 xiii', 0, 1, 1.2, 'X000', 1, '*'),
(1085, '7 xiii sretenskij', '7 xiii sretenskij', 0, 1, 1.5667, 'X6352', 1, '*'),
(1086, '9 novosti 7', '9 novosti 7', 0, 1, 1.3667, 'N123', 1, '*'),
(1087, 'novosti 7', 'novosti 7', 0, 1, 1.3, 'N123', 1, '*'),
(1088, 'novosti 7 xiii', 'novosti 7 xiii', 0, 1, 1.4667, 'N1232', 1, '*'),
(1089, 'рпрапрари', 'рпрапрари', 0, 0, 0.6, 'р000', 1, '*'),
(1091, '6', '6', 0, 0, 0.1, '', 1, '*'),
(1092, '6 maslenitsa', '6 maslenitsa', 0, 1, 1.4, 'M24532', 1, '*'),
(1093, '6 maslenitsa v', '6 maslenitsa v', 0, 1, 1.4667, 'M245321', 1, '*'),
(1094, '9 novosti 6', '9 novosti 6', 0, 1, 1.3667, 'N123', 1, '*'),
(1095, 'maslenitsa', 'maslenitsa', 0, 0, 0.6667, 'M24532', 1, '*'),
(1096, 'maslenitsa v', 'maslenitsa v', 0, 1, 1.4, 'M245321', 1, '*'),
(1097, 'maslenitsa v aleksandrovke', 'maslenitsa v aleksandrovke', 0, 1, 1.8667, 'M2453214253612', 1, '*'),
(1098, 'novosti 6', 'novosti 6', 0, 1, 1.3, 'N123', 1, '*'),
(1099, 'novosti 6 maslenitsa', 'novosti 6 maslenitsa', 0, 1, 1.6667, 'N123524532', 1, '*'),
(1100, 'масленица', 'масленица', 0, 0, 0.6, 'м000', 1, '*'),
(1101, 'масленица в', 'масленица в', 0, 1, 1.3667, 'м000', 1, '*'),
(1102, 'масленица в александровке', 'масленица в александровке', 0, 1, 1.8333, 'м000', 1, '*'),
(1103, 'отормрпарп', 'отормрпарп', 0, 0, 0.6667, 'о000', 1, '*'),
(1106, '5 prazdnovanie', '5 prazdnovanie', 0, 1, 1.4667, 'P623515', 1, '*'),
(1107, '5 prazdnovanie paskhi', '5 prazdnovanie paskhi', 0, 1, 1.7, 'P62351512', 1, '*'),
(1108, '9 novosti 5', '9 novosti 5', 0, 1, 1.3667, 'N123', 1, '*'),
(1109, 'khristovoj', 'khristovoj', 0, 0, 0.6667, 'K62312', 1, '*'),
(1110, 'khristovoj v', 'khristovoj v', 0, 1, 1.4, 'K623121', 1, '*'),
(1111, 'khristovoj v aleksandrovke', 'khristovoj v aleksandrovke', 0, 1, 1.8667, 'K6231214253612', 1, '*'),
(1112, 'novosti 5', 'novosti 5', 0, 1, 1.3, 'N123', 1, '*'),
(1113, 'novosti 5 prazdnovanie', 'novosti 5 prazdnovanie', 0, 1, 1.7333, 'N1231623515', 1, '*'),
(1114, 'paskhi', 'paskhi', 0, 0, 0.4, 'P200', 1, '*'),
(1115, 'paskhi khristovoj', 'paskhi khristovoj', 0, 1, 1.5667, 'P262312', 1, '*'),
(1116, 'paskhi khristovoj v', 'paskhi khristovoj v', 0, 1, 1.6333, 'P2623121', 1, '*'),
(1117, 'prazdnovanie', 'prazdnovanie', 0, 0, 0.8, 'P623515', 1, '*'),
(1118, 'prazdnovanie paskhi', 'prazdnovanie paskhi', 0, 1, 1.6333, 'P62351512', 1, '*'),
(1119, 'prazdnovanie paskhi khristovoj', 'prazdnovanie paskhi khristovoj', 0, 1, 2, 'P6235151262312', 1, '*'),
(1120, 'мпапрапри', 'мпапрапри', 0, 0, 0.6, 'м000', 1, '*'),
(1121, 'мпапрапри р', 'мпапрапри р', 0, 1, 1.3667, 'м000', 1, '*'),
(1122, 'пасхи', 'пасхи', 0, 0, 0.3333, 'п000', 1, '*'),
(1123, 'пасхи христовой', 'пасхи христовой', 0, 1, 1.5, 'п000', 1, '*'),
(1124, 'пасхи христовой в', 'пасхи христовой в', 0, 1, 1.5667, 'п000', 1, '*'),
(1125, 'празднование', 'празднование', 0, 0, 0.8, 'п000', 1, '*'),
(1126, 'празднование пасхи', 'празднование пасхи', 0, 1, 1.6, 'п000', 1, '*'),
(1127, 'празднование пасхи христовой', 'празднование пасхи христовой', 0, 1, 1.9333, 'п000', 1, '*'),
(1128, 'р', 'р', 0, 0, 0.0667, 'р000', 1, '*'),
(1129, 'христовой', 'христовой', 0, 0, 0.6, 'х000', 1, '*'),
(1130, 'христовой в', 'христовой в', 0, 1, 1.3667, 'х000', 1, '*'),
(1131, 'христовой в александровке', 'христовой в александровке', 0, 1, 1.8333, 'х000', 1, '*'),
(1137, '4 vody', '4 vody', 0, 1, 1.2, 'V300', 1, '*'),
(1138, '4 vody tekushchie', '4 vody tekushchie', 0, 1, 1.5667, 'V320', 1, '*'),
(1139, '9 novosti 4', '9 novosti 4', 0, 1, 1.3667, 'N123', 1, '*'),
(1140, 'novosti 4', 'novosti 4', 0, 1, 1.3, 'N123', 1, '*'),
(1141, 'novosti 4 vody', 'novosti 4 vody', 0, 1, 1.4667, 'N12313', 1, '*'),
(1142, 'tekushchie', 'tekushchie', 0, 0, 0.6667, 'T200', 1, '*'),
(1143, 'tekushchie v', 'tekushchie v', 0, 1, 1.4, 'T210', 1, '*'),
(1144, 'tekushchie v zhizn', 'tekushchie v zhizn', 0, 1, 1.6, 'T2125', 1, '*'),
(1145, 'v zhizn', 'v zhizn', 0, 1, 1.2333, 'V250', 1, '*'),
(1146, 'v zhizn vechnuyu', 'v zhizn vechnuyu', 0, 1, 1.5333, 'V25125', 1, '*'),
(1147, 'vechnuyu', 'vechnuyu', 0, 0, 0.5333, 'V250', 1, '*'),
(1148, 'vody', 'vody', 0, 0, 0.2667, 'V300', 1, '*'),
(1149, 'vody tekushchie', 'vody tekushchie', 0, 1, 1.5, 'V320', 1, '*'),
(1150, 'vody tekushchie v', 'vody tekushchie v', 0, 1, 1.5667, 'V321', 1, '*'),
(1151, 'zhizn', 'zhizn', 0, 0, 0.3333, 'Z500', 1, '*'),
(1152, 'zhizn vechnuyu', 'zhizn vechnuyu', 0, 1, 1.4667, 'Z5125', 1, '*'),
(1153, 'в жизнь', 'в жизнь', 0, 1, 1.2333, 'в000', 1, '*'),
(1154, 'в жизнь вечную', 'в жизнь вечную', 0, 1, 1.4667, 'в000', 1, '*'),
(1155, 'вечную', 'вечную', 0, 0, 0.4, 'в000', 1, '*'),
(1156, 'воды', 'воды', 0, 0, 0.2667, 'в000', 1, '*'),
(1157, 'воды текущие', 'воды текущие', 0, 1, 1.4, 'в000', 1, '*'),
(1158, 'воды текущие в', 'воды текущие в', 0, 1, 1.4667, 'в000', 1, '*'),
(1159, 'жизнь', 'жизнь', 0, 0, 0.3333, 'ж000', 1, '*'),
(1160, 'жизнь вечную', 'жизнь вечную', 0, 1, 1.4, 'ж000', 1, '*'),
(1161, 'лмпапри', 'лмпапри', 0, 0, 0.4667, 'л000', 1, '*'),
(1162, 'текущие', 'текущие', 0, 0, 0.4667, 'т000', 1, '*'),
(1163, 'текущие в', 'текущие в', 0, 1, 1.3, 'т000', 1, '*'),
(1164, 'текущие в жизнь', 'текущие в жизнь', 0, 1, 1.5, 'т000', 1, '*');

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_finder_terms_common`
--

CREATE TABLE IF NOT EXISTS `yawnc_finder_terms_common` (
  `term` varchar(75) NOT NULL,
  `language` varchar(3) NOT NULL,
  KEY `idx_word_lang` (`term`,`language`),
  KEY `idx_lang` (`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yawnc_finder_terms_common`
--

INSERT INTO `yawnc_finder_terms_common` (`term`, `language`) VALUES
('a', 'en'),
('about', 'en'),
('after', 'en'),
('ago', 'en'),
('all', 'en'),
('am', 'en'),
('an', 'en'),
('and', 'en'),
('ani', 'en'),
('any', 'en'),
('are', 'en'),
('aren''t', 'en'),
('as', 'en'),
('at', 'en'),
('be', 'en'),
('but', 'en'),
('by', 'en'),
('for', 'en'),
('from', 'en'),
('get', 'en'),
('go', 'en'),
('how', 'en'),
('if', 'en'),
('in', 'en'),
('into', 'en'),
('is', 'en'),
('isn''t', 'en'),
('it', 'en'),
('its', 'en'),
('me', 'en'),
('more', 'en'),
('most', 'en'),
('must', 'en'),
('my', 'en'),
('new', 'en'),
('no', 'en'),
('none', 'en'),
('not', 'en'),
('noth', 'en'),
('nothing', 'en'),
('of', 'en'),
('off', 'en'),
('often', 'en'),
('old', 'en'),
('on', 'en'),
('onc', 'en'),
('once', 'en'),
('onli', 'en'),
('only', 'en'),
('or', 'en'),
('other', 'en'),
('our', 'en'),
('ours', 'en'),
('out', 'en'),
('over', 'en'),
('page', 'en'),
('she', 'en'),
('should', 'en'),
('small', 'en'),
('so', 'en'),
('some', 'en'),
('than', 'en'),
('thank', 'en'),
('that', 'en'),
('the', 'en'),
('their', 'en'),
('theirs', 'en'),
('them', 'en'),
('then', 'en'),
('there', 'en'),
('these', 'en'),
('they', 'en'),
('this', 'en'),
('those', 'en'),
('thus', 'en'),
('time', 'en'),
('times', 'en'),
('to', 'en'),
('too', 'en'),
('true', 'en'),
('under', 'en'),
('until', 'en'),
('up', 'en'),
('upon', 'en'),
('use', 'en'),
('user', 'en'),
('users', 'en'),
('veri', 'en'),
('version', 'en'),
('very', 'en'),
('via', 'en'),
('want', 'en'),
('was', 'en'),
('way', 'en'),
('were', 'en'),
('what', 'en'),
('when', 'en'),
('where', 'en'),
('whi', 'en'),
('which', 'en'),
('who', 'en'),
('whom', 'en'),
('whose', 'en'),
('why', 'en'),
('wide', 'en'),
('will', 'en'),
('with', 'en'),
('within', 'en'),
('without', 'en'),
('would', 'en'),
('yes', 'en'),
('yet', 'en'),
('you', 'en'),
('your', 'en'),
('yours', 'en');

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_finder_tokens`
--

CREATE TABLE IF NOT EXISTS `yawnc_finder_tokens` (
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `phrase` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `weight` float unsigned NOT NULL DEFAULT '1',
  `context` tinyint(1) unsigned NOT NULL DEFAULT '2',
  `language` char(3) NOT NULL DEFAULT '',
  KEY `idx_word` (`term`),
  KEY `idx_context` (`context`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_finder_tokens_aggregate`
--

CREATE TABLE IF NOT EXISTS `yawnc_finder_tokens_aggregate` (
  `term_id` int(10) unsigned NOT NULL,
  `map_suffix` char(1) NOT NULL,
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `phrase` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `term_weight` float unsigned NOT NULL,
  `context` tinyint(1) unsigned NOT NULL DEFAULT '2',
  `context_weight` float unsigned NOT NULL,
  `total_weight` float unsigned NOT NULL,
  `language` char(3) NOT NULL DEFAULT '',
  KEY `token` (`term`),
  KEY `keyword_id` (`term_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_finder_types`
--

CREATE TABLE IF NOT EXISTS `yawnc_finder_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `mime` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `yawnc_finder_types`
--

INSERT INTO `yawnc_finder_types` (`id`, `title`, `mime`) VALUES
(1, 'Tag', ''),
(2, 'Category', ''),
(3, 'Contact', ''),
(4, 'Article', ''),
(5, 'News Feed', '');

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_addons`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_addons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `key` text NOT NULL,
  `usekey` tinyint(1) NOT NULL,
  `version` varchar(255) NOT NULL,
  `uninstall` varchar(255) NOT NULL,
  `params` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `alias` (`alias`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_attr`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_attr` (
  `attr_id` int(11) NOT NULL AUTO_INCREMENT,
  `attr_ordering` int(11) NOT NULL,
  `attr_type` tinyint(1) NOT NULL,
  `independent` tinyint(1) NOT NULL,
  `allcats` tinyint(1) NOT NULL DEFAULT '1',
  `cats` text NOT NULL,
  `group` tinyint(4) NOT NULL,
  PRIMARY KEY (`attr_id`),
  KEY `group` (`group`),
  KEY `attr_ordering` (`attr_ordering`),
  KEY `attr_type` (`attr_type`),
  KEY `independent` (`independent`),
  KEY `allcats` (`allcats`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_attr_groups`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_attr_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ordering` int(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_attr_values`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_attr_values` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT,
  `attr_id` int(11) NOT NULL,
  `value_ordering` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`value_id`),
  KEY `attr_id` (`attr_id`),
  KEY `value_ordering` (`value_ordering`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_cart_temp`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_cart_temp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_cookie` varchar(255) NOT NULL,
  `cart` text NOT NULL,
  `type_cart` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_cookie` (`id_cookie`),
  KEY `type_cart` (`type_cart`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_categories`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_image` varchar(255) DEFAULT NULL,
  `category_parent_id` int(11) NOT NULL,
  `category_publish` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `category_ordertype` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `category_template` varchar(64) DEFAULT NULL,
  `ordering` int(3) NOT NULL,
  `category_add_date` datetime DEFAULT NULL,
  `products_page` int(8) NOT NULL DEFAULT '12',
  `products_row` int(3) NOT NULL DEFAULT '3',
  `access` int(3) NOT NULL DEFAULT '1',
  PRIMARY KEY (`category_id`),
  KEY `sort_add_date` (`category_add_date`),
  KEY `category_parent_id` (`category_parent_id`),
  KEY `category_publish` (`category_publish`),
  KEY `category_ordertype` (`category_ordertype`),
  KEY `category_template` (`category_template`),
  KEY `ordering` (`ordering`),
  KEY `category_add_date` (`category_add_date`),
  KEY `products_page` (`products_page`),
  KEY `products_row` (`products_row`),
  KEY `access` (`access`),
  KEY `category_publish_2` (`category_publish`,`access`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_config`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `count_products_to_page` int(4) NOT NULL,
  `count_products_to_row` int(2) NOT NULL,
  `count_category_to_row` int(2) NOT NULL,
  `image_category_width` int(4) NOT NULL,
  `image_category_height` int(4) NOT NULL,
  `image_product_width` int(4) NOT NULL,
  `image_product_height` int(4) NOT NULL,
  `image_product_full_width` int(4) NOT NULL,
  `image_product_full_height` int(4) NOT NULL,
  `image_product_original_width` int(4) NOT NULL,
  `image_product_original_height` int(4) NOT NULL,
  `video_product_width` int(4) NOT NULL,
  `video_product_height` int(4) NOT NULL,
  `adminLanguage` varchar(8) NOT NULL,
  `defaultLanguage` varchar(8) NOT NULL,
  `mainCurrency` int(4) NOT NULL,
  `decimal_count` tinyint(1) NOT NULL,
  `decimal_symbol` varchar(5) NOT NULL,
  `thousand_separator` varchar(5) NOT NULL,
  `currency_format` tinyint(1) NOT NULL,
  `use_rabatt_code` tinyint(1) NOT NULL,
  `enable_wishlist` tinyint(1) NOT NULL,
  `default_status_order` tinyint(1) NOT NULL,
  `store_address_format` varchar(32) NOT NULL,
  `store_date_format` varchar(32) NOT NULL,
  `contact_email` varchar(128) NOT NULL,
  `allow_reviews_prod` tinyint(1) NOT NULL,
  `allow_reviews_only_registered` tinyint(1) NOT NULL,
  `allow_reviews_manuf` tinyint(1) NOT NULL,
  `max_mark` int(11) NOT NULL,
  `summ_null_shipping` decimal(12,2) NOT NULL,
  `without_shipping` tinyint(1) NOT NULL,
  `without_payment` tinyint(1) NOT NULL,
  `pdf_parameters` varchar(32) NOT NULL,
  `next_order_number` int(11) NOT NULL DEFAULT '1',
  `shop_user_guest` tinyint(1) NOT NULL,
  `hide_product_not_avaible_stock` tinyint(1) NOT NULL,
  `show_buy_in_category` tinyint(1) NOT NULL,
  `user_as_catalog` tinyint(1) NOT NULL,
  `show_tax_in_product` tinyint(1) NOT NULL,
  `show_tax_product_in_cart` tinyint(1) NOT NULL,
  `show_plus_shipping_in_product` tinyint(1) NOT NULL,
  `hide_buy_not_avaible_stock` tinyint(1) NOT NULL,
  `show_sort_product` tinyint(1) NOT NULL,
  `show_count_select_products` tinyint(1) NOT NULL,
  `order_send_pdf_client` tinyint(1) NOT NULL,
  `order_send_pdf_admin` tinyint(1) NOT NULL,
  `show_delivery_time` tinyint(1) NOT NULL,
  `securitykey` varchar(128) NOT NULL,
  `demo_type` tinyint(1) NOT NULL,
  `product_show_manufacturer_logo` tinyint(1) NOT NULL,
  `product_show_manufacturer` tinyint(1) NOT NULL,
  `product_show_weight` tinyint(1) NOT NULL,
  `max_count_order_one_product` int(11) NOT NULL,
  `min_count_order_one_product` int(11) NOT NULL,
  `min_price_order` int(11) NOT NULL,
  `max_price_order` int(11) NOT NULL,
  `hide_tax` tinyint(1) NOT NULL,
  `licensekod` text NOT NULL,
  `product_attribut_first_value_empty` tinyint(1) NOT NULL,
  `show_hits` tinyint(1) NOT NULL,
  `show_registerform_in_logintemplate` tinyint(1) NOT NULL,
  `admin_show_product_basic_price` tinyint(1) NOT NULL,
  `admin_show_attributes` tinyint(1) NOT NULL,
  `admin_show_delivery_time` tinyint(1) NOT NULL,
  `admin_show_languages` tinyint(1) NOT NULL,
  `use_different_templates_cat_prod` tinyint(1) NOT NULL,
  `admin_show_product_video` tinyint(1) NOT NULL,
  `admin_show_product_related` tinyint(1) NOT NULL,
  `admin_show_product_files` tinyint(1) NOT NULL,
  `admin_show_product_bay_price` tinyint(1) NOT NULL,
  `admin_show_product_labels` tinyint(1) NOT NULL,
  `sorting_country_in_alphabet` tinyint(1) NOT NULL,
  `hide_text_product_not_available` tinyint(1) NOT NULL,
  `show_weight_order` tinyint(1) NOT NULL,
  `discount_use_full_sum` tinyint(1) NOT NULL,
  `show_cart_all_step_checkout` tinyint(1) NOT NULL,
  `use_plugin_content` tinyint(1) NOT NULL,
  `display_price_admin` tinyint(1) NOT NULL,
  `display_price_front` tinyint(1) NOT NULL,
  `product_list_show_weight` tinyint(1) NOT NULL,
  `product_list_show_manufacturer` tinyint(1) NOT NULL,
  `use_extend_tax_rule` tinyint(4) NOT NULL,
  `use_extend_display_price_rule` tinyint(4) NOT NULL,
  `fields_register` text NOT NULL,
  `template` varchar(128) NOT NULL,
  `show_product_code` tinyint(1) NOT NULL,
  `show_product_code_in_cart` tinyint(1) NOT NULL,
  `savelog` tinyint(1) NOT NULL,
  `savelogpaymentdata` tinyint(1) NOT NULL,
  `product_list_show_min_price` tinyint(1) NOT NULL,
  `product_count_related_in_row` tinyint(4) NOT NULL,
  `category_sorting` tinyint(1) NOT NULL DEFAULT '1',
  `product_sorting` tinyint(1) NOT NULL DEFAULT '1',
  `product_sorting_direction` tinyint(1) NOT NULL,
  `show_product_list_filters` tinyint(1) NOT NULL,
  `admin_show_product_extra_field` tinyint(1) NOT NULL,
  `product_list_display_extra_fields` text NOT NULL,
  `filter_display_extra_fields` text NOT NULL,
  `product_hide_extra_fields` text NOT NULL,
  `cart_display_extra_fields` text NOT NULL,
  `default_country` int(11) NOT NULL,
  `show_return_policy_in_email_order` tinyint(1) NOT NULL,
  `client_allow_cancel_order` tinyint(1) NOT NULL,
  `admin_show_vendors` tinyint(1) NOT NULL,
  `vendor_order_message_type` tinyint(1) NOT NULL,
  `admin_not_send_email_order_vendor_order` tinyint(1) NOT NULL,
  `not_redirect_in_cart_after_buy` tinyint(1) NOT NULL,
  `product_show_vendor` tinyint(1) NOT NULL,
  `product_show_vendor_detail` tinyint(1) NOT NULL,
  `product_list_show_vendor` tinyint(1) NOT NULL,
  `admin_show_freeattributes` tinyint(1) NOT NULL,
  `product_show_button_back` tinyint(1) NOT NULL,
  `calcule_tax_after_discount` tinyint(1) NOT NULL,
  `product_list_show_product_code` tinyint(1) NOT NULL,
  `radio_attr_value_vertical` tinyint(1) NOT NULL,
  `attr_display_addprice` tinyint(1) NOT NULL,
  `use_ssl` tinyint(1) NOT NULL,
  `product_list_show_price_description` tinyint(1) NOT NULL,
  `display_button_print` tinyint(1) NOT NULL,
  `hide_shipping_step` tinyint(1) NOT NULL,
  `hide_payment_step` tinyint(1) NOT NULL,
  `image_resize_type` tinyint(1) NOT NULL,
  `use_extend_attribute_data` tinyint(1) NOT NULL,
  `product_list_show_price_default` tinyint(1) NOT NULL,
  `product_list_show_qty_stock` tinyint(1) NOT NULL,
  `product_show_qty_stock` tinyint(1) NOT NULL,
  `displayprice` tinyint(1) NOT NULL,
  `use_decimal_qty` tinyint(1) NOT NULL,
  `ext_tax_rule_for` tinyint(1) NOT NULL,
  `display_reviews_without_confirm` tinyint(1) NOT NULL,
  `manufacturer_sorting` tinyint(1) NOT NULL,
  `admin_show_units` tinyint(1) NOT NULL,
  `main_unit_weight` tinyint(3) NOT NULL,
  `create_alias_product_category_auto` tinyint(1) NOT NULL,
  `delivery_order_depends_delivery_product` tinyint(1) NOT NULL,
  `show_delivery_time_step5` tinyint(1) NOT NULL,
  `other_config` text NOT NULL,
  `shop_mode` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_config_display_prices`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_config_display_prices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zones` text NOT NULL,
  `display_price` tinyint(1) NOT NULL,
  `display_price_firma` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `display_price` (`display_price`),
  KEY `display_price_firma` (`display_price_firma`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_config_seo`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_config_seo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(64) NOT NULL,
  `ordering` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `alias` (`alias`),
  KEY `ordering` (`ordering`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_config_statictext`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_config_statictext` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(64) NOT NULL,
  `use_for_return_policy` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `alias` (`alias`),
  KEY `use_for_return_policy` (`use_for_return_policy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_countries`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_countries` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_publish` tinyint(4) NOT NULL,
  `ordering` smallint(6) NOT NULL,
  `country_code` varchar(5) NOT NULL,
  `country_code_2` varchar(5) NOT NULL,
  `name_en-GB` varchar(255) NOT NULL,
  `name_de-DE` varchar(255) NOT NULL,
  PRIMARY KEY (`country_id`),
  KEY `country_publish` (`country_publish`),
  KEY `ordering` (`ordering`),
  KEY `country_code` (`country_code`),
  KEY `country_code_2` (`country_code_2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_coupons`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_coupons` (
  `coupon_id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_type` tinyint(4) NOT NULL COMMENT 'value_or_percent',
  `coupon_code` varchar(100) NOT NULL,
  `coupon_value` decimal(12,2) NOT NULL DEFAULT '0.00',
  `tax_id` int(11) NOT NULL,
  `used` int(11) NOT NULL,
  `for_user_id` int(11) NOT NULL,
  `coupon_start_date` date NOT NULL DEFAULT '0000-00-00',
  `coupon_expire_date` date NOT NULL DEFAULT '0000-00-00',
  `finished_after_used` int(11) NOT NULL,
  `coupon_publish` tinyint(4) NOT NULL,
  PRIMARY KEY (`coupon_id`),
  KEY `coupon_type` (`coupon_type`),
  KEY `coupon_code` (`coupon_code`),
  KEY `tax_id` (`tax_id`),
  KEY `used` (`used`),
  KEY `for_user_id` (`for_user_id`),
  KEY `coupon_publish` (`coupon_publish`),
  KEY `coupon_start_date` (`coupon_start_date`),
  KEY `coupon_expire_date` (`coupon_expire_date`),
  KEY `finished_after_used` (`finished_after_used`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_currencies`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_currencies` (
  `currency_id` int(11) NOT NULL AUTO_INCREMENT,
  `currency_name` varchar(64) NOT NULL,
  `currency_code` varchar(20) NOT NULL,
  `currency_code_iso` varchar(3) NOT NULL,
  `currency_code_num` varchar(3) NOT NULL,
  `currency_ordering` int(11) NOT NULL,
  `currency_value` decimal(14,6) NOT NULL,
  `currency_publish` tinyint(1) NOT NULL,
  PRIMARY KEY (`currency_id`),
  KEY `currency_code_iso` (`currency_code_iso`),
  KEY `currency_code_num` (`currency_code_num`),
  KEY `currency_ordering` (`currency_ordering`),
  KEY `currency_publish` (`currency_publish`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_delivery_times`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_delivery_times` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `days` decimal(8,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_free_attr`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_free_attr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ordering` int(11) NOT NULL,
  `required` tinyint(1) NOT NULL,
  `type` tinyint(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_import_export`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_import_export` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `params` text NOT NULL,
  `endstart` int(11) NOT NULL,
  `steptime` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_languages`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(32) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `publish` int(11) NOT NULL,
  `ordering` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `publish` (`publish`),
  KEY `ordering` (`ordering`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_manufacturers`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_manufacturers` (
  `manufacturer_id` int(11) NOT NULL AUTO_INCREMENT,
  `manufacturer_url` varchar(255) NOT NULL,
  `manufacturer_logo` varchar(255) NOT NULL,
  `manufacturer_publish` tinyint(1) NOT NULL,
  `products_page` int(11) NOT NULL,
  `products_row` int(11) NOT NULL,
  `ordering` int(6) NOT NULL,
  PRIMARY KEY (`manufacturer_id`),
  KEY `manufacturer_publish` (`manufacturer_publish`),
  KEY `products_page` (`products_page`),
  KEY `products_row` (`products_row`),
  KEY `ordering` (`ordering`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_orders`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_number` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_total` decimal(14,4) NOT NULL,
  `order_subtotal` decimal(14,4) NOT NULL,
  `order_tax` decimal(14,4) NOT NULL,
  `order_tax_ext` text NOT NULL,
  `order_shipping` decimal(14,4) NOT NULL,
  `order_payment` decimal(14,4) NOT NULL,
  `order_discount` decimal(14,4) NOT NULL,
  `shipping_tax` decimal(12,4) NOT NULL,
  `shipping_tax_ext` text NOT NULL,
  `payment_tax` decimal(12,4) NOT NULL,
  `payment_tax_ext` text NOT NULL,
  `order_package` decimal(12,2) NOT NULL,
  `package_tax_ext` text NOT NULL,
  `currency_code` varchar(20) NOT NULL,
  `currency_code_iso` varchar(3) NOT NULL,
  `currency_exchange` decimal(14,6) NOT NULL,
  `order_status` tinyint(4) NOT NULL,
  `order_created` tinyint(1) NOT NULL,
  `order_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `invoice_date` datetime NOT NULL,
  `order_m_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `shipping_method_id` int(11) NOT NULL,
  `delivery_times_id` int(11) NOT NULL,
  `payment_method_id` int(11) NOT NULL,
  `payment_params` text NOT NULL,
  `payment_params_data` text NOT NULL,
  `shipping_params` text NOT NULL,
  `shipping_params_data` text NOT NULL,
  `delivery_time` varchar(100) NOT NULL,
  `delivery_date` datetime NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `order_add_info` text NOT NULL,
  `title` tinyint(1) NOT NULL,
  `f_name` varchar(255) NOT NULL,
  `l_name` varchar(255) NOT NULL,
  `m_name` varchar(255) NOT NULL,
  `firma_name` varchar(255) NOT NULL,
  `client_type` tinyint(1) NOT NULL,
  `client_type_name` varchar(100) NOT NULL,
  `firma_code` varchar(100) NOT NULL,
  `tax_number` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `birthday` date NOT NULL,
  `street` varchar(100) NOT NULL,
  `street_nr` varchar(16) NOT NULL,
  `home` varchar(20) NOT NULL,
  `apartment` varchar(20) NOT NULL,
  `zip` varchar(20) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` int(11) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `mobil_phone` varchar(20) NOT NULL,
  `fax` varchar(20) NOT NULL,
  `ext_field_1` varchar(255) NOT NULL,
  `ext_field_2` varchar(255) NOT NULL,
  `ext_field_3` varchar(255) NOT NULL,
  `d_title` tinyint(1) NOT NULL,
  `d_f_name` varchar(255) NOT NULL,
  `d_l_name` varchar(255) NOT NULL,
  `d_m_name` varchar(255) NOT NULL,
  `d_firma_name` varchar(255) NOT NULL,
  `d_email` varchar(255) NOT NULL,
  `d_birthday` date NOT NULL,
  `d_street` varchar(100) NOT NULL,
  `d_street_nr` varchar(16) NOT NULL,
  `d_home` varchar(20) NOT NULL,
  `d_apartment` varchar(20) NOT NULL,
  `d_zip` varchar(20) NOT NULL,
  `d_city` varchar(100) NOT NULL,
  `d_state` varchar(100) NOT NULL,
  `d_country` int(11) NOT NULL,
  `d_phone` varchar(30) NOT NULL,
  `d_mobil_phone` varchar(20) NOT NULL,
  `d_fax` varchar(20) NOT NULL,
  `d_ext_field_1` varchar(255) NOT NULL,
  `d_ext_field_2` varchar(255) NOT NULL,
  `d_ext_field_3` varchar(255) NOT NULL,
  `pdf_file` varchar(50) NOT NULL,
  `order_hash` varchar(32) NOT NULL,
  `file_hash` varchar(64) NOT NULL,
  `file_stat_downloads` text NOT NULL,
  `order_custom_info` text NOT NULL,
  `display_price` tinyint(1) NOT NULL,
  `vendor_type` tinyint(1) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `lang` varchar(16) NOT NULL,
  `transaction` text NOT NULL,
  `product_stock_removed` tinyint(1) NOT NULL,
  PRIMARY KEY (`order_id`),
  KEY `order_number` (`order_number`),
  KEY `user_id` (`user_id`),
  KEY `currency_code_iso` (`currency_code_iso`),
  KEY `order_status` (`order_status`),
  KEY `order_created` (`order_created`),
  KEY `shipping_method_id` (`shipping_method_id`),
  KEY `delivery_times_id` (`delivery_times_id`),
  KEY `payment_method_id` (`payment_method_id`),
  KEY `coupon_id` (`coupon_id`),
  KEY `client_type` (`client_type`),
  KEY `country` (`country`),
  KEY `phone` (`phone`),
  KEY `d_title` (`d_title`),
  KEY `d_country` (`d_country`),
  KEY `display_price` (`display_price`),
  KEY `vendor_type` (`vendor_type`),
  KEY `vendor_id` (`vendor_id`),
  KEY `lang` (`lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_order_history`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_order_history` (
  `order_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `order_status_id` tinyint(1) NOT NULL,
  `status_date_added` datetime NOT NULL,
  `customer_notify` int(1) DEFAULT NULL,
  `comments` text,
  PRIMARY KEY (`order_history_id`),
  KEY `order_id` (`order_id`),
  KEY `order_status_id` (`order_status_id`),
  KEY `status_date_added` (`status_date_added`),
  KEY `customer_notify` (`customer_notify`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_order_item`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_order_item` (
  `order_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_ean` varchar(50) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `product_quantity` decimal(12,2) NOT NULL,
  `product_item_price` decimal(14,4) NOT NULL,
  `product_tax` decimal(14,4) NOT NULL,
  `product_attributes` text NOT NULL,
  `product_freeattributes` text NOT NULL,
  `attributes` text NOT NULL,
  `freeattributes` text NOT NULL,
  `extra_fields` text NOT NULL,
  `files` text NOT NULL,
  `weight` decimal(14,4) NOT NULL,
  `thumb_image` varchar(255) NOT NULL,
  `manufacturer` varchar(255) NOT NULL,
  `delivery_times_id` int(4) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `basicprice` decimal(12,2) NOT NULL,
  `basicpriceunit` varchar(255) NOT NULL,
  `params` text NOT NULL,
  PRIMARY KEY (`order_item_id`),
  KEY `order_id` (`order_id`),
  KEY `product_id` (`product_id`),
  KEY `delivery_times_id` (`delivery_times_id`),
  KEY `vendor_id` (`vendor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_order_status`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_order_status` (
  `status_id` int(11) NOT NULL AUTO_INCREMENT,
  `status_code` char(1) NOT NULL,
  `name_en-GB` varchar(100) NOT NULL,
  `name_de-DE` varchar(100) NOT NULL,
  PRIMARY KEY (`status_id`),
  KEY `status_code` (`status_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_payment_method`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_payment_method` (
  `payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_code` varchar(32) NOT NULL,
  `payment_class` varchar(100) NOT NULL,
  `scriptname` varchar(100) NOT NULL,
  `payment_publish` tinyint(1) NOT NULL,
  `payment_ordering` int(11) NOT NULL,
  `payment_params` text NOT NULL,
  `payment_type` tinyint(4) NOT NULL,
  `price` decimal(12,2) NOT NULL,
  `price_type` tinyint(1) NOT NULL,
  `tax_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `show_descr_in_email` tinyint(1) NOT NULL,
  `show_bank_in_order` tinyint(1) NOT NULL DEFAULT '1',
  `order_description` text NOT NULL,
  `name_en-GB` varchar(100) NOT NULL,
  `description_en-GB` text NOT NULL,
  `name_de-DE` varchar(100) NOT NULL,
  `description_de-DE` text NOT NULL,
  PRIMARY KEY (`payment_id`),
  KEY `payment_code` (`payment_code`),
  KEY `payment_publish` (`payment_publish`),
  KEY `payment_ordering` (`payment_ordering`),
  KEY `payment_type` (`payment_type`),
  KEY `price_type` (`price_type`),
  KEY `tax_id` (`tax_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_payment_trx`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_payment_trx` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `transaction` varchar(255) NOT NULL,
  `rescode` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `transaction` (`transaction`),
  KEY `rescode` (`rescode`),
  KEY `status_id` (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_payment_trx_data`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_payment_trx_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trx_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `trx_id` (`trx_id`),
  KEY `order_id` (`order_id`),
  KEY `key` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_products`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_products` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `product_ean` varchar(32) NOT NULL,
  `product_quantity` decimal(12,2) NOT NULL,
  `unlimited` tinyint(1) NOT NULL,
  `product_availability` varchar(1) NOT NULL,
  `product_date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modify` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `product_publish` tinyint(1) NOT NULL,
  `product_tax_id` tinyint(3) NOT NULL,
  `currency_id` int(4) NOT NULL,
  `product_template` varchar(64) NOT NULL DEFAULT 'default',
  `product_url` varchar(255) NOT NULL,
  `product_old_price` decimal(14,4) NOT NULL,
  `product_buy_price` decimal(14,4) NOT NULL,
  `product_price` decimal(18,6) NOT NULL,
  `min_price` decimal(12,2) NOT NULL,
  `different_prices` tinyint(1) NOT NULL,
  `product_weight` decimal(14,4) NOT NULL,
  `image` varchar(255) NOT NULL,
  `product_manufacturer_id` int(11) NOT NULL,
  `product_is_add_price` tinyint(1) NOT NULL,
  `add_price_unit_id` int(3) NOT NULL,
  `average_rating` float(4,2) NOT NULL,
  `reviews_count` int(11) NOT NULL,
  `delivery_times_id` int(4) NOT NULL,
  `hits` int(11) NOT NULL,
  `weight_volume_units` decimal(14,4) NOT NULL,
  `basic_price_unit_id` int(3) NOT NULL,
  `label_id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `access` int(3) NOT NULL DEFAULT '1',
  PRIMARY KEY (`product_id`),
  KEY `product_manufacturer_id` (`product_manufacturer_id`),
  KEY `parent_id` (`parent_id`),
  KEY `product_ean` (`product_ean`),
  KEY `unlimited` (`unlimited`),
  KEY `product_publish` (`product_publish`),
  KEY `product_tax_id` (`product_tax_id`),
  KEY `currency_id` (`currency_id`),
  KEY `product_price` (`product_price`),
  KEY `min_price` (`min_price`),
  KEY `add_price_unit_id` (`add_price_unit_id`),
  KEY `average_rating` (`average_rating`),
  KEY `reviews_count` (`reviews_count`),
  KEY `delivery_times_id` (`delivery_times_id`),
  KEY `hits` (`hits`),
  KEY `basic_price_unit_id` (`basic_price_unit_id`),
  KEY `label_id` (`label_id`),
  KEY `vendor_id` (`vendor_id`),
  KEY `access` (`access`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_products_attr`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_products_attr` (
  `product_attr_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `buy_price` decimal(12,2) NOT NULL,
  `price` decimal(14,4) NOT NULL,
  `old_price` decimal(14,4) NOT NULL,
  `count` decimal(14,4) NOT NULL,
  `ean` varchar(100) NOT NULL,
  `weight` decimal(12,4) NOT NULL,
  `weight_volume_units` decimal(14,4) NOT NULL,
  `ext_attribute_product_id` int(11) NOT NULL,
  PRIMARY KEY (`product_attr_id`),
  KEY `product_id` (`product_id`),
  KEY `ext_attribute_product_id` (`ext_attribute_product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_products_attr2`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_products_attr2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `attr_id` int(11) NOT NULL,
  `attr_value_id` int(11) NOT NULL,
  `price_mod` char(1) NOT NULL,
  `addprice` decimal(14,4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `attr_id` (`attr_id`),
  KEY `attr_value_id` (`attr_value_id`),
  KEY `price_mod` (`price_mod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_products_extra_fields`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_products_extra_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `allcats` tinyint(1) NOT NULL,
  `cats` text NOT NULL,
  `type` tinyint(1) NOT NULL,
  `multilist` tinyint(1) NOT NULL,
  `group` tinyint(4) NOT NULL,
  `ordering` int(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `group` (`group`),
  KEY `allcats` (`allcats`),
  KEY `type` (`type`),
  KEY `multilist` (`multilist`),
  KEY `group_2` (`group`),
  KEY `ordering` (`ordering`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_products_extra_field_groups`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_products_extra_field_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ordering` int(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ordering` (`ordering`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_products_extra_field_values`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_products_extra_field_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_id` int(11) NOT NULL,
  `ordering` int(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `field_id` (`field_id`),
  KEY `ordering` (`ordering`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_products_files`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_products_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `demo` varchar(255) NOT NULL,
  `demo_descr` varchar(255) NOT NULL,
  `file` varchar(255) NOT NULL,
  `file_descr` varchar(255) NOT NULL,
  `ordering` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `ordering` (`ordering`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_products_free_attr`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_products_free_attr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `attr_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `attr_id` (`attr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_products_images`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_products_images` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `ordering` tinyint(4) NOT NULL,
  PRIMARY KEY (`image_id`),
  KEY `product_id` (`product_id`),
  KEY `ordering` (`ordering`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_products_option`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_products_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `key` varchar(64) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `prodkey` (`product_id`,`key`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_products_prices`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_products_prices` (
  `price_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `discount` decimal(16,6) NOT NULL,
  `product_quantity_start` int(11) NOT NULL,
  `product_quantity_finish` int(11) NOT NULL,
  PRIMARY KEY (`price_id`),
  KEY `product_id` (`product_id`),
  KEY `product_quantity_start` (`product_quantity_start`),
  KEY `product_quantity_finish` (`product_quantity_finish`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_products_relations`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_products_relations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `product_related_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`,`product_related_id`),
  KEY `product_id_2` (`product_id`),
  KEY `product_related_id` (`product_related_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_products_reviews`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_products_reviews` (
  `review_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `time` datetime NOT NULL,
  `review` text NOT NULL,
  `mark` int(11) NOT NULL,
  `publish` tinyint(1) NOT NULL,
  `ip` varchar(20) NOT NULL,
  PRIMARY KEY (`review_id`),
  KEY `product_id` (`product_id`),
  KEY `user_id` (`user_id`),
  KEY `user_email` (`user_email`),
  KEY `mark` (`mark`),
  KEY `publish` (`publish`),
  KEY `ip` (`ip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_products_to_categories`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_products_to_categories` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_ordering` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`category_id`),
  KEY `category_id` (`category_id`),
  KEY `product_id` (`product_id`),
  KEY `product_id_2` (`product_id`,`category_id`,`product_ordering`),
  KEY `product_ordering` (`product_ordering`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_products_videos`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_products_videos` (
  `video_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `video_name` varchar(255) NOT NULL,
  `video_code` text NOT NULL,
  `video_preview` varchar(255) NOT NULL,
  PRIMARY KEY (`video_id`),
  KEY `video_id` (`video_id`,`product_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_product_labels`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_product_labels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `name_en-GB` varchar(255) NOT NULL,
  `name_de-DE` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_shipping_ext_calc`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_shipping_ext_calc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `alias` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `params` longtext NOT NULL,
  `shipping_method` text NOT NULL,
  `published` tinyint(1) NOT NULL,
  `ordering` int(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `alias` (`alias`),
  KEY `published` (`published`),
  KEY `ordering` (`ordering`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_shipping_method`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_shipping_method` (
  `shipping_id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(100) NOT NULL,
  `name_en-GB` varchar(100) NOT NULL,
  `description_en-GB` text NOT NULL,
  `name_de-DE` varchar(100) NOT NULL,
  `description_de-DE` text NOT NULL,
  `published` tinyint(1) NOT NULL,
  `payments` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `ordering` int(6) NOT NULL,
  `params` longtext NOT NULL,
  PRIMARY KEY (`shipping_id`),
  KEY `alias` (`alias`),
  KEY `published` (`published`),
  KEY `ordering` (`ordering`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_shipping_method_price`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_shipping_method_price` (
  `sh_pr_method_id` int(11) NOT NULL AUTO_INCREMENT,
  `shipping_method_id` int(11) NOT NULL,
  `shipping_tax_id` int(11) NOT NULL,
  `shipping_stand_price` decimal(14,4) NOT NULL,
  `package_tax_id` int(11) NOT NULL,
  `package_stand_price` decimal(14,4) NOT NULL,
  `delivery_times_id` int(11) NOT NULL,
  `params` longtext NOT NULL,
  PRIMARY KEY (`sh_pr_method_id`),
  KEY `shipping_method_id` (`shipping_method_id`),
  KEY `shipping_tax_id` (`shipping_tax_id`),
  KEY `package_tax_id` (`package_tax_id`),
  KEY `delivery_times_id` (`delivery_times_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_shipping_method_price_countries`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_shipping_method_price_countries` (
  `sh_method_country_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `sh_pr_method_id` int(11) NOT NULL,
  PRIMARY KEY (`sh_method_country_id`),
  KEY `country_id` (`country_id`),
  KEY `sh_pr_method_id` (`sh_pr_method_id`),
  KEY `sh_method_country_id` (`sh_method_country_id`,`country_id`,`sh_pr_method_id`),
  KEY `country_id_2` (`country_id`,`sh_pr_method_id`),
  KEY `sh_method_country_id_2` (`sh_method_country_id`,`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_shipping_method_price_weight`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_shipping_method_price_weight` (
  `sh_pr_weight_id` int(11) NOT NULL AUTO_INCREMENT,
  `sh_pr_method_id` int(11) NOT NULL,
  `shipping_price` decimal(12,2) NOT NULL,
  `shipping_weight_from` decimal(14,4) NOT NULL,
  `shipping_weight_to` decimal(14,4) NOT NULL,
  `shipping_package_price` decimal(14,4) NOT NULL,
  PRIMARY KEY (`sh_pr_weight_id`),
  KEY `sh_pr_method_id` (`sh_pr_method_id`),
  KEY `sh_pr_weight_id` (`sh_pr_weight_id`,`sh_pr_method_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_taxes`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_taxes` (
  `tax_id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_name` varchar(50) NOT NULL,
  `tax_value` decimal(12,2) NOT NULL,
  PRIMARY KEY (`tax_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_taxes_ext`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_taxes_ext` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_id` int(11) NOT NULL,
  `zones` text NOT NULL,
  `tax` decimal(12,2) NOT NULL,
  `firma_tax` decimal(12,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_unit`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_unit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qty` int(11) NOT NULL DEFAULT '1',
  `name_de-DE` varchar(255) NOT NULL,
  `name_en-GB` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_usergroups`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_usergroups` (
  `usergroup_id` int(11) NOT NULL AUTO_INCREMENT,
  `usergroup_name` varchar(64) NOT NULL,
  `usergroup_discount` decimal(12,2) NOT NULL,
  `usergroup_description` text NOT NULL,
  `usergroup_is_default` tinyint(1) NOT NULL,
  `name_en-GB` varchar(255) NOT NULL,
  `name_de-DE` varchar(255) NOT NULL,
  PRIMARY KEY (`usergroup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_users`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_users` (
  `user_id` int(11) NOT NULL,
  `usergroup_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `shipping_id` int(11) NOT NULL,
  `u_name` varchar(150) NOT NULL,
  `number` varchar(32) NOT NULL,
  `title` tinyint(1) NOT NULL,
  `f_name` varchar(255) NOT NULL,
  `l_name` varchar(255) NOT NULL,
  `m_name` varchar(255) NOT NULL,
  `firma_name` varchar(100) NOT NULL,
  `client_type` tinyint(1) NOT NULL,
  `firma_code` varchar(100) NOT NULL,
  `tax_number` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `birthday` date NOT NULL,
  `street` varchar(255) NOT NULL,
  `street_nr` varchar(16) NOT NULL,
  `home` varchar(20) NOT NULL,
  `apartment` varchar(20) NOT NULL,
  `zip` varchar(20) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` int(11) DEFAULT NULL,
  `phone` varchar(20) NOT NULL,
  `mobil_phone` varchar(20) NOT NULL,
  `fax` varchar(20) NOT NULL,
  `ext_field_1` varchar(255) NOT NULL,
  `ext_field_2` varchar(255) NOT NULL,
  `ext_field_3` varchar(255) NOT NULL,
  `delivery_adress` tinyint(1) NOT NULL,
  `d_title` tinyint(1) NOT NULL,
  `d_f_name` varchar(255) NOT NULL,
  `d_l_name` varchar(255) NOT NULL,
  `d_m_name` varchar(255) NOT NULL,
  `d_firma_name` varchar(100) NOT NULL,
  `d_email` varchar(255) NOT NULL,
  `d_birthday` date NOT NULL,
  `d_street` varchar(255) NOT NULL,
  `d_street_nr` varchar(16) NOT NULL,
  `d_home` varchar(20) NOT NULL,
  `d_apartment` varchar(20) NOT NULL,
  `d_zip` varchar(20) NOT NULL,
  `d_city` varchar(100) NOT NULL,
  `d_state` varchar(100) NOT NULL,
  `d_country` int(11) NOT NULL,
  `d_phone` varchar(20) NOT NULL,
  `d_mobil_phone` varchar(20) NOT NULL,
  `d_fax` varchar(20) NOT NULL,
  `d_ext_field_1` varchar(255) NOT NULL,
  `d_ext_field_2` varchar(255) NOT NULL,
  `d_ext_field_3` varchar(255) NOT NULL,
  `lang` varchar(5) NOT NULL,
  UNIQUE KEY `user_id` (`user_id`),
  KEY `u_name` (`u_name`),
  KEY `usergroup_id` (`usergroup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_jshopping_vendors`
--

CREATE TABLE IF NOT EXISTS `yawnc_jshopping_vendors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_name` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `adress` varchar(255) NOT NULL,
  `city` varchar(100) NOT NULL,
  `zip` varchar(20) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` int(11) NOT NULL,
  `f_name` varchar(255) NOT NULL,
  `l_name` varchar(255) NOT NULL,
  `middlename` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `fax` varchar(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `benef_bank_info` varchar(64) NOT NULL,
  `benef_bic` varchar(64) NOT NULL,
  `benef_conto` varchar(64) NOT NULL,
  `benef_payee` varchar(64) NOT NULL,
  `benef_iban` varchar(64) NOT NULL,
  `benef_bic_bic` varchar(64) NOT NULL,
  `benef_swift` varchar(64) NOT NULL,
  `interm_name` varchar(64) NOT NULL,
  `interm_swift` varchar(64) NOT NULL,
  `identification_number` varchar(64) NOT NULL,
  `tax_number` varchar(64) NOT NULL,
  `additional_information` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `main` tinyint(1) NOT NULL,
  `publish` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_languages`
--

CREATE TABLE IF NOT EXISTS `yawnc_languages` (
  `lang_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `lang_code` char(7) NOT NULL,
  `title` varchar(50) NOT NULL,
  `title_native` varchar(50) NOT NULL,
  `sef` varchar(50) NOT NULL,
  `image` varchar(50) NOT NULL,
  `description` varchar(512) NOT NULL,
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `sitename` varchar(1024) NOT NULL DEFAULT '',
  `published` int(11) NOT NULL DEFAULT '0',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`lang_id`),
  UNIQUE KEY `idx_sef` (`sef`),
  UNIQUE KEY `idx_image` (`image`),
  UNIQUE KEY `idx_langcode` (`lang_code`),
  KEY `idx_access` (`access`),
  KEY `idx_ordering` (`ordering`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `yawnc_languages`
--

INSERT INTO `yawnc_languages` (`lang_id`, `lang_code`, `title`, `title_native`, `sef`, `image`, `description`, `metakey`, `metadesc`, `sitename`, `published`, `access`, `ordering`) VALUES
(1, 'en-GB', 'English (UK)', 'English (UK)', 'en', 'en', '', '', '', '', 1, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_menu`
--

CREATE TABLE IF NOT EXISTS `yawnc_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menutype` varchar(24) NOT NULL COMMENT 'The type of menu this item belongs to. FK to #__menu_types.menutype',
  `title` varchar(255) NOT NULL COMMENT 'The display title of the menu item.',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'The SEF alias of the menu item.',
  `note` varchar(255) NOT NULL DEFAULT '',
  `path` varchar(1024) NOT NULL COMMENT 'The computed path of the menu item based on the alias field.',
  `link` varchar(1024) NOT NULL COMMENT 'The actually link the menu item refers to.',
  `type` varchar(16) NOT NULL COMMENT 'The type of link: Component, URL, Alias, Separator',
  `published` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'The published state of the menu link.',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'The parent menu item in the menu tree.',
  `level` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The relative level in the tree.',
  `component_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to #__extensions.id',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to #__users.id',
  `checked_out_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'The time the menu item was checked out.',
  `browserNav` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'The click behaviour of the link.',
  `access` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The access level required to view the menu item.',
  `img` varchar(255) NOT NULL COMMENT 'The image of the menu item.',
  `template_style_id` int(10) unsigned NOT NULL DEFAULT '0',
  `params` text NOT NULL COMMENT 'JSON encoded data for the menu item.',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `home` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Indicates if this menu item is the home or default page.',
  `language` char(7) NOT NULL DEFAULT '',
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_client_id_parent_id_alias_language` (`client_id`,`parent_id`,`alias`,`language`),
  KEY `idx_componentid` (`component_id`,`menutype`,`published`,`access`),
  KEY `idx_menutype` (`menutype`),
  KEY `idx_left_right` (`lft`,`rgt`),
  KEY `idx_alias` (`alias`),
  KEY `idx_path` (`path`(255)),
  KEY `idx_language` (`language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=116 ;

--
-- Дамп данных таблицы `yawnc_menu`
--

INSERT INTO `yawnc_menu` (`id`, `menutype`, `title`, `alias`, `note`, `path`, `link`, `type`, `published`, `parent_id`, `level`, `component_id`, `checked_out`, `checked_out_time`, `browserNav`, `access`, `img`, `template_style_id`, `params`, `lft`, `rgt`, `home`, `language`, `client_id`) VALUES
(1, '', 'Menu_Item_Root', 'root', '', '', '', '', 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, 0, '', 0, '', 0, 71, 0, '*', 0),
(2, 'menu', 'com_banners', 'Banners', '', 'Banners', 'index.php?option=com_banners', 'component', 0, 1, 1, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners', 0, '', 1, 10, 0, '*', 1),
(3, 'menu', 'com_banners', 'Banners', '', 'Banners/Banners', 'index.php?option=com_banners', 'component', 0, 2, 2, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners', 0, '', 2, 3, 0, '*', 1),
(4, 'menu', 'com_banners_categories', 'Categories', '', 'Banners/Categories', 'index.php?option=com_categories&extension=com_banners', 'component', 0, 2, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-cat', 0, '', 4, 5, 0, '*', 1),
(5, 'menu', 'com_banners_clients', 'Clients', '', 'Banners/Clients', 'index.php?option=com_banners&view=clients', 'component', 0, 2, 2, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-clients', 0, '', 6, 7, 0, '*', 1),
(6, 'menu', 'com_banners_tracks', 'Tracks', '', 'Banners/Tracks', 'index.php?option=com_banners&view=tracks', 'component', 0, 2, 2, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-tracks', 0, '', 8, 9, 0, '*', 1),
(7, 'menu', 'com_contact', 'Contacts', '', 'Contacts', 'index.php?option=com_contact', 'component', 0, 1, 1, 8, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact', 0, '', 11, 16, 0, '*', 1),
(8, 'menu', 'com_contact', 'Contacts', '', 'Contacts/Contacts', 'index.php?option=com_contact', 'component', 0, 7, 2, 8, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact', 0, '', 12, 13, 0, '*', 1),
(9, 'menu', 'com_contact_categories', 'Categories', '', 'Contacts/Categories', 'index.php?option=com_categories&extension=com_contact', 'component', 0, 7, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact-cat', 0, '', 14, 15, 0, '*', 1),
(10, 'menu', 'com_messages', 'Messaging', '', 'Messaging', 'index.php?option=com_messages', 'component', 0, 1, 1, 15, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages', 0, '', 17, 22, 0, '*', 1),
(11, 'menu', 'com_messages_add', 'New Private Message', '', 'Messaging/New Private Message', 'index.php?option=com_messages&task=message.add', 'component', 0, 10, 2, 15, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages-add', 0, '', 18, 19, 0, '*', 1),
(12, 'menu', 'com_messages_read', 'Read Private Message', '', 'Messaging/Read Private Message', 'index.php?option=com_messages', 'component', 0, 10, 2, 15, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages-read', 0, '', 20, 21, 0, '*', 1),
(13, 'menu', 'com_newsfeeds', 'News Feeds', '', 'News Feeds', 'index.php?option=com_newsfeeds', 'component', 0, 1, 1, 17, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds', 0, '', 23, 28, 0, '*', 1),
(14, 'menu', 'com_newsfeeds_feeds', 'Feeds', '', 'News Feeds/Feeds', 'index.php?option=com_newsfeeds', 'component', 0, 13, 2, 17, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds', 0, '', 24, 25, 0, '*', 1),
(15, 'menu', 'com_newsfeeds_categories', 'Categories', '', 'News Feeds/Categories', 'index.php?option=com_categories&extension=com_newsfeeds', 'component', 0, 13, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds-cat', 0, '', 26, 27, 0, '*', 1),
(16, 'menu', 'com_redirect', 'Redirect', '', 'Redirect', 'index.php?option=com_redirect', 'component', 0, 1, 1, 24, 0, '0000-00-00 00:00:00', 0, 0, 'class:redirect', 0, '', 29, 30, 0, '*', 1),
(17, 'menu', 'com_search', 'Basic Search', '', 'Basic Search', 'index.php?option=com_search', 'component', 0, 1, 1, 19, 0, '0000-00-00 00:00:00', 0, 0, 'class:search', 0, '', 31, 32, 0, '*', 1),
(18, 'menu', 'com_finder', 'Smart Search', '', 'Smart Search', 'index.php?option=com_finder', 'component', 0, 1, 1, 27, 0, '0000-00-00 00:00:00', 0, 0, 'class:finder', 0, '', 33, 34, 0, '*', 1),
(19, 'menu', 'com_joomlaupdate', 'Joomla! Update', '', 'Joomla! Update', 'index.php?option=com_joomlaupdate', 'component', 1, 1, 1, 28, 0, '0000-00-00 00:00:00', 0, 0, 'class:joomlaupdate', 0, '', 35, 36, 0, '*', 1),
(20, 'main', 'com_tags', 'Tags', '', 'Tags', 'index.php?option=com_tags', 'component', 0, 1, 1, 29, 0, '0000-00-00 00:00:00', 0, 1, 'class:tags', 0, '', 37, 38, 0, '', 1),
(21, 'main', 'com_postinstall', 'Post-installation messages', '', 'Post-installation messages', 'index.php?option=com_postinstall', 'component', 0, 1, 1, 32, 0, '0000-00-00 00:00:00', 0, 1, 'class:postinstall', 0, '', 39, 40, 0, '*', 1),
(101, 'mainmenu', 'Главная', 'home', '', 'home', 'index.php?option=com_content&view=featured', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{"featured_categories":[""],"layout_type":"blog","num_leading_articles":"1","num_intro_articles":"3","num_columns":"3","num_links":"0","multi_column_order":"1","orderby_pri":"","orderby_sec":"front","order_date":"","show_pagination":"2","show_pagination_results":"1","show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_readmore":"","show_readmore_title":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_tags":"","show_noauth":"","show_feed_link":"1","feed_summary":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 41, 42, 1, '*', 0),
(102, 'mainmenu', 'Товары', 'tovary', '', 'tovary', 'index.php?option=com_content&view=category&id=2', 'component', -2, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{"show_category_title":"","show_description":"","show_description_image":"","maxLevel":"","show_empty_categories":"","show_no_articles":"","show_category_heading_title_text":"","show_subcat_desc":"","show_cat_num_articles":"","show_cat_tags":"","page_subheading":"","show_pagination_limit":"","filter_field":"","show_headings":"","list_show_date":"","date_format":"","list_show_hits":"","list_show_author":"","orderby_pri":"","orderby_sec":"","order_date":"","show_pagination":"","show_pagination_results":"","display_num":"10","show_featured":"","show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_readmore":"","show_readmore_title":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","show_feed_link":"","feed_summary":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 43, 44, 0, '*', 0),
(103, 'mainmenu', 'Контакты', 'material', '', 'material', 'index.php?option=com_content&view=form&layout=edit', 'component', -2, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{"enable_category":"1","catid":"2","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 45, 46, 0, '*', 0),
(111, 'mainmenu', 'Товары', 'tov', '', 'tov', 'index.php?option=com_jshopping&view=products', 'component', 1, 1, 1, 10003, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 61, 62, 0, '*', 0),
(112, 'mainmenu', 'Как купить', 'kypit', '', 'kypit', 'index.php?option=com_content&view=form&layout=edit', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{"enable_category":"0","catid":"2","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 63, 64, 0, '*', 0),
(113, 'mainmenu', 'Статьи', 'stati', '', 'stati', 'index.php?option=com_content&view=form&layout=edit', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{"enable_category":"0","catid":"2","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 65, 66, 0, '*', 0),
(114, 'mainmenu', 'Пожелания', 'pozhelaniya', '', 'pozhelaniya', 'index.php?option=com_content&view=form&layout=edit', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{"enable_category":"0","catid":"2","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 67, 68, 0, '*', 0),
(115, 'mainmenu', 'Контакты', 'kontakty', '', 'kontakty', 'index.php?option=com_content&view=form&layout=edit', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{"enable_category":"0","catid":"2","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 69, 70, 0, '*', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_menu_types`
--

CREATE TABLE IF NOT EXISTS `yawnc_menu_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menutype` varchar(24) NOT NULL,
  `title` varchar(48) NOT NULL,
  `description` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_menutype` (`menutype`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `yawnc_menu_types`
--

INSERT INTO `yawnc_menu_types` (`id`, `menutype`, `title`, `description`) VALUES
(1, 'mainmenu', 'Main Menu', 'The main menu for the site');

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_messages`
--

CREATE TABLE IF NOT EXISTS `yawnc_messages` (
  `message_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id_from` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id_to` int(10) unsigned NOT NULL DEFAULT '0',
  `folder_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `priority` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `subject` varchar(255) NOT NULL DEFAULT '',
  `message` text NOT NULL,
  PRIMARY KEY (`message_id`),
  KEY `useridto_state` (`user_id_to`,`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_messages_cfg`
--

CREATE TABLE IF NOT EXISTS `yawnc_messages_cfg` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `cfg_name` varchar(100) NOT NULL DEFAULT '',
  `cfg_value` varchar(255) NOT NULL DEFAULT '',
  UNIQUE KEY `idx_user_var_name` (`user_id`,`cfg_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_modules`
--

CREATE TABLE IF NOT EXISTS `yawnc_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `title` varchar(100) NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `position` varchar(50) NOT NULL DEFAULT '',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `module` varchar(50) DEFAULT NULL,
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `showtitle` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `params` text NOT NULL,
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `published` (`published`,`access`),
  KEY `newsfeeds` (`module`,`published`),
  KEY `idx_language` (`language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=97 ;

--
-- Дамп данных таблицы `yawnc_modules`
--

INSERT INTO `yawnc_modules` (`id`, `asset_id`, `title`, `note`, `content`, `ordering`, `position`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `published`, `module`, `access`, `showtitle`, `params`, `client_id`, `language`) VALUES
(1, 39, 'Главное Меню', '', '', 1, 'top-menu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 0, '{"menutype":"mainmenu","base":"","startLevel":"1","endLevel":"0","showAllChildren":"1","tag_id":"","class_sfx":"nav nav-list","window_open":"","layout":"_:default","moduleclass_sfx":"_menu","cache":"1","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(2, 40, 'Login', '', '', 1, 'login', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_login', 1, 1, '', 1, '*'),
(3, 41, 'Popular Articles', '', '', 3, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_popular', 3, 1, '{"count":"5","catid":"","user_id":"0","layout":"_:default","moduleclass_sfx":"","cache":"0"}', 1, '*'),
(4, 42, 'Recently Added Articles', '', '', 4, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_latest', 3, 1, '{"count":"5","ordering":"c_dsc","catid":"","user_id":"0","layout":"_:default","moduleclass_sfx":"","cache":"0"}', 1, '*'),
(8, 43, 'Toolbar', '', '', 1, 'toolbar', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_toolbar', 3, 1, '', 1, '*'),
(9, 44, 'Quick Icons', '', '', 1, 'icon', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_quickicon', 3, 1, '', 1, '*'),
(10, 45, 'Logged-in Users', '', '', 2, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_logged', 3, 1, '{"count":"5","name":"1","layout":"_:default","moduleclass_sfx":"","cache":"0"}', 1, '*'),
(12, 46, 'Admin Menu', '', '', 1, 'menu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 3, 1, '{"layout":"","moduleclass_sfx":"","shownew":"1","showhelp":"1","cache":"0"}', 1, '*'),
(13, 47, 'Admin Submenu', '', '', 1, 'submenu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_submenu', 3, 1, '', 1, '*'),
(14, 48, 'User Status', '', '', 2, 'status', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_status', 3, 1, '', 1, '*'),
(15, 49, 'Title', '', '', 1, 'title', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_title', 3, 1, '', 1, '*'),
(16, 50, 'Login Form', '', '', 7, 'position-7', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_login', 1, 1, '{"greeting":"1","name":"0"}', 0, '*'),
(17, 51, 'Breadcrumbs', '', '', 1, 'position-2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_breadcrumbs', 1, 1, '{"showHere":"1","showHome":"1","homeText":"","showLast":"1","separator":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(79, 52, 'Multilanguage status', '', '', 1, 'status', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_multilangstatus', 3, 1, '{"layout":"_:default","moduleclass_sfx":"","cache":"0"}', 1, '*'),
(86, 53, 'Joomla Version', '', '', 1, 'footer', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_version', 3, 1, '{"format":"short","product":"1","layout":"_:default","moduleclass_sfx":"","cache":"0"}', 1, '*'),
(87, 55, 'Верхнее меню', '', '', 1, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', -2, 'mod_menu', 1, 1, '{"menutype":"mainmenu","base":"","startLevel":"1","endLevel":"0","showAllChildren":"1","tag_id":"","class_sfx":"","window_open":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(88, 56, 'Поиск', '', '', 1, 'search', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_finder', 1, 0, '{"searchfilter":"","show_autosuggest":"1","show_advanced":"0","field_size":20,"show_label":"0","label_pos":"left","alt_label":"","show_button":"0","button_pos":"left","opensearch":"1","opensearch_title":"Андреевская Лавка","set_itemid":"0","layout":"_:default","moduleclass_sfx":"","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(89, 57, 'Логотип', '', '<p><a href="/"><img src="images/2015/09/logo.png" alt="" /></a></p>', 1, 'logo', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(90, 58, 'Телефон', '', '+7 (901)374-68-73', 1, 'telephone', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(91, 59, 'Текст на баннере', '', 'Часть доходов Лавки<br>\r\nпереводится на восстановление<br>\r\n<a href="aleksandrovka.com">Храма в Александровке<br></a>', 1, 'link', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', -2, 'mod_custom', 1, 0, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(92, 60, 'Баннер', '', '<div class="link">Часть доходов Лавки<br>\r\nпереводится на восстановление<br>\r\n<a href="http://aleksandrovka.com/" target="_blank">Храма в Александровке</a>\r\n</div>', 1, 'banner', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(93, 64, 'Контент', '', '<div class="picture1">\r\n<div class="name"><p>Иконы Храмовые, аналойные,<br>малые и средние,<br>бархатные складни.</p></div>\r\n<div class="image"><img src="images/2015/09/icon.jpg" alt="" /></div>\r\n<div class="text"><p>У нас Вы можете купить<br>иконы для Храма или дома.<br>Аналойные иконы ручной<br>работы станут прекрасным<br>подарком.</p>\r\n</div>\r\n<div class="catalog"><a class="category-link" href="#">В КАТАЛОГ</a></div>\r\n</div>\r\n<div class="picture2">\r\n<div class="name"><p>Свечи восковые,<br>парафиновые,<br>восковые красные.</p></div>\r\n<div class="image"><img src="images/2015/09/candle.jpg" alt="" /></div>\r\n<div class="text"><p>Наши свечи произведены из<br>натурального пчелиного<br>воска. Они имеют натуральн-<br>ый восковой цвет и приятный<br>восковой запах, они не<br>коптят, не трещат.</p></div>\r\n<div class="catalog"><a class="category-link" href="#">В КАТАЛОГ</a></div>\r\n</div>\r\n<div class="picture3">\r\n<div class="name"><p>Книги, полиграфия,<br>молитвы на бересте.</p></div>\r\n<div class="image"><img src="images/2015/09/book.jpg"/></div>\r\n<div class="text"><p>Полноцветная высоко-<br>качественная печать,<br>молитвы, соответствующие<br>лику святого...</p></div>\r\n<div class="catalog"><a class="category-link" href="#">В КАТАЛОГ</a></div>\r\n</div>', 1, 'content', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(94, 71, 'Новости', '', '', 1, 'news', 0, '0000-00-00 00:00:00', '2015-09-07 16:05:24', '0000-00-00 00:00:00', 1, 'mod_articles_news', 1, 1, '{"catid":["9"],"image":"0","item_title":"1","link_titles":"","item_heading":"h4","showLastSeparator":"1","readmore":"1","count":"5","ordering":"a.created","direction":"1","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"p","header_class":"","style":"0"}', 0, '*'),
(95, 72, 'Статья', '', '<div class="text-article"><h3>Продажа церковной утвари</h3>\r\n<p>С принятием Православия началась новая эпоха в жизни рус-\r\nского народа, его духовное формирование и тесная взаимо-\r\nсвязь с церковью. Даже сегодня, несмотря на научный про-\r\nгресс, полеты в космос и развитие компьютерных технологий,\r\nрусский человек нередко обращается к Высшим силам. Много-<a href="http://aleksandrovka.com/" target="_blank"><img src="images/2015/09/image-article.png" class="image-article"></a>\r\nчисленные церковные праздники \r\nи посты, обряд венчания молодо-\r\nженов и прощания с умершими -\r\nдля соблюдения всех этих право-\r\nславных традиций необходима \r\nцерковная утварь. И вот здесь \r\nчеловек нередко оказывается \r\nв затрудни тельном положении:\r\nтак, если хлеб и пакет молока \r\nмы покупаем ежедневно в про-\r\nдуктовом супермаркете, то \r\nутварь для церкви, венчальные \r\nсвечи, неугасимые лампады или \r\nлампадное масло относятся к, \r\nтак называемым, "специальным"\r\n</p></div>\r\n<div class="link1-article"><a href="http://aleksandrovka.com/" target="_blank">Александровское собрание</a></div>\r\n<div class="link2-article"><a href="http://aleksandrovka.com/" target="_blank">Храм в Александровке</a></div>', 1, 'article', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(96, 73, 'Footer', '', '<div class="footer-content1"><h4>Подписка на наши новости</h4>\r\n<div class="mail">\r\n<p>Ваш почтовый ящик</p>\r\n<input type="text" size="30" placeholder="E-mail" >\r\n</div>\r\n<div class="checkbox">\r\n<label><input type="checkbox">       Получать новости</div></label>\r\n</div>\r\n<div class="footer-content2"><h4>Наши контакты</h4>\r\n<p>+7 (901)374-68-73</p>\r\n</div>\r\n<div class="footer-content3"><h4>Пожертвования</h4>\r\n<a href="http://aleksandrovka.com/" target="_blank">Помочь на восстановление Храма</a>\r\n</div>', 1, 'footer', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*');

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_modules_menu`
--

CREATE TABLE IF NOT EXISTS `yawnc_modules_menu` (
  `moduleid` int(11) NOT NULL DEFAULT '0',
  `menuid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`moduleid`,`menuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yawnc_modules_menu`
--

INSERT INTO `yawnc_modules_menu` (`moduleid`, `menuid`) VALUES
(1, 0),
(2, 0),
(3, 0),
(4, 0),
(6, 0),
(7, 0),
(8, 0),
(9, 0),
(10, 0),
(12, 0),
(13, 0),
(14, 0),
(15, 0),
(16, 0),
(17, 0),
(79, 0),
(86, 0),
(87, 0),
(88, 0),
(89, 0),
(90, 0),
(91, 0),
(92, 0),
(93, 0),
(94, 0),
(95, 0),
(96, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_newsfeeds`
--

CREATE TABLE IF NOT EXISTS `yawnc_newsfeeds` (
  `catid` int(11) NOT NULL DEFAULT '0',
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `link` varchar(200) NOT NULL DEFAULT '',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `numarticles` int(10) unsigned NOT NULL DEFAULT '1',
  `cache_time` int(10) unsigned NOT NULL DEFAULT '3600',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rtl` tinyint(4) NOT NULL DEFAULT '0',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `metadata` text NOT NULL,
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `description` text NOT NULL,
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `images` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`published`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`),
  KEY `idx_language` (`language`),
  KEY `idx_xreference` (`xreference`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_overrider`
--

CREATE TABLE IF NOT EXISTS `yawnc_overrider` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `constant` varchar(255) NOT NULL,
  `string` text NOT NULL,
  `file` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2261 ;

--
-- Дамп данных таблицы `yawnc_overrider`
--

INSERT INTO `yawnc_overrider` (`id`, `constant`, `string`, `file`) VALUES
(1, 'COM_AJAX', 'Интерфейс Ajax', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_ajax.ini'),
(2, 'COM_AJAX_XML_DESCRIPTION', 'Расширяемый интерфейс Ajax для Joomla', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_ajax.ini'),
(3, 'COM_AJAX_SPECIFY_FORMAT', 'Пожалуйста, укажите корректный формат данных ответа (отличный от HTML, например json, raw, debug).', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_ajax.ini'),
(4, 'COM_AJAX_METHOD_NOT_EXISTS', 'Метод %s не существует', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_ajax.ini'),
(5, 'COM_AJAX_FILE_NOT_EXISTS', 'Файл %s не существует', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_ajax.ini'),
(6, 'COM_AJAX_MODULE_NOT_ACCESSIBLE', 'Модуль %s не опубликован, у вас остутствуют права доступа на модуль или модуль не назначен для текущего пункта меню', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_ajax.ini'),
(7, 'COM_CONFIG', 'Панель управления сайтом', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_config.ini'),
(8, 'COM_CONFIG_CONFIGURATION', 'Параметры конфигурации сайта', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_config.ini'),
(9, 'COM_CONFIG_ERROR_CONTROLLER_NOT_FOUND', 'Контроллер не найден!', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_config.ini'),
(10, 'COM_CONFIG_FIELD_DEFAULT_ACCESS_LEVEL_DESC', 'Выбор уровня доступа по умолчанию для новых материалов, пунктов меню и прочих элементов, создаваемых на сайте.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_config.ini'),
(11, 'COM_CONFIG_FIELD_DEFAULT_ACCESS_LEVEL_LABEL', 'Уровень доступа по&#160;умолчанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_config.ini'),
(12, 'COM_CONFIG_FIELD_DEFAULT_LIST_LIMIT_DESC', 'Устанавливает для всех пользователей значение длины списков объектов в рабочих областях менеджеров панели управления по умолчанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_config.ini'),
(13, 'COM_CONFIG_FIELD_DEFAULT_LIST_LIMIT_LABEL', 'Длина списка по&#160;умолчанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_config.ini'),
(14, 'COM_CONFIG_FIELD_METADESC_DESC', 'Введите общее описание веб-сайта, которое следует передавать поисковым системам. Как правило, оптимальным является описание длиной не более 20 слов.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_config.ini'),
(15, 'COM_CONFIG_FIELD_METADESC_LABEL', 'Мета-тег Description для сайта', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_config.ini'),
(16, 'COM_CONFIG_FIELD_METAKEYS_DESC', 'Введите ключевые слова и фразы, которые лучше всего описывают ваш сайт. Отдельные ключевые слова и фразы разделяйте с помощью запятой.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_config.ini'),
(17, 'COM_CONFIG_FIELD_METAKEYS_LABEL', 'Мета-тег Keywords', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_config.ini'),
(18, 'COM_CONFIG_FIELD_SEF_URL_DESC', 'Определяет, следует ли оптимизировать URL-адреса страниц для поисковых систем.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_config.ini'),
(19, 'COM_CONFIG_FIELD_SEF_URL_LABEL', 'Включить SEF (ЧПУ)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_config.ini'),
(20, 'COM_CONFIG_FIELD_SITE_NAME_DESC', 'Введите название вашего веб-сайта. Введённое значение будет использоваться в различных местах (например, в заголовке панели управления и на странице <em>Сайт выключен</em>).', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_config.ini'),
(21, 'COM_CONFIG_FIELD_SITE_NAME_LABEL', 'Название сайта', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_config.ini'),
(22, 'COM_CONFIG_FIELD_VALUE_AFTER', 'После', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_config.ini'),
(23, 'COM_CONFIG_FIELD_VALUE_BEFORE', 'До', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_config.ini'),
(24, 'COM_CONFIG_FIELD_SITE_OFFLINE_DESC', 'Позволяет заблокировать доступ к сайту для всех пользователей. Если&#160;выбрать <strong>Да</strong>, на странице сайта будет отображаться сообщение, заданное в поле <strong>Сообщение при выключенном сайте</strong>.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_config.ini'),
(25, 'COM_CONFIG_FIELD_SITE_OFFLINE_LABEL', 'Сайт выключен (offline)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_config.ini'),
(26, 'COM_CONFIG_FIELD_SITENAME_PAGETITLES_DESC', 'Включать название сайта в заголовки страниц (до или после названия текущей страницы). Например, Название сайта - Название материала.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_config.ini'),
(27, 'COM_CONFIG_FIELD_SITENAME_PAGETITLES_LABEL', 'Включать название сайта в&#160;заголовок страницы', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_config.ini'),
(28, 'COM_CONFIG_METADATA_SETTINGS', 'Параметры метаданных', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_config.ini'),
(29, 'COM_CONFIG_MODULES_MODULE_NAME', 'Название модуля', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_config.ini'),
(30, 'COM_CONFIG_MODULES_MODULE_TYPE', 'Тип модуля', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_config.ini'),
(31, 'COM_CONFIG_MODULES_SETTINGS_TITLE', 'Параметры модуля', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_config.ini'),
(32, 'COM_CONFIG_MODULES_SAVE_SUCCESS', 'Модуль успешно сохранен.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_config.ini'),
(33, 'COM_CONFIG_SAVE_SUCCESS', 'Настройки успешно сохранены.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_config.ini'),
(34, 'COM_CONFIG_SEO_SETTINGS', 'Параметры SEO', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_config.ini'),
(35, 'COM_CONFIG_SITE_SETTINGS', 'Параметры сайта', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_config.ini'),
(36, 'COM_CONFIG_TEMPLATE_SETTINGS', 'Параметры шаблона', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_config.ini'),
(37, 'COM_CONFIG_XML_DESCRIPTION', 'Менеджер конфигурации сайта', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_config.ini'),
(38, 'COM_CONTACT_ADDRESS', 'Адрес', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(39, 'COM_CONTACT_ARTICLES_HEADING', 'Материалы контакта', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(40, 'COM_CONTACT_CAPTCHA_LABEL', 'CAPTCHA', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(41, 'COM_CONTACT_CAPTCHA_DESC', 'Введите текст, который вы видите на картинке.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(42, 'COM_CONTACT_CAT_NUM', 'Кол-во контактов:', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(43, 'COM_CONTACT_CONTACT_EMAIL_A_COPY_DESC', 'Отправляет копию данного сообщения на указанный вами адрес.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(44, 'COM_CONTACT_CONTACT_EMAIL_A_COPY_LABEL', 'Отправить копию этого сообщения на ваш адрес', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(45, 'COM_CONTACT_CONTACT_EMAIL_NAME_DESC', 'Ваше имя', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(46, 'COM_CONTACT_CONTACT_EMAIL_NAME_LABEL', 'Имя', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(47, 'COM_CONTACT_CONTACT_ENTER_MESSAGE_DESC', 'Введите текст вашего сообщения', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(48, 'COM_CONTACT_CONTACT_ENTER_MESSAGE_LABEL', 'Сообщение', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(49, 'COM_CONTACT_CONTACT_ENTER_VALID_EMAIL', 'Пожалуйста, введите корректный адрес электронной почты.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(50, 'COM_CONTACT_CONTENT_TYPE_CONTACT', 'Контакт', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(51, 'COM_CONTACT_CONTENT_TYPE_CATEGORY', 'Категория контактов', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(52, 'COM_CONTACT_FILTER_LABEL', 'Фильтр', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(53, 'COM_CONTACT_FILTER_SEARCH_DESC', 'Фильтр поиска по контактам', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(54, 'COM_CONTACT_CONTACT_MESSAGE_SUBJECT_DESC', 'Тема сообщения', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(55, 'COM_CONTACT_CONTACT_MESSAGE_SUBJECT_LABEL', 'Тема', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(56, 'COM_CONTACT_CONTACT_SEND', 'Отправить сообщение', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(57, 'COM_CONTACT_COPYSUBJECT_OF', 'Копия: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(58, 'COM_CONTACT_COPYTEXT_OF', 'Это копия сообщения, которое вы отправили %s через %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(59, 'COM_CONTACT_COUNT', 'Кол-во контактов:', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(60, 'COM_CONTACT_COUNTRY', 'Страна', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(61, 'COM_CONTACT_DEFAULT_PAGE_TITLE', 'Контакты', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(62, 'COM_CONTACT_DETAILS', 'Контакт', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(63, 'COM_CONTACT_DOWNLOAD_INFORMATION_AS', 'Загрузить информацию как:', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(64, 'COM_CONTACT_EMAIL_BANNEDTEXT', 'Поле ''%s'' вашего письма содержит фрагмент текста, который не допускается на этом сайте.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(65, 'COM_CONTACT_EMAIL_DESC', 'Адрес электронной почты контакта', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(66, 'COM_CONTACT_EMAIL_FORM', 'Форма обратной связи', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(67, 'COM_CONTACT_EMAIL_LABEL', 'E-mail', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(68, 'COM_CONTACT_EMAIL_THANKS', 'Спасибо за ваше письмо!', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(69, 'COM_CONTACT_ENQUIRY_TEXT', 'Это письмо отправлено с сайта %s от:', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(70, 'COM_CONTACT_ERROR_CONTACT_NOT_FOUND', 'Контакт не найден', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(71, 'COM_CONTACT_FAX', 'Факс', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(72, 'COM_CONTACT_FAX_NUMBER', 'Факс: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(73, 'COM_CONTACT_FORM_LABEL', 'Отправить сообщение. Все поля, отмеченные звёздочкой, являются обязательными.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(74, 'COM_CONTACT_FORM_NC', 'Пожалуйста, убедитесь, что форма заполнена правильно и полностью.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(75, 'COM_CONTACT_IMAGE_DETAILS', 'Изображение контакта', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(76, 'COM_CONTACT_LINKS', 'Ссылки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(77, 'COM_CONTACT_MAILENQUIRY', '%s контакт', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(78, 'COM_CONTACT_MOBILE', 'Мобильный', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(79, 'COM_CONTACT_MOBILE_NUMBER', 'Мобильный: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(80, 'COM_CONTACT_NO_CONTACTS', 'Нет контактов', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(81, 'COM_CONTACT_NOT_MORE_THAN_ONE_EMAIL_ADDRESS', 'Вы не можете ввести более одного адреса электронной почты.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(82, 'COM_CONTACT_OPTIONAL', '(опционально)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(83, 'COM_CONTACT_OTHER_INFORMATION', 'Дополнительная информация', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(84, 'COM_CONTACT_POSITION', 'Должность', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(85, 'COM_CONTACT_PROFILE', 'Профиль', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(86, 'COM_CONTACT_PROFILE_HEADING', 'Профиль контакта', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(87, 'COM_CONTACT_SELECT_CONTACT', 'Выберите контакт:', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(88, 'COM_CONTACT_STATE', 'Область', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(89, 'COM_CONTACT_SUBURB', 'Город', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(90, 'COM_CONTACT_TELEPHONE', 'Телефон', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(91, 'COM_CONTACT_TELEPHONE_NUMBER', 'Телефон: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(92, 'COM_CONTACT_VCARD', 'Визитная карточка (vCard)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(93, 'COM_CONTACT_NO_ARTICLES', 'Нет материалов', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_contact.ini'),
(94, 'COM_CONTENT_ACCESS_DELETE_DESC', 'Унаследованное состояние права на действие <strong>Удалять</strong> для данного материала, а так же суммарное состояние, вычисленное для выбранного меню.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(95, 'COM_CONTENT_ACCESS_EDITSTATE_DESC', 'Унаследованное состояние права на действие <strong>Изменять состояние</strong> для данного материала, а так же суммарное состояние, на основе выбранного меню.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(96, 'COM_CONTENT_ACCESS_EDIT_DESC', 'Унаследованное состояние права на действие <strong>Изменять</strong> для данного материала, а так же суммарное состояние, на основе выбранного меню.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(97, 'COM_CONTENT_ARTICLE_CONTENT', 'Материалы', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(98, 'COM_CONTENT_ARTICLE_HITS', 'Просмотров: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(99, 'COM_CONTENT_ARTICLE_INFO', 'Подробности', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(100, 'COM_CONTENT_ARTICLE_VOTE_SUCCESS', 'Спасибо за оценку!', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(101, 'COM_CONTENT_ARTICLE_VOTE_FAILURE', 'Вы уже сегодня оценивали этот материал!', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(102, 'COM_CONTENT_AUTHOR_FILTER_LABEL', 'Фильтр по автору', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(103, 'COM_CONTENT_CATEGORY', 'Категория: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(104, 'COM_CONTENT_CHECKED_OUT_BY', 'Заблокировано %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(105, 'COM_CONTENT_CONTENT_TYPE_ARTICLE', 'Материал', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(106, 'COM_CONTENT_CONTENT_TYPE_CATEGORY', 'Категория материалов', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(107, 'COM_CONTENT_CREATE_ARTICLE', 'Создать новый материал', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(108, 'COM_CONTENT_CREATED_DATE', 'Дата создания', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(109, 'COM_CONTENT_CREATED_DATE_ON', 'Создано: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(110, 'COM_CONTENT_EDIT_ITEM', 'Изменить материал', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(111, 'COM_CONTENT_ERROR_ARTICLE_NOT_FOUND', 'Материал не найден', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(112, 'COM_CONTENT_ERROR_LOGIN_TO_VIEW_ARTICLE', 'Для просмотра данного материала необходимо пройти авторизацию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(113, 'COM_CONTENT_ERROR_PARENT_CATEGORY_NOT_FOUND', 'Родительская категория не найдена', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(114, 'COM_CONTENT_FEED_READMORE', 'Подробнее...', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(115, 'COM_CONTENT_FILTER_SEARCH_DESC', 'Фильтр поиска по материалам', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(116, 'COM_CONTENT_FORM_EDIT_ARTICLE', 'Изменить материал', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(117, 'COM_CONTENT_HEADING_TITLE', 'Заголовок', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(118, 'COM_CONTENT_HITS_FILTER_LABEL', 'Фильтр по кол-ву просмотров', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(119, 'COM_CONTENT_INTROTEXT', 'Материал должен содержать текст', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(120, 'COM_CONTENT_INVALID_RATING', 'Рейтинг статьи: Неверный рейтинг: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(121, 'COM_CONTENT_LAST_UPDATED', 'Обновлено: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(122, 'COM_CONTENT_METADATA', 'Метаданные', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(123, 'COM_CONTENT_MODIFIED_DATE', 'Дата изменения', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(124, 'COM_CONTENT_MONTH', 'Месяц', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(125, 'COM_CONTENT_MORE_ARTICLES', 'Ещё статьи...', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(126, 'COM_CONTENT_NEW_ARTICLE', 'Новый материал', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(127, 'COM_CONTENT_NO_ARTICLES', 'В данной категории нет материалов.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(128, 'COM_CONTENT_NONE', 'Нет', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(129, 'COM_CONTENT_NUM_ITEMS', 'Кол-во материалов:', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(130, 'COM_CONTENT_ON_NEW_CONTENT', 'Пользователем ''%1$s'' был создан новый материал с заголовком ''%2$s''.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(131, 'COM_CONTENT_ORDERING', 'Порядок:<br />Новая статья по умолчанию занимает первую позицию в категории. Порядок можно потом изменить.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(132, 'COM_CONTENT_PAGEBREAK_DOC_TITLE', 'Разрыв страницы', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(133, 'COM_CONTENT_PAGEBREAK_INSERT_BUTTON', 'Вставить разрыв страницы', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(134, 'COM_CONTENT_PAGEBREAK_TITLE', 'Заголовок страницы:', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(135, 'COM_CONTENT_PAGEBREAK_TOC', 'Заголовок оглавления:', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(136, 'COM_CONTENT_PARENT', 'Родительская категория: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(137, 'COM_CONTENT_PUBLISHED_DATE', 'Дата публикации', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(138, 'COM_CONTENT_PUBLISHED_DATE_ON', 'Опубликовано: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(139, 'COM_CONTENT_PUBLISHING', 'Публикация', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(140, 'COM_CONTENT_READ_MORE', 'Подробнее: ', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(141, 'COM_CONTENT_READ_MORE_TITLE', 'Подробнее...', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(142, 'COM_CONTENT_REGISTER_TO_READ_MORE', 'Зарегистрируйтесь, чтобы прочесть подробности...', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(143, 'COM_CONTENT_SAVE_SUCCESS', 'Материал успешно сохранён', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(144, 'COM_CONTENT_SAVE_WARNING', 'Алиас уже существует, поэтому к текущему тексту алиаса была добавлена цифра. Если вы хотите изменить данный алиас, обратитесь к администратору сайта.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(145, 'COM_CONTENT_SELECT_AN_ARTICLE', 'Выбор материала', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(146, 'COM_CONTENT_SUBMIT_SAVE_SUCCESS', 'Материал успешно создан', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(147, 'COM_CONTENT_TITLE_FILTER_LABEL', 'Фильтр по заголовку', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(148, 'COM_CONTENT_WRITTEN_BY', 'Автор: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(149, 'COM_CONTENT_FIELD_FULL_DESC', 'Изображение для режима просмотра полного текста материала', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(150, 'COM_CONTENT_FIELD_FULL_LABEL', 'Изображение полного текста материала', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(151, 'COM_CONTENT_FIELD_IMAGE_DESC', 'Изображение, которое будет отображено', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(152, 'COM_CONTENT_FIELD_IMAGE_ALT_DESC', 'Альтернативный текст для пользователей, у которых нет доступа к изображениям. Если не указан, используется текст заголовка.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(153, 'COM_CONTENT_FIELD_IMAGE_ALT_LABEL', 'Альтернативный текст', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(154, 'COM_CONTENT_FIELD_IMAGE_CAPTION_DESC', 'Текст, который будет отображаться под изображением.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(155, 'COM_CONTENT_FIELD_IMAGE_CAPTION_LABEL', 'Заголовок', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(156, 'COM_CONTENT_FIELD_INTRO_DESC', 'Изображение для вступительного текста (материалы блогов и избранных материалов)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(157, 'COM_CONTENT_FIELD_INTRO_LABEL', 'Изображение для вступительного текста материала', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(158, 'COM_CONTENT_FIELD_URLC_LABEL', 'Ссылка C', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(159, 'COM_CONTENT_FIELD_URL_DESC', 'Ссылка (необходимо использовать полные ссылки).', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(160, 'COM_CONTENT_FIELD_URLA_LABEL', 'Ссылка A', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(161, 'COM_CONTENT_FIELD_URLA_LINK_TEXT_LABEL', 'Текст ссылки A', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(162, 'COM_CONTENT_FIELD_URLB_LABEL', 'Ссылка B', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(163, 'COM_CONTENT_FIELD_URL_LINK_TEXT_DESC', 'Текст для ссылки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(164, 'COM_CONTENT_FIELD_URLB_LINK_TEXT_LABEL', 'Текст ссылки B', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(165, 'COM_CONTENT_FIELD_URLC_LINK_TEXT_LABEL', 'Текст ссылки C', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(166, 'COM_CONTENT_FLOAT_DESC', 'Позволяет задать расположение изображения относительно текста материала.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(167, 'COM_CONTENT_FLOAT_LABEL', 'Выравнивание изображения', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(168, 'COM_CONTENT_FLOAT_INTRO_LABEL', 'Выравнивание изображения<br />для вступительного текста материала', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(169, 'COM_CONTENT_FLOAT_FULLTEXT_LABEL', 'Выравнивание изображения<br />для полного текста материала', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(170, 'COM_CONTENT_LEFT', 'Влево', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(171, 'COM_CONTENT_RIGHT', 'Вправо', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(172, 'COM_CONTENT_FIELD_URL_LINK_TEXT_LABEL', 'Текст ссылки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(173, 'COM_CONTENT_IMAGES_AND_URLS', 'Изображения и ссылки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_content.ini'),
(174, 'COM_FINDER', 'Умный поиск', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_finder.ini'),
(175, 'COM_FINDER_ADVANCED_SEARCH_TOGGLE', 'Расширенный поиск', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_finder.ini'),
(176, 'COM_FINDER_ADVANCED_TIPS', '<p>Примеры работы Умного Поиска:</p><p>Если ввести в поле поиска фразу <span class="term">Война и Мир</span>, будут показаны материалы, содержащие и слово "Война" и слово "Мир".</p><p>Если ввести <span class="term">Война не Мир</span> - материалы, содержащие слово "Война", но не содержащие слово "Мир".</p><p>Если ввести <span class="term">Война или Мир</span> - материалы, содержащие либо слово "Война", либо слово "Мир" (либо оба эти слова).</p><p>Если ввести <span class="term">"Война и Мир"</span> (с кавычками), будут показаны материалы, содержащие именно фразу "Война и Мир" целиком.</p><p>Результаты поиска можно ограничить с помощью фильтров по различным критериям, которые приводятся ниже.</p>', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_finder.ini'),
(177, 'COM_FINDER_DEFAULT_PAGE_TITLE', 'Результаты поиска', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_finder.ini'),
(178, 'COM_FINDER_FILTER_BRANCH_LABEL', 'Искать по критерию %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_finder.ini'),
(179, 'COM_FINDER_FILTER_DATE_BEFORE', 'До', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_finder.ini'),
(180, 'COM_FINDER_FILTER_DATE_EXACTLY', 'Точно', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_finder.ini'),
(181, 'COM_FINDER_FILTER_DATE_AFTER', 'После', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_finder.ini'),
(182, 'COM_FINDER_FILTER_DATE1', 'Дата начала', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_finder.ini'),
(183, 'COM_FINDER_FILTER_DATE1_DESC', 'Введите дату в формате YYYY-MM-DD', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_finder.ini'),
(184, 'COM_FINDER_FILTER_DATE2', 'Дата окончания', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_finder.ini'),
(185, 'COM_FINDER_FILTER_DATE2_DESC', 'Введите дату в формате YYYY-MM-DD', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_finder.ini'),
(186, 'COM_FINDER_FILTER_SELECT_ALL_LABEL', 'Искать всё', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_finder.ini'),
(187, 'COM_FINDER_FILTER_WHEN_AFTER', 'После', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_finder.ini'),
(188, 'COM_FINDER_FILTER_WHEN_BEFORE', 'До', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_finder.ini'),
(189, 'COM_FINDER_QUERY_DATE_CONDITION_AFTER', 'после', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_finder.ini'),
(190, 'COM_FINDER_QUERY_DATE_CONDITION_BEFORE', 'до', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_finder.ini'),
(191, 'COM_FINDER_QUERY_DATE_CONDITION_EXACT', 'точное совпадение', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_finder.ini'),
(192, 'COM_FINDER_QUERY_END_DATE', 'дата окончания <span class="when">%s</span> <span class="date">%s</span>', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_finder.ini'),
(193, 'COM_FINDER_QUERY_OPERATOR_AND', 'и', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_finder.ini'),
(194, 'COM_FINDER_QUERY_OPERATOR_OR', 'или', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_finder.ini'),
(195, 'COM_FINDER_QUERY_OPERATOR_NOT', 'не', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_finder.ini'),
(196, 'COM_FINDER_QUERY_FILTER_BRANCH_VENUE', 'место', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_finder.ini'),
(197, 'COM_FINDER_QUERY_START_DATE', 'дата начала <span class="when">%s</span> <span class="date">%s</span>', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_finder.ini'),
(198, 'COM_FINDER_QUERY_TAXONOMY_NODE', 'с <span class="node">%s</span> как <span class="branch">%s</span> ', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_finder.ini'),
(199, 'COM_FINDER_QUERY_TOKEN_EXCLUDED', '<span class="term">%s</span> должны быть исключены', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_finder.ini'),
(200, 'COM_FINDER_QUERY_TOKEN_GLUE', ', и ', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_finder.ini'),
(201, 'COM_FINDER_QUERY_TOKEN_INTERPRETED', 'По запросу %s найдены следующие материалы:', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_finder.ini'),
(202, 'COM_FINDER_QUERY_TOKEN_OPTIONAL', '<span class="term">%s</span> не обязательно', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_finder.ini'),
(203, 'COM_FINDER_QUERY_TOKEN_REQUIRED', '<span class="term">%s</span> обязательно', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_finder.ini'),
(204, 'COM_FINDER_SEARCH_NO_RESULTS_BODY', 'Не найдено никаких результатов по запросу: %s.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_finder.ini'),
(205, 'COM_FINDER_SEARCH_NO_RESULTS_BODY_MULTILANG', 'Не найдено никаких результатов (на английском) по запросу: %s.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_finder.ini'),
(206, 'COM_FINDER_SEARCH_NO_RESULTS_HEADING', 'Ничего не найдено', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_finder.ini'),
(207, 'COM_FINDER_SEARCH_RESULTS_OF', 'Результаты <strong>%s</strong> - <strong>%s</strong> из <strong>%s</strong>', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_finder.ini'),
(208, 'COM_FINDER_SEARCH_SIMILAR', 'Вы имели в виду: %s?', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_finder.ini'),
(209, 'COM_FINDER_SEARCH_TERMS', 'Условия поиска:', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_finder.ini'),
(210, 'COM_MAILTO', 'Кому', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_mailto.ini'),
(211, 'COM_MAILTO_CANCEL', 'Отмена', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_mailto.ini'),
(212, 'COM_MAILTO_CLOSE_WINDOW', 'Закрыть окно', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_mailto.ini'),
(213, 'COM_MAILTO_EMAIL_ERR_NOINFO', 'Пожалуйста, введите корректный адрес электронной почты.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_mailto.ini'),
(214, 'COM_MAILTO_EMAIL_INVALID', 'Адрес ''%s'' некорректен. Проверьте его, пожалуйста.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_mailto.ini'),
(215, 'COM_MAILTO_EMAIL_MSG', 'Здравствуйте!\\n\\nЭто письмо отправлено вам с сайта «%s».\\n\\nПосетитель нашего сайта, %s (%s) , предлагает вам ознакомиться с содержанием страницы: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_mailto.ini'),
(216, 'COM_MAILTO_EMAIL_NOT_SENT', 'Не удаётся отправить электронное письмо.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_mailto.ini'),
(217, 'COM_MAILTO_EMAIL_SENT', 'Письмо было успешно отправлено.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_mailto.ini'),
(218, 'COM_MAILTO_EMAIL_TO', 'E-mail адресата', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_mailto.ini'),
(219, 'COM_MAILTO_EMAIL_TO_A_FRIEND', 'Отправить ссылку другу.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_mailto.ini'),
(220, 'COM_MAILTO_LINK_IS_MISSING', 'Ссылка отсутствует', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_mailto.ini'),
(221, 'COM_MAILTO_SEND', 'Отправить', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_mailto.ini'),
(222, 'COM_MAILTO_SENDER', 'Ваше имя', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_mailto.ini'),
(223, 'COM_MAILTO_SENT_BY', 'Информацию прислал', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_mailto.ini'),
(224, 'COM_MAILTO_SUBJECT', 'Тема', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_mailto.ini'),
(225, 'COM_MAILTO_YOUR_EMAIL', 'Ваш E-mail', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_mailto.ini'),
(226, 'COM_MEDIA_ALIGN', 'Выравнивание', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(227, 'COM_MEDIA_ALIGN_DESC', 'Назначает свойства ''pull-left'', ''pull-center'' или ''pull-right'' для элементов ''<figure>'' или ''<img>''.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(228, 'COM_MEDIA_BROWSE_FILES', 'Файлы для просмотра', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(229, 'COM_MEDIA_CAPTION', 'Заголовок', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(230, 'COM_MEDIA_CAPTION_CLASS_LABEL', 'CSS-класс заголовка', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(231, 'COM_MEDIA_CAPTION_CLASS_DESC', 'Позволяет назначить указанный класс для элемента ''<figcaption>''. Например: ''text-left'', ''text-right'', ''text-center''', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(232, 'COM_MEDIA_CLEAR_LIST', 'Очистить список', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(233, 'COM_MEDIA_CONFIGURATION', 'Настройки медиа-менеджера', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(234, 'COM_MEDIA_CREATE_FOLDER', 'Создать каталог', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(235, 'COM_MEDIA_CURRENT_PROGRESS', 'Текущий прогресс', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(236, 'COM_MEDIA_DESCFTP', 'Для загрузки, изменения и удаления медиафайлов системе необходим доступ по FTP. Пожалуйста, введите параметры FTP-доступа в поля формы ниже.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(237, 'COM_MEDIA_DESCFTPTITLE', 'Параметры доступа по FTP', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(238, 'COM_MEDIA_DETAIL_VIEW', 'Подробно', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(239, 'COM_MEDIA_DIRECTORY', 'Каталог', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(240, 'COM_MEDIA_DIRECTORY_UP', 'На уровень выше', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(241, 'COM_MEDIA_ERROR_BAD_REQUEST', 'Некорректный запрос', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(242, 'COM_MEDIA_ERROR_FILE_EXISTS', 'Файл уже существует', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(243, 'COM_MEDIA_ERROR_UNABLE_TO_CREATE_FOLDER_WARNDIRNAME', 'Невозможно создать каталог. Имя каталога может содержать только буквенно-цифровые символов, без пробелов.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(244, 'COM_MEDIA_ERROR_UNABLE_TO_BROWSE_FOLDER_WARNDIRNAME', 'Невозможно открыть каталог:&#160;%s. Имя каталога может содержать только буквенно-цифровые символов, без пробелов.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(245, 'COM_MEDIA_ERROR_UNABLE_TO_DELETE', 'Не удалось удалить:&#160;', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(246, 'COM_MEDIA_ERROR_UNABLE_TO_DELETE_FILE_WARNFILENAME', 'Не удалось удалить:&#160;%s. Имя файла должно состоять только из букв и цифр, без пробелов.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(247, 'COM_MEDIA_ERROR_UNABLE_TO_DELETE_FOLDER_NOT_EMPTY', 'Не удалось удалить:&#160;%s. Каталог не пуст!', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(248, 'COM_MEDIA_ERROR_UNABLE_TO_DELETE_FOLDER_WARNDIRNAME', 'Не удалось удалить:&#160;%s. Имя каталога должно состоять только из букв и цифр, без пробелов.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(249, 'COM_MEDIA_ERROR_UNABLE_TO_UPLOAD_FILE', 'Не удаётся загрузить файл.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(250, 'COM_MEDIA_ERROR_WARNFILETOOLARGE', 'Размер данного файла слишком велик для загрузки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(251, 'COM_MEDIA_ERROR_WARNUPLOADTOOLARGE', 'Общий размер загружаемых файлов превышает лимит.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(252, 'COM_MEDIA_FIELD_CHECK_MIME_DESC', 'Использовать Fileinfo или MIME Magic для проверки файлов. Отключите, если не хотите видеть сообщения об ошибках определения типа файла (mime type errors)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(253, 'COM_MEDIA_FIELD_CHECK_MIME_LABEL', 'Проверять тип файла (MIME)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(254, 'COM_MEDIA_FIELD_IGNORED_EXTENSIONS_DESC', 'Перечень (через запятую) расширений файлов, которые блокируются при проверке MIME-типов и&#160;при&#160;ограниченной загрузке.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(255, 'COM_MEDIA_FIELD_IGNORED_EXTENSIONS_LABEL', 'Запрещённые расширения файлов', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(256, 'COM_MEDIA_FIELD_ILLEGAL_MIME_TYPES_DESC', 'Перечень (через запятую) запрещённых для загрузки типов файлов (MIME) (чёрный список)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(257, 'COM_MEDIA_FIELD_ILLEGAL_MIME_TYPES_LABEL', 'Недопустимые типы файлов (MIME)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(258, 'COM_MEDIA_FIELD_LEGAL_EXTENSIONS_DESC', 'Перечень (через запятую) расширений файлов, которые разрешено загружать на сервер.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(259, 'COM_MEDIA_FIELD_LEGAL_EXTENSIONS_LABEL', 'Разрешённые расширения', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(260, 'COM_MEDIA_FIELD_LEGAL_IMAGE_EXTENSIONS_DESC', 'Перечень (через запятую) расширений файлов изображений, разрешённых для загрузки на сервер. Используется для проверки заголовков изображений.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(261, 'COM_MEDIA_FIELD_LEGAL_IMAGE_EXTENSIONS_LABEL', 'Разрешённые расширения изображений', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(262, 'COM_MEDIA_FIELD_LEGAL_MIME_TYPES_DESC', 'Перечень (через запятую) разрешённых для загрузки типов файлов (MIME)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(263, 'COM_MEDIA_FIELD_LEGAL_MIME_TYPES_LABEL', 'Разрешённые типы файлов (MIME)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(264, 'COM_MEDIA_FIELD_MAXIMUM_SIZE_DESC', 'Максимальный размер загружаемых файлов (в мегабайтах). Введите 0, если хотите позволить загружать файлы любого размера.<br /><br />Примечание: У сервера, на котором размещён сайт, может иметься собственное ограничение максимального объёма загружаемых файлов.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(265, 'COM_MEDIA_FIELD_MAXIMUM_SIZE_LABEL', 'Максимальный размер (в MB)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(266, 'COM_MEDIA_FIELD_PATH_FILE_FOLDER_DESC', 'Введите путь к каталогу с файлами относительно корневого каталога сайта', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(267, 'COM_MEDIA_FIELD_PATH_FILE_FOLDER_LABEL', 'Путь к каталогу с файлами', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(268, 'COM_MEDIA_FIELD_PATH_IAMGE_FOLDER_DESC', 'Введите путь к каталогу изображений относительно корневого каталога сайта', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(269, 'COM_MEDIA_FIELD_PATH_IMAGE_FOLDER_LABEL', 'Путь к каталогу с изображениями', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(270, 'COM_MEDIA_FIELD_RESTRICT_UPLOADS_DESC', 'Ограничить загрузку для пользователей с правами ниже <em>Менеджера</em>, если Fileinfo или MIME magic не установлены.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(271, 'COM_MEDIA_FIELD_RESTRICT_UPLOADS_LABEL', 'Ограничение загрузки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(272, 'COM_MEDIA_FILES', 'Файлы', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(273, 'COM_MEDIA_FILESIZE', 'Размер файла', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(274, 'COM_MEDIA_FOLDER', 'Каталог', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(275, 'COM_MEDIA_FOLDERS', 'Каталоги', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini');
INSERT INTO `yawnc_overrider` (`id`, `constant`, `string`, `file`) VALUES
(276, 'COM_MEDIA_IMAGE_DESCRIPTION', 'Описание изображения', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(277, 'COM_MEDIA_IMAGE_URL', 'Адрес (URL) изображения', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(278, 'COM_MEDIA_INSERT', 'Вставить', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(279, 'COM_MEDIA_INSERT_IMAGE', 'Вставить изображение', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(280, 'COM_MEDIA_MAXIMUM_SIZE', 'Максимальный размер', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(281, 'COM_MEDIA_MEDIA', 'Медиа', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(282, 'COM_MEDIA_NAME', 'Название файла', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(283, 'COM_MEDIA_NO_IMAGES_FOUND', 'Изображения не найдены', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(284, 'COM_MEDIA_NOT_SET', 'Не определено', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(285, 'COM_MEDIA_OVERALL_PROGRESS', 'Общий прогресс', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(286, 'COM_MEDIA_PIXEL_DIMENSIONS', 'Размеры (Ш&#160;х&#160;В), в&#160;пикселях', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(287, 'COM_MEDIA_START_UPLOAD', 'Загрузить', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(288, 'COM_MEDIA_THUMBNAIL_VIEW', 'Эскизы', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(289, 'COM_MEDIA_TITLE', 'Заголовок изображения', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(290, 'COM_MEDIA_UP', 'Вверх', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(291, 'COM_MEDIA_UPLOAD', 'Загрузить', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(292, 'COM_MEDIA_UPLOAD_COMPLETE', 'Загрузка завершена', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(293, 'COM_MEDIA_UPLOAD_FILE', 'Загрузить файл', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(294, 'COM_MEDIA_UPLOAD_FILES', 'Загрузка файлов (максимальный размер: %s MB)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(295, 'COM_MEDIA_UPLOAD_FILES_NOLIMIT', 'Загрузка файлов (максимальный размер не ограничен)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(296, 'COM_MEDIA_UPLOAD_SUCCESSFUL', 'Загрузка успешно завершена', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_media.ini'),
(297, 'COM_MESSAGES_ERR_SEND_FAILED', 'Пользователь заблокировал свой почтовый ящик. Доставить сообщение не удалось.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_messages.ini'),
(298, 'COM_MESSAGES_NEW_MESSAGE_ARRIVED', 'Вы получили новое личное сообщение от %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_messages.ini'),
(299, 'COM_MESSAGES_PLEASE_LOGIN', 'Отправлено', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_messages.ini'),
(300, 'COM_NEWSFEEDS_CACHE_DIRECTORY_UNWRITABLE', 'Директория кэша недоступна по записи. Лента новостей не может быть отображена. Пожалуйста, свяжитесь с администратором сайта.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_newsfeeds.ini'),
(301, 'COM_NEWSFEEDS_CAT_NUM', 'Кол-во новостных лент:', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_newsfeeds.ini'),
(302, 'COM_NEWSFEEDS_CONTENT_TYPE_NEWSFEED', 'Лента новостей', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_newsfeeds.ini'),
(303, 'COM_NEWSFEEDS_CONTENT_TYPE_CATEGORY', 'Категория новостных лент', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_newsfeeds.ini'),
(304, 'COM_NEWSFEEDS_DEFAULT_PAGE_TITLE', 'Ленты новостей', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_newsfeeds.ini'),
(305, 'COM_NEWSFEEDS_ERROR_FEED_NOT_FOUND', 'Ошибка. Лента новостей не найдена.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_newsfeeds.ini'),
(306, 'COM_NEWSFEEDS_ERRORS_FEED_NOT_RETRIEVED', 'Ошибка. Не удалось восстановить ленту новостей.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_newsfeeds.ini'),
(307, 'COM_NEWSFEEDS_FEED_LINK', 'Ссылка ленты', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_newsfeeds.ini'),
(308, 'COM_NEWSFEEDS_FEED_NAME', 'Название ленты', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_newsfeeds.ini'),
(309, 'COM_NEWSFEEDS_FILTER_LABEL', 'Фильтр', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_newsfeeds.ini'),
(310, 'COM_NEWSFEEDS_FILTER_SEARCH_DESC', 'Фильтр поиска по новостным лентам', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_newsfeeds.ini'),
(311, 'COM_NEWSFEEDS_NO_ARTICLES', 'В этой ленте новостей нет материалов', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_newsfeeds.ini'),
(312, 'COM_NEWSFEEDS_NUM_ARTICLES', 'Кол-во статей', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_newsfeeds.ini'),
(313, 'COM_NEWSFEEDS_NUM_ARTICLES_COUNT', 'Кол-во статей: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_newsfeeds.ini'),
(314, 'COM_NEWSFEEDS_NUM_ITEMS', '# кол-во', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_newsfeeds.ini'),
(315, 'COM_SEARCH_ALL_WORDS', 'Все слова', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_search.ini'),
(316, 'COM_SEARCH_ALPHABETICAL', 'По алфавиту', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_search.ini'),
(317, 'COM_SEARCH_ANY_WORDS', 'Любое из слов', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_search.ini'),
(318, 'COM_SEARCH_ERROR_ENTERKEYWORD', 'Введите ключевое слово для поиска', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_search.ini'),
(319, 'COM_SEARCH_ERROR_IGNOREKEYWORD', 'Одно или более общих слов были проигнорированы при поиске.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_search.ini'),
(320, 'COM_SEARCH_ERROR_SEARCH_MESSAGE', 'Для выполнения поиска длина фразы должна быть не менее %1$s символов и не более %2$s.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_search.ini'),
(321, 'COM_SEARCH_EXACT_PHRASE', 'Точное совпадение', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_search.ini'),
(322, 'COM_SEARCH_FIELD_SEARCH_AREAS_DESC', 'Показывать список доступных областей поиска', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_search.ini'),
(323, 'COM_SEARCH_FIELD_SEARCH_AREAS_LABEL', 'Использовать области поиска', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_search.ini'),
(324, 'COM_SEARCH_FOR', 'Совпадение', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_search.ini'),
(325, 'COM_SEARCH_MOST_POPULAR', 'Популярные первыми', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_search.ini'),
(326, 'COM_SEARCH_NEWEST_FIRST', 'Новые первыми', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_search.ini'),
(327, 'COM_SEARCH_OLDEST_FIRST', 'Старые первыми', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_search.ini'),
(328, 'COM_SEARCH_ORDERING', 'Порядок', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_search.ini'),
(329, 'COM_SEARCH_SEARCH', 'Искать', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_search.ini'),
(330, 'COM_SEARCH_SEARCH_AGAIN', 'Искать снова', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_search.ini'),
(331, 'COM_SEARCH_SEARCH_KEYWORD', 'Текст для поиска', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_search.ini'),
(332, 'COM_SEARCH_SEARCH_KEYWORD_N_RESULTS', '<strong>Результат поиска: найдено %s объектов.</strong>', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_search.ini'),
(333, 'COM_SEARCH_SEARCH_KEYWORD_N_RESULTS_1', '<strong>Результат поиска: найден один объект.</strong>', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_search.ini'),
(334, 'COM_SEARCH_SEARCH_KEYWORD_N_RESULTS_2', '<strong>Результат поиска: найдено %s объекта.</strong>', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_search.ini'),
(335, 'COM_SEARCH_SEARCH_ONLY', 'Ограничение области поиска', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_search.ini'),
(336, 'COM_SEARCH_SEARCH_RESULT', 'Результат поиска', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_search.ini'),
(337, 'COM_TAGS_CREATED_DATE', 'Дата создания', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_tags.ini'),
(338, 'COM_TAGS_FILTER_SEARCH_DESC', 'Введите заголовок метки или его часть для поиска.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_tags.ini'),
(339, 'COM_TAGS_MODIFIED_DATE', 'Дата изменения', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_tags.ini'),
(340, 'COM_TAGS_NO_ITEMS', 'Нет элементов с такой меткой', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_tags.ini'),
(341, 'COM_TAGS_NO_TAGS', 'Метки отсутствуют', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_tags.ini'),
(342, 'COM_TAGS_PUBLISHED_DATE', 'Дата публикации', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_tags.ini'),
(343, 'COM_TAGS_TITLE_FILTER_LABEL', 'Начните ввод заголовка метки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_tags.ini'),
(344, 'COM_USERS_ACTIVATION_TOKEN_NOT_FOUND', 'Код подтверждения не найден.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(345, 'COM_USERS_CAPTCHA_LABEL', 'CAPTCHA', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(346, 'COM_USERS_CAPTCHA_DESC', 'Введите текст, который вы видите на картинке.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(347, 'COM_USERS_DATABASE_ERROR', 'Не удалось загрузить данные о пользователе из базы данных: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(348, 'COM_USERS_DESIRED_PASSWORD', 'Введите пароль.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(349, 'COM_USERS_DESIRED_USERNAME', 'Введите желаемый логин.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(350, 'COM_USERS_EDIT_PROFILE', 'Изменить профиль', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(351, 'COM_USERS_EMAIL_ACCOUNT_DETAILS', 'Параметры учётной записи для %s на сайте %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(352, 'COM_USERS_EMAIL_ACTIVATE_WITH_ADMIN_ACTIVATION_BODY', 'Здравствуйте, уважаемый администратор!\\n\\nНовый пользователь только что зарегистрировался на сайте %s.\\nПользователь подтвердил e-mail и просит активировать его учётную запись.\\nВ этом письме содержатся его регистрационные данные:\\n\\n Имя: %s \\n E-mail: %s \\n Логин: %s \\n\\nВы можете активировать учётную запись пользователя, перейдя по следующей ссылке:\\n %s \\n', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(353, 'COM_USERS_EMAIL_ACTIVATE_WITH_ADMIN_ACTIVATION_SUBJECT', 'Запрос активации учётной записи: %s на сайте %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(354, 'COM_USERS_EMAIL_ACTIVATED_BY_ADMIN_ACTIVATION_BODY', 'Здравствуйте %s,\\n\\nВаша учётная запись активирована. Вы можете войти на сайт %s. Ваш логин %s. Для учётной записи установлен тот пароль, который вы вводили при регистрации.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(355, 'COM_USERS_EMAIL_ACTIVATED_BY_ADMIN_ACTIVATION_SUBJECT', 'Активирована учётная запись %s на сайте %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(356, 'COM_USERS_EMAIL_PASSWORD_RESET_BODY', 'Здравствуйте,\\n\\nНа сайте %s был сделан запрос на восстановление пароля к вашей учётной записи. Чтобы восстановить пароль вам потребуется ввести указанный ниже код подтверждения.\\n\\nКод подтверждения: %s\\n\\nДля ввода кода подтверждения перейдите на страницу по ссылке ниже.\\n\\n %s \\n\\nСпасибо.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(357, 'COM_USERS_EMAIL_PASSWORD_RESET_SUBJECT', 'Запрос сброса пароля на сайте %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(358, 'COM_USERS_EMAIL_REGISTERED_BODY', 'Здравствуйте %s,\\n\\nСпасибо за регистрацию на %s.\\n\\nТеперь вы можете войти на %s используя следующие данные:\\n\\nЛогин: %s\\nПароль: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(359, 'COM_USERS_EMAIL_REGISTERED_BODY_NOPW', 'Здравствуйте %s,\\n\\nСпасибо за регистрацию на %s.\\n\\nТеперь вы можете войти на %s используя логин и пароль, указанные при регистрации.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(360, 'COM_USERS_EMAIL_REGISTERED_NOTIFICATION_TO_ADMIN_BODY', 'Здравствуйте, уважаемый администратор, \\n\\nНовый пользователь ''%s'', логин ''%s'', зарегистрировался на сайте %s.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(361, 'COM_USERS_EMAIL_REGISTERED_WITH_ACTIVATION_BODY', 'Здравствуйте, %s,\\n\\nБлагодарим вас за регистрацию на сайте %s. Ваша учётная запись создана, но должна быть активирована прежде, чем вы сможете ею воспользоваться.\\nЧтобы активировать учётную запись, перейдите по ссылке ниже, или скопируйте её в адресную строку браузера:\\n%s \\n\\nПосле активации вы сможете входить на сайт %s с помощью указанных ниже логина и пароля:\\n\\nЛогин: %s\\nПароль: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(362, 'COM_USERS_EMAIL_REGISTERED_WITH_ACTIVATION_BODY_NOPW', 'Здравствуйте, %s,\\n\\nБлагодарим вас за регистрацию на сайте %s. Ваша учётная запись создана, но должна быть активирована прежде, чем вы сможете ею воспользоваться.\\nЧтобы активировать учётную запись, перейдите по ссылке ниже, или скопируйте её в адресную строку браузера:\\n%s \\n\\nПосле активации вы сможете входить на сайт %s с помощью указанных ниже логина и пароля:\\n\\nЛогин: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(363, 'COM_USERS_EMAIL_REGISTERED_WITH_ADMIN_ACTIVATION_BODY', 'Здравствуйте %s,\\n\\nБлагодарим вас за регистрацию на сайте %s. Ваша учётная запись создана, но должна быть проверена прежде, чем вы сможете ею воспользоваться.\\nЧтобы подтвердить вашу учётную запись, перейдите по ссылке, приведённой ниже, или вставьте её в адресную строку браузера:\\n %s \\n\\nПосле проверки, администратору сайта будет отправлено сообщение о необходимости активировать вашу учётную запись.\\nПосле активации учётной записи вы сможете войти на сайт %s, используя логин и пароль приведённые ниже:\\n\\nЛогин: %s\\nПароль: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(364, 'COM_USERS_EMAIL_REGISTERED_WITH_ADMIN_ACTIVATION_BODY_NOPW', 'Здравствуйте %s,\\n\\nБлагодарим вас за регистрацию на сайте %s. Ваша учётная запись создана, но должна быть проверена прежде, чем вы сможете ею воспользоваться.\\nЧтобы подтвердить вашу учётную запись, перейдите по ссылке, приведённой ниже, или вставьте её в адресную строку браузера:\\n %s \\n\\nПосле проверки, администратору сайта будет отправлено сообщение о необходимости активировать вашу учётную запись.\\nПосле активации учётной записи вы сможете войти на сайт %s, используя логин и пароль приведённые ниже:\\n\\nЛогин: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(365, 'COM_USERS_EMAIL_USERNAME_REMINDER_BODY', 'Здравствуйте,\\n\\nНа сайте %s была сделана заявка на восстановление логина вашей учётной записи.\\n\\nВаш логин: %s.\\n\\nДля входа на сайт под вашими учётными данными перейдите по ссылке ниже.\\n\\n%s \\n\\nСпасибо.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(366, 'COM_USERS_EMAIL_USERNAME_REMINDER_SUBJECT', 'Восстановление логина на сайте %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(367, 'COM_USERS_ERROR_SECRET_CODE_WITHOUT_TFA', 'Вы ввели секретный код, однако двухфакторная аутентификация не включена для вашей учётной записи. Если вы желаете использовать секретный код для повышения безопасности авторизации, разрешите двухфакторную аутентификацию в параметрах вашего профиля.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(368, 'COM_USERS_FIELD_PASSWORD_RESET_DESC', 'Введите, пожалуйста, адрес электронной почты, указанный в параметрах вашей учётной записи.<br />На этот адрес будет отправлен специальный проверочный код. После его получения вы сможете ввести новый пароль для вашей учётной записи.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(369, 'COM_USERS_FIELD_PASSWORD_RESET_LABEL', 'Адрес электронной почты', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(370, 'COM_USERS_FIELD_REMIND_EMAIL_DESC', 'Введите, пожалуйста, адрес электронной почты, указанный в&#160;параметрах вашей учётной записи. На этот адрес будет отправлено письмо, содержащее ваш Логин.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(371, 'COM_USERS_FIELD_REMIND_EMAIL_LABEL', 'Адрес электронной почты', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(372, 'COM_USERS_FIELD_RESET_CONFIRM_TOKEN_DESC', 'Введите проверочный код, который получили по электронной почте.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(373, 'COM_USERS_FIELD_RESET_CONFIRM_TOKEN_LABEL', 'Код подтверждения:', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(374, 'COM_USERS_FIELD_RESET_CONFIRM_USERNAME_DESC', 'Введите ваш логин', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(375, 'COM_USERS_FIELD_RESET_CONFIRM_USERNAME_LABEL', 'Логин', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(376, 'COM_USERS_FIELD_RESET_PASSWORD1_DESC', 'Введите ваш новый пароль', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(377, 'COM_USERS_FIELD_RESET_PASSWORD1_LABEL', 'Пароль', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(378, 'COM_USERS_FIELD_RESET_PASSWORD1_MESSAGE', 'Введённые вами пароли не совпадают. Пожалуйста, введите желаемый пароль в поле пароля и в поле подтверждения.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(379, 'COM_USERS_FIELD_RESET_PASSWORD2_DESC', 'Подтвердите ваш новый пароль', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(380, 'COM_USERS_FIELD_RESET_PASSWORD2_LABEL', 'Повтор пароля', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(381, 'COM_USERS_INVALID_EMAIL', 'Недопустимый адрес электронной почты', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(382, 'COM_USERS_LOGIN_IMAGE_ALT', 'Изображение логина', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(383, 'COM_USERS_LOGIN_REGISTER', 'Ещё нет учётной записи?', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(384, 'COM_USERS_LOGIN_REMEMBER_ME', 'Запомнить меня', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(385, 'COM_USERS_LOGIN_REMIND', 'Забыли логин?', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(386, 'COM_USERS_LOGIN_RESET', 'Забыли пароль?', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(387, 'COM_USERS_LOGIN_USERNAME_LABEL', 'Логин', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(388, 'COM_USERS_MAIL_FAILED', 'Не удалось отправить электронную почту.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(389, 'COM_USERS_MAIL_SEND_FAILURE_BODY', 'При отправке письма c регистрационными данными пользователя произошла ошибка: %s Пользователь, для которого отправлялось письмо: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(390, 'COM_USERS_MAIL_SEND_FAILURE_SUBJECT', 'Ошибка при отправке письма', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(391, 'COM_USERS_MSG_NOT_ENOUGH_INTEGERS_N', 'В пароле недостаточно цифровых символов. Требуется не менее %s цифровых символов.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(392, 'COM_USERS_MSG_NOT_ENOUGH_INTEGERS_N_1', 'В пароле недостаточно цифровых символов. Требуется не менее 1 цифры.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(393, 'COM_USERS_MSG_NOT_ENOUGH_SYMBOLS_N', 'В пароле недостаточно символов. Требуется не менее %s символов.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(394, 'COM_USERS_MSG_NOT_ENOUGH_SYMBOLS_N_1', 'В пароле недостаточно символов. Требуется не менее 1 символа.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(395, 'COM_USERS_MSG_NOT_ENOUGH_UPPERCASE_LETTERS_N', 'В пароле недостаточно символов в верхнем регистре (заглавных букв). Требуется не менее %s символов в верхнем регистре.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(396, 'COM_USERS_MSG_NOT_ENOUGH_UPPERCASE_LETTERS_N_1', 'В пароле недостаточно символов в верхнем регистре (заглавных букв). Требуется не менее 1 символа в верхнем регистре.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(397, 'COM_USERS_MSG_PASSWORD_TOO_LONG', 'Пароль слишком длинный. Длина пароля должна быть меньше 100 символов.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(398, 'COM_USERS_MSG_PASSWORD_TOO_SHORT_N', 'Пароль слишком короткий. Минимальная длина пароля - %s символов.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(399, 'COM_USERS_MSG_SPACES_IN_PASSWORD', 'Пароль не должен содержать пробелов в начале или конце.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(400, 'COM_USERS_OPTIONAL', '(необязательно)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(401, 'COM_USERS_OR', 'или', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(402, 'COM_USERS_PROFILE', 'Профиль пользователя', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(403, 'COM_USERS_PROFILE_BIND_FAILED', 'Ошибка загрузки данных профиля: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(404, 'COM_USERS_PROFILE_CORE_LEGEND', 'Профиль', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(405, 'COM_USERS_PROFILE_CUSTOM_LEGEND', 'Профиль пользователя', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(406, 'COM_USERS_PROFILE_DEFAULT_LABEL', 'Изменить свой профиль', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(407, 'COM_USERS_PROFILE_EMAIL1_DESC', 'Введите адрес электронной почты', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(408, 'COM_USERS_PROFILE_EMAIL1_LABEL', 'Адрес электронной почты', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(409, 'COM_USERS_PROFILE_EMAIL1_MESSAGE', 'Ваш адрес электронной почты уже используется или введён некорректно. Пожалуйста, введите другой адрес электронной почты.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(410, 'COM_USERS_PROFILE_EMAIL2_DESC', 'Подтвердите указанный вами адрес электронной почты', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(411, 'COM_USERS_PROFILE_EMAIL2_LABEL', 'Подтвердите адрес электронной почты:', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(412, 'COM_USERS_PROFILE_EMAIL2_MESSAGE', 'Адреса электронной почты не совпадают. Пожалуйста, введите ваш адрес электронной почты в поле адреса и в поле подтверждения.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(413, 'COM_USERS_PROFILE_LAST_VISITED_DATE_LABEL', 'Дата последнего посещения', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(414, 'COM_USERS_PROFILE_MY_PROFILE', 'Мой профиль', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(415, 'COM_USERS_PROFILE_NAME_DESC', 'Введите ваше полное имя', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(416, 'COM_USERS_PROFILE_NAME_LABEL', 'Имя', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(417, 'COM_USERS_PROFILE_NEVER_VISITED', 'Вы посетили этот сайт впервые', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(418, 'COM_USERS_PROFILE_NOCHANGE_USERNAME_DESC', 'Если вы хотите изменить логин - обратитесь к администратору сайта', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(419, 'COM_USERS_PROFILE_OTEPS', 'Одноразовые аварийные пароли', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(420, 'COM_USERS_PROFILE_OTEPS_DESC', 'Если у вас нет доступа к вашему устройству для двухфакторной аутентификации вы можете использовать один из данных паролей вместо секретного кода. Каждый из данных аварийных паролей является одноразовым - он уничтожается сразу после использования. Мы рекомендуем распечатать эти пароли и держать распечатку в надёжном и доступном месте - в кошельке или сейфе.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(421, 'COM_USERS_PROFILE_OTEPS_WAIT_DESC', 'Одноразовые аварийные пароли для вашей учётной записи отсутствуют. Одноразовые пароли формируются автоматически и отображаются здесь сразу после активации двухфакторной аутентификации.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(422, 'COM_USERS_PROFILE_PASSWORD1_LABEL', 'Пароль', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(423, 'COM_USERS_PROFILE_PASSWORD1_MESSAGE', 'Введённые вами пароли не совпадают. Пожалуйста, введите желаемый пароль в поле пароля и в поле подтверждения.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(424, 'COM_USERS_PROFILE_PASSWORD2_DESC', 'Подтверждение пароля', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(425, 'COM_USERS_PROFILE_PASSWORD2_LABEL', 'Повтор пароля', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(426, 'COM_USERS_PROFILE_REGISTERED_DATE_LABEL', 'Дата регистрации', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(427, 'COM_USERS_PROFILE_SAVE_FAILED', 'Не удаётся сохранить профиль: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(428, 'COM_USERS_PROFILE_SAVE_SUCCESS', 'Профиль сохранен', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(429, 'COM_USERS_PROFILE_TWO_FACTOR_AUTH', 'Двухфакторная аутентификация', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(430, 'COM_USERS_PROFILE_TWOFACTOR_LABEL', 'Тип аутентификации', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(431, 'COM_USERS_PROFILE_TWOFACTOR_DESC', 'Тип двухфакторной аутентификации для вашей учётной записи', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(432, 'COM_USERS_PROFILE_USERNAME_DESC', 'Введите желаемый логин', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(433, 'COM_USERS_PROFILE_USERNAME_LABEL', 'Логин', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(434, 'COM_USERS_PROFILE_USERNAME_MESSAGE', 'Введённый вами логин некорректен. Пожалуйста, введите другой логин.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(435, 'COM_USERS_PROFILE_VALUE_NOT_FOUND', 'Нет информации', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(436, 'COM_USERS_PROFILE_WELCOME', 'Добро пожаловать, %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(437, 'COM_USERS_REGISTER_DEFAULT_LABEL', 'Регистрация', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(438, 'COM_USERS_REGISTER_EMAIL1_DESC', 'Введите адрес электронной почты', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(439, 'COM_USERS_REGISTER_EMAIL1_LABEL', 'Адрес электронной почты', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(440, 'COM_USERS_REGISTER_EMAIL1_MESSAGE', 'Ваш адрес электронной почты уже используется или введён некорректно. Пожалуйста, введите другой адрес электронной почты.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(441, 'COM_USERS_REGISTER_EMAIL2_DESC', 'Подтвердите указанный вами адрес электронной почты', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(442, 'COM_USERS_REGISTER_EMAIL2_LABEL', 'Подтверждение адреса электронной почты:', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(443, 'COM_USERS_REGISTER_EMAIL2_MESSAGE', 'Адреса электронной почты не совпадают. Пожалуйста, введите ваш адрес электронной почты в поле адреса и в поле подтверждения.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(444, 'COM_USERS_REGISTER_NAME_DESC', 'Введите ваше полное имя', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(445, 'COM_USERS_REGISTER_NAME_LABEL', 'Имя', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(446, 'COM_USERS_REGISTER_PASSWORD1_LABEL', 'Пароль', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(447, 'COM_USERS_REGISTER_PASSWORD1_MESSAGE', 'Введённые вами пароли не совпадают. Пожалуйста, введите желаемый пароль в поле пароля и в поле подтверждения.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(448, 'COM_USERS_REGISTER_PASSWORD2_DESC', 'Подтверждение пароля', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(449, 'COM_USERS_REGISTER_PASSWORD2_LABEL', 'Повтор пароля', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(450, 'COM_USERS_REGISTER_REQUIRED', '<strong class="red">*</strong> Обязательное поле', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(451, 'COM_USERS_REGISTER_USERNAME_DESC', 'Введите логин, под которым вы хотите входить на сайт', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(452, 'COM_USERS_REGISTER_USERNAME_LABEL', 'Логин', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(453, 'COM_USERS_REGISTER_USERNAME_MESSAGE', 'Введённый вами логин некорректен. Пожалуйста, введите другой логин.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(454, 'COM_USERS_REGISTRATION', 'Регистрация пользователя', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(455, 'COM_USERS_REGISTRATION_ACTIVATE_SUCCESS', 'Ваша учётная запись была успешно активирована. Теперь вы можете войти, используя логин и пароль, указанные при регистрации.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(456, 'COM_USERS_REGISTRATION_ACTIVATION_NOTIFY_SEND_MAIL_FAILED', 'При отправке e-mail с информацией об активации учётной записи пользователя произошла ошибка', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(457, 'COM_USERS_REGISTRATION_ACTIVATION_SAVE_FAILED', 'Не удалось сохранить данные об активации учётной записи: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(458, 'COM_USERS_REGISTRATION_ADMINACTIVATE_SUCCESS', 'Учётная запись пользователя была успешно активирована. Пользователю было отправлено уведомление об этом.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(459, 'COM_USERS_REGISTRATION_BIND_FAILED', 'Не удалось привязать регистрационные данные: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(460, 'COM_USERS_REGISTRATION_COMPLETE_ACTIVATE', 'Учётная запись для вас была создана. На указанный при регистрации адрес электронной почты была отправлена ссылка для её активации. Обратите внимание, что необходимо активировать учётную запись, перейдя по содержащейся в письме ссылке. Только после этого вы сможете проходить авторизацию на сайте под вашим логином и паролем.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(461, 'COM_USERS_REGISTRATION_COMPLETE_VERIFY', 'Учётная запись для вас была создана. На указанный при регистрации адрес электронной почты была отправлена ссылка для её активации. Обратите внимание, что необходимо подтвердить учётную запись, перейдя по содержащейся в письме ссылке. После этого Администратор активирует учётную запись и вы сможете входить на сайт под вашим логином и паролем.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(462, 'COM_USERS_REGISTRATION_DEFAULT_LABEL', 'Регистрация пользователя', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(463, 'COM_USERS_REGISTRATION_SAVE_FAILED', 'Не удалось зарегистрировать пользователя: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(464, 'COM_USERS_REGISTRATION_SAVE_SUCCESS', 'Спасибо за регистрацию. Теперь вы можете войти на сайт, используя логин и пароль, указанные при регистрации.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(465, 'COM_USERS_REGISTRATION_SEND_MAIL_FAILED', 'Произошла ошибка при отправке письма с регистрационными данными. Администратору сайта было отправлено сообщение о возникшей проблеме.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(466, 'COM_USERS_REGISTRATION_VERIFY_SUCCESS', 'Ваш адрес электронной почты был проверен успешно. Как только администратор активирует созданную учётную запись, вам будет отправлено сообщение по электронной почте, после чего вы сможете войти на сайт.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(467, 'COM_USERS_REMIND', 'Восстановление', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(468, 'COM_USERS_REMIND_DEFAULT_LABEL', 'Введите, пожалуйста, адрес электронной почты, указанный в параметрах вашей учётной записи. На этот адрес будет отправлено письмо, содержащее ваш Логин.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(469, 'COM_USERS_REMIND_EMAIL_LABEL', 'Ваш e-mail', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(470, 'COM_USERS_REMIND_LIMIT_ERROR_N_HOURS', 'Вы превысили максимально-допустимое количество попыток восстановления пароля. Пожалуйста, повторите попытку через %s часов.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(471, 'COM_USERS_REMIND_LIMIT_ERROR_N_HOURS_1', 'Вы превысили максимально-допустимое количество попыток восстановления пароля. Пожалуйста, повторите попытку через 1 час.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(472, 'COM_USERS_REMIND_REQUEST_FAILED', 'Не удалось восстановить данные: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(473, 'COM_USERS_REMIND_REQUEST_SUCCESS', 'Сообщение с информацией отправлено на указанный адрес. Пожалуйста, проверьте почту.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(474, 'COM_USERS_REMIND_SUPERADMIN_ERROR', 'Суперадминистратор не может запросить восстановление пароля. Пожалуйста, свяжитесь с другим Суперадминистратором или используйте альтернативный метод.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(475, 'COM_USERS_RESET', 'Восстановление пароля', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(476, 'COM_USERS_RESET_COMPLETE_ERROR', 'При восстановлении пароля произошла ошибка.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(477, 'COM_USERS_RESET_COMPLETE_FAILED', 'При восстановлении пароля произошла ошибка: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(478, 'COM_USERS_RESET_COMPLETE_LABEL', 'Введите, пожалуйста, новый пароль.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(479, 'COM_USERS_RESET_COMPLETE_SUCCESS', 'Пароль успешно восстановлен. Теперь вы можете войти на сайт.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(480, 'COM_USERS_RESET_CONFIRM_ERROR', 'При подтверждении пароля произошла ошибка.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(481, 'COM_USERS_RESET_CONFIRM_FAILED', 'Не удалось восстановить пароль, поскольку проверочный код был указан неверно. %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(482, 'COM_USERS_RESET_CONFIRM_LABEL', 'На ваш адрес электронной почты было отправлено письмо, содержащее проверочный код. Введите его, пожалуйста, в поле ниже. Это подтвердит, что именно вы являетесь владельцем данной учётной записи.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(483, 'COM_USERS_RESET_COMPLETE_TOKENS_MISSING', 'Восстановление пароля не выполнено, поскольку отсутствует проверочный код.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(484, 'COM_USERS_RESET_REQUEST_ERROR', 'Ошибка при восстановлении пароля.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(485, 'COM_USERS_RESET_REQUEST_FAILED', 'Ошибка при восстановлении пароля: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(486, 'COM_USERS_RESET_REQUEST_LABEL', 'Пожалуйста, введите адрес электронной почты, указанный в параметрах вашей учётной записи. На него будет отправлен специальный проверочный код. После его получения вы сможете ввести новый пароль для вашей учётной записи.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(487, 'COM_USERS_SETTINGS_FIELDSET_LABEL', 'Основные настройки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(488, 'COM_USERS_USER_BLOCKED', 'Этот пользователь заблокирован. Если вы считаете, что это сделано по ошибке, пожалуйста, сообщите администратору сайта.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(489, 'COM_USERS_USER_FIELD_BACKEND_LANGUAGE_DESC', 'Выберите предпочтительный лично для вас язык панели управления.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(490, 'COM_USERS_USER_FIELD_BACKEND_LANGUAGE_LABEL', 'Язык панели управления', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(491, 'COM_USERS_USER_FIELD_BACKEND_TEMPLATE_DESC', 'Выбор шаблона панели управления. Параметр будет применён только для данного пользователя.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(492, 'COM_USERS_USER_FIELD_BACKEND_TEMPLATE_LABEL', 'Шаблон панели управления', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini');
INSERT INTO `yawnc_overrider` (`id`, `constant`, `string`, `file`) VALUES
(493, 'COM_USERS_USER_FIELD_EDITOR_DESC', 'Выберите предпочтительный лично для вас текстовый редактор', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(494, 'COM_USERS_USER_FIELD_EDITOR_LABEL', 'Редактор', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(495, 'COM_USERS_USER_FIELD_FRONTEND_LANGUAGE_DESC', 'Выберите предпочтительный лично для вас язык сайта.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(496, 'COM_USERS_USER_FIELD_FRONTEND_LANGUAGE_LABEL', 'Язык сайта', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(497, 'COM_USERS_USER_FIELD_HELPSITE_DESC', 'Сайт справочной системы панели управления', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(498, 'COM_USERS_USER_FIELD_HELPSITE_LABEL', 'Сайт справки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(499, 'COM_USERS_USER_FIELD_TIMEZONE_DESC', 'Укажите ваш часовой пояс', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(500, 'COM_USERS_USER_FIELD_TIMEZONE_LABEL', 'Часовой пояс', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(501, 'COM_USERS_USER_NOT_FOUND', 'Пользователь не найден', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(502, 'COM_USERS_USER_SAVE_FAILED', 'Ошибка при сохранении информации о пользователе: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(503, 'COM_USERS_REMIND_LIMIT_ERROR_N_HOURS_2', 'Вы превысили максимально-допустимое количество попыток восстановления пароля. Пожалуйста, повторите попытку через %s часа.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_users.ini'),
(504, 'COM_WEBLINKS_CONTENT_TYPE_WEBLINK', 'Ссылка', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_weblinks.ini'),
(505, 'COM_WEBLINKS_CONTENT_TYPE_CATEGORY', 'Категория ссылок', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_weblinks.ini'),
(506, 'COM_WEBLINKS_DEFAULT_PAGE_TITLE', 'Каталог ссылок', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_weblinks.ini'),
(507, 'COM_WEBLINKS_EDIT', 'Изменить ссылку', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_weblinks.ini'),
(508, 'COM_WEBLINKS_ERR_TABLES_NAME', 'Ссылка с таким названием уже присутствует в выбранной категории. Введите другое название.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_weblinks.ini'),
(509, 'COM_WEBLINKS_ERR_TABLES_PROVIDE_URL', 'Пожалуйста, введите корректный URL-адрес', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_weblinks.ini'),
(510, 'COM_WEBLINKS_ERR_TABLES_TITLE', 'Необходимо указать название ссылки.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_weblinks.ini'),
(511, 'COM_WEBLINKS_ERROR_CATEGORY_NOT_FOUND', 'Категория ссылок не найдена', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_weblinks.ini'),
(512, 'COM_WEBLINKS_ERROR_UNIQUE_ALIAS', 'Другая ссылка с таким же алиасом уже присутствует в данной категории', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_weblinks.ini'),
(513, 'COM_WEBLINKS_ERROR_WEBLINK_NOT_FOUND', 'Ссылка не найдена', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_weblinks.ini'),
(514, 'COM_WEBLINKS_ERROR_WEBLINK_URL_INVALID', 'URL-адрес ссылки не корректен', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_weblinks.ini'),
(515, 'COM_WEBLINKS_FIELD_ALIAS_DESC', 'Алиас используется только для внутренних нужд системы. Оставьте это&#160;поле пустым и Joomla автоматически заполнит его значением, созданным на основе названия ссылки. В одной категории не должно быть двух ссылок с одинаковым алиасом.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_weblinks.ini'),
(516, 'COM_WEBLINKS_FIELD_CATEGORY_DESC', 'Вы должны выбрать категорию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_weblinks.ini'),
(517, 'COM_WEBLINKS_FIELD_DESCRIPTION_DESC', 'В это поле можно ввести описание ссылки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_weblinks.ini'),
(518, 'COM_WEBLINKS_FILTER_LABEL', 'Фильтр', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_weblinks.ini'),
(519, 'COM_WEBLINKS_FILTER_SEARCH_DESC', 'Фильтр поиска по ссылкам', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_weblinks.ini'),
(520, 'COM_WEBLINKS_FIELD_TITLE_DESC', 'Необходимо ввести название ссылки.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_weblinks.ini'),
(521, 'COM_WEBLINKS_FIELD_URL_DESC', 'Необходимо ввести ссылку (URL).', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_weblinks.ini'),
(522, 'COM_WEBLINKS_FIELD_URL_LABEL', 'Ссылка (URL)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_weblinks.ini'),
(523, 'COM_WEBLINKS_FORM_CREATE_WEBLINK', 'Добавить ссылку', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_weblinks.ini'),
(524, 'COM_WEBLINKS_GRID_TITLE', 'Название', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_weblinks.ini'),
(525, 'COM_WEBLINKS_LINK', 'Ссылка', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_weblinks.ini'),
(526, 'COM_WEBLINKS_NAME', 'Название', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_weblinks.ini'),
(527, 'COM_WEBLINKS_NO_WEBLINKS', 'В этой категории отсутствуют ссылки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_weblinks.ini'),
(528, 'COM_WEBLINKS_NUM', 'Кол-во ссылок:', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_weblinks.ini'),
(529, 'COM_WEBLINKS_FORM_EDIT_WEBLINK', 'Редактирование ссылки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_weblinks.ini'),
(530, 'COM_WEBLINKS_FORM_SUBMIT_WEBLINK', 'Добавление новой ссылки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_weblinks.ini'),
(531, 'COM_WEBLINKS_SAVE_SUCCESS', 'Ссылка успешно сохранена', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_weblinks.ini'),
(532, 'COM_WEBLINKS_SUBMIT_SAVE_SUCCESS', 'Ссылка успешно добавлена', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_weblinks.ini'),
(533, 'COM_WEBLINKS_WEB_LINKS', 'Ссылки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_weblinks.ini'),
(534, 'JGLOBAL_NEWITEMSLAST_DESC', 'Новые ссылки по умолчанию располагаются последними. Очерёдность может быть изменена после сохранения новой ссылки.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_weblinks.ini'),
(535, 'COM_WRAPPER_NO_IFRAMES', 'Эта функция работает неправильно. К сожалению, ваш браузер не поддерживает Inline Frames.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.com_wrapper.ini'),
(536, 'FILES_JOOMLA', 'CMS Joomla', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.files_joomla.sys.ini'),
(537, 'FILES_JOOMLA_ERROR_FILE_FOLDER', 'Ошибка удаления файла или директории %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.files_joomla.sys.ini'),
(538, 'FILES_JOOMLA_ERROR_MANIFEST', 'Ошибка обновления кэша манифеста: (тип, элемент, директория, клиент) = (%s, %s, %s, %s)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.files_joomla.sys.ini'),
(539, 'FILES_JOOMLA_XML_DESCRIPTION', 'Система управления сайтом Joomla! 3', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.files_joomla.sys.ini'),
(540, 'FINDER_CLI', 'Индексатор системы <strong>Умный Поиск</strong>', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.finder_cli.ini'),
(541, 'FINDER_CLI_BATCH_COMPLETE', ' * Обработано %s заданий за %s секунд.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.finder_cli.ini'),
(542, 'FINDER_CLI_FILTER_RESTORE_WARNING', 'Предупреждение: Невозможно найти термин %s/%s в фильтре %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.finder_cli.ini'),
(543, 'FINDER_CLI_INDEX_PURGE', 'Очистка индекса', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.finder_cli.ini'),
(544, 'FINDER_CLI_INDEX_PURGE_FAILED', '- очистка индекса не удалась', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.finder_cli.ini'),
(545, 'FINDER_CLI_INDEX_PURGE_SUCCESS', '- индекс успешно очищен', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.finder_cli.ini'),
(546, 'FINDER_CLI_PROCESS_COMPLETE', 'Общее время выполнения: %s секунд.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.finder_cli.ini'),
(547, 'FINDER_CLI_RESTORE_FILTER_COMPLETED', '- кол-во восстановленных фильтров: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.finder_cli.ini'),
(548, 'FINDER_CLI_RESTORE_FILTERS', 'Восстанавление фильтров', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.finder_cli.ini'),
(549, 'FINDER_CLI_SAVE_FILTER_COMPLETED', '- кол-во сохраненных фильтров: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.finder_cli.ini'),
(550, 'FINDER_CLI_SAVE_FILTERS', 'Сохранение фильтров', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.finder_cli.ini'),
(551, 'FINDER_CLI_SETTING_UP_PLUGINS', 'Подключение плагинов поиска', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.finder_cli.ini'),
(552, 'FINDER_CLI_SETUP_ITEMS', 'Создано %s объектов за %s секунд.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.finder_cli.ini'),
(553, 'FINDER_CLI_STARTING_INDEXER', 'Запуск индексатора', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.finder_cli.ini'),
(554, 'JERROR_PARSING_LANGUAGE_FILE', '&#160; имеются ошибки в строках %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(555, 'ERROR', 'Ошибка', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(556, 'MESSAGE', 'Сообщение', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(557, 'NOTICE', 'Внимание', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(558, 'WARNING', 'Предупреждение', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(559, 'J1', '1', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(560, 'J2', '2', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(561, 'J3', '3', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(562, 'J4', '4', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(563, 'J5', '5', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(564, 'J6', '6', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(565, 'J7', '7', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(566, 'J8', '8', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(567, 'J9', '9', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(568, 'J10', '10', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(569, 'J15', '15', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(570, 'J20', '20', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(571, 'J25', '25', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(572, 'J30', '30', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(573, 'J50', '50', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(574, 'J100', '100', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(575, 'JACTION_ADMIN', 'Настраивать', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(576, 'JACTION_ADMIN_GLOBAL', 'Суперадминистратор', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(577, 'JACTION_COMPONENT_SETTINGS', 'Параметры компонента', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(578, 'JACTION_CREATE', 'Создать', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(579, 'JACTION_DELETE', 'Удалить', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(580, 'JACTION_EDIT', 'Изменить', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(581, 'JACTION_EDITOWN', 'Изменять свои', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(582, 'JACTION_EDITSTATE', 'Изменять состояние', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(583, 'JACTION_LOGIN_ADMIN', 'Вход в панель управления', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(584, 'JACTION_LOGIN_SITE', 'Вход на сайт', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(585, 'JACTION_MANAGE', 'Управление компонентом', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(586, 'JADMINISTRATOR', 'Панель управления', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(587, 'JALL', 'Все', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(588, 'JALL_LANGUAGE', 'Все', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(589, 'JARCHIVED', 'В архиве', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(590, 'JAUTHOR', 'Автор', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(591, 'JCANCEL', 'Отмена', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(592, 'JCATEGORY', 'Категория', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(593, 'JDATE', 'Дата', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(594, 'JDEFAULT', 'По умолчанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(595, 'JDETAILS', 'Подробности', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(596, 'JDISABLED', 'Отключено', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(597, 'JEDITOR', 'Редактор', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(598, 'JENABLED', 'Включен', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(599, 'JEXPIRED', 'Устарело', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(600, 'JFALSE', 'False', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(601, 'JFEATURED', 'Избранные', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(602, 'JHIDE', 'Скрыть', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(603, 'JINVALID_TOKEN', 'Неверный параметр', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(604, 'JLOGIN', 'Войти', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(605, 'JLOGOUT', 'Выйти', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(606, 'JNEW', 'Новый', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(607, 'JNEXT', 'Вперёд', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(608, 'JNO', 'Нет', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(609, 'JNONE', 'Не найдено', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(610, 'JNOTPUBLISHEDYET', 'Ещё не опубликовано', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(611, 'JNOTICE', 'Внимание', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(612, 'JOFF', 'Выкл.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(613, 'JOFFLINE_MESSAGE', 'Сайт закрыт на техническое обслуживание.<br />Пожалуйста, зайдите позже.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(614, 'JON', 'Вкл.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(615, 'JOPTIONS', 'Настройки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(616, 'JPAGETITLE', '%1$s - %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(617, 'JPREV', 'Назад', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(618, 'JPREVIOUS', 'Предыдущий', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(619, 'JPUBLISHED', 'Опубликовано', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(620, 'JREGISTER', 'Регистрация', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(621, 'JREQUIRED', 'Обязательно', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(622, 'JSAVE', 'Сохранить', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(623, 'JSHOW', 'Показывать', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(624, 'JSITE', 'Сайт', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(625, 'JSTATUS', 'Состояние', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(626, 'JSUBMIT', 'Отправить', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(627, 'JTAG', 'Метки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(628, 'JTAG_DESC', 'Добавление и удаление меток.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(629, 'JTOOLBAR_VERSIONS', 'Версии', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(630, 'JTRASH', 'В корзину', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(631, 'JTRASHED', 'В корзине', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(632, 'JTRUE', 'True', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(633, 'JUNPUBLISHED', 'Не опубликовано', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(634, 'JYEAR', 'Год', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(635, 'JYES', 'Да', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(636, 'JBROWSERTARGET_MODAL', 'Модально', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(637, 'JBROWSERTARGET_NEW', 'Открывать в новом окне', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(638, 'JBROWSERTARGET_PARENT', 'Открывать в родительском окне', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(639, 'JBROWSERTARGET_POPUP', 'Открывать во всплывающем окне', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(640, 'JERROR_ALERTNOAUTHOR', 'Для просмотра этой информации необходимо пройти авторизацию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(641, 'JERROR_ALERTNOTEMPLATE', '<strong>Шаблон для данной страницы недоступен. Пожалуйста, сообщите об этом Администратору сайта.</strong>', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(642, 'JERROR_AN_ERROR_HAS_OCCURRED', 'Обнаружена ошибка.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(643, 'JERROR_COULD_NOT_FIND_TEMPLATE', 'Шаблон "%s" не найден.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(644, 'JERROR_ERROR', 'Ошибка', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(645, 'JERROR_LAYOUT_AN_OUT_OF_DATE_BOOKMARK_FAVOURITE', '<strong>просроченная закладка/избранное</strong>', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(646, 'JERROR_LAYOUT_ERROR_HAS_OCCURRED_WHILE_PROCESSING_YOUR_REQUEST', 'В процессе обработки вашего запроса произошла ошибка.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(647, 'JERROR_LAYOUT_GO_TO_THE_HOME_PAGE', 'Вернуться на Домашнюю страницу', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(648, 'JERROR_LAYOUT_HOME_PAGE', 'Домашняя страница', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(649, 'JERROR_LAYOUT_MIS_TYPED_ADDRESS', '<strong>пропущен адрес</strong>', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(650, 'JERROR_LAYOUT_NOT_ABLE_TO_VISIT', 'Вы не можете посетить текущую страницу по причине:', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(651, 'JERROR_LAYOUT_PAGE_NOT_FOUND', 'Такой страницы не существует.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(652, 'JERROR_LAYOUT_PLEASE_CONTACT_THE_SYSTEM_ADMINISTRATOR', 'Если проблемы продолжатся, пожалуйста, обратитесь к системному администратору сайта и сообщите об ошибке, описание которой приведено ниже.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(653, 'JERROR_LAYOUT_PLEASE_TRY_ONE_OF_THE_FOLLOWING_PAGES', 'Пожалуйста, перейдите на одну из следующих страниц:', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(654, 'JERROR_LAYOUT_REQUESTED_RESOURCE_WAS_NOT_FOUND', 'Запрашиваемый ресурс не найден.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(655, 'JERROR_LAYOUT_SEARCH', 'Вы можете воспользоваться поиском по сайту или перейти на главную страницу сайта.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(656, 'JERROR_LAYOUT_SEARCH_ENGINE_OUT_OF_DATE_LISTING', 'поисковый механизм, у которого <strong>просрочен список для этого сайта</strong>', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(657, 'JERROR_LAYOUT_SEARCH_PAGE', 'Поиск по сайту', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(658, 'JERROR_LAYOUT_YOU_HAVE_NO_ACCESS_TO_THIS_PAGE', 'у вас <strong>нет права доступа</strong> на эту страницу', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(659, 'JERROR_LOADING_MENUS', 'Ошибка загрузки меню: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(660, 'JERROR_LOGIN_DENIED', 'У вас нет права доступа к закрытой части сайта.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(661, 'JERROR_NOLOGIN_BLOCKED', 'Вход запрещён! Ваша учётная запись заблокирована или ещё не активирована.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(662, 'JERROR_SESSION_STARTUP', 'Ошибка инициализации сессии.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(663, 'JERROR_TABLE_BIND_FAILED', 'hmm %s ...', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(664, 'JERROR_USERS_PROFILE_NOT_FOUND', 'Профиль пользователя не найден', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(665, 'JFIELD_ACCESS_DESC', 'Уровень доступа для данной страницы', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(666, 'JFIELD_ACCESS_LABEL', 'Доступ', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(667, 'JFIELD_ALIAS_DESC', 'Алиас (псевдоним) применяется для создания сокращённой URL-ссылки (SEF URL). Если оставить это поле пустым, Joomla! заполнит его значением по умолчанию, созданным на основе заголовка объекта. Это значение будет зависеть от установленных на сайте параметров SEO (Общие настройки -> Сайт).<br /> В случае использования адресов страниц в формате Unicode, Алиас будет состоять из символов того же языка, что и заголовок. При желании можно вручную ввести любые символы в кодировке UTF-8 (т.е. создавать адреса на русском языке, как в Wiki), но следует помнить, что пробелы и некоторые другие служебные символы при этом будут заменены на дефисы.<br /> По умолчанию, без использования Unicode, Алиас генерируется из символов заголовка, переведённых в нижний регистр. Пробелы при этом также заменяются на тире. Можно ввести Алиас вручную, латинскими символами, используя строчные буквы и дефисы без пробелов. Допускается вводить символ подчёркивания. Если заголовок состоит не из латинских букв, значение алиаса будет состоять из текущей даты и времени.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(668, 'JFIELD_ALIAS_LABEL', 'Алиас', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(669, 'JFIELD_ALIAS_PLACEHOLDER', 'Автоматически создавать из заголовка', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(670, 'JFIELD_ALT_PAGE_TITLE_LABEL', 'Альтернативный заголовок страницы', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(671, 'JFIELD_CATEGORY_DESC', 'Категория', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(672, 'JFIELD_LANGUAGE_DESC', 'Назначить язык этому материалу.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(673, 'JFIELD_LANGUAGE_LABEL', 'Язык', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(674, 'JFIELD_META_DESCRIPTION_DESC', 'Необязательный текст для использования в качестве описания HTML-страницы. Как правило, этот текст используется поисковыми системами для показа описания страницы в результатах поиска.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(675, 'JFIELD_META_DESCRIPTION_LABEL', 'Мета-тег Description', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(676, 'JFIELD_META_KEYWORDS_DESC', 'Слова и фразы, разделённые запятыми, которые будут выведены в мета-теге Keywords HTML-страницы.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(677, 'JFIELD_META_KEYWORDS_LABEL', 'Мета-тег Keywords', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(678, 'JFIELD_META_RIGHTS_DESC', 'Описание авторских прав на данный материал.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(679, 'JFIELD_META_RIGHTS_LABEL', 'Авторские права', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(680, 'JFIELD_ORDERING_DESC', 'Порядок материалов в данной категории', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(681, 'JFIELD_ORDERING_LABEL', 'Порядок', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(682, 'JFIELD_PUBLISHED_DESC', 'Устанавливает состояние публикации.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(683, 'JFIELD_TITLE_DESC', 'Заголовок материала', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(684, 'JGLOBAL_ARTICLES', 'Материалы', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(685, 'JGLOBAL_AUTH_ACCESS_DENIED', 'В доступе отказано', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(686, 'JGLOBAL_AUTH_ACCESS_GRANTED', 'Доступ разрешён', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(687, 'JGLOBAL_AUTH_BIND_FAILED', 'Ошибка привязки к серверу LDAP', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(688, 'JGLOBAL_AUTH_CANCEL', 'Аутентификация отменена', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(689, 'JGLOBAL_AUTH_CURL_NOT_INSTALLED', 'Curl не установлен', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(690, 'JGLOBAL_AUTH_EMPTY_PASS_NOT_ALLOWED', 'Пустой пароль не допускается', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(691, 'JGLOBAL_AUTH_FAIL', 'Ошибка аутентификации', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(692, 'JGLOBAL_AUTH_FAILED', 'Не удалось проверить подлинность: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(693, 'JGLOBAL_AUTH_INCORRECT', 'Неправильное имя пользователя или пароль', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(694, 'JGLOBAL_AUTH_INVALID_PASS', 'Имя пользователя и пароль не совпадают или у вас ещё нет учётной записи на сайте', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(695, 'JGLOBAL_AUTH_INVALID_SECRETKEY', 'Неверный Секретный код для двухфакторной аутентификации.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(696, 'JGLOBAL_AUTH_NO_BIND', 'Невозможно привязать LDAP', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(697, 'JGLOBAL_AUTH_NO_CONNECT', 'Не удаётся подключиться к серверу LDAP', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(698, 'JGLOBAL_AUTH_NO_REDIRECT', 'Не удалось перенаправить на сервер: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(699, 'JGLOBAL_AUTH_NO_USER', 'Имя пользователя и пароль не совпадают или у вас ещё нет учётной записи на сайте', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(700, 'JGLOBAL_AUTH_NOT_CREATE_DIR', 'Не удалось создать каталог %s. Пожалуйста, проверьте права доступа.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(701, 'JGLOBAL_AUTH_PASS_BLANK', 'Пароль LDAP не может быть пустым', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(702, 'JGLOBAL_AUTH_UNKNOWN_ACCESS_DENIED', 'Результат неизвестен. В доступе отказано', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(703, 'JGLOBAL_AUTH_USER_BLACKLISTED', 'Пользователь занесён в чёрный список', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(704, 'JGLOBAL_AUTH_USER_NOT_FOUND', 'Не удалось найти пользователя', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(705, 'JGLOBAL_AUTO', 'Авто', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(706, 'JGLOBAL_CATEGORY_NOT_FOUND', 'Категория не найдена', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(707, 'JGLOBAL_CENTER', 'Центр', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(708, 'JGLOBAL_CHECK_ALL', 'Выбрать все', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(709, 'JGLOBAL_CLICK_TO_SORT_THIS_COLUMN', 'Нажмите для сортировки по этому столбцу', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(710, 'JGLOBAL_CREATED_DATE_ON', 'Создано %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(711, 'JGLOBAL_DESCRIPTION', 'Описание', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(712, 'JGLOBAL_DISPLAY_NUM', 'Кол-во строк:', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(713, 'JGLOBAL_EDIT', 'Изменить', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(714, 'JGLOBAL_EMAIL', 'E-mail', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(715, 'JGLOBAL_FIELD_CREATED_BY_ALIAS_DESC', 'Подменяет имя автора при отображении материала', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(716, 'JGLOBAL_FIELD_CREATED_BY_ALIAS_LABEL', 'Псевдоним автора', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(717, 'JGLOBAL_FIELD_FEATURED_DESC', 'Связь материала с макетом блога избранных материалов', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(718, 'JGLOBAL_FIELD_FEATURED_LABEL', 'Избранные', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(719, 'JGLOBAL_FIELD_PUBLISH_DOWN_DESC', 'Дата снятия с публикации (не обязательно)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(720, 'JGLOBAL_FIELD_PUBLISH_DOWN_LABEL', 'Завершение публикации', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(721, 'JGLOBAL_FIELD_PUBLISH_UP_DESC', 'Дата начала публикации (не обязательно)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(722, 'JGLOBAL_FIELD_PUBLISH_UP_LABEL', 'Начало публикации', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(723, 'JGLOBAL_FIELD_VERSION_NOTE_DESC', 'Введите необязательный комментарий для данной версии объекта.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(724, 'JGLOBAL_FIELD_VERSION_NOTE_LABEL', 'Комментарий версии', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(725, 'JGLOBAL_FILTER_BUTTON', 'Применить', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(726, 'JGLOBAL_FILTER_LABEL', 'Фильтр', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(727, 'JGLOBAL_FULL_TEXT', 'Полный текст', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(728, 'JGLOBAL_GT', '&gt;', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(729, 'JGLOBAL_HELPREFRESH_BUTTON', 'Обновить', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(730, 'JGLOBAL_HITS', 'Просмотры', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(731, 'JGLOBAL_HITS_COUNT', 'Просмотров: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(732, 'JGLOBAL_ICON_SEP', '|', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(733, 'JGLOBAL_INHERIT', 'Наследовать', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(734, 'JGLOBAL_INTRO_TEXT', 'Вступительный текст', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(735, 'JGLOBAL_KEEP_TYPING', 'Продолжайте ввод...', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(736, 'JGLOBAL_LEFT', 'Слева', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(737, 'JGLOBAL_LOOKING_FOR', 'Поиск', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(738, 'JGLOBAL_LT', '&lt;', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(739, 'JGLOBAL_NEWITEMSLAST_DESC', 'Новый элемент по умолчанию будет первым. Изменить порядок можно после сохранения.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(740, 'JGLOBAL_NO_MATCHING_RESULTS', 'Результаты отсутствуют', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(741, 'JGLOBAL_NUM', '№', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(742, 'JGLOBAL_OTPMETHOD_NONE', 'Отключить двухфакторную аутентификацию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(743, 'JGLOBAL_PASSWORD', 'Пароль', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(744, 'JGLOBAL_PASSWORD_RESET_REQUIRED', 'Перед продолжением необходимо сбросить ваш текущий пароль.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(745, 'JGLOBAL_PRINT', 'Печать', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(746, 'JGLOBAL_RECORD_NUMBER', 'ID записи: %d', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(747, 'JGLOBAL_REMEMBER_ME', 'Запомнить меня', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(748, 'JGLOBAL_REMEMBER_MUST_LOGIN', 'Из соображений безопасности вам необходимо авторизоваться для редактирования вашей персональной информации.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(749, 'JGLOBAL_RESOURCE_NOT_FOUND', 'Ресурс не найден', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(750, 'JGLOBAL_RIGHT', 'Справа', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(751, 'JGLOBAL_ROOT', 'Корень', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(752, 'JGLOBAL_SECRETKEY', 'Секретный код', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(753, 'JGLOBAL_SECRETKEY_HELP', 'Если вы включили двухфакторную аутентификацию для вашей учётной записи, введите ваш секретный код. Если вы не знаете, что это такое - оставьте поле пустым.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(754, 'JGLOBAL_SELECT_AN_OPTION', 'Выберите значение', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(755, 'JGLOBAL_SELECT_NO_RESULTS_MATCH', 'Результаты не совпадают', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(756, 'JGLOBAL_SELECT_SOME_OPTIONS', 'Выберите несколько значений', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(757, 'JGLOBAL_START_PUBLISH_AFTER_FINISH', 'Дата начала публикации должна быть меньше даты окончания публикации', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(758, 'JGLOBAL_SUBCATEGORIES', 'Подкатегории', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(759, 'JGLOBAL_SUBHEADING_DESC', 'Произвольный текст для отображения в качестве подзаголовка.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(760, 'JGLOBAL_TITLE', 'Заголовок', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(761, 'JGLOBAL_USE_GLOBAL', 'По умолчанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(762, 'JGLOBAL_USERNAME', 'Логин', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(763, 'JGLOBAL_VALIDATION_FORM_FAILED', 'Неверная форма', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(764, 'JGLOBAL_YOU_MUST_LOGIN_FIRST', 'Пожалуйста, прежде пройдите авторизацию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(765, 'JGRID_HEADING_ACCESS', 'Доступ', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(766, 'JGRID_HEADING_ID', 'ID', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(767, 'JGRID_HEADING_LANGUAGE', 'Язык', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(768, 'JLIB_DATABASE_ERROR_ADAPTER_MYSQL', 'MySQL-адаптер ''mysql'' недоступен.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(769, 'JLIB_DATABASE_ERROR_ADAPTER_MYSQLI', 'MySQLi-адаптер ''mysqli'' недоступен.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(770, 'JLIB_DATABASE_ERROR_CONNECT_DATABASE', 'Не удаётся подключиться к базе данных: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(771, 'JLIB_DATABASE_ERROR_CONNECT_MYSQL', 'Не удаётся подключиться к MySQL.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(772, 'JLIB_DATABASE_ERROR_DATABASE_CONNECT', 'Не удаётся подключиться к базе данных', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(773, 'JLIB_DATABASE_ERROR_LOAD_DATABASE_DRIVER', 'Не удалось загрузить драйвер базы данных: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(774, 'JLIB_ERROR_INFINITE_LOOP', 'В JError обнаружен бесконечный цикл', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(775, 'JOPTION_SELECT_ACCESS', '- Выберите уровень доступа -', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(776, 'JOPTION_SELECT_AUTHOR', '- Выберите автора -', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(777, 'JOPTION_SELECT_CATEGORY', '- Выберите категорию -', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(778, 'JOPTION_SELECT_LANGUAGE', '- Выберите язык -', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(779, 'JOPTION_SELECT_PUBLISHED', '- Выберите состояние -', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(780, 'JOPTION_SELECT_MAX_LEVELS', '- Выберите кол-во уровней -', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(781, 'JOPTION_SELECT_TAG', '- Выберите метку -', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(782, 'JOPTION_USE_DEFAULT', '- Использовать по умолчанию -', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(783, 'JSEARCH_FILTER_CLEAR', 'Очистить', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(784, 'JSEARCH_FILTER_LABEL', 'Фильтр', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(785, 'JSEARCH_FILTER_SUBMIT', 'Искать', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(786, 'JSEARCH_FILTER', 'Искать', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(787, 'DATE_FORMAT_LC', 'd.m.Y', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(788, 'DATE_FORMAT_LC1', 'd.m.Y', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(789, 'DATE_FORMAT_LC2', 'd.m.Y H:i', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(790, 'DATE_FORMAT_LC3', 'd F Y', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(791, 'DATE_FORMAT_LC4', 'd.m.Y', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(792, 'DATE_FORMAT_JS1', 'd.m.y', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(793, 'JANUARY_SHORT', 'Янв', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(794, 'JANUARY', 'Январь', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(795, 'FEBRUARY_SHORT', 'Фев', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(796, 'FEBRUARY', 'Февраль', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(797, 'MARCH_SHORT', 'Март', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(798, 'MARCH', 'Март', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(799, 'APRIL_SHORT', 'Апр', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(800, 'APRIL', 'Апрель', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(801, 'MAY_SHORT', 'Май', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(802, 'MAY', 'Май', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(803, 'JUNE_SHORT', 'Июнь', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(804, 'JUNE', 'Июнь', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(805, 'JULY_SHORT', 'Июль', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(806, 'JULY', 'Июль', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(807, 'AUGUST_SHORT', 'Авг', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(808, 'AUGUST', 'Август', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(809, 'SEPTEMBER_SHORT', 'Сен', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(810, 'SEPTEMBER', 'Сентябрь', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(811, 'OCTOBER_SHORT', 'Окт', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(812, 'OCTOBER', 'Октябрь', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(813, 'NOVEMBER_SHORT', 'Нояб', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(814, 'NOVEMBER', 'Ноябрь', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(815, 'DECEMBER_SHORT', 'Дек', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(816, 'DECEMBER', 'Декабрь', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(817, 'SAT', 'Сб', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(818, 'SATURDAY', 'Суббота', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(819, 'SUN', 'Вс', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(820, 'SUNDAY', 'Воскресенье', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(821, 'MON', 'Пн', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(822, 'MONDAY', 'Понедельник', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(823, 'TUE', 'Вт', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(824, 'TUESDAY', 'Вторник', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(825, 'WED', 'Ср', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(826, 'WEDNESDAY', 'Среда', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(827, 'THU', 'Чт', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(828, 'THURSDAY', 'Четверг', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(829, 'FRI', 'Пт', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(830, 'FRIDAY', 'Пятница', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(831, 'DECIMALS_SEPARATOR', '.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(832, 'THOUSANDS_SEPARATOR', ' ', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(833, 'PHPMAILER_PROVIDE_ADDRESS', 'Необходимо указать хотя бы одного получателя электронной почты.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(834, 'PHPMAILER_MAILER_IS_NOT_SUPPORTED', 'Почтовый агент не поддерживается', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(835, 'PHPMAILER_EXECUTE', 'Не удалось выполнить: ', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(836, 'PHPMAILER_INSTANTIATE', 'Не удалось вызвать функцию mail.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(837, 'PHPMAILER_AUTHENTICATE', 'Ошибка SMTP! Не удалось пройти авторизацию.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(838, 'PHPMAILER_FROM_FAILED', 'Ошибка в перечисленных адресах отправителей: ', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini');
INSERT INTO `yawnc_overrider` (`id`, `constant`, `string`, `file`) VALUES
(839, 'PHPMAILER_RECIPIENTS_FAILED', 'Ошибка SMTP! Следующие получатели недоступны: ', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(840, 'PHPMAILER_DATA_NOT_ACCEPTED', 'Ошибка SMTP! Данные были не приняты.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(841, 'PHPMAILER_CONNECT_HOST', 'Ошибка SMTP! Не удаётся подключиться к хосту SMTP.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(842, 'PHPMAILER_FILE_ACCESS', 'Не удалось получить доступ к файлу: ', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(843, 'PHPMAILER_FILE_OPEN', 'Файловая ошибка: Не удалось открыть файл: ', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(844, 'PHPMAILER_ENCODING', 'Неизвестная кодировка: ', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(845, 'PHPMAILER_SIGNING_ERROR', 'Ошибка отправки письма: ', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(846, 'PHPMAILER_SMTP_ERROR', 'Ошибка SMTP: ', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(847, 'PHPMAILER_EMPTY_MESSAGE', 'Пустое тело письма', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(848, 'PHPMAILER_INVALID_ADDRESS', 'Некорректный адрес', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(849, 'PHPMAILER_VARIABLE_SET', 'Ошибка установки/сброса переменной: ', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(850, 'PHPMAILER_SMTP_CONNECT_FAILED', 'Ошибка подключения к SMTP', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(851, 'PHPMAILER_TLS', 'Не удалось запустить TLS', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(852, 'MYSQL', 'MySQL', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(853, 'MYSQLI', 'MySQLi', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(854, 'ORACLE', 'Oracle', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(855, 'PDOMYSQL', 'MySQL (PDO)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(856, 'POSTGRESQL', 'PostgreSQL', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(857, 'SQLAZURE', 'Microsoft SQL Azure', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(858, 'SQLITE', 'SQLite', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(859, 'SQLSRV', 'Microsoft SQL Server', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(860, 'JSEARCH_TOOLS', 'Фильтрация', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(861, 'JSEARCH_TOOLS_DESC', 'Фильтрация списка.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(862, 'JSEARCH_TOOLS_ORDERING', 'Порядок отображения:', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.ini'),
(863, 'LIB_FOF_XML_DESCRIPTION', 'Framework-on-Framework (FOF) — фреймворк для быстрой разработки расширений Joomla!', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_fof.sys.ini'),
(864, 'LIB_IDNA_XML_DESCRIPTION', 'Класс idna_convert предназначен для преобразования интернационализованных доменных имён (см. подробнее  RFC 3490, 3491, 3492 и 3454) - из формы содержащей символы национальных алфавитов, в форму, используемую в DNS (символы ASCII).', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_idna_convert.sys.ini'),
(865, 'JERROR_PARSING_LANGUAGE_FILE', '&#160; имеются ошибки в строках %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(866, 'JLIB_APPLICATION_ERROR_ACCESS_FORBIDDEN', 'Доступ запрещён', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(867, 'JLIB_APPLICATION_ERROR_APPLICATION_GET_NAME', 'JApplication: :getName() : Не может получить или обработать имя класса.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(868, 'JLIB_APPLICATION_ERROR_APPLICATION_LOAD', 'Не удалось загрузить приложение: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(869, 'JLIB_APPLICATION_ERROR_BATCH_CANNOT_CREATE', 'У вас нет прав на создание новых элементов в данной категории.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(870, 'JLIB_APPLICATION_ERROR_BATCH_CANNOT_EDIT', 'У вас нет прав на редактирование одно или нескольких элементов.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(871, 'JLIB_APPLICATION_ERROR_BATCH_FAILED', 'Пакетная операция завершилась с ошибкой: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(872, 'JLIB_APPLICATION_ERROR_BATCH_MOVE_CATEGORY_NOT_FOUND', 'Категория, в которую выполняется перемещение, не найдена.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(873, 'JLIB_APPLICATION_ERROR_BATCH_MOVE_ROW_NOT_FOUND', 'Перемещаемый объект не найден.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(874, 'JLIB_APPLICATION_ERROR_CHECKIN_FAILED', 'Снятие блокировки прервано из-за следующей ошибки: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(875, 'JLIB_APPLICATION_ERROR_CHECKIN_NOT_CHECKED', 'Объект не был заблокирован', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(876, 'JLIB_APPLICATION_ERROR_CHECKIN_USER_MISMATCH', 'Пользователь, пытающийся снять блокировку, не тот, который её установил.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(877, 'JLIB_APPLICATION_ERROR_CHECKOUT_FAILED', 'установка блокировки прервана с ошибкой: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(878, 'JLIB_APPLICATION_ERROR_CHECKOUT_USER_MISMATCH', 'Пользователь, пытающийся установить блокировку, не тот, который уже установил её ранее.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(879, 'JLIB_APPLICATION_ERROR_COMPONENT_NOT_FOUND', 'Компонент не найден', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(880, 'JLIB_APPLICATION_ERROR_COMPONENT_NOT_LOADING', 'Ошибка при загрузке компонента: %1$s, %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(881, 'JLIB_APPLICATION_ERROR_CONTROLLER_GET_NAME', 'JController: :getName() : Не может получить или опознать имя класса.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(882, 'JLIB_APPLICATION_ERROR_CREATE_RECORD_NOT_PERMITTED', 'Создание записей не допускается', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(883, 'JLIB_APPLICATION_ERROR_DELETE_NOT_PERMITTED', 'Удаление не допускается', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(884, 'JLIB_APPLICATION_ERROR_EDITSTATE_NOT_PERMITTED', 'Изменение состояния не допускается', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(885, 'JLIB_APPLICATION_ERROR_EDIT_ITEM_NOT_PERMITTED', 'Правка не допускается', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(886, 'JLIB_APPLICATION_ERROR_EDIT_NOT_PERMITTED', 'Правка не допускается', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(887, 'JLIB_APPLICATION_ERROR_HISTORY_ID_MISMATCH', 'Ошибка восстановления версии элемента из истории.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(888, 'JLIB_APPLICATION_ERROR_INSUFFICIENT_BATCH_INFORMATION', 'Недостаточно данных для выполнения пакетной операции', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(889, 'JLIB_APPLICATION_ERROR_INVALID_CONTROLLER_CLASS', 'Неверный класс контроллера: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(890, 'JLIB_APPLICATION_ERROR_INVALID_CONTROLLER', 'Недействительный контроллер: имя = ''%s'', формат = ''%s''', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(891, 'JLIB_APPLICATION_ERROR_LAYOUTFILE_NOT_FOUND', 'Макет %s не найден', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(892, 'JLIB_APPLICATION_ERROR_LIBRARY_NOT_FOUND', 'Библиотека не найдена', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(893, 'JLIB_APPLICATION_ERROR_LIBRARY_NOT_LOADING', 'Ошибка загрузки библиотеки: %1$s, %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(894, 'JLIB_APPLICATION_ERROR_MODEL_GET_NAME', 'JModel:: GetName (): Не удаётся получить или разобрать имя класса.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(895, 'JLIB_APPLICATION_ERROR_MODULE_LOAD', 'Ошибка при загрузке модуля %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(896, 'JLIB_APPLICATION_ERROR_PATHWAY_LOAD', 'Не удаётся загрузить пути: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(897, 'JLIB_APPLICATION_ERROR_REORDER_FAILED', 'Изменение порядка не удалось. Ошибка: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(898, 'JLIB_APPLICATION_ERROR_ROUTER_LOAD', 'Не удаётся загрузить маршрутизатор: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(899, 'JLIB_APPLICATION_ERROR_MODELCLASS_NOT_FOUND', 'Модель класса %s не найдена в файле', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(900, 'JLIB_APPLICATION_ERROR_SAVE_FAILED', 'Сохранить не удалось из-за ошибки: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(901, 'JLIB_APPLICATION_ERROR_SAVE_NOT_PERMITTED', 'Сохранение не разрешено', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(902, 'JLIB_APPLICATION_ERROR_TABLE_NAME_NOT_SUPPORTED', 'Таблица %s не поддерживается. Файл не найден.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(903, 'JLIB_APPLICATION_ERROR_TASK_NOT_FOUND', 'Задача [%s] не найдена', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(904, 'JLIB_APPLICATION_ERROR_UNHELD_ID', 'У вас нет прав на доступ к данной странице по прямой ссылке (#%d).', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(905, 'JLIB_APPLICATION_ERROR_VIEW_CLASS_NOT_FOUND', 'Класс представления не найден [class, file]: %1$s, %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(906, 'JLIB_APPLICATION_ERROR_VIEW_GET_NAME_SUBSTRING', 'JView: :getName (): Имя вашего класса содержит подстроку ''view''. Это вызывает проблемы при извлечении имени класса из имени представления объектов. Избегайте создания имён объектов, содержащих строку ''view''.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(907, 'JLIB_APPLICATION_ERROR_VIEW_GET_NAME', 'JView: :getName (): Невозможно получить или разобрать имя класса.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(908, 'JLIB_APPLICATION_ERROR_VIEW_NOT_FOUND', 'Представление не найдено [name, type, prefix]: %1$s, %2$s, %3$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(909, 'JLIB_APPLICATION_SAVE_SUCCESS', 'Пункт успешно сохранён.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(910, 'JLIB_APPLICATION_SUBMIT_SAVE_SUCCESS', 'Элемент успешно создан.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(911, 'JLIB_APPLICATION_SUCCESS_BATCH', 'Пакетная операция завершена успешно.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(912, 'JLIB_APPLICATION_SUCCESS_ITEM_REORDERED', 'Порядок успешно сохранён.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(913, 'JLIB_APPLICATION_SUCCESS_ORDERING_SAVED', 'Порядок успешно сохранён.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(914, 'JLIB_APPLICATION_SUCCESS_LOAD_HISTORY', 'Предыдущая версия успешно восстановлена. Сохранено в %s %s.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(915, 'JLIB_LOGIN_AUTHENTICATE', 'Логин или пароль введены неправильно, либо такой учётной записи ещё не существует.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(916, 'JLIB_CACHE_ERROR_CACHE_HANDLER_LOAD', 'Не удалось загрузить обработчик кэша: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(917, 'JLIB_CACHE_ERROR_CACHE_STORAGE_LOAD', 'Не удалось загрузить Cache Storage: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(918, 'JLIB_CAPTCHA_ERROR_PLUGIN_NOT_FOUND', 'Плагин CAPTCHA не найден (или не установлен). Пожалуйста, свяжитесь с администратором сайта', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(919, 'JLIB_CLIENT_ERROR_JFTP_NO_CONNECT', 'JFTP: :connect: Невозможно подключиться к серверу '' %1$s '' через порт '' %2$s ''', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(920, 'JLIB_CLIENT_ERROR_JFTP_NO_CONNECT_SOCKET', 'JFTP: :connect: Невозможно подключиться к серверу '' %1$s '' через порт '' %2$s ''. Ошибка сокета: %3$s и сообщение об ошибке: %4$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(921, 'JLIB_CLIENT_ERROR_JFTP_BAD_RESPONSE', 'JFTP: :connect: Некорректный ответ. Ответ сервера: %s [Expected: 220]', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(922, 'JLIB_CLIENT_ERROR_JFTP_BAD_USERNAME', 'JFTP: :login: Неверный логин. Ответ сервера: %1$s [Ожидали: 331]. Отправлен логин: %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(923, 'JLIB_CLIENT_ERROR_JFTP_BAD_PASSWORD', 'JFTP: :login: Неверный пароль. Ответ сервера: %1$s [Ожидали: 230]. Отправлен пароль: %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(924, 'JLIB_CLIENT_ERROR_JFTP_PWD_BAD_RESPONSE_NATIVE', 'FTP: :pwd: Некорректный ответ', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(925, 'JLIB_CLIENT_ERROR_JFTP_PWD_BAD_RESPONSE', 'JFTP: :pwd: Некорректный ответ. Ответ сервера: %s [Ожидали: 257]', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(926, 'JLIB_CLIENT_ERROR_JFTP_SYST_BAD_RESPONSE_NATIVE', 'JFTP: :syst: Некорректный ответ', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(927, 'JLIB_CLIENT_ERROR_JFTP_SYST_BAD_RESPONSE', 'JFTP: :syst: Некорректный ответ. Ответ сервера: %s [Ожидали: 215]', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(928, 'JLIB_CLIENT_ERROR_JFTP_CHDIR_BAD_RESPONSE_NATIVE', 'JFTP: :chdir: Некорректный ответ', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(929, 'JLIB_CLIENT_ERROR_JFTP_CHDIR_BAD_RESPONSE', 'JFTP: :chdir: Некорректный ответ. Ответ сервера: %1$s [Ожидали: 250]. Передан путь: %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(930, 'JLIB_CLIENT_ERROR_JFTP_REINIT_BAD_RESPONSE_NATIVE', 'JFTP: :reinit: Некорректный ответ', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(931, 'JLIB_CLIENT_ERROR_JFTP_REINIT_BAD_RESPONSE', 'JFTP: :reinit: Некорректный ответ. Ответ сервера: %s [Ожидали: 220]', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(932, 'JLIB_CLIENT_ERROR_JFTP_RENAME_BAD_RESPONSE_NATIVE', 'JFTP: :rename: Некорректный ответ', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(933, 'JLIB_CLIENT_ERROR_JFTP_RENAME_BAD_RESPONSE_FROM', 'JFTP: :rename: Некорректный ответ. Ответ сервера: %1$s [Ожидалось: 350]. Исходный путь: %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(934, 'JLIB_CLIENT_ERROR_JFTP_RENAME_BAD_RESPONSE_TO', 'JFTP: :rename: Некорректный ответ. Ответ сервера: %1$s [Ожидали: 250]. К пути: %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(935, 'JLIB_CLIENT_ERROR_JFTP_CHMOD_BAD_RESPONSE_NATIVE', 'JFTP: :chmod: Некорректный ответ', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(936, 'JLIB_CLIENT_ERROR_JFTP_CHMOD_BAD_RESPONSE', 'JFTP: :chmod: Некорректный ответ. Ответ сервера: %1$s [Ожидали: 250]. Передан путь: %2$s. Передан режим: %3$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(937, 'JLIB_CLIENT_ERROR_JFTP_DELETE_BAD_RESPONSE_NATIVE', 'JFTP: :delete: Некорректный ответ', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(938, 'JLIB_CLIENT_ERROR_JFTP_DELETE_BAD_RESPONSE', 'JFTP: :delete: Некорректный ответ. Ответ сервера: %1$s [Ожидали: 250]. Передан путь: %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(939, 'JLIB_CLIENT_ERROR_JFTP_MKDIR_BAD_RESPONSE_NATIVE', 'JFTP: :mkdir: Некорректный ответ', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(940, 'JLIB_CLIENT_ERROR_JFTP_MKDIR_BAD_RESPONSE', 'JFTP: :mkdir: Некорректный ответ. Ответ сервера: %1$s [Ожидали: 257]. Передан путь: %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(941, 'JLIB_CLIENT_ERROR_JFTP_RESTART_BAD_RESPONSE_NATIVE', 'JFTP: :restart: Некорректный ответ', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(942, 'JLIB_CLIENT_ERROR_JFTP_RESTART_BAD_RESPONSE', 'JFTP: :restart: Некорректный ответ. Ответ сервера: %1$s [Ожидали: 350]. Отправлена точка рестарта: %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(943, 'JLIB_CLIENT_ERROR_JFTP_CREATE_BAD_RESPONSE_BUFFER', 'JFTP: :create: Некорректный ответ', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(944, 'JLIB_CLIENT_ERROR_JFTP_CREATE_BAD_RESPONSE_PASSIVE', 'JFTP: :create: Невозможно использовать пассивный режим', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(945, 'JLIB_CLIENT_ERROR_JFTP_CREATE_BAD_RESPONSE', 'JFTP: :create: Некорректный ответ. Ответ сервера: %1$s [Ожидали: 150 или 125]. Передан путь: %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(946, 'JLIB_CLIENT_ERROR_JFTP_CREATE_BAD_RESPONSE_TRANSFER', 'JFTP: :create: Передача прервана. Ответ сервера: %1$s [Ожидали: 226]. Передан путь: %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(947, 'JLIB_CLIENT_ERROR_JFTP_READ_BAD_RESPONSE_BUFFER', 'JFTP: :read: Некорректный ответ', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(948, 'JLIB_CLIENT_ERROR_JFTP_READ_BAD_RESPONSE_PASSIVE', 'JFTP: :read: Невозможно использовать пассивный режим', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(949, 'JLIB_CLIENT_ERROR_JFTP_READ_BAD_RESPONSE', 'JFTP: :read: Некорректный ответ. Ответ сервера: %1$s [Ожидали: 150 или 125]. Передан путь: %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(950, 'JLIB_CLIENT_ERROR_JFTP_READ_BAD_RESPONSE_TRANSFER', 'JFTP: :read: Передача прервана. Ответ сервера: %1$s [Ожидали: 226]. Передан путь: %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(951, 'JLIB_CLIENT_ERROR_JFTP_GET_BAD_RESPONSE', 'JFTP: :get: Некорректный ответ', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(952, 'JLIB_CLIENT_ERROR_JFTP_GET_PASSIVE', 'JFTP: :get: Невозможно использовать пассивный режим', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(953, 'JLIB_CLIENT_ERROR_JFTP_GET_WRITING_LOCAL', 'JFTP: :get: Невозможно открыть локальный файл для записи Локальный путь: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(954, 'JLIB_CLIENT_ERROR_JFTP_GET_BAD_RESPONSE_RETR', 'JFTP: :get: Некорректный ответ. Ответ сервера: %1$s [Ожидали: 150 или 125]. Передан путь: %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(955, 'JLIB_CLIENT_ERROR_JFTP_GET_BAD_RESPONSE_TRANSFER', 'JFTP: :get: Передача прервана. Ответ сервера: %1$s [Ожидали: 226]. Передан путь: %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(956, 'JLIB_CLIENT_ERROR_JFTP_STORE_PASSIVE', 'JFTP: :store: Невозможно использовать пассивный режим', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(957, 'JLIB_CLIENT_ERROR_JFTP_STORE_BAD_RESPONSE', 'JFTP: :store: Некорректный ответ', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(958, 'JLIB_CLIENT_ERROR_JFTP_STORE_READING_LOCAL', 'JFTP: :store: Не удаётся открыть локальный файл для чтения. Локальный путь: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(959, 'JLIB_CLIENT_ERROR_JFTP_STORE_FIND_LOCAL', 'JFTP: :store: Не удаётся найти локальный файл. Локальный путь: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(960, 'JLIB_CLIENT_ERROR_JFTP_STORE_BAD_RESPONSE_STOR', 'JFTP: :store: Некорректный ответ. Ответ сервера: %1$s [Ожидали: 150 или 125]. Передан путь: %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(961, 'JLIB_CLIENT_ERROR_JFTP_STORE_DATA_PORT', 'JFTP: :store: Не удаётся произвести запись данных в порт сокета', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(962, 'JLIB_CLIENT_ERROR_JFTP_STORE_BAD_RESPONSE_TRANSFER', 'JFTP: :store: Передача прервана. Ответ сервера: %1$s [Ожидали: 226]. Передан путь: %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(963, 'JLIB_CLIENT_ERROR_JFTP_WRITE_PASSIVE', 'JFTP: :write: Невозможно использовать пассивный режим', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(964, 'JLIB_CLIENT_ERROR_JFTP_WRITE_BAD_RESPONSE', 'JFTP: :write: Некорректный ответ', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(965, 'JLIB_CLIENT_ERROR_JFTP_WRITE_BAD_RESPONSE_STOR', 'JFTP: :write: Некорректный ответ. Ответ сервера: %1$s [Ожидали: 150 или 125]. Передан путь: %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(966, 'JLIB_CLIENT_ERROR_JFTP_WRITE_DATA_PORT', 'JFTP: :write: Не могу записать данные порта сокета', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(967, 'JLIB_CLIENT_ERROR_JFTP_WRITE_BAD_RESPONSE_TRANSFER', 'JFTP: :write: Передача прервана. Ответ сервера: %1$s [Ожидали: 226]. Передан путь: %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(968, 'JLIB_CLIENT_ERROR_JFTP_LISTNAMES_PASSIVE', 'JFTP: :listNames: Невозможно использовать пассивный режим', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(969, 'JLIB_CLIENT_ERROR_JFTP_LISTNAMES_BAD_RESPONSE', 'JFTP: :listNames: Некорректный ответ', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(970, 'JLIB_CLIENT_ERROR_JFTP_LISTNAMES_BAD_RESPONSE_NLST', 'JFTP: :listNames: Некорректный ответ. Ответ сервера: %1$s [Ожидали: 150 или 125]. Передан путь: %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(971, 'JLIB_CLIENT_ERROR_JFTP_LISTNAMES_BAD_RESPONSE_TRANSFER', 'JFTP: :listNames: Передача прервана. Ответ сервера: %1$s [Ожидали: 226]. Передан путь: %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(972, 'JLIB_CLIENT_ERROR_JFTP_LISTDETAILS_BAD_RESPONSE', 'JFTP: :listDetails: Некорректный ответ', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(973, 'JLIB_CLIENT_ERROR_JFTP_LISTDETAILS_PASSIVE', 'JFTP: :listDetails: Невозможно использовать пассивный режим', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(974, 'JLIB_CLIENT_ERROR_JFTP_LISTDETAILS_BAD_RESPONSE_LIST', 'JFTP: :listDetails: Некорректный ответ. Ответ сервера: %1$s [Ожидали: 150 или 125]. Передан путь: %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(975, 'JLIB_CLIENT_ERROR_JFTP_LISTDETAILS_BAD_RESPONSE_TRANSFER', 'JFTP: :listDetails: Передача прервана. Ответ сервера: %1$s [Ожидали: 226]. Передан путь: %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(976, 'JLIB_CLIENT_ERROR_JFTP_LISTDETAILS_UNRECOGNISED', 'JFTP: :listDetails: Неизвестный формат списка каталогов', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(977, 'JLIB_CLIENT_ERROR_JFTP_PUTCMD_UNCONNECTED', 'JFTP: :_putCmd: Не подключен к порту управления', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(978, 'JLIB_CLIENT_ERROR_JFTP_PUTCMD_SEND', 'JFTP: :_putCmd: Невозможно послать команду: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(979, 'JLIB_CLIENT_ERROR_JFTP_VERIFYRESPONSE', 'JFTP: :_verifyResponse: превышено время ожидания или не был распознан ответ во время ожидания ответа от сервера. Ответ сервера: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(980, 'JLIB_CLIENT_ERROR_JFTP_PASSIVE_CONNECT_PORT', 'JFTP: :_passive: Нет подключения к порту управления', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(981, 'JLIB_CLIENT_ERROR_JFTP_PASSIVE_RESPONSE', 'JFTP: :_passive: Тайм-аут или нераспознанный ответ во время ожидания ответа от сервера. Ответ сервера: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(982, 'JLIB_CLIENT_ERROR_JFTP_PASSIVE_IP_OBTAIN', 'JFTP: :_passive: Невозможно получить IP и порт для передачи данных. Ответ сервера: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(983, 'JLIB_CLIENT_ERROR_JFTP_PASSIVE_IP_VALID', 'JFTP: :_passive: IP и порт для передачи данных не действительны. Ответ сервера: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(984, 'JLIB_CLIENT_ERROR_JFTP_PASSIVE_CONNECT', 'JFTP: :_passive: Невозможно подключиться к серверу %1$s через порт %2$s. Ошибка сокета: %3$s и сообщение об ошибке: %4$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(985, 'JLIB_CLIENT_ERROR_JFTP_MODE_BINARY', 'JFTP: :_mode: Некорректный ответ. Ответ сервера: %s [Ожидали: 200]. Передан режим: Binary', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(986, 'JLIB_CLIENT_ERROR_JFTP_MODE_ASCII', 'JFTP: :_mode: Некорректный ответ. Ответ сервера: %s [Ожидали: 200]. Передан режим: Ascii', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(987, 'JLIB_CLIENT_ERROR_HELPER_SETCREDENTIALSFROMREQUEST_FAILED', 'Похоже, что учётные данные пользователя не корректны...', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(988, 'JLIB_CLIENT_ERROR_LDAP_ADDRESS_NOT_AVAILABLE', 'Адрес недоступен.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(989, 'JLIB_DATABASE_ERROR_ADAPTER_MYSQL', 'Адаптер ''mysql'' для СУБД MySQL недоступен.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(990, 'JLIB_DATABASE_ERROR_ADAPTER_MYSQLI', 'Адаптер ''mysqli'' для СУБД MySQLi недоступен.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(991, 'JLIB_DATABASE_ERROR_BIND_FAILED_INVALID_SOURCE_ARGUMENT', '%s: :bind не выполнен. Неправильный аргумент.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(992, 'JLIB_DATABASE_ERROR_ARTICLE_UNIQUE_ALIAS', 'Другой материал из этой категории уже имеет такой алиас', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(993, 'JLIB_DATABASE_ERROR_CATEGORY_UNIQUE_ALIAS', 'Другая категория с той же родительской категорией уже содержит такой алиас', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(994, 'JLIB_DATABASE_ERROR_CHECK_FAILED', '%s: :check не выполнен - %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(995, 'JLIB_DATABASE_ERROR_CHECKIN_FAILED', '%s: :checkIn не выполнен - %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(996, 'JLIB_DATABASE_ERROR_CHECKOUT_FAILED', '%s: :checkOut не выполнен - %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(997, 'JLIB_DATABASE_ERROR_CHILD_ROWS_CHECKED_OUT', 'Дочерняя запись заблокирована.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(998, 'JLIB_DATABASE_ERROR_CLASS_DOES_NOT_SUPPORT_ORDERING', '%s не поддерживает сортировку.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(999, 'JLIB_DATABASE_ERROR_CLASS_IS_MISSING_FIELD', 'В базе данных отсутствует поле: %s &#160; %s.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1000, 'JLIB_DATABASE_ERROR_CLASS_NOT_FOUND_IN_FILE', 'Класс таблицы %s не найден в файле.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1001, 'JLIB_DATABASE_ERROR_CONNECT_DATABASE', 'Не удаётся подключиться к базе данных: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1002, 'JLIB_DATABASE_ERROR_CONNECT_MYSQL', 'Не удаётся подключиться к MySQL.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1003, 'JLIB_DATABASE_ERROR_DATABASE_CONNECT', 'Не удаётся подключиться к базе данных', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1004, 'JLIB_DATABASE_ERROR_DELETE_CATEGORY', 'Данные слева и справа не соответствуют. Невозможно удалить категорию.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1005, 'JLIB_DATABASE_ERROR_DELETE_FAILED', '%s: :delete не выполнен - %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1006, 'JLIB_DATABASE_ERROR_DELETE_ROOT_CATEGORIES', 'Категории родительского уровня не могут быть удалены.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1007, 'JLIB_DATABASE_ERROR_EMAIL_INUSE', 'Этот адрес уже зарегистрирован.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1008, 'JLIB_DATABASE_ERROR_EMPTY_ROW_RETURNED', 'Строка в базе данных пуста.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1009, 'JLIB_DATABASE_ERROR_FUNCTION_FAILED', 'Ошибка базы данных с номером %s <br /><span style="color: red;">%s</span>', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1010, 'JLIB_DATABASE_ERROR_GET_NEXT_ORDER_FAILED', '%s: :getNextOrder не выполнен - %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1011, 'JLIB_DATABASE_ERROR_GET_TREE_FAILED', '%s: :getTree не выполнен - %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1012, 'JLIB_DATABASE_ERROR_GETNODE_FAILED', '%s: :_getNode не выполнен - %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1013, 'JLIB_DATABASE_ERROR_GETROOTID_FAILED', '%s: :getRootId не выполнен - %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1014, 'JLIB_DATABASE_ERROR_HIT_FAILED', '%s: :hit не выполнен - %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1015, 'JLIB_DATABASE_ERROR_INVALID_LOCATION', '%s: :setLocation - Недопустимое расположение', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1016, 'JLIB_DATABASE_ERROR_INVALID_NODE_RECURSION', '%s: :move Failed - Нельзя переместить узел сам в себя', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1017, 'JLIB_DATABASE_ERROR_INVALID_PARENT_ID', 'Неверный ID родителя.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1018, 'JLIB_DATABASE_ERROR_LANGUAGE_NO_TITLE', 'Язык должен иметь название', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1019, 'JLIB_DATABASE_ERROR_LANGUAGE_UNIQUE_IMAGE', 'Обнаружен язык контента в котором уже используется выбранный Префикс изображения', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1020, 'JLIB_DATABASE_ERROR_LANGUAGE_UNIQUE_LANG_CODE', 'Обнаружен язык контента в котором уже используется выбранный Тег языка', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1021, 'JLIB_DATABASE_ERROR_LANGUAGE_UNIQUE_SEF', 'Обнаружен язык контента в котором уже используется выбранный Код языка для URL', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1022, 'JLIB_DATABASE_ERROR_LOAD_DATABASE_DRIVER', 'Не удалось загрузить драйвер базы данных: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1023, 'JLIB_DATABASE_ERROR_MENUTYPE', 'Некоторые пункты меню или некоторые модули меню, связанные с этим типом меню заблокированы другим пользователем или являются пунктом по умолчанию в этом меню', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1024, 'JLIB_DATABASE_ERROR_MENUTYPE_CHECKOUT', 'Вы пытаетесь изменить пункт/модуль меню, который был заблокирован другим пользователем.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1025, 'JLIB_DATABASE_ERROR_MENUTYPE_EMPTY', 'Тип меню пустой', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1026, 'JLIB_DATABASE_ERROR_MENUTYPE_EXISTS', 'Тип меню существует: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1027, 'JLIB_DATABASE_ERROR_MENU_CANNOT_UNSET_DEFAULT', 'Параметру языка данного пункта меню должно быть присвоено значение ''Все''. Как минимум один пункт меню, из тех, что назначены пунктом ''по умолчанию'', должен быть привязан ко всем языкам, даже если сайт является многоязычным.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1028, 'JLIB_DATABASE_ERROR_MENU_CANNOT_UNSET_DEFAULT_DEFAULT', 'Как минимум один пункт меню должен быть отмечен как ''По умолчанию''.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1029, 'JLIB_DATABASE_ERROR_MENU_UNPUBLISH_DEFAULT_HOME', 'Нельзя снимать с публикации пункт меню, назначенный ''главной страницей''', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1030, 'JLIB_DATABASE_ERROR_MENU_DEFAULT_CHECKIN_USER_MISMATCH', 'Текущее главное меню для данного языка заблокировано другим пользователем', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1031, 'JLIB_DATABASE_ERROR_MENU_UNIQUE_ALIAS', 'Другой пункт меню с таким же родителем, уже содержит такой Алиас', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1032, 'JLIB_DATABASE_ERROR_MENU_UNIQUE_ALIAS_ROOT', 'Другой пункт меню, верхнего уровня, уже содержит такой Алиас', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1033, 'JLIB_DATABASE_ERROR_MENU_HOME_NOT_COMPONENT', 'Пункт меню, являющийся ''главной страницей'' сайта, должен ссылаться на компонент.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1034, 'JLIB_DATABASE_ERROR_MENU_HOME_NOT_UNIQUE_IN_MENU', 'Меню должно содержать только один пункт, отмеченный как ''главная страница''.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1035, 'JLIB_DATABASE_ERROR_MENU_ROOT_ALIAS_COMPONENT', 'Алиас пункта меню, находящегося на первом уровне вложенности, не может быть ''component''.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1036, 'JLIB_DATABASE_ERROR_MENU_ROOT_ALIAS_FOLDER', 'Алиас пункта меню первого уровня не может быть ''%s'' поскольку ''%s'' совпадает с именем подкаталога в каталоге с системными файлами Joomla!.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1037, 'JLIB_DATABASE_ERROR_MOVE_FAILED', '%s: :move Ошибка - %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1038, 'JLIB_DATABASE_ERROR_MUSTCONTAIN_A_TITLE_CATEGORY', 'Категория должна иметь заголовок', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1039, 'JLIB_DATABASE_ERROR_MUSTCONTAIN_A_TITLE_EXTENSION', 'Расширение должно иметь название', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1040, 'JLIB_DATABASE_ERROR_MUSTCONTAIN_A_TITLE_MODULE', 'Модуль должен иметь заголовок', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1041, 'JLIB_DATABASE_ERROR_MUSTCONTAIN_A_TITLE_UPDATESITE', 'Сервер обновления должен иметь название.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1042, 'JLIB_DATABASE_ERROR_NEGATIVE_NOT_PERMITTED', '%s не может быть меньше нуля', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1043, 'JLIB_DATABASE_ERROR_NO_ROWS_SELECTED', 'Нет выбранных строк.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1044, 'JLIB_DATABASE_ERROR_NOT_SUPPORTED_FILE_NOT_FOUND', 'Таблица %s не поддерживается. Файл не найден.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1045, 'JLIB_DATABASE_ERROR_NULL_PRIMARY_KEY', 'Пустой первичный ключ не допускается.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1046, 'JLIB_DATABASE_ERROR_ORDERDOWN_FAILED', '%s: :orderDown не выполнено - %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1047, 'JLIB_DATABASE_ERROR_ORDERUP_FAILED', '%s: :orderUp не выполнено - %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1048, 'JLIB_DATABASE_ERROR_PLEASE_ENTER_A_USER_NAME', 'Пожалуйста, укажите имя пользователя.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1049, 'JLIB_DATABASE_ERROR_PLEASE_ENTER_YOUR_NAME', 'Пожалуйста, укажите ваше имя.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1050, 'JLIB_DATABASE_ERROR_PUBLISH_FAILED', '%s: :publish не выполнено - %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1051, 'JLIB_DATABASE_ERROR_REBUILD_FAILED', '%s: :rebuild не выполнено - %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1052, 'JLIB_DATABASE_ERROR_REBUILDPATH_FAILED', '%s: :rebuildPath не выполнено - %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1053, 'JLIB_DATABASE_ERROR_REORDER_FAILED', '%s: :reorder не выполнено - %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1054, 'JLIB_DATABASE_ERROR_REORDER_UPDATE_ROW_FAILED', '%s: :reorder обновление порядка строки %s не выполнено - %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1055, 'JLIB_DATABASE_ERROR_ROOT_NODE_NOT_FOUND', 'Корневой узел не найден.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1056, 'JLIB_DATABASE_ERROR_STORE_FAILED_UPDATE_ASSET_ID', 'Поле asset_id не может быть обновлено', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1057, 'JLIB_DATABASE_ERROR_STORE_FAILED', '%1$s: :store не выполнено<br />%2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1058, 'JLIB_DATABASE_ERROR_USERGROUP_TITLE', 'Группа должна иметь название', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1059, 'JLIB_DATABASE_ERROR_USERGROUP_TITLE_EXISTS', 'Группа пользователей с таким названием уже существует. Название должно быть уникальным в пределах одного родителя.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1060, 'JLIB_DATABASE_ERROR_USERLEVEL_NAME_EXISTS', 'Уровень доступа &quot;%s&quot; уже существует.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1061, 'JLIB_DATABASE_ERROR_USERNAME_CANNOT_CHANGE', 'Невозможно использовать это имя пользователя', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1062, 'JLIB_DATABASE_ERROR_USERNAME_INUSE', 'Имя пользователя занято', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1063, 'JLIB_DATABASE_ERROR_VALID_AZ09', 'Пожалуйста, введите корректный логин. Без пробелов, не менее %d символов. Так же в логине <strong>не должно быть символов:</strong> < > " '' &#37; ; ( ) &', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1064, 'JLIB_DATABASE_ERROR_VALID_MAIL', 'Пожалуйста, введите корректный адрес электронной почты.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1065, 'JLIB_DATABASE_ERROR_VIEWLEVEL', 'Необходимо ввести заголовок', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1066, 'JLIB_DATABASE_FUNCTION_NOERROR', 'Функция базы данных сработала без ошибок', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1067, 'JLIB_DATABASE_QUERY_FAILED', 'Ошибка выполнения SQL-запроса (ошибка # %s): %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1068, 'JLIB_DOCUMENT_ERROR_UNABLE_LOAD_DOC_CLASS', 'Не удаётся загрузить класс документа', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1069, 'JLIB_ENVIRONMENT_SESSION_EXPIRED', 'Время сессии истекло, пожалуйста, пройдите авторизацию заново.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1070, 'JLIB_ERROR_INFINITE_LOOP', 'В JError обнаружен бесконечный цикл', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1071, 'JLIB_EVENT_ERROR_DISPATCHER', 'JEventDispatcher: :register: Обработчик событий не распознан. Обработчик: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1072, 'JLIB_FILESYSTEM_BZIP_NOT_SUPPORTED', 'BZip2 не поддерживается', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1073, 'JLIB_FILESYSTEM_BZIP_UNABLE_TO_READ', 'Невозможно прочитать архив (bz2)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1074, 'JLIB_FILESYSTEM_BZIP_UNABLE_TO_WRITE', 'Невозможно записать архив (bz2)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini');
INSERT INTO `yawnc_overrider` (`id`, `constant`, `string`, `file`) VALUES
(1075, 'JLIB_FILESYSTEM_BZIP_UNABLE_TO_WRITE_FILE', 'Невозможно записать файл (bz2)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1076, 'JLIB_FILESYSTEM_GZIP_NOT_SUPPORTED', 'GZlib не поддерживается', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1077, 'JLIB_FILESYSTEM_GZIP_UNABLE_TO_READ', 'Невозможно прочитать архив (gz)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1078, 'JLIB_FILESYSTEM_GZIP_UNABLE_TO_WRITE', 'Невозможно записать архив (gz)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1079, 'JLIB_FILESYSTEM_GZIP_UNABLE_TO_WRITE_FILE', 'Невозможно записать файл (gz)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1080, 'JLIB_FILESYSTEM_GZIP_UNABLE_TO_DECOMPRESS', 'Не удалось распаковать данные', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1081, 'JLIB_FILESYSTEM_TAR_UNABLE_TO_READ', 'Невозможно прочитать архив (tar)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1082, 'JLIB_FILESYSTEM_TAR_UNABLE_TO_DECOMPRESS', 'Не удалось распаковать данные', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1083, 'JLIB_FILESYSTEM_TAR_UNABLE_TO_CREATE_DESTINATION', 'Невозможно создать целевой объект', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1084, 'JLIB_FILESYSTEM_TAR_UNABLE_TO_WRITE_ENTRY', 'Невозможно записать данные', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1085, 'JLIB_FILESYSTEM_ZIP_NOT_SUPPORTED', 'Zlib не поддерживается', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1086, 'JLIB_FILESYSTEM_ZIP_UNABLE_TO_READ', 'Невозможно прочитать архив (zip)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1087, 'JLIB_FILESYSTEM_ZIP_INFO_FAILED', 'Получить информацию о ZIP-архиве не удалось', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1088, 'JLIB_FILESYSTEM_ZIP_UNABLE_TO_CREATE_DESTINATION', 'Невозможно создать целевой объект', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1089, 'JLIB_FILESYSTEM_ZIP_UNABLE_TO_WRITE_ENTRY', 'Невозможно записать данные', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1090, 'JLIB_FILESYSTEM_ZIP_UNABLE_TO_READ_ENTRY', 'Невозможно прочитать объект', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1091, 'JLIB_FILESYSTEM_ZIP_UNABLE_TO_OPEN_ARCHIVE', 'Невозможно открыть архив', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1092, 'JLIB_FILESYSTEM_ZIP_INVALID_ZIP_DATA', 'Некорректные ZIP-данные', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1093, 'JLIB_FILESYSTEM_STREAM_FAILED', 'Не удалось зарегистрировать строковый поток', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1094, 'JLIB_FILESYSTEM_UNKNOWNARCHIVETYPE', 'Неизвестный тип архива', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1095, 'JLIB_FILESYSTEM_UNABLE_TO_LOAD_ARCHIVE', 'Не удаётся загрузить архив', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1096, 'JLIB_FILESYSTEM_ERROR_JFILE_FIND_COPY', 'JFile: :copy: Не удаётся найти или прочитать файл: $%s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1097, 'JLIB_FILESYSTEM_ERROR_JFILE_STREAMS', 'JFile: :copy(%1$s, %2$s): %3$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1098, 'JLIB_FILESYSTEM_ERROR_COPY_FAILED', 'Копирование не удалось', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1099, 'JLIB_FILESYSTEM_DELETE_FAILED', 'Не удалось удалить %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1100, 'JLIB_FILESYSTEM_CANNOT_FIND_SOURCE_FILE', 'Не удаётся найти исходный файл', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1101, 'JLIB_FILESYSTEM_ERROR_JFILE_MOVE_STREAMS', 'JFile: :move: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1102, 'JLIB_FILESYSTEM_ERROR_RENAME_FILE', 'Переименовать не удалось', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1103, 'JLIB_FILESYSTEM_ERROR_READ_UNABLE_TO_OPEN_FILE', 'JFile: :read: Не удаётся открыть файл: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1104, 'JLIB_FILESYSTEM_ERROR_WRITE_STREAMS', 'JFile: :write(%1$s): %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1105, 'JLIB_FILESYSTEM_ERROR_UPLOAD', 'JFile: :upload: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1106, 'JLIB_FILESYSTEM_ERROR_WARNFS_ERR01', 'Внимание! Не удалось изменить права на файл!', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1107, 'JLIB_FILESYSTEM_ERROR_WARNFS_ERR02', 'Внимание! Не удалось переместить файл!', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1108, 'JLIB_FILESYSTEM_ERROR_WARNFS_ERR03', 'Внимание: файл %s  не был загружен из соображений безопасности!', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1109, 'JLIB_FILESYSTEM_ERROR_FIND_SOURCE_FOLDER', 'Не удаётся найти исходный каталог', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1110, 'JLIB_FILESYSTEM_ERROR_FOLDER_EXISTS', 'Каталог уже существует', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1111, 'JLIB_FILESYSTEM_ERROR_FOLDER_CREATE', 'Невозможно создать каталог', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1112, 'JLIB_FILESYSTEM_ERROR_FOLDER_OPEN', 'Невозможно открыть исходный каталог', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1113, 'JLIB_FILESYSTEM_ERROR_FOLDER_LOOP', 'Обнаружен Бесконечный цикл', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1114, 'JLIB_FILESYSTEM_ERROR_FOLDER_PATH', 'Путь не в пределах значения переменной open_basedir', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1115, 'JLIB_FILESYSTEM_ERROR_COULD_NOT_CREATE_DIRECTORY', 'Не удалось создать каталог', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1116, 'JLIB_FILESYSTEM_ERROR_DELETE_BASE_DIRECTORY', 'Вы не можете удалить главный каталог', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1117, 'JLIB_FILESYSTEM_ERROR_PATH_IS_NOT_A_FOLDER', 'JFolder: :delete: Путь ведёт не к каталогу. Путь: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1118, 'JLIB_FILESYSTEM_ERROR_FOLDER_DELETE', 'JFolder: :delete: Не удаётся удалить каталог. Путь: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1119, 'JLIB_FILESYSTEM_ERROR_FOLDER_RENAME', 'Не удалось переименовать: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1120, 'JLIB_FILESYSTEM_ERROR_PATH_IS_NOT_A_FOLDER_FILES', 'JFolder: :files: Путь ведёт не к каталогу. Путь: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1121, 'JLIB_FILESYSTEM_ERROR_PATH_IS_NOT_A_FOLDER_FOLDER', 'JFolder: :folder: Путь ведёт не к каталогу. Путь: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1122, 'JLIB_FILESYSTEM_ERROR_STREAMS_FILE_SIZE', 'Не удалось определить размер файла. На некоторых потоках это может не сработать!', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1123, 'JLIB_FILESYSTEM_ERROR_STREAMS_FILE_NOT_OPEN', 'Файл не открыт', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1124, 'JLIB_FILESYSTEM_ERROR_STREAMS_FILENAME', 'Имя файла не указано', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1125, 'JLIB_FILESYSTEM_ERROR_NO_DATA_WRITTEN', 'Предупреждение: данные не были сохранены', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1126, 'JLIB_FILESYSTEM_ERROR_STREAMS_FAILED_TO_OPEN_WRITER', 'Не удаётся открыть для записи: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1127, 'JLIB_FILESYSTEM_ERROR_STREAMS_FAILED_TO_OPEN_READER', 'Не удаётся открыть для чтения: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1128, 'JLIB_FILESYSTEM_ERROR_STREAMS_NOT_UPLOADED_FILE', 'Файл не был загружен!', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1129, 'JLIB_FORM_BUTTON_CLEAR', 'Очистить', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1130, 'JLIB_FORM_BUTTON_SELECT', 'Выбрать', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1131, 'JLIB_FORM_CHANGE_IMAGE', 'Изменить изображение', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1132, 'JLIB_FORM_CHANGE_IMAGE_BUTTON', 'Изменить изображение кнопки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1133, 'JLIB_FORM_CHANGE_USER', 'Выбрать пользователя', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1134, 'JLIB_FORM_ERROR_FIELDS_CATEGORY_ERROR_EXTENSION_EMPTY', 'В поле категории не указан атрибут расширения', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1135, 'JLIB_FORM_ERROR_FIELDS_GROUPEDLIST_ELEMENT_NAME', 'Неизвестный тип элемента: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1136, 'JLIB_FORM_ERROR_NO_DATA', 'Нет данных', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1137, 'JLIB_FORM_ERROR_VALIDATE_FIELD', 'Неправильное XML-поле', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1138, 'JLIB_FORM_ERROR_XML_FILE_DID_NOT_LOAD', 'XML-файл не был загружен', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1139, 'JLIB_FORM_FIELD_INVALID', 'Некорректно заполнено поле:&#160;', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1140, 'JLIB_FORM_INPUTMODE', 'latin', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1141, 'JLIB_FORM_INVALID_FORM_OBJECT', 'Недопустимый объект формы: :%s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1142, 'JLIB_FORM_INVALID_FORM_RULE', 'Недопустимое правило формы: :%s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1143, 'JLIB_FORM_MEDIA_PREVIEW_ALT', 'Выбранное изображение', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1144, 'JLIB_FORM_MEDIA_PREVIEW_EMPTY', 'Изображение не выбрано.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1145, 'JLIB_FORM_MEDIA_PREVIEW_SELECTED_IMAGE', 'Выбранное изображение', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1146, 'JLIB_FORM_MEDIA_PREVIEW_TIP_TITLE', 'Предпросмотр', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1147, 'JLIB_FORM_SELECT_USER', 'Выбор пользователя', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1148, 'JLIB_FORM_VALIDATE_FIELD_INVALID', 'Недопустимое поле: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1149, 'JLIB_FORM_VALIDATE_FIELD_REQUIRED', 'Требуется поле: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1150, 'JLIB_FORM_VALIDATE_FIELD_RULE_MISSING', 'Правило валидации отсутствует: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1151, 'JLIB_FORM_VALUE_CACHE_APC', 'Alternative PHP Cache', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1152, 'JLIB_FORM_VALUE_CACHE_CACHELITE', 'Cache_Lite', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1153, 'JLIB_FORM_VALUE_CACHE_EACCELERATOR', 'eAccelerator', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1154, 'JLIB_FORM_VALUE_CACHE_FILE', 'Файл', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1155, 'JLIB_FORM_VALUE_CACHE_MEMCACHE', 'Memcache', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1156, 'JLIB_FORM_VALUE_CACHE_MEMCACHED', 'Memcached (Experimental)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1157, 'JLIB_FORM_VALUE_CACHE_REDIS', 'Redis', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1158, 'JLIB_FORM_VALUE_CACHE_WINCACHE', 'Windows Кэш', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1159, 'JLIB_FORM_VALUE_CACHE_XCACHE', 'XCache', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1160, 'JLIB_FORM_VALUE_SESSION_APC', 'Alternative PHP Cache', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1161, 'JLIB_FORM_VALUE_SESSION_DATABASE', 'База данных', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1162, 'JLIB_FORM_VALUE_SESSION_EACCELERATOR', 'eAccelerator', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1163, 'JLIB_FORM_VALUE_SESSION_MEMCACHE', 'Memcache', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1164, 'JLIB_FORM_VALUE_SESSION_MEMCACHED', 'Memcached (Experimental)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1165, 'JLIB_FORM_VALUE_SESSION_NONE', 'Нет', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1166, 'JLIB_FORM_VALUE_SESSION_WINCACHE', 'Windows Кэш', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1167, 'JLIB_FORM_VALUE_SESSION_XCACHE', 'XCache', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1168, 'JLIB_FORM_VALUE_TIMEZONE_UTC', 'Всемирное время, Coordinated (UTC)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1169, 'JLIB_FORM_VALUE_FROM_TEMPLATE', 'Из шаблона', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1170, 'JLIB_FORM_VALUE_INHERITED', 'Унаследовано', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1171, 'JLIB_HTML_ACCESS_MODIFY_DESC_CAPTION_ACL', 'ACL', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1172, 'JLIB_HTML_ACCESS_MODIFY_DESC_CAPTION_TABLE', 'Таблица', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1173, 'JLIB_HTML_ACCESS_SUMMARY_DESC_CAPTION', 'Суммарная таблица прав доступа', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1174, 'JLIB_HTML_ACCESS_SUMMARY_DESC', 'Ниже приведён обзор настройки разрешений для этой статьи. Переходя по вкладкам выше, можно настроить параметры отдельных действий.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1175, 'JLIB_HTML_ACCESS_SUMMARY', 'Итог', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1176, 'JLIB_HTML_ADD_TO_ROOT', 'Создать в корне', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1177, 'JLIB_HTML_ADD_TO_THIS_MENU', 'Добавить к этому меню', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1178, 'JLIB_HTML_BATCH_ACCESS_LABEL', 'Изменить уровень доступа', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1179, 'JLIB_HTML_BATCH_ACCESS_LABEL_DESC', 'Если ничего не выбрать, будет сохранён исходный уровень доступа.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1180, 'JLIB_HTML_BATCH_COPY', 'Копировать', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1181, 'JLIB_HTML_BATCH_LANGUAGE_LABEL', 'Установить язык', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1182, 'JLIB_HTML_BATCH_LANGUAGE_LABEL_DESC', 'Если ничего не выбрать, будет сохранён исходный язык.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1183, 'JLIB_HTML_BATCH_LANGUAGE_NOCHANGE', '- Сохранить исходный язык -', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1184, 'JLIB_HTML_BATCH_MENU_LABEL', 'Выберите категорию для Перемещения/Копирования', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1185, 'JLIB_HTML_BATCH_MOVE', 'Переместить', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1186, 'JLIB_HTML_BATCH_NOCHANGE', '- Сохранить исходный уровень доступа -', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1187, 'JLIB_HTML_BATCH_TAG_LABEL', 'Добавить метку', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1188, 'JLIB_HTML_BATCH_TAG_LABEL_DESC', 'Добавить метку выбранным элементам.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1189, 'JLIB_HTML_BATCH_TAG_NOCHANGE', '- Сохранить исходные метки -', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1190, 'JLIB_HTML_BATCH_USER_LABEL', 'Установить Пользователя', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1191, 'JLIB_HTML_BATCH_USER_LABEL_DESC', 'Если ничего не выбрать, будет сохранён исходный пользователь.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1192, 'JLIB_HTML_BATCH_USER_NOCHANGE', '- Сохранить исходного пользователя -', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1193, 'JLIB_HTML_BATCH_USER_NOUSER', 'Нет пользователя', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1194, 'JLIB_HTML_BEHAVIOR_ABOUT_THE_CALENDAR', 'О календаре', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1195, 'JLIB_HTML_BEHAVIOR_CLOSE', 'Закрыть', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1196, 'JLIB_HTML_BEHAVIOR_DATE_SELECTION', 'Выбор даты:\\n', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1197, 'JLIB_HTML_BEHAVIOR_DISPLAY_S_FIRST', 'Показывать первые %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1198, 'JLIB_HTML_BEHAVIOR_DRAG_TO_MOVE', 'Потяните, чтобы переместить', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1199, 'JLIB_HTML_BEHAVIOR_GO_TODAY', 'Текущая дата', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1200, 'JLIB_HTML_BEHAVIOR_GREEN', 'Зелёный', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1201, 'JLIB_HTML_BEHAVIOR_HOLD_MOUSE', '- Удерживайте кнопку мыши на любой из кнопок, расположенных выше, для быстрого выбора.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1202, 'JLIB_HTML_BEHAVIOR_MONTH_SELECT', '- Чтобы выбрать месяц воспользуйтесь кнопками < и > \\n', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1203, 'JLIB_HTML_BEHAVIOR_NEXT_MONTH_HOLD_FOR_MENU', 'Нажмите, что бы перейти на следующий месяц. Нажмите и удерживайте для показа списка месяцев.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1204, 'JLIB_HTML_BEHAVIOR_NEXT_YEAR_HOLD_FOR_MENU', 'Нажмите, что бы перейти на следующий год. Нажмите и удерживайте для показа списка лет.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1205, 'JLIB_HTML_BEHAVIOR_PREV_MONTH_HOLD_FOR_MENU', 'Нажмите, что бы перейти на предыдущий месяц. Нажмите и удерживайте для показа списка месяцев.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1206, 'JLIB_HTML_BEHAVIOR_PREV_YEAR_HOLD_FOR_MENU', 'Нажмите, что бы перейти на предыдущий год. Нажмите и удерживайте для показа списка лет.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1207, 'JLIB_HTML_BEHAVIOR_SELECT_DATE', 'Выбор даты.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1208, 'JLIB_HTML_BEHAVIOR_SHIFT_CLICK_OR_DRAG_TO_CHANGE_VALUE', 'Shift + клик или перетаскивание мышкой позволит изменить значение.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1209, 'JLIB_HTML_BEHAVIOR_TIME', 'Время:', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1210, 'JLIB_HTML_BEHAVIOR_TODAY', 'Сегодня', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1211, 'JLIB_HTML_BEHAVIOR_TT_DATE_FORMAT', '%a, %b %e', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1212, 'JLIB_HTML_BEHAVIOR_WK', 'нед.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1213, 'JLIB_HTML_BEHAVIOR_YEAR_SELECT', '- Чтобы выбрать год, используйте кнопками < и > \\n', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1214, 'JLIB_HTML_BUTTON_BASE_CLASS', 'Не удалось загрузить базовый класс кнопок.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1215, 'JLIB_HTML_BUTTON_NO_LOAD', 'Не удалось загрузить кнопку %s (%s);', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1216, 'JLIB_HTML_BUTTON_NOT_DEFINED', 'Не назначена кнопка для типа = %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1217, 'JLIB_HTML_CALENDAR', 'Календарь', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1218, 'JLIB_HTML_CHECKED_OUT', 'Проверено', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1219, 'JLIB_HTML_CHECKIN', 'Разблокировать', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1220, 'JLIB_HTML_CLOAKING', 'Этот адрес электронной почты защищён от спам-ботов. У вас должен быть включен JavaScript для просмотра.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1221, 'JLIB_HTML_DATE_RELATIVE_DAYS', '%s дней назад', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1222, 'JLIB_HTML_DATE_RELATIVE_DAYS_1', '%s день назад', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1223, 'JLIB_HTML_DATE_RELATIVE_DAYS_0', '%s дней назад', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1224, 'JLIB_HTML_DATE_RELATIVE_HOURS', '%s часов назад', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1225, 'JLIB_HTML_DATE_RELATIVE_HOURS_1', '%s час назад', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1226, 'JLIB_HTML_DATE_RELATIVE_HOURS_0', '%s часов назад', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1227, 'JLIB_HTML_DATE_RELATIVE_LESSTHANAMINUTE', 'Менее, чем минуту назад', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1228, 'JLIB_HTML_DATE_RELATIVE_MINUTES', '%s минут назад', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1229, 'JLIB_HTML_DATE_RELATIVE_MINUTES_1', '%s минуту назад', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1230, 'JLIB_HTML_DATE_RELATIVE_MINUTES_0', '%s минут назад', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1231, 'JLIB_HTML_DATE_RELATIVE_WEEKS', '%s недель назад', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1232, 'JLIB_HTML_DATE_RELATIVE_WEEKS_1', '%s неделя назад', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1233, 'JLIB_HTML_DATE_RELATIVE_WEEKS_0', '%s недель назад', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1234, 'JLIB_HTML_EDIT_MENU_ITEM', 'Редактировать пункт меню', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1235, 'JLIB_HTML_EDIT_MENU_ITEM_ID', 'ID: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1236, 'JLIB_HTML_EDIT_MODULE', 'Редактировать модуль', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1237, 'JLIB_HTML_EDIT_MODULE_IN_POSITION', 'Позиция: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1238, 'JLIB_HTML_EDITOR_CANNOT_LOAD', 'Не удалось загрузить редактор', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1239, 'JLIB_HTML_END', 'В конец', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1240, 'JLIB_HTML_ERROR_FUNCTION_NOT_SUPPORTED', 'Функция не поддерживается.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1241, 'JLIB_HTML_ERROR_NOTFOUNDINFILE', '%s: :%s не найден в файле.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1242, 'JLIB_HTML_ERROR_NOTSUPPORTED_NOFILE', '%s: :%s не поддерживается. Файл, не найден.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1243, 'JLIB_HTML_ERROR_NOTSUPPORTED', '%s: :%s не поддерживается.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1244, 'JLIB_HTML_MOVE_DOWN', 'Вниз', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1245, 'JLIB_HTML_MOVE_UP', 'Вверх', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1246, 'JLIB_HTML_NO_PARAMETERS_FOR_THIS_ITEM', 'Нет параметров для этого элемента.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1247, 'JLIB_HTML_NO_RECORDS_FOUND', 'Записей не найдено', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1248, 'JLIB_HTML_PAGE_CURRENT_OF_TOTAL', 'Страница %s из %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1249, 'JLIB_HTML_PLEASE_MAKE_A_SELECTION_FROM_THE_LIST', 'Пожалуйста, выберите объект из списка', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1250, 'JLIB_HTML_PUBLISH_ITEM', 'Опубликовать', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1251, 'JLIB_HTML_PUBLISHED_EXPIRED_ITEM', 'Опубликовано, но срок истёк', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1252, 'JLIB_HTML_PUBLISHED_FINISHED', 'Окончание: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1253, 'JLIB_HTML_PUBLISHED_ITEM', 'Опубликовано и действует', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1254, 'JLIB_HTML_PUBLISHED_PENDING_ITEM', 'Опубликовано, но приостановлено', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1255, 'JLIB_HTML_PUBLISHED_START', 'Старт: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1256, 'JLIB_HTML_RESULTS_OF', 'Показано %s - %s из %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1257, 'JLIB_HTML_SAVE_ORDER', 'Сохранить порядок', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1258, 'JLIB_HTML_SELECT_STATE', 'Выбрать состояние', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1259, 'JLIB_HTML_START', 'В начало', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1260, 'JLIB_HTML_UNPUBLISH_ITEM', 'Снять с публикации', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1261, 'JLIB_HTML_VIEW_ALL', 'Посмотреть все', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1262, 'JLIB_HTML_SETDEFAULT_ITEM', 'Установить по умолчанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1263, 'JLIB_HTML_UNSETDEFAULT_ITEM', 'Отключить по умолчанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1264, 'JLIB_INSTALLER_ABORT', 'Отмена установки языка %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1265, 'JLIB_INSTALLER_ABORT_ALREADYINSTALLED', 'Расширение уже установлено', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1266, 'JLIB_INSTALLER_ABORT_ALREADY_EXISTS', 'Расширение %1$s: Расширение %2$s уже существует.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1267, 'JLIB_INSTALLER_ABORT_COMP_BUILDADMINMENUS_FAILED', 'Ошибка при создании меню панели управления', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1268, 'JLIB_INSTALLER_ABORT_COMP_COPY_MANIFEST', 'Компонент %1$s: Ошибка копирования файла PHP-манифеста.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1269, 'JLIB_INSTALLER_ABORT_COMP_COPY_SETUP', 'Компонент %1$s: Ошибка копирования установочного файла.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1270, 'JLIB_INSTALLER_ABORT_COMP_FAIL_ADMIN_FILES', 'Компонент %s: Ошибка копирования файлов панели управления.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1271, 'JLIB_INSTALLER_ABORT_COMP_FAIL_SITE_FILES', 'Компонент %s: Ошибка копирования файлов сайта.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1272, 'JLIB_INSTALLER_ABORT_COMP_INSTALL_COPY_SETUP', 'Установка компонента: Не удалось скопировать файл установки.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1273, 'JLIB_INSTALLER_ABORT_COMP_INSTALL_CUSTOM_INSTALL_FAILURE', 'Установка компонента: Ошибка в пользовательском скрипте установки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1274, 'JLIB_INSTALLER_ABORT_COMP_INSTALL_MANIFEST', 'Установка компонента: Не удалось скопировать манифест PHP-файла.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1275, 'JLIB_INSTALLER_ABORT_COMP_INSTALL_PHP_INSTALL', 'Установка компонента: Не удалось скопировать файл установки PHP.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1276, 'JLIB_INSTALLER_ABORT_COMP_INSTALL_PHP_UNINSTALL', 'Установка компонента: Не удалось скопировать файл деинсталляции PHP.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1277, 'JLIB_INSTALLER_ABORT_COMP_INSTALL_ROLLBACK', 'Установка компонента: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1278, 'JLIB_INSTALLER_ABORT_COMP_INSTALL_SQL_ERROR', 'Установка компонента: Файл ошибок SQL %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1279, 'JLIB_INSTALLER_ABORT_COMP_UPDATE_ADMIN_ELEMENT', 'Обновление компонента: В XML-файле отсутствует элемент управления', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1280, 'JLIB_INSTALLER_ABORT_COMP_UPDATE_COPY_SETUP', 'Обновление компонента: Не удалось скопировать файл установки.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1281, 'JLIB_INSTALLER_ABORT_COMP_UPDATE_MANIFEST', 'Обновление компонента: Не удалось скопировать манифест PHP-файла.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1282, 'JLIB_INSTALLER_ABORT_COMP_UPDATE_PHP_INSTALL', 'Обновление компонента: Не удалось скопировать PHP-файл установки.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1283, 'JLIB_INSTALLER_ABORT_COMP_UPDATE_PHP_UNINSTALL', 'Обновление компонента: Не удалось скопировать PHP-файл деинсталляции.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1284, 'JLIB_INSTALLER_ABORT_COMP_UPDATE_ROLLBACK', 'Обновление компонента: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1285, 'JLIB_INSTALLER_ABORT_COMP_UPDATE_SQL_ERROR', 'Обновление компонента: файл ошибок SQL %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1286, 'JLIB_INSTALLER_ABORT_CREATE_DIRECTORY', 'Расширение %1$s: Ошибка создания каталога: %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1287, 'JLIB_INSTALLER_ABORT_DEBUG', 'Установка была неожиданно прервана:', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1288, 'JLIB_INSTALLER_ABORT_DETECTMANIFEST', 'Не удалось обнаружить файл манифеста', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1289, 'JLIB_INSTALLER_ABORT_DIRECTORY', 'Расширение %1$s: %2$s уже использует каталог: %3$s. Вы пытаетесь повторно установить данное расширение?', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1290, 'JLIB_INSTALLER_ABORT_EXTENSIONNOTVALID', 'Недопустимое расширение', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1291, 'JLIB_INSTALLER_ABORT_FILE_INSTALL_COPY_SETUP', 'Установка файлов: Не удалось скопировать файл установки.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1292, 'JLIB_INSTALLER_ABORT_FILE_INSTALL_CUSTOM_INSTALL_FAILURE', 'Установка файлов: Ошибка в пользовательском скрипте установки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1293, 'JLIB_INSTALLER_ABORT_FILE_INSTALL_FAIL_SOURCE_DIRECTORY', 'Установка файлов: Не удалось найти исходный каталог: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1294, 'JLIB_INSTALLER_ABORT_FILE_INSTALL_ROLLBACK', 'Установка файлов: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1295, 'JLIB_INSTALLER_ABORT_FILE_INSTALL_SQL_ERROR', 'Установка файлов %1$s: файл ошибок SQL %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1296, 'JLIB_INSTALLER_ABORT_FILE_ROLLBACK', 'Установка файлов: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1297, 'JLIB_INSTALLER_ABORT_FILE_SAME_NAME', 'Установка файлов: Модуль с таким же именем уже существует.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1298, 'JLIB_INSTALLER_ABORT_FILE_UPDATE_SQL_ERROR', 'Обновление файлов: файл ошибок SQL %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1299, 'JLIB_INSTALLER_ABORT_INSTALL_CUSTOM_INSTALL_FAILURE', 'Расширение %s: Ошибка пользовательского скрипта установки.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1300, 'JLIB_INSTALLER_ABORT_LIB_COPY_FILES', 'Библиотека %s: Ошибка копирования исходных файлов.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1301, 'JLIB_INSTALLER_ABORT_LIB_INSTALL_ALREADY_INSTALLED', 'Установка библиотеки: Библиотека уже установлена', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1302, 'JLIB_INSTALLER_ABORT_LIB_INSTALL_COPY_SETUP', 'Установка библиотеки: Не удалось скопировать файл установки.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1303, 'JLIB_INSTALLER_ABORT_LIB_INSTALL_FAILED_TO_CREATE_DIRECTORY', 'Установка библиотеки: Не удалось создать каталог: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1304, 'JLIB_INSTALLER_ABORT_LIB_INSTALL_NOFILE', 'Установка библиотеки: Файл библиотеки не указан', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1305, 'JLIB_INSTALLER_ABORT_LIB_INSTALL_ROLLBACK', 'Установка библиотеки: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1306, 'JLIB_INSTALLER_ABORT_LOAD_DETAILS', 'Не удалось загрузить параметры расширения', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1307, 'JLIB_INSTALLER_ABORT_MANIFEST', 'Расширение %1$s: Ошибка копирования файла PHP-манифеста.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1308, 'JLIB_INSTALLER_ABORT_METHODNOTSUPPORTED', 'Для данного типа расширений метод не поддерживается', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1309, 'JLIB_INSTALLER_ABORT_METHODNOTSUPPORTED_TYPE', 'Для данного типа расширений не поддерживается метод: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1310, 'JLIB_INSTALLER_ABORT_MOD_COPY_FILES', 'Модуль %s: Ошибка копирования исходных файлов.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1311, 'JLIB_INSTALLER_ABORT_MOD_INSTALL_COPY_SETUP', 'Установка модуля: Не удалось скопировать установочный файл.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1312, 'JLIB_INSTALLER_ABORT_MOD_INSTALL_CREATE_DIRECTORY', 'Модуль %1$s: Не удалось создать каталог: %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1313, 'JLIB_INSTALLER_ABORT_MOD_INSTALL_CUSTOM_INSTALL_FAILURE', 'Установка модуля: Ошибка в пользовательском скрипте установки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1314, 'JLIB_INSTALLER_ABORT_MOD_INSTALL_DIRECTORY', 'Модуль %1$s: Другой модуль уже использует каталог: %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1315, 'JLIB_INSTALLER_ABORT_MOD_INSTALL_MANIFEST', 'Установка модуля: Не удалось скопировать манифест PHP-файла.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1316, 'JLIB_INSTALLER_ABORT_MOD_INSTALL_NOFILE', 'Модуль %s: Модуль не указан', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1317, 'JLIB_INSTALLER_ABORT_MOD_INSTALL_SQL_ERROR', 'Модуль %1$s: файл ошибок SQL %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1318, 'JLIB_INSTALLER_ABORT_MOD_ROLLBACK', 'Модуль %1$s: %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1319, 'JLIB_INSTALLER_ABORT_MOD_UNINSTALL_UNKNOWN_CLIENT', 'Удаление модуля: Тип клиента не определён [%s]', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1320, 'JLIB_INSTALLER_ABORT_MOD_UNKNOWN_CLIENT', 'Модуль %1$s: Тип клиента не определён [%2$s]', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1321, 'JLIB_INSTALLER_ABORT_NOINSTALLPATH', 'Путь установки не существует', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1322, 'JLIB_INSTALLER_ABORT_NOUPDATEPATH', 'Путь обновления не существует', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1323, 'JLIB_INSTALLER_ABORT_PACK_INSTALL_COPY_SETUP', 'Установка пакета: Не удалось скопировать установочный файл.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1324, 'JLIB_INSTALLER_ABORT_PACK_INSTALL_CREATE_DIRECTORY', 'Установка пакета: Не удалось создать каталог:%s ', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1325, 'JLIB_INSTALLER_ABORT_PACKAGE_INSTALL_CUSTOM_INSTALL_FAILURE', 'Установка пакета: Ошибка в пользовательском скрипте установки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1326, 'JLIB_INSTALLER_ABORT_PACKAGE_INSTALL_MANIFEST', 'Установка прервана: Не удалось скопировать манифест PHP-файла.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1327, 'JLIB_INSTALLER_ABORT_PACK_INSTALL_ERROR_EXTENSION', 'Пакет %1$s: В процессе установки произошла ошибка: %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1328, 'JLIB_INSTALLER_ABORT_PACK_INSTALL_NO_FILES', 'Пакет %s: Файлов для установки не обнаружено!', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1329, 'JLIB_INSTALLER_ABORT_PACK_INSTALL_NO_PACK', 'Пакет %s: Файл пакета не указан', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1330, 'JLIB_INSTALLER_ABORT_PACK_INSTALL_ROLLBACK', 'Установка пакета: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1331, 'JLIB_INSTALLER_ABORT_PLG_COPY_FILES', 'Плагин %s: Ошибка копирования исходных файлов.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1332, 'JLIB_INSTALLER_ABORT_PLG_INSTALL_ALLREADY_EXISTS', 'Плагин %1$s: Плагин %2$s уже существуют', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1333, 'JLIB_INSTALLER_ABORT_PLG_INSTALL_COPY_SETUP', 'Плагин %s: Не удалось скопировать установочный файл.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1334, 'JLIB_INSTALLER_ABORT_PLG_INSTALL_CREATE_DIRECTORY', 'Плагин %1$s: Не удалось создать каталог: %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1335, 'JLIB_INSTALLER_ABORT_PLG_INSTALL_CUSTOM_INSTALL_FAILURE', 'Установка плагина: Ошибка в пользовательском скрипте установки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1336, 'JLIB_INSTALLER_ABORT_PLG_INSTALL_DIRECTORY', 'Плагин %1$s: Ещё один плагин уже использует каталог: %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1337, 'JLIB_INSTALLER_ABORT_PLG_INSTALL_MANIFEST', 'Плагин %s: Не удалось скопировать манифест PHP-файла.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1338, 'JLIB_INSTALLER_ABORT_PLG_INSTALL_NO_FILE', 'Плагин %s: Файл плагина не указан', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1339, 'JLIB_INSTALLER_ABORT_PLG_INSTALL_ROLLBACK', 'Плагин %1$s: %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1340, 'JLIB_INSTALLER_ABORT_PLG_INSTALL_SQL_ERROR', 'Плагин %1$s: Файл ошибок SQL %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1341, 'JLIB_INSTALLER_ABORT_PLG_UNINSTALL_SQL_ERROR', 'Удаление плагина: Файл ошибок SQL %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1342, 'JLIB_INSTALLER_ABORT_REFRESH_MANIFEST_CACHE', 'Обновить кэш манифеста не удалось: Расширение в настоящее время не установлено.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1343, 'JLIB_INSTALLER_ABORT_REFRESH_MANIFEST_CACHE_VALID', 'Обновить кэш манифеста не удалось: Расширение некорректно.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1344, 'JLIB_INSTALLER_ABORT_ROLLBACK', 'Расширение %1$s: %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1345, 'JLIB_INSTALLER_ABORT_SQL_ERROR', 'Расширение %1$s: ошибка выполения SQL-запроса: %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1346, 'JLIB_INSTALLER_ABORT_TPL_INSTALL_ALREADY_INSTALLED', 'Установка шаблона: Шаблон уже установлен', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1347, 'JLIB_INSTALLER_ABORT_TPL_INSTALL_ANOTHER_TEMPLATE_USING_DIRECTORY', 'Установка шаблона: Уже существует шаблон, использующий указанный каталог: %s. Вы пытаетесь установить тот же шаблон ещё раз?', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1348, 'JLIB_INSTALLER_ABORT_TPL_INSTALL_COPY_FILES', 'Установка шаблона: Ошибка копирования исходных файлов %s.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini');
INSERT INTO `yawnc_overrider` (`id`, `constant`, `string`, `file`) VALUES
(1349, 'JLIB_INSTALLER_ABORT_TPL_INSTALL_COPY_SETUP', 'Установка шаблона: Не удалось скопировать установочный файл.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1350, 'JLIB_INSTALLER_ABORT_TPL_INSTALL_FAILED_CREATE_DIRECTORY', 'Установка шаблона: Не удалось создать каталог: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1351, 'JLIB_INSTALLER_ABORT_TPL_INSTALL_ROLLBACK', 'Установка шаблона: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1352, 'JLIB_INSTALLER_ABORT_TPL_INSTALL_UNKNOWN_CLIENT', 'Установка шаблона: Тип клиента не определён [%s]', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1353, 'JLIB_INSTALLER_AVAILABLE_UPDATE_PHP_VERSION', 'Для расширения %1$s доступна версия %2$s, однако она требует PHP версии %3$s (установленная версия PHP: %4$s)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1354, 'JLIB_INSTALLER_PURGED_UPDATES', 'Кэш обновлений очищен', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1355, 'JLIB_INSTALLER_FAILED_TO_PURGE_UPDATES', 'Не удалось очистить кэш обновлений', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1356, 'JLIB_INSTALLER_DEFAULT_STYLE', '%s - По умолчанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1357, 'JLIB_INSTALLER_DISCOVER', 'Поиск', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1358, 'JLIB_INSTALLER_ERROR_COMP_DISCOVER_STORE_DETAILS', 'Поиск дистрибутивов компонентов: Не удалось сохранить детали компонента', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1359, 'JLIB_INSTALLER_ERROR_COMP_FAILED_TO_CREATE_DIRECTORY', 'Компонент %1$s: Ошибка создания каталога: %2$s.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1360, 'JLIB_INSTALLER_ERROR_COMP_INSTALL_ADMIN_ELEMENT', 'Установка компонента: В XML-файле отсутствует элемент управления', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1361, 'JLIB_INSTALLER_ERROR_COMP_INSTALL_DIR_ADMIN', 'Установка компонента: Другой компонент уже использует каталог: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1362, 'JLIB_INSTALLER_ERROR_COMP_INSTALL_DIR_SITE', 'Установка компонента: Другой компонент уже использует каталог: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1363, 'JLIB_INSTALLER_ERROR_COMP_INSTALL_FAILED_TO_CREATE_DIRECTORY_ADMIN', 'Установка компонента: Не удалось создать каталог для административной части системы: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1364, 'JLIB_INSTALLER_ERROR_COMP_INSTALL_FAILED_TO_CREATE_DIRECTORY_SITE', 'Установка компонента: Не удалось создать каталог в разделе сайта: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1365, 'JLIB_INSTALLER_ERROR_COMP_REFRESH_MANIFEST_CACHE', 'Обновление кэша манифеста компонента: не удалось сохранить детали компонента', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1366, 'JLIB_INSTALLER_ERROR_COMP_REMOVING_ADMIN_MENUS_FAILED', 'Нельзя удалять меню панели управления.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1367, 'JLIB_INSTALLER_ERROR_COMP_UNINSTALL_CUSTOM', 'Удаление компонента: Пользовательский сценарий удаления не выполнен', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1368, 'JLIB_INSTALLER_ERROR_COMP_UNINSTALL_FAILED_DELETE_CATEGORIES', 'Удаление компонента: Не удалось удалить категории компонента', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1369, 'JLIB_INSTALLER_ERROR_COMP_UNINSTALL_ERRORREMOVEMANUALLY', 'Удаление компонента: Не удаётся удалить. Пожалуйста, удалите вручную', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1370, 'JLIB_INSTALLER_ERROR_COMP_UNINSTALL_ERRORUNKOWNEXTENSION', 'Удаление компонента: Неизвестное расширение', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1371, 'JLIB_INSTALLER_ERROR_COMP_UNINSTALL_FAILED_REMOVE_DIRECTORY_ADMIN', 'Удаление компонента: Не удалось создать каталог в административном разделе', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1372, 'JLIB_INSTALLER_ERROR_COMP_UNINSTALL_FAILED_REMOVE_DIRECTORY_SITE', 'Удаление компонента: Не удалось удалить каталог в административном разделе', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1373, 'JLIB_INSTALLER_ERROR_COMP_UNINSTALL_NO_OPTION', 'Удаление компонента: Поле параметра не заполнено. Не удаётся удалить файлы', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1374, 'JLIB_INSTALLER_ERROR_COMP_UNINSTALL_SQL_ERROR', 'Удаление компонента: Файл ошибок SQL %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1375, 'JLIB_INSTALLER_ERROR_COMP_UNINSTALL_WARNCORECOMPONENT', 'Удаление компонента: Попытка удаления компонента ядра', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1376, 'JLIB_INSTALLER_ERROR_COMP_UPDATE_FAILED_TO_CREATE_DIRECTORY_ADMIN', 'Обновление компонента: Не удалось создать каталог в административном разделе: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1377, 'JLIB_INSTALLER_ERROR_COMP_UPDATE_FAILED_TO_CREATE_DIRECTORY_SITE', 'Обновление компонента: Не удалось создать каталог на сайте: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1378, 'JLIB_INSTALLER_ERROR_CREATE_DIRECTORY', 'JInstaller: :Install: Невозможно создать каталог: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1379, 'JLIB_INSTALLER_ERROR_CREATE_FOLDER_FAILED', 'Не удалось создать каталог [%s]', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1380, 'JLIB_INSTALLER_ERROR_DEPRECATED_FORMAT', 'Устаревший формат установки (client="both"), рекомендуется использовать инсталлятор', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1381, 'JLIB_INSTALLER_ERROR_DISCOVER_INSTALL_UNSUPPORTED', 'Расширение %s не может быть установлено используя метод Найти. Пожалуйста, установите это расширение из Менеджера расширений: Установка.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1382, 'JLIB_INSTALLER_ERROR_DOWNLOAD_SERVER_CONNECT', 'Ошибка подключения к серверу: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1383, 'JLIB_INSTALLER_ERROR_FAIL_COPY_FILE', 'JInstaller: :Install: Не удалось скопировать файл %1$s to %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1384, 'JLIB_INSTALLER_ERROR_FAIL_COPY_FOLDER', 'JInstaller: :Install: Не удалось скопировать каталог %1$s to %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1385, 'JLIB_INSTALLER_ERROR_FAILED_READING_NETWORK_RESOURCES', 'Ошибка чтения сетевых ресурсов: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1386, 'JLIB_INSTALLER_ERROR_FILE_EXISTS', 'JInstaller: :Install: Файл уже существует %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1387, 'JLIB_INSTALLER_ERROR_FILE_UNINSTALL_INVALID_MANIFEST', 'Удаление файлов: Недопустимый манифест файла', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1388, 'JLIB_INSTALLER_ERROR_FILE_UNINSTALL_INVALID_NOTFOUND_MANIFEST', 'Удаление файлов: манифест файла недопустим или не найден.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1389, 'JLIB_INSTALLER_ERROR_FILE_UNINSTALL_LOAD_ENTRY', 'Удаление файлов: Не удалось загрузить расширение', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1390, 'JLIB_INSTALLER_ERROR_FILE_UNINSTALL_LOAD_MANIFEST', 'Удаление файлов: Не удалось загрузить файл манифеста', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1391, 'JLIB_INSTALLER_ERROR_FILE_UNINSTALL_SQL_ERROR', 'Удаление файлов: файл ошибок SQL %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1392, 'JLIB_INSTALLER_ERROR_FILE_UNINSTALL_WARNCOREFILE', 'Удаление файлов: Попытка удаления системных файлов', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1393, 'JLIB_INSTALLER_ERROR_FOLDER_IN_USE', 'Другое расширение уже использует каталог [%s]', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1394, 'JLIB_INSTALLER_ERROR_LANG_DISCOVER_STORE_DETAILS', 'Поиск пакетов локализации: Не удалось сохранить параметры языка', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1395, 'JLIB_INSTALLER_ERROR_LANG_UNINSTALL_DEFAULT', 'Язык не может быть удалён, пока он используется в качестве языка сайта по умолчанию.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1396, 'JLIB_INSTALLER_ERROR_LANG_UNINSTALL_DIRECTORY', 'Удаление языка: Невозможно удалить указанный каталог языка.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1397, 'JLIB_INSTALLER_ERROR_LANG_UNINSTALL_ELEMENT_EMPTY', 'Удаление языка: Элемент пуст, невозможно удалить файлы', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1398, 'JLIB_INSTALLER_ERROR_LANG_UNINSTALL_PATH_EMPTY', 'Удаление языка: Путь к языку пуст. невозможно удалить файлы', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1399, 'JLIB_INSTALLER_ERROR_LANG_UNINSTALL_PROTECTED', 'Данный язык не может быть удалён. Язык помечен запрещённым к удалению в базе данных (обычно en-GB)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1400, 'JLIB_INSTALLER_ERROR_LIB_DISCOVER_STORE_DETAILS', 'Поиск пакетов с библиотеками: Не удалось сохранить параметры библиотеки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1401, 'JLIB_INSTALLER_ERROR_LIB_UNINSTALL_INVALID_MANIFEST', 'Удаление библиотеки: Недопустимый манифест файла', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1402, 'JLIB_INSTALLER_ERROR_LIB_UNINSTALL_INVALID_NOTFOUND_MANIFEST', 'Удаление библиотеки: Манифест файла недопустим или не найден.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1403, 'JLIB_INSTALLER_ERROR_LIB_UNINSTALL_LOAD_MANIFEST', 'Удаление библиотеки: Невозможно загрузить манифест файла', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1404, 'JLIB_INSTALLER_ERROR_LIB_UNINSTALL_WARNCORELIBRARY', 'Удаление библиотеки: Попытка удалить библиотеку ядра', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1405, 'JLIB_INSTALLER_ERROR_LOAD_XML', 'JInstaller: :Install: Не удалось загрузить XML-файл: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1406, 'JLIB_INSTALLER_ERROR_MOD_DISCOVER_STORE_DETAILS', 'Поиск дистрибутивов модулей: Не удалось сохранить параметры модуля', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1407, 'JLIB_INSTALLER_ERROR_MOD_REFRESH_MANIFEST_CACHE', 'Обновление кэша манифеста модуля: Не удалось сохранить параметры модуля', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1408, 'JLIB_INSTALLER_ERROR_MOD_UNINSTALL_ERRORUNKOWNEXTENSION', 'Удаление модуля: Неизвестное расширение', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1409, 'JLIB_INSTALLER_ERROR_MOD_UNINSTALL_EXCEPTION', 'Удаление модуля: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1410, 'JLIB_INSTALLER_ERROR_MOD_UNINSTALL_INVALID_NOTFOUND_MANIFEST', 'Удаление модуля: Манифест файла недопустим или не найден.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1411, 'JLIB_INSTALLER_ERROR_MOD_UNINSTALL_SQL_ERROR', 'Удаление модуля: Файл ошибок SQL %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1412, 'JLIB_INSTALLER_ERROR_MOD_UNINSTALL_WARNCOREMODULE', 'Удаление модуля: Попытка удалить модуль ядра: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1413, 'JLIB_INSTALLER_ERROR_NO_CORE_LANGUAGE', 'Основной пакет для языка не существует [%s]', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1414, 'JLIB_INSTALLER_ERROR_NO_FILE', 'JInstaller: :Install: Файл не существует %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1415, 'JLIB_INSTALLER_ERROR_NO_LANGUAGE_TAG', 'В пакете отсутствует указание на язык. Вы устанавливаете старую версию языкового пакета?', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1416, 'JLIB_INSTALLER_ERROR_NOTFINDJOOMLAXMLSETUPFILE', 'JInstaller: :Install: Не найден установочный XML-файл Joomla', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1417, 'JLIB_INSTALLER_ERROR_NOTFINDXMLSETUPFILE', 'JInstaller: :Install: Не удалось найти XML-файл установки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1418, 'JLIB_INSTALLER_ERROR_PACK_UNINSTALL_INVALID_MANIFEST', 'Удаление пакета: Недопустимый манифест файла', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1419, 'JLIB_INSTALLER_ERROR_PACK_UNINSTALL_INVALID_NOTFOUND_MANIFEST', 'Удаление пакета: Файл манифеста недействителен или не найден: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1420, 'JLIB_INSTALLER_ERROR_PACK_UNINSTALL_LOAD_MANIFEST', 'Удаление пакета: Невозможно загрузить манифест файла', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1421, 'JLIB_INSTALLER_ERROR_PACK_UNINSTALL_MANIFEST_NOT_REMOVED', 'Удаление пакета: Манифест файла не удалён, поскольку возникли ошибки!', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1422, 'JLIB_INSTALLER_ERROR_PACK_UNINSTALL_MISSINGMANIFEST', 'Удаление пакета: Отсутствует файл манифеста', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1423, 'JLIB_INSTALLER_ERROR_PACK_UNINSTALL_NOT_PROPER', 'Удаление пакета: Это расширение вероятно уже было удалено или же его удаление было прервано в результате ошибки: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1424, 'JLIB_INSTALLER_ERROR_PACK_UNINSTALL_WARNCOREPACK', 'Удаление пакета: Попытка удаления системного пакета', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1425, 'JLIB_INSTALLER_ERROR_PLG_DISCOVER_STORE_DETAILS', 'Поиск дистрибутивов плагинов: Не удалось сохранить параметры плагина', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1426, 'JLIB_INSTALLER_ERROR_PLG_REFRESH_MANIFEST_CACHE', 'Обновление кэша манифеста плагина: Не удалось сохранить параметры плагина', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1427, 'JLIB_INSTALLER_ERROR_PLG_UNINSTALL_ERRORUNKOWNEXTENSION', 'Удаление плагина: Неизвестное расширение', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1428, 'JLIB_INSTALLER_ERROR_PLG_UNINSTALL_FOLDER_FIELD_EMPTY', 'Удаление плагина: Поле каталога не заполнено, невозможно удалить файлы', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1429, 'JLIB_INSTALLER_ERROR_PLG_UNINSTALL_INVALID_MANIFEST', 'Удаление плагина: Недопустимый манифест файла', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1430, 'JLIB_INSTALLER_ERROR_PLG_UNINSTALL_INVALID_NOTFOUND_MANIFEST', 'Удаление плагина: Манифест файла недопустим или не найден.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1431, 'JLIB_INSTALLER_ERROR_PLG_UNINSTALL_LOAD_MANIFEST', 'Удаление плагина: Невозможно загрузить манифест файла', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1432, 'JLIB_INSTALLER_ERROR_PLG_UNINSTALL_WARNCOREPLUGIN', 'Удаление плагина: Попытка удалить плагин ядра: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1433, 'JLIB_INSTALLER_ERROR_SQL_ERROR', 'JInstaller: :Install: Ошибка SQL %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1434, 'JLIB_INSTALLER_ERROR_SQL_FILENOTFOUND', 'JInstaller: :Install: SQL-файл не найден %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1435, 'JLIB_INSTALLER_ERROR_SQL_READBUFFER', 'JInstaller: :Install: Ошибка чтения файла буфера SQL', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1436, 'JLIB_INSTALLER_ERROR_TPL_DISCOVER_STORE_DETAILS', 'Поиск дистрибутивов шаблонов: Не удалось сохранить параметры шаблона', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1437, 'JLIB_INSTALLER_ERROR_TPL_UNINSTALL_ERRORUNKOWNEXTENSION', 'Удаление шаблона: Неизвестное расширение', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1438, 'JLIB_INSTALLER_ERROR_TPL_UNINSTALL_INVALID_CLIENT', 'Удаление шаблона: Клиент недопустим.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1439, 'JLIB_INSTALLER_ERROR_TPL_UNINSTALL_INVALID_NOTFOUND_MANIFEST', 'Удаление шаблона: Манифест файла недопустим или не найден.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1440, 'JLIB_INSTALLER_ERROR_TPL_UNINSTALL_TEMPLATE_DEFAULT', 'Удаление шаблона: Нельзя удалять шаблон, назначенный ''по умолчанию''.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1441, 'JLIB_INSTALLER_ERROR_TPL_UNINSTALL_TEMPLATE_DIRECTORY', 'Удаление шаблона: Каталог не существует, не удаётся удалить файлы', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1442, 'JLIB_INSTALLER_ERROR_TPL_UNINSTALL_TEMPLATE_ID_EMPTY', 'Удаление шаблона: ID шаблона не указано, не удаётся удалить файлы', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1443, 'JLIB_INSTALLER_ERROR_TPL_UNINSTALL_WARNCORETEMPLATE', 'Удаление шаблона: Попытка удалить базовый шаблон ядра: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1444, 'JLIB_INSTALLER_ERROR_UNKNOWN_CLIENT_TYPE', 'Тип клиента не определён [%s]', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1445, 'JLIB_INSTALLER_INSTALL', 'Установить', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1446, 'JLIB_INSTALLER_NOTICE_LANG_RESET_USERS', 'Язык назначен языком ''по умолчанию'' у %d пользователей', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1447, 'JLIB_INSTALLER_NOTICE_LANG_RESET_USERS_1', 'Язык назначен языком ''по умолчанию'' у %d пользователя', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1448, 'JLIB_INSTALLER_UNINSTALL', 'Деинсталлировать', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1449, 'JLIB_INSTALLER_UPDATE', 'Обновить', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1450, 'JLIB_INSTALLER_ERROR_EXTENSION_INVALID_CLIENT_IDENTIFIER', 'Неверный идентификатор клиента, указанный в продолжение манифеста.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1451, 'JLIB_INSTALLER_ERROR_PACK_UNINSTALL_UNKNOWN_EXTENSION', 'Попытка удалить неизвестное расширение из пакета. Это расширение вероятно уже было ранее удалено.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1452, 'JLIB_INSTALLER_NOT_ERROR', 'If the error is related to the installation of TinyMCE language files it has no effect on the installation of the language(s). Some языковые пакеты созданные для версий Joomla! меньше 3.2.0 могут содержать отдельную локализацию для TinyMCE В связи с тем, что локализация TinyMCE уже включена в дистрибутив, эти файлы более не требуются.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1453, 'JLIB_INSTALLER_UPDATE_LOG_QUERY', 'Выполнение запроса из файла %1$s. Текст запроса: %2$s.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1454, 'JLIB_MAIL_FUNCTION_DISABLED', 'Почта не может быть оправлена, поскольку PHP-функция mail() заблокирована на данном сервере.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1455, 'JLIB_MAIL_FUNCTION_OFFLINE', 'Функция отправки почты временно выключена на данном сайте. Попробуйте позже.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1456, 'JLIB_MAIL_INVALID_EMAIL_SENDER', 'JMail: : Неправильный e-mail отправителя: %s, JMail: :setSender(%s)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1457, 'JLIB_MEDIA_ERROR_UPLOAD_INPUT', 'Ошибка загрузки файла.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1458, 'JLIB_MEDIA_ERROR_WARNFILENAME', 'Имя файла должно содержать только буквы и цифры, без пробелов.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1459, 'JLIB_MEDIA_ERROR_WARNFILETOOLARGE', 'Размер данного файла слишком велик для загрузки.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1460, 'JLIB_MEDIA_ERROR_WARNFILETYPE', 'Данный тип файлов не поддерживается.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1461, 'JLIB_MEDIA_ERROR_WARNIEXSS', 'Зафиксирована попытка XSS-атаки.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1462, 'JLIB_MEDIA_ERROR_WARNINVALID_IMG', 'Изображение некорректно.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1463, 'JLIB_MEDIA_ERROR_WARNINVALID_MIME', 'Обнаружен запрещённый либо некорректный тип файла (MIME).', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1464, 'JLIB_MEDIA_ERROR_WARNNOTADMIN', 'Загружаемый файл не является изображением либо Ваши права ниже менеджера.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1465, 'JLIB_PLUGIN_ERROR_LOADING_PLUGINS', 'Ошибка при установке плагинов: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1466, 'JLIB_REGISTRY_EXCEPTION_LOAD_FORMAT_CLASS', 'Не удаётся загрузить класс формата', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1467, 'JLIB_RULES_ACTION', 'Действие', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1468, 'JLIB_RULES_ALLOWED', 'Разрешено', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1469, 'JLIB_RULES_ALLOWED_ADMIN', 'Разрешено (Суперадминистратор)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1470, 'JLIB_RULES_CALCULATED_SETTING', 'Суммарное значение <sup>2</sup>', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1471, 'JLIB_RULES_CONFLICT', 'Конфликт', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1472, 'JLIB_RULES_DENIED', 'Запрещено', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1473, 'JLIB_RULES_GROUP', '%s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1474, 'JLIB_RULES_GROUPS', 'Менеджер групп', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1475, 'JLIB_RULES_INHERIT', 'Наследовать', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1476, 'JLIB_RULES_INHERITED', 'Унаследовано', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1477, 'JLIB_RULES_NOT_ALLOWED', 'Не разрешено', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1478, 'JLIB_RULES_NOT_ALLOWED_ADMIN_CONFLICT', 'Конфликт', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1479, 'JLIB_RULES_NOT_ALLOWED_LOCKED', 'Не разрешено (Заблокировано)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1480, 'JLIB_RULES_NOT_SET', 'Не определено', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1481, 'JLIB_RULES_SELECT_ALLOW_DENY_GROUP', 'Разрешать или блокировать действие %s для пользователей группы %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1482, 'JLIB_RULES_SELECT_SETTING', 'Выбор нового значения <sup>1</sup>', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1483, 'JLIB_RULES_SETTING_NOTES', '1. Если изменить значение, оно будет применено здесь, а так же во всех дочерних группах, компонентах и содержимом. Обратите внимание: значение <em>Запрещено</em> переопределит любое унаследовано значение, а так же значение в любой дочерней группе, компоненте или содержимом. В случае противоречия в значениях, <em>Запрещено</em> будет иметь большую силу. Значение <em>Не задано</em> будут трактоваться, как <em>Запрещено</em>, но его можно будет переопределить в дочерних группах, компонентах и содержимом.<br />2. Если вы зададите новое значение, для обновления суммарных значений нажмите кнопку <em>Сохранить</em>.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1484, 'JLIB_RULES_SETTING_NOTES_ITEM', '1. Если изменить значение, оно будет применено для этого объекта. Обратите внимание: <br />Значение <em>Унаследовано</em> - означает, что будет применено значение данного права, указанное в общих настройках, вышестоящей группе и категории.<br /><em>Запрещено</em> - означает, что независимо от значения в общих настройках, вышестоящей группе или категории, пользователи данной группы не смогут выполнить указанное действие над этим объектом.<br /><em>Разрешено</em> - означает, что пользователи данной группы смогут выполнить указанное действие над этим объектом (но, в случае противоречия со значением в общих настройках, вышестоящей группе или категории, право предоставлено не будет. Противоречие будет отмечено в поле суммарных значений, как <em>Не разрешено (со значком блокировки)</em>).<br />2. Если вы зададите новое значение, для обновления суммарных значений, нажмите кнопку <em>Сохранить</em>.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1485, 'JLIB_RULES_SETTINGS_DESC', 'Управление настройками прав доступа для групп пользователей. Ознакомьтесь с примечаниями в нижней части страницы.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1486, 'JLIB_UNKNOWN', 'Неизвестный', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1487, 'JLIB_UPDATER_ERROR_COLLECTION_FOPEN', 'В конфигурации PHP параметр allow_url_fopen отключен. Для использования функций обновления необходимо включить allow_url_fopen.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1488, 'JLIB_UPDATER_ERROR_COLLECTION_OPEN_URL', 'Update: :Collection: Не удалось открыть %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1489, 'JLIB_UPDATER_ERROR_COLLECTION_PARSE_URL', 'Update: :Collection: Не удалось обработать %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1490, 'JLIB_UPDATER_ERROR_EXTENSION_OPEN_URL', 'Update: :Extension: Не удалось открыть %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1491, 'JLIB_UPDATER_ERROR_EXTENSION_PARSE_URL', 'Update: :Extension: Не удалось обработать %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1492, 'JLIB_UPDATER_ERROR_OPEN_UPDATE_SITE', 'Обновление: Ошибка открытия сервера обновлений #%d &quot;%s&quot;, URL: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1493, 'JLIB_USER_ERROR_AUTHENTICATION_FAILED_LOAD_PLUGIN', 'JAuthentication: :authenticate: Не удалось загрузить плагин: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1494, 'JLIB_USER_ERROR_AUTHENTICATION_LIBRARIES', 'JAuthentication: :__construct: Не удаётся загрузить библиотеки проверки подлинности.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1495, 'JLIB_USER_ERROR_BIND_ARRAY', 'Невозможно сопоставить массив объекта пользователя', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1496, 'JLIB_USER_ERROR_CANNOT_DEMOTE_SELF', 'Нельзя отменять права вашей собственно учётной записи Суперадминистратора.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1497, 'JLIB_USER_ERROR_CANNOT_REUSE_PASSWORD', 'Вы не можете использовать ваш старый пароль. Пожалуйста, введите новый пароль.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1498, 'JLIB_USER_ERROR_ID_NOT_EXISTS', 'JUser: :_load: Пользователь %s не существует', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1499, 'JLIB_USER_ERROR_NOT_SUPERADMIN', 'Изменять параметры пользователей, являющихся Суперадминистраторами могут только другие Суперадминистраторы.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1500, 'JLIB_USER_ERROR_PASSWORD_NOT_MATCH', 'Пароли не совпадают. Пожалуйста, введите их заново.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1501, 'JLIB_USER_ERROR_UNABLE_TO_FIND_USER', 'Не удалось найти пользователя с такой строкой активации', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1502, 'JLIB_USER_ERROR_UNABLE_TO_LOAD_USER', 'JUser: :_load: Не удалось загрузить пользователя с ID: %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1503, 'JLIB_USER_EXCEPTION_ACCESS_USERGROUP_INVALID', 'Группа пользователей не существует', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1504, 'JLIB_UTIL_ERROR_APP_INSTANTIATION', 'Ошибка при создании экземпляра приложения', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1505, 'JLIB_UTIL_ERROR_CONNECT_DATABASE', 'JDatabase: :getInstance: Не смог подключиться к базе данных <br />joomla.library: %1$s - %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1506, 'JLIB_UTIL_ERROR_DOMIT', 'DommitDocument устарела. Используйте вместо неё DomDocument', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1507, 'JLIB_UTIL_ERROR_LOADING_FEED_DATA', 'Невозможно загрузить ленту новостей', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1508, 'JLIB_UTIL_ERROR_XML_LOAD', 'Не удалось загрузить XML-файл', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1509, 'JLIB_USER_ERROR_PASSWORD_TOO_LONG', 'Пароль слишком длинный. Пожалуйста, выберите пароль не длиннее, чем 55 символов', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1510, 'JLIB_USER_ERROR_PASSWORD_TRUNCATED', 'Пароль был усечён до первых 55 символов', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.ini'),
(1511, 'LIB_JOOMLA_XML_DESCRIPTION', 'Joomla! Platform - программная платформа, на которой построена система управления сайтом Joomla!', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_joomla.sys.ini'),
(1512, 'LIB_PHPASS_XML_DESCRIPTION', 'phpass это PHP-фреймворк для шифрования паролей.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_phpass.sys.ini'),
(1513, 'LIB_PHPUTF8_XML_DESCRIPTION', 'Библиотека для работы с UTF-8', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_phputf8.sys.ini'),
(1514, 'LIB_SIMPLEPIE_XML_DESCRIPTION', 'Библиотека для работы с RSS и Atom', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.lib_simplepie.sys.ini'),
(1515, 'MOD_ARTICLES_ARCHIVE', 'Материалы - Материалы в архиве', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_archive.ini'),
(1516, 'MOD_ARTICLES_ARCHIVE_FIELD_COUNT_LABEL', 'Кол-во месяцев', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_archive.ini'),
(1517, 'MOD_ARTICLES_ARCHIVE_FIELD_COUNT_DESC', 'Количество отображаемых материалов (по умолчанию - 10)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_archive.ini'),
(1518, 'MOD_ARTICLES_ARCHIVE_XML_DESCRIPTION', 'Этот модуль показывает список календарных месяцев, которые содержат архивные материалы. После того, как вы измените статус материала на ''архивный'', этот список будет сгенерирован автоматически.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_archive.ini'),
(1519, 'MOD_ARTICLES_ARCHIVE_DATE', '%1$s, %2$s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_archive.ini'),
(1520, 'MOD_ARTICLES_ARCHIVE', 'Материалы - Материалы в архиве', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_archive.sys.ini'),
(1521, 'MOD_ARTICLES_ARCHIVE_XML_DESCRIPTION', 'Этот модуль показывает список календарных месяцев, которые содержат архивные материалы. После того, как вы измените статус материала на ''архивный'', этот список будет сгенерирован автоматически.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_archive.sys.ini'),
(1522, 'MOD_ARTICLES_ARCHIVE_LAYOUT_DEFAULT', 'По умолчанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_archive.sys.ini'),
(1523, 'MOD_ARTICLES_CATEGORIES', 'Категории', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_categories.ini'),
(1524, 'MOD_ARTICLES_CATEGORIES_FIELD_COUNT_DESC', 'Укажите количество первых уровней подкатегорий, которые следует выводить. По умолчанию - все.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_categories.ini'),
(1525, 'MOD_ARTICLES_CATEGORIES_FIELD_COUNT_LABEL', 'Количество первых подкатегорий', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_categories.ini'),
(1526, 'MOD_ARTICLES_CATEGORIES_FIELD_MAXLEVEL_DESC', 'Введите здесь максимальную глубину вложения подкатегорий. По умолчанию - все.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_categories.ini'),
(1527, 'MOD_ARTICLES_CATEGORIES_FIELD_MAXLEVEL_LABEL', 'Максимальная глубина вложения', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_categories.ini'),
(1528, 'MOD_ARTICLES_CATEGORIES_FIELD_PARENT_DESC', 'Выберите родительскую категорию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_categories.ini'),
(1529, 'MOD_ARTICLES_CATEGORIES_FIELD_PARENT_LABEL', 'Родительская категория', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_categories.ini'),
(1530, 'MOD_ARTICLES_CATEGORIES_FIELD_SHOW_CHILDREN_DESC', 'Показывать или скрывать подкатегории', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_categories.ini'),
(1531, 'MOD_ARTICLES_CATEGORIES_FIELD_SHOW_CHILDREN_LABEL', 'Показывать подкатегории', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_categories.ini'),
(1532, 'MOD_ARTICLES_CATEGORIES_FIELD_SHOW_DESCRIPTION_DESC', 'Показывать или скрывать описание категории', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_categories.ini'),
(1533, 'MOD_ARTICLES_CATEGORIES_FIELD_SHOW_DESCRIPTION_LABEL', 'Описание категории', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_categories.ini'),
(1534, 'MOD_ARTICLES_CATEGORIES_FIELD_NUMITEMS_DESC', 'Показывать или скрывать количество материалов', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_categories.ini'),
(1535, 'MOD_ARTICLES_CATEGORIES_FIELD_NUMITEMS_LABEL', 'Показывать кол-во материалов', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_categories.ini'),
(1536, 'MOD_ARTICLES_CATEGORIES_XML_DESCRIPTION', 'Этот модуль отображает список категорий, входящих в одну общую родительскую категорию.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_categories.ini'),
(1537, 'MOD_ARTICLES_CATEGORIES_TITLE_HEADING_LABEL', 'Стиль заголовка', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_categories.ini'),
(1538, 'MOD_ARTICLES_CATEGORIES_TITLE_HEADING_DESC', 'Определяет - какой именно стиль заголовка будет применён', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_categories.ini'),
(1539, 'MOD_ARTICLES_CATEGORIES', 'Категории', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_categories.sys.ini'),
(1540, 'MOD_ARTICLES_CATEGORIES_XML_DESCRIPTION', 'Этот модуль показывает список категорий, входящих в одну общую родительскую категорию.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_categories.sys.ini'),
(1541, 'MOD_ARTICLES_CATEGORIES_LAYOUT_DEFAULT', 'По умолчанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_categories.sys.ini'),
(1542, 'MOD_ARTICLES_CATEGORY', 'Материалы - Список материалов категории', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1543, 'MOD_ARTICLES_CATEGORY_FIELD_ARTICLEGROUPING_DESC', 'Выберите предпочтительный способ группировки материалов.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1544, 'MOD_ARTICLES_CATEGORY_FIELD_ARTICLEGROUPING_LABEL', 'Группировка материалов', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1545, 'MOD_ARTICLES_CATEGORY_FIELD_ARTICLEGROUPINGDIR_DESC', 'Выберите порядок, в котором следует выводить группы материалов.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1546, 'MOD_ARTICLES_CATEGORY_FIELD_ARTICLEGROUPINGDIR_LABEL', 'Порядок отображения групп', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1547, 'MOD_ARTICLES_CATEGORY_FIELD_ARTICLEORDERING_DESC', 'Выберите поле, по которому следует сортировать материалы. Значение ''Порядок Избранных материалов'' может использоваться лишь в том случае, если параметр ''Избранные материалы'' имеет значение ''Только Избранные''.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1548, 'MOD_ARTICLES_CATEGORY_FIELD_ARTICLEORDERING_LABEL', 'Поле материала, по которому будет выполняться сортировка', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1549, 'MOD_ARTICLES_CATEGORY_FIELD_ARTICLEORDERINGDIR_DESC', 'Выберите направление сортировки, в котором следует выводить материалы.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1550, 'MOD_ARTICLES_CATEGORY_FIELD_ARTICLEORDERINGDIR_LABEL', 'Направление сортировки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1551, 'MOD_ARTICLES_CATEGORY_FIELD_AUTHOR_DESC', 'Выберите одного или нескольких авторов из списка ниже.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1552, 'MOD_ARTICLES_CATEGORY_FIELD_AUTHOR_LABEL', 'Авторы', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1553, 'MOD_ARTICLES_CATEGORY_FIELD_AUTHORALIAS_DESC', 'Выберите один или несколько псевдонимов авторов из списка ниже.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1554, 'MOD_ARTICLES_CATEGORY_FIELD_AUTHORALIAS_LABEL', 'Псевдоним автора', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1555, 'MOD_ARTICLES_CATEGORY_FIELD_AUTHORALIASFILTERING_DESC', 'Выберите ''Включая'', чтобы включить выбранные псевдонимы авторов, или ''Исключая'', чтобы исключить их.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1556, 'MOD_ARTICLES_CATEGORY_FIELD_AUTHORALIASFILTERING_LABEL', 'Тип фильтра по псевдониму автора', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1557, 'MOD_ARTICLES_CATEGORY_FIELD_AUTHORFILTERING_DESC', 'Выберите ''Включая'', чтобы включить выбранных авторов, или ''Исключая'', чтобы исключить их.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1558, 'MOD_ARTICLES_CATEGORY_FIELD_AUTHORFILTERING_LABEL', 'Тип фильтра по автору', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini');
INSERT INTO `yawnc_overrider` (`id`, `constant`, `string`, `file`) VALUES
(1559, 'MOD_ARTICLES_CATEGORY_FIELD_CATDEPTH_DESC', 'Количество возвращаемых уровней дочерних категорий.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1560, 'MOD_ARTICLES_CATEGORY_FIELD_CATDEPTH_LABEL', 'Глубина категорий', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1561, 'MOD_ARTICLES_CATEGORY_FIELD_CATEGORY_DESC', 'Выберите, пожалуйста, одну или несколько категорий.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1562, 'MOD_ARTICLES_CATEGORY_FIELD_CATFILTERINGTYPE_DESC', 'Выберите ''Включая'', чтобы включить выбранные категории, или ''Исключая'', чтобы исключить их.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1563, 'MOD_ARTICLES_CATEGORY_FIELD_CATFILTERINGTYPE_LABEL', 'Тип фильтра по категории', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1564, 'MOD_ARTICLES_CATEGORY_FIELD_COUNT_DESC', 'Кол-во отображаемых объектов. Если установлено число "0" (значение по умолчанию), будут показаны все материалы.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1565, 'MOD_ARTICLES_CATEGORY_FIELD_COUNT_LABEL', 'Кол-во', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1566, 'MOD_ARTICLES_CATEGORY_FIELD_DATERANGEFIELD_DESC', 'Укажите, по которому из существующих полей даты следует ограничивать диапазон дат.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1567, 'MOD_ARTICLES_CATEGORY_FIELD_DATERANGEFIELD_LABEL', 'Поле для фильтрации', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1568, 'MOD_ARTICLES_CATEGORY_FIELD_DATEFIELD_DESC', 'Выберите, какое именно из существующих полей даты следует выводить.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1569, 'MOD_ARTICLES_CATEGORY_FIELD_DATEFIELD_LABEL', 'Поле даты', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1570, 'MOD_ARTICLES_CATEGORY_FIELD_DATEFIELDFORMAT_DESC', 'Введите пожалуйста корректный код формата. Для более подробной информации ознакомьтесь с материалами справочной системы языка PHP: http://php.net/date.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1571, 'MOD_ARTICLES_CATEGORY_FIELD_DATEFIELDFORMAT_LABEL', 'Формат даты', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1572, 'MOD_ARTICLES_CATEGORY_FIELD_DATEFILTERING_DESC', 'Выберите тип фильтрации по дате.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1573, 'MOD_ARTICLES_CATEGORY_FIELD_DATEFILTERING_LABEL', 'Фильтр по дате', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1574, 'MOD_ARTICLES_CATEGORY_FIELD_ENDDATE_DESC', 'Если выше указано значение в поле ''Диапазон дат'', выберите, пожалуйста, дату окончания.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1575, 'MOD_ARTICLES_CATEGORY_FIELD_ENDDATE_LABEL', 'Дата конца диапазона', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1576, 'MOD_ARTICLES_CATEGORY_FIELD_EXCLUDEDARTICLES_DESC', 'Пожалуйста, введите ID каждого материала на новой строке.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1577, 'MOD_ARTICLES_CATEGORY_FIELD_EXCLUDEDARTICLES_LABEL', 'ID материала, который необходимо исключать', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1578, 'MOD_ARTICLES_CATEGORY_FIELD_GROUP_DISPLAY_LABEL', 'Параметры отображения', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1579, 'MOD_ARTICLES_CATEGORY_FIELD_GROUP_DYNAMIC_LABEL', 'Параметры динамического режима', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1580, 'MOD_ARTICLES_CATEGORY_FIELD_GROUP_FILTERING_LABEL', 'Параметры фильтрации', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1581, 'MOD_ARTICLES_CATEGORY_FIELD_GROUP_GROUPING_LABEL', 'Параметры группировки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1582, 'MOD_ARTICLES_CATEGORY_FIELD_GROUP_ORDERING_LABEL', 'Параметры сортировки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1583, 'MOD_ARTICLES_CATEGORY_FIELD_INTROTEXTLIMIT_DESC', 'Пожалуйста, укажите максимально допустимое количество символов во вступительном тексте. Остальная часть вводного текста будет отсечена.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1584, 'MOD_ARTICLES_CATEGORY_FIELD_INTROTEXTLIMIT_LABEL', 'Ограничение вводного текста', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1585, 'MOD_ARTICLES_CATEGORY_FIELD_LINKTITLES_LABEL', 'Заголовок как ссылка', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1586, 'MOD_ARTICLES_CATEGORY_FIELD_LINKTITLES_DESC', 'Заголовок как ссылка', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1587, 'MOD_ARTICLES_CATEGORY_FIELD_MODE_DESC', 'Пожалуйста, выберите режим, который вы хотели бы использовать. Если выбрать ''Обычный'', то просто настройте модуль и он будет отображать статичный список статей в пункте меню, к которому будет привязан. Если выбрать ''Динамический'', то вы так же можете настроить модуль, однако значение параметра Категория учитываться не будет. Вместо этого, модуль будет динамически определять представление страницы. Если используется представление ''Список материалов категории'', будет отображать список материалов этой категории. Если выбран ''Динамический'', можно привязать модуль ''ко всем страницам'' сайта. В этом случае он будет автоматически определять, следует ли ему что-либо показывать.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1588, 'MOD_ARTICLES_CATEGORY_FIELD_MODE_LABEL', 'Режим', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1589, 'MOD_ARTICLES_CATEGORY_FIELD_MONTHYEARFORMAT_DESC', 'Введите пожалуйста корректный код формата. Для более подробной информации ознакомьтесь с материалами справочной системы языка PHP: http://php.net/date.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1590, 'MOD_ARTICLES_CATEGORY_FIELD_MONTHYEARFORMAT_LABEL', 'Формат показа месяца и года', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1591, 'MOD_ARTICLES_CATEGORY_FIELD_RELATIVEDATE_DESC', 'Если выше выбран режим ''Относительная дата'', то, введите количество дней. Продолжительность будет автоматически вычислена исходя из текущей даты и указанного вами значения.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1592, 'MOD_ARTICLES_CATEGORY_FIELD_RELATIVEDATE_LABEL', 'Относительная дата', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1593, 'MOD_ARTICLES_CATEGORY_FIELD_SHOWAUTHOR_DESC', 'Выберите <strong>Показать</strong>, если вы хотите, чтобы автор (или его псевдоним вместо имени) были видны на сайте.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1594, 'MOD_ARTICLES_CATEGORY_FIELD_SHOWCATEGORY_DESC', 'Выберите <strong>Показать</strong>, если вы хотели бы вывести название категории на странице сайта.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1595, 'MOD_ARTICLES_CATEGORY_FIELD_SHOWCHILDCATEGORYARTICLES_DESC', 'Включать или исключать из показа материалы, хранящиеся в дочерних категориях.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1596, 'MOD_ARTICLES_CATEGORY_FIELD_SHOWCHILDCATEGORYARTICLES_LABEL', 'Материалы дочерних категорий', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1597, 'MOD_ARTICLES_CATEGORY_FIELD_SHOWDATE_DESC', 'Выберите Показывать, если хотите отобразить дату.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1598, 'MOD_ARTICLES_CATEGORY_FIELD_SHOWFEATURED_DESC', 'Выберите <strong>Показать</strong>, если вы хотите показывать избранные материалы. Так же можно установить отображение только избранных материалов.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1599, 'MOD_ARTICLES_CATEGORY_FIELD_SHOWFEATURED_LABEL', 'Избранные материалы', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1600, 'MOD_ARTICLES_CATEGORY_FIELD_SHOWHITS_DESC', 'Выберите <strong>Показать</strong>, если вы хотите отобразить на сайте количество просмотров для каждого материала.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1601, 'MOD_ARTICLES_CATEGORY_FIELD_SHOWHITS_LABEL', 'Кол-во просмотров', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1602, 'MOD_ARTICLES_CATEGORY_FIELD_SHOWINTROTEXT_DESC', 'Выберите <strong>Показать</strong>, если вы хотите отображать вступительный текст материала.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1603, 'MOD_ARTICLES_CATEGORY_FIELD_SHOWINTROTEXT_LABEL', 'Вводный текст', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1604, 'MOD_ARTICLES_CATEGORY_FIELD_SHOWONARTICLEPAGE_DESC', 'Выберите, <strong>Показать</strong> ли список материалов на странице текста материала. Это означает, что модуль будет отображаться только на страницах Категорий.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1605, 'MOD_ARTICLES_CATEGORY_FIELD_SHOWONARTICLEPAGE_LABEL', 'Показывать на странице материала', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1606, 'MOD_ARTICLES_CATEGORY_FIELD_STARTDATE_DESC', 'Если выше включен параметр ''Период'' введите, пожалуйста, дату начала.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1607, 'MOD_ARTICLES_CATEGORY_FIELD_STARTDATE_LABEL', 'Дата начала диапазона', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1608, 'MOD_ARTICLES_CATEGORY_OPTION_ASCENDING_VALUE', 'По возрастанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1609, 'MOD_ARTICLES_CATEGORY_OPTION_CREATED_VALUE', 'Дата создания', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1610, 'MOD_ARTICLES_CATEGORY_OPTION_DATERANGE_VALUE', 'Диапазон дат', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1611, 'MOD_ARTICLES_CATEGORY_OPTION_DESCENDING_VALUE', 'По убыванию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1612, 'MOD_ARTICLES_CATEGORY_OPTION_DYNAMIC_VALUE', 'Динамический', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1613, 'MOD_ARTICLES_CATEGORY_OPTION_EXCLUDE_VALUE', 'Исключать', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1614, 'MOD_ARTICLES_CATEGORY_OPTION_EXCLUSIVE_VALUE', 'Исключающий', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1615, 'MOD_ARTICLES_CATEGORY_OPTION_HITS_VALUE', 'Количество просмотров', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1616, 'MOD_ARTICLES_CATEGORY_OPTION_ID_VALUE', 'ID', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1617, 'MOD_ARTICLES_CATEGORY_OPTION_INCLUDE_VALUE', 'Включать', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1618, 'MOD_ARTICLES_CATEGORY_OPTION_INCLUSIVE_VALUE', 'Включающий', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1619, 'MOD_ARTICLES_CATEGORY_OPTION_MODIFIED_VALUE', 'Дата изменения', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1620, 'MOD_ARTICLES_CATEGORY_OPTION_MONTHYEAR_VALUE', 'Месяц и Год', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1621, 'MOD_ARTICLES_CATEGORY_OPTION_NORMAL_VALUE', 'Обычный', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1622, 'MOD_ARTICLES_CATEGORY_OPTION_OFF_VALUE', 'Выкл.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1623, 'MOD_ARTICLES_CATEGORY_OPTION_ONLYFEATURED_VALUE', 'Только Избранные', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1624, 'MOD_ARTICLES_CATEGORY_OPTION_ORDERING_VALUE', 'Порядок, определённый Joomla!', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1625, 'MOD_ARTICLES_CATEGORY_OPTION_ORDERINGFEATURED_VALUE', 'Порядок Избранных материалов', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1626, 'MOD_ARTICLES_CATEGORY_OPTION_RELATIVEDAY_VALUE', 'Относительная дата', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1627, 'MOD_ARTICLES_CATEGORY_OPTION_STARTPUBLISHING_VALUE', 'Дата начала публикации', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1628, 'MOD_ARTICLES_CATEGORY_OPTION_FINISHPUBLISHING_VALUE', 'Дата завершения публикации', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1629, 'MOD_ARTICLES_CATEGORY_OPTION_YEAR_VALUE', 'Год', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1630, 'MOD_ARTICLES_CATEGORY_READ_MORE', 'Подробнее: ', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1631, 'MOD_ARTICLES_CATEGORY_READ_MORE_TITLE', 'Подробнее...', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1632, 'MOD_ARTICLES_CATEGORY_REGISTER_TO_READ_MORE', '''Подробнее'' только для зарегистрированных', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1633, 'MOD_ARTICLES_CATEGORY_XML_DESCRIPTION', 'Этот модуль отображает список материалов из одной или нескольких категорий.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.ini'),
(1634, 'MOD_ARTICLES_CATEGORY', 'Материалы - Список материалов категории', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.sys.ini'),
(1635, 'MOD_ARTICLES_CATEGORY_XML_DESCRIPTION', 'Этот модуль отображает список материалов из одной или нескольких категорий.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.sys.ini'),
(1636, 'MOD_ARTICLES_CATEGORY_LAYOUT_DEFAULT', 'По умолчанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_category.sys.ini'),
(1637, 'MOD_ARTICLES_LATEST', 'Материалы - Последние новости', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_latest.ini'),
(1638, 'MOD_LATEST_NEWS_FIELD_CATEGORY_DESC', 'Выделите категории, материалы из которых следует отображать. Если не выделить ни одной, будут показаны материалы из всех категорий.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_latest.ini'),
(1639, 'MOD_LATEST_NEWS_FIELD_COUNT_DESC', 'Количество отображаемых материалов (по умолчанию - 5)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_latest.ini'),
(1640, 'MOD_LATEST_NEWS_FIELD_COUNT_LABEL', 'Кол-во', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_latest.ini'),
(1641, 'MOD_LATEST_NEWS_FIELD_FEATURED_DESC', 'Показывать/Скрывать материалы, отмеченные, как ''избранные''', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_latest.ini'),
(1642, 'MOD_LATEST_NEWS_FIELD_FEATURED_LABEL', 'Избранные материалы', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_latest.ini'),
(1643, 'MOD_LATEST_NEWS_FIELD_ORDERING_DESC', '''Последние созданные - первыми'': выводить материалы, отсортировав по дате создания.<br /><br />''Последние изменённые - первыми'': выводить материалы, отсортировав по дате последнего изменения.<br /><br />''Последние опубликованные - первыми'': выводить материалы, отсортировав по дате публикации.<br /><br />''Последние обработанные - первыми'': выводить материалы, отсортировав по датам создания или модификации.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_latest.ini'),
(1644, 'MOD_LATEST_NEWS_FIELD_ORDERING_LABEL', 'Порядок', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_latest.ini'),
(1645, 'MOD_LATEST_NEWS_FIELD_USER_DESC', 'Фильтр по автору', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_latest.ini'),
(1646, 'MOD_LATEST_NEWS_FIELD_USER_LABEL', 'Авторы', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_latest.ini'),
(1647, 'MOD_LATEST_NEWS_VALUE_ADDED_BY_ME', 'Добавлены или изменены мной', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_latest.ini'),
(1648, 'MOD_LATEST_NEWS_VALUE_ANYONE', 'Кто угодно', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_latest.ini'),
(1649, 'MOD_LATEST_NEWS_VALUE_NOTADDED_BY_ME', 'Добавлены или изменены НЕ мной', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_latest.ini'),
(1650, 'MOD_LATEST_NEWS_VALUE_ONLY_SHOW_FEATURED', 'Показывать только избранные материалы', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_latest.ini'),
(1651, 'MOD_LATEST_NEWS_VALUE_RECENT_ADDED', 'Последние созданные - первыми', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_latest.ini'),
(1652, 'MOD_LATEST_NEWS_VALUE_RECENT_MODIFIED', 'Последние изменённые - первыми', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_latest.ini'),
(1653, 'MOD_LATEST_NEWS_VALUE_RECENT_PUBLISHED', 'Последние опубликованные - первыми', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_latest.ini'),
(1654, 'MOD_LATEST_NEWS_VALUE_RECENT_RAND', 'Случайные материалы', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_latest.ini'),
(1655, 'MOD_LATEST_NEWS_VALUE_RECENT_TOUCHED', 'Последние обработанные - первыми', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_latest.ini'),
(1656, 'MOD_LATEST_NEWS_XML_DESCRIPTION', 'Этот модуль отображает список самых последних опубликованных материалов, у которых не истёк срок публикации.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_latest.ini'),
(1657, 'MOD_ARTICLES_LATEST', 'Материалы - Последние новости', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_latest.sys.ini'),
(1658, 'MOD_LATEST_NEWS_XML_DESCRIPTION', 'Этот модуль отображает список самых последних опубликованных материалов, у которых не истёк срок публикации.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_latest.sys.ini'),
(1659, 'MOD_ARTICLES_LATEST_LAYOUT_DEFAULT', 'По умолчанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_latest.sys.ini'),
(1660, 'MOD_ARTICLES_NEWS', 'Материалы - Новости', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_news.ini'),
(1661, 'MOD_ARTICLES_NEWS_FIELD_CATEGORY_DESC', 'Выделите категории, материалы из которых следует отображать. Если не выделить ни одной, будут показаны материалы из всех категорий.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_news.ini'),
(1662, 'MOD_ARTICLES_NEWS_FIELD_IMAGES_DESC', 'Показывать изображения из материалов', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_news.ini'),
(1663, 'MOD_ARTICLES_NEWS_FIELD_IMAGES_LABEL', 'Показывать изображения', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_news.ini'),
(1664, 'MOD_ARTICLES_NEWS_FIELD_ITEMS_DESC', 'Количество отображаемых материалов', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_news.ini'),
(1665, 'MOD_ARTICLES_NEWS_FIELD_ITEMS_LABEL', 'Количество материалов', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_news.ini'),
(1666, 'MOD_ARTICLES_NEWS_FIELD_LINKTITLE_DESC', 'Выводит заголовки материалов как ссылки на полный текст материала.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_news.ini'),
(1667, 'MOD_ARTICLES_NEWS_FIELD_LINKTITLE_LABEL', 'Заголовок как ссылка', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_news.ini'),
(1668, 'MOD_ARTICLES_NEWS_FIELD_ORDERING_DESC', 'Порядок отображения материалов', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_news.ini'),
(1669, 'MOD_ARTICLES_NEWS_FIELD_ORDERING_LABEL', 'Порядок', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_news.ini'),
(1670, 'MOD_ARTICLES_NEWS_FIELD_ORDERING_CREATED_DATE', 'Дата создания', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_news.ini'),
(1671, 'MOD_ARTICLES_NEWS_FIELD_ORDERING_PUBLISHED_DATE', 'Дата публикации', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_news.ini'),
(1672, 'MOD_ARTICLES_NEWS_FIELD_ORDERING_ORDERING', 'Порядок', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_news.ini'),
(1673, 'MOD_ARTICLES_NEWS_FIELD_ORDERING_RANDOM', 'Случайно', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_news.ini'),
(1674, 'MOD_ARTICLES_NEWS_FIELD_READMORE_DESC', 'Если установлено значение <strong>Показать</strong>, вместо основной части материала будет выводиться ссылка ''Подробнее...'', ведущая на отдельную страницу просмотра полного текста.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_news.ini'),
(1675, 'MOD_ARTICLES_NEWS_FIELD_READMORE_LABEL', 'Ссылка ''Подробнее...''', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_news.ini'),
(1676, 'MOD_ARTICLES_NEWS_FIELD_SEPARATOR_DESC', 'Показывать разделитель после последнего материала', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_news.ini'),
(1677, 'MOD_ARTICLES_NEWS_FIELD_SEPARATOR_LABEL', 'Показывать последний разделитель', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_news.ini'),
(1678, 'MOD_ARTICLES_NEWS_FIELD_TITLE_DESC', 'Показывать/Скрывать заголовок материала', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_news.ini'),
(1679, 'MOD_ARTICLES_NEWS_FIELD_TITLE_LABEL', 'Показывать заголовок', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_news.ini'),
(1680, 'MOD_ARTICLES_NEWS_READMORE', 'Подробнее...', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_news.ini'),
(1681, 'MOD_ARTICLES_NEWS_READMORE_REGISTER', 'Зарегистрируйтесь для доступа к полному тексту материала', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_news.ini'),
(1682, 'MOD_ARTICLES_NEWS_TITLE_HEADING', 'Уровень заголовка', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_news.ini'),
(1683, 'MOD_ARTICLES_NEWS_TITLE_HEADING_DESCRIPTION', 'Выберите HTML-тег, в котором следует выводить заголовок материала.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_news.ini'),
(1684, 'MOD_ARTICLES_NEWS_XML_DESCRIPTION', 'Модуль Последних новостей выводит фиксированное количество материалов из конкретной категории или набора категорий.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_news.ini'),
(1685, 'MOD_ARTICLES_NEWS', 'Материалы - Новости', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_news.sys.ini'),
(1686, 'MOD_ARTICLES_NEWS_XML_DESCRIPTION', 'Модуль Новостей выводит фиксированное количество материалов из конкретной категории или набора категорий.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_news.sys.ini'),
(1687, 'MOD_ARTICLES_NEWS_LAYOUT_DEFAULT', 'По умолчанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_news.sys.ini'),
(1688, 'MOD_ARTICLES_POPULAR', 'Материалы - Самые читаемые', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_popular.ini'),
(1689, 'MOD_POPULAR_FIELD_CATEGORY_DESC', 'Выделите категории, материалы из которых следует отображать. Если не выделить ни одной, будут показаны материалы из всех категорий.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_popular.ini'),
(1690, 'MOD_POPULAR_FIELD_COUNT_DESC', 'Количество отображаемых материалов (по умолчанию - 5)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_popular.ini'),
(1691, 'MOD_POPULAR_FIELD_COUNT_LABEL', 'Кол-во', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_popular.ini'),
(1692, 'MOD_POPULAR_FIELD_FEATURED_DESC', 'Показывать/Скрывать материалы, отмеченные, как ''избранные''', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_popular.ini'),
(1693, 'MOD_POPULAR_FIELD_FEATURED_LABEL', 'Избранные материалы', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_popular.ini'),
(1694, 'MOD_POPULAR_XML_DESCRIPTION', 'Этот модуль отображает список опубликованных материалов, которые были просмотрены чаще всех - определяется по количеству просмотров.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_popular.ini'),
(1695, 'MOD_POPULAR_FIELD_DATEFIELD_DESC', 'Выберите поле с датой для фильтрации.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_popular.ini'),
(1696, 'MOD_POPULAR_FIELD_DATEFIELD_LABEL', 'Поле с датой', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_popular.ini'),
(1697, 'MOD_POPULAR_FIELD_DATEFILTERING_DESC', 'Выберите тип фильтрации', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_popular.ini'),
(1698, 'MOD_POPULAR_FIELD_DATEFILTERING_LABEL', 'Фильтрация дат', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_popular.ini'),
(1699, 'MOD_POPULAR_FIELD_ENDDATE_DESC', 'Если выше выбран Диапазон дат необходимо указать Дату окончания', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_popular.ini'),
(1700, 'MOD_POPULAR_FIELD_ENDDATE_LABEL', 'Дата окончания', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_popular.ini'),
(1701, 'MOD_POPULAR_FIELD_STARTDATE_DESC', 'Если выше выбран Диапазон дат необходимо указать Дату начала', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_popular.ini'),
(1702, 'MOD_POPULAR_FIELD_STARTDATE_LABEL', 'Дата начала', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_popular.ini'),
(1703, 'MOD_POPULAR_FIELD_RELATIVEDATE_DESC', 'Если выше выбрано Относительная дата, укажите количество дней..Результаты будут отфильтрованы относительно текущей даты и введенного значения.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_popular.ini'),
(1704, 'MOD_POPULAR_FIELD_RELATIVEDATE_LABEL', 'Относительная дата', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_popular.ini'),
(1705, 'MOD_POPULAR_OPTION_CREATED_VALUE', 'Дата создания', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_popular.ini'),
(1706, 'MOD_POPULAR_OPTION_DATERANGE_VALUE', 'Диапазон дат', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_popular.ini'),
(1707, 'MOD_POPULAR_OPTION_MODIFIED_VALUE', 'Дата изменения', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_popular.ini'),
(1708, 'MOD_POPULAR_OPTION_OFF_VALUE', 'Нет', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_popular.ini'),
(1709, 'MOD_POPULAR_OPTION_RELATIVEDAY_VALUE', 'Относительная дата', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_popular.ini'),
(1710, 'MOD_POPULAR_OPTION_STARTPUBLISHING_VALUE', 'Дата начала публикации', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_popular.ini'),
(1711, 'MOD_ARTICLES_POPULAR', 'Материалы - Самые читаемые', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_popular.sys.ini'),
(1712, 'MOD_POPULAR_XML_DESCRIPTION', 'Этот модуль отображает список опубликованных материалов, которые были просмотрены чаще всех - определяется по количеству просмотров.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_popular.sys.ini'),
(1713, 'MOD_ARTICLES_POPULAR_LAYOUT_DEFAULT', 'По умолчанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_articles_popular.sys.ini'),
(1714, 'COM_BANNERS_NO_CLIENT', '- Нет клиента -', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_banners.ini'),
(1715, 'MOD_BANNERS', 'Баннеры', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_banners.ini'),
(1716, 'MOD_BANNERS_BANNER', 'Баннер', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_banners.ini'),
(1717, 'MOD_BANNERS_FIELD_BANNERCLIENT_DESC', 'Показывать баннеры только одного клиента.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_banners.ini'),
(1718, 'MOD_BANNERS_FIELD_BANNERCLIENT_LABEL', 'Клиент', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_banners.ini'),
(1719, 'MOD_BANNERS_FIELD_CACHETIME_DESC', 'Время, по истечении которого кэш модуля будет обновлён', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_banners.ini'),
(1720, 'MOD_BANNERS_FIELD_CACHETIME_LABEL', 'Время кэширования', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_banners.ini'),
(1721, 'MOD_BANNERS_FIELD_CATEGORY_DESC', 'Показывать баннеры только из одной категории.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_banners.ini'),
(1722, 'MOD_BANNERS_FIELD_COUNT_DESC', 'Количество баннеров для отображения (по умолчанию - 5)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_banners.ini'),
(1723, 'MOD_BANNERS_FIELD_COUNT_LABEL', 'Кол-во', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_banners.ini'),
(1724, 'MOD_BANNERS_FIELD_FOOTER_DESC', 'Текст или HTML-код, который будет выводиться после группы баннеров', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_banners.ini'),
(1725, 'MOD_BANNERS_FIELD_FOOTER_LABEL', 'Нижний колонтитул', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_banners.ini'),
(1726, 'MOD_BANNERS_FIELD_HEADER_DESC', 'Текст или HTML-код, который будет выводиться перед группой баннеров', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_banners.ini'),
(1727, 'MOD_BANNERS_FIELD_HEADER_LABEL', 'Текст заголовка', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_banners.ini'),
(1728, 'MOD_BANNERS_FIELD_RANDOMISE_DESC', 'Выводить баннеры в случайном порядке', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_banners.ini'),
(1729, 'MOD_BANNERS_FIELD_RANDOMISE_LABEL', 'Случайно', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_banners.ini'),
(1730, 'MOD_BANNERS_FIELD_TAG_DESC', 'Баннер выбирается посредством поиска баннерных тегов в ключевых словах текущего документа.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_banners.ini'),
(1731, 'MOD_BANNERS_FIELD_TAG_LABEL', 'Поиск по тегу', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_banners.ini'),
(1732, 'MOD_BANNERS_FIELD_TARGET_DESC', 'Окно, в котором откроется ссылка', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_banners.ini'),
(1733, 'MOD_BANNERS_FIELD_TARGET_LABEL', 'Где открывать ссылку', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_banners.ini'),
(1734, 'MOD_BANNERS_VALUE_STICKYORDERING', 'По порядку', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_banners.ini'),
(1735, 'MOD_BANNERS_VALUE_STICKYRANDOMISE', 'Случайным образом', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_banners.ini'),
(1736, 'MOD_BANNERS_XML_DESCRIPTION', 'Модуль отображает действующие баннеры, созданные в компоненте баннеров.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_banners.ini'),
(1737, 'MOD_BANNERS', 'Баннеры', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_banners.sys.ini'),
(1738, 'MOD_BANNERS_LAYOUT_DEFAULT', 'По умолчанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_banners.sys.ini'),
(1739, 'MOD_BANNERS_XML_DESCRIPTION', 'Модуль отображает действующие баннеры, созданные в компоненте баннеров.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_banners.sys.ini'),
(1740, 'MOD_BREADCRUMBS', 'Навигатор сайта', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_breadcrumbs.ini'),
(1741, 'MOD_BREADCRUMBS_FIELD_HOMETEXT_DESC', 'Название самого первого пункта (главной страницы) в строке навигатора сайта. Если оставить поле пустым, будет использовано значение по умолчанию из файла mod_breadcrumbs.ini', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_breadcrumbs.ini'),
(1742, 'MOD_BREADCRUMBS_FIELD_HOMETEXT_LABEL', 'Название главной страницы', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_breadcrumbs.ini'),
(1743, 'MOD_BREADCRUMBS_FIELD_SEPARATOR_DESC', 'Текстовый разделитель.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_breadcrumbs.ini'),
(1744, 'MOD_BREADCRUMBS_FIELD_SEPARATOR_LABEL', 'Разделитель текста', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_breadcrumbs.ini'),
(1745, 'MOD_BREADCRUMBS_FIELD_SHOWHERE_DESC', 'Показывать/Скрывать текст &quot;Вы здесь&quot; в строке пути', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_breadcrumbs.ini'),
(1746, 'MOD_BREADCRUMBS_FIELD_SHOWHERE_LABEL', 'Показывать надпись &quot;Вы здесь&quot;', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_breadcrumbs.ini'),
(1747, 'MOD_BREADCRUMBS_FIELD_SHOWHOME_DESC', 'Показывать/Скрывать элемент Главная в навигаторе', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_breadcrumbs.ini'),
(1748, 'MOD_BREADCRUMBS_FIELD_SHOWHOME_LABEL', 'Показывать главную', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_breadcrumbs.ini'),
(1749, 'MOD_BREADCRUMBS_FIELD_SHOWLAST_DESC', 'Показывать/Скрывать последний элемент в навигаторе', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_breadcrumbs.ini'),
(1750, 'MOD_BREADCRUMBS_FIELD_SHOWLAST_LABEL', 'Показывать последний', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_breadcrumbs.ini'),
(1751, 'MOD_BREADCRUMBS_HERE', 'Вы здесь: ', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_breadcrumbs.ini'),
(1752, 'MOD_BREADCRUMBS_HOME', 'Главная', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_breadcrumbs.ini'),
(1753, 'MOD_BREADCRUMBS_XML_DESCRIPTION', 'Этот модуль выводит путь к текущей странице в виде строки, аналогично пути к каталогу в файловом менеджере.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_breadcrumbs.ini'),
(1754, 'MOD_BREADCRUMBS', 'Навигатор сайта', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_breadcrumbs.sys.ini'),
(1755, 'MOD_BREADCRUMBS_XML_DESCRIPTION', 'Этот модуль выводит путь к текущей странице в виде строки, аналогично пути к каталогу в файловом менеджере.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_breadcrumbs.sys.ini'),
(1756, 'MOD_BREADCRUMBS_LAYOUT_DEFAULT', 'По умолчанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_breadcrumbs.sys.ini'),
(1757, 'MOD_CUSTOM', 'HTML-код', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_custom.ini'),
(1758, 'MOD_CUSTOM_FIELD_PREPARE_CONTENT_DESC', 'Разрешает/запрещает обработку содержимого модуля плагинами.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_custom.ini'),
(1759, 'MOD_CUSTOM_FIELD_PREPARE_CONTENT_LABEL', 'Обрабатывать плагинами', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_custom.ini'),
(1760, 'MOD_CUSTOM_FIELD_BACKGROUNDIMAGE_LABEL', 'Укажите фоновое изображение', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_custom.ini'),
(1761, 'MOD_CUSTOM_XML_DESCRIPTION', 'Модуль отображает на сайте фрагмент HTML-кода, набранного вручную или с помощью визуального HTML-редактора (WYSIWYG).', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_custom.ini'),
(1762, 'MOD_BACKGROUNDIMAGE_FIELD_LOGO_DESC', 'Если вы укажете изображение в этом поле, оно автоматически будет привязано как inline-элемент для окружающего DIV-элемента', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_custom.ini'),
(1763, 'MOD_CUSTOM', 'HTML-код', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_custom.sys.ini'),
(1764, 'MOD_CUSTOM_LAYOUT_DEFAULT', 'По умолчанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_custom.sys.ini'),
(1765, 'MOD_CUSTOM_XML_DESCRIPTION', 'Модуль отображает на сайте фрагмент HTML-кода, набранного вручную или с помощью визуального HTML-редактора (WYSIWYG).', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_custom.sys.ini'),
(1766, 'MOD_FEED', 'RSS-лента новостей', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_feed.ini'),
(1767, 'MOD_FEED_ERR_CACHE', 'Пожалуйста, установите права на запись в каталог кэша', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_feed.ini'),
(1768, 'MOD_FEED_ERR_NO_URL', 'Не указан URL ленты новостей.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_feed.ini'),
(1769, 'MOD_FEED_ERR_FEED_NOT_RETRIEVED', 'Лента не найдена', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_feed.ini'),
(1770, 'MOD_FEED_FIELD_DESCRIPTION_DESC', 'Показывать описание ленты новостей', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_feed.ini'),
(1771, 'MOD_FEED_FIELD_DESCRIPTION_LABEL', 'Описание ленты новостей', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_feed.ini'),
(1772, 'MOD_FEED_FIELD_IMAGE_DESC', 'Показывать изображение, связанное с лентой новостей', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_feed.ini'),
(1773, 'MOD_FEED_FIELD_IMAGE_LABEL', 'Иконка/логотип ленты', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_feed.ini'),
(1774, 'MOD_FEED_FIELD_ITEMDESCRIPTION_DESC', 'Отображать описание или вступительную часть отдельных элементов RSS', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_feed.ini'),
(1775, 'MOD_FEED_FIELD_ITEMDESCRIPTION_LABEL', 'Описание элемента', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_feed.ini'),
(1776, 'MOD_FEED_FIELD_ITEMS_DESC', 'Укажите количество материалов из RSS, которые будут отображаться', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_feed.ini'),
(1777, 'MOD_FEED_FIELD_ITEMS_LABEL', 'Количество материалов', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_feed.ini'),
(1778, 'MOD_FEED_FIELD_RSSTITLE_DESC', 'Отображать заголовок RSS-ленты', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_feed.ini'),
(1779, 'MOD_FEED_FIELD_RSSTITLE_LABEL', 'Заголовок ленты', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_feed.ini'),
(1780, 'MOD_FEED_FIELD_RSSURL_DESC', 'Введите URL ленты RSS/RDF', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_feed.ini'),
(1781, 'MOD_FEED_FIELD_RSSURL_LABEL', 'URL ленты новостей', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_feed.ini'),
(1782, 'MOD_FEED_FIELD_RTL_DESC', 'Отображать текст справа налево', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_feed.ini'),
(1783, 'MOD_FEED_FIELD_RTL_LABEL', 'Текст справа налево', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_feed.ini'),
(1784, 'MOD_FEED_FIELD_WORDCOUNT_DESC', 'Позволяет ограничить длину видимой части текста описания. При значении 0 будет отображаться весь текст', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_feed.ini'),
(1785, 'MOD_FEED_FIELD_WORDCOUNT_LABEL', 'Количество слов', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_feed.ini'),
(1786, 'MOD_FEED_XML_DESCRIPTION', 'Этот модуль позволяет показывать ленту новостей', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_feed.ini'),
(1787, 'MOD_FEED', 'RSS-лента новостей', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_feed.sys.ini'),
(1788, 'MOD_FEED_XML_DESCRIPTION', 'Этот модуль позволяет показывать ленту новостей', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_feed.sys.ini'),
(1789, 'MOD_FEED_LAYOUT_DEFAULT', 'По умолчанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_feed.sys.ini'),
(1790, 'COM_FINDER_FILTER_BRANCH_LABEL', 'Искать по %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.ini'),
(1791, 'COM_FINDER_FILTER_SELECT_ALL_LABEL', 'Искать всё', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.ini'),
(1792, 'COM_FINDER_ADVANCED_SEARCH', 'Расширенный поиск', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.ini'),
(1793, 'COM_FINDER_SELECT_SEARCH_FILTER', '- Без фильтра -', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.ini'),
(1794, 'MOD_FINDER', 'Умный поиск', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.ini'),
(1795, 'MOD_FINDER_CONFIG_OPTION_BOTTOM', 'Ниже', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.ini'),
(1796, 'MOD_FINDER_CONFIG_OPTION_TOP', 'Выше', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.ini');
INSERT INTO `yawnc_overrider` (`id`, `constant`, `string`, `file`) VALUES
(1797, 'MOD_FINDER_FIELDSET_ADVANCED_ALT_DESCRIPTION', 'Альтернативный заголовок поля поиска.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.ini'),
(1798, 'MOD_FINDER_FIELDSET_ADVANCED_ALT_LABEL', 'Альтернативный заголовок', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.ini'),
(1799, 'MOD_FINDER_FIELDSET_ADVANCED_BUTTON_POS_DESCRIPTION', 'Положение кнопки относительно поля поиска.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.ini'),
(1800, 'MOD_FINDER_FIELDSET_ADVANCED_BUTTON_POS_LABEL', 'Позиция кнопки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.ini'),
(1801, 'MOD_FINDER_FIELDSET_ADVANCED_FIELD_SIZE_DESCRIPTION', 'Ширина поля в символах.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.ini'),
(1802, 'MOD_FINDER_FIELDSET_ADVANCED_FIELD_SIZE_LABEL', 'Размер поля поиска', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.ini'),
(1803, 'MOD_FINDER_FIELDSET_ADVANCED_LABEL_POS_DESCRIPTION', 'Положение заголовка относительно поля поиска.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.ini'),
(1804, 'MOD_FINDER_FIELDSET_ADVANCED_LABEL_POS_LABEL', 'Позиция заголовка', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.ini'),
(1805, 'MOD_FINDER_FIELDSET_ADVANCED_SETITEMID_DESCRIPTION', 'Устанавливает значение параметра Itemid посредством выбора пункта меню, который будет использован в результатах поиска, если в меню нет ссылки на компонент поиска (com_finder). Если вы не знаете, что это такое - скорее всего вам это не требуется.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.ini'),
(1806, 'MOD_FINDER_FIELDSET_ADVANCED_SETITEMID_LABEL', 'ItemID', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.ini'),
(1807, 'MOD_FINDER_FIELDSET_ADVANCED_SHOW_BUTTON_DESCRIPTION', 'Показывать кнопку в форме поиска?', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.ini'),
(1808, 'MOD_FINDER_FIELDSET_ADVANCED_SHOW_BUTTON_LABEL', 'Кнопка поиска', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.ini'),
(1809, 'MOD_FINDER_FIELDSET_ADVANCED_SHOW_LABEL_DESCRIPTION', 'Показывать заголовок в форме поиска?', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.ini'),
(1810, 'MOD_FINDER_FIELDSET_ADVANCED_SHOW_LABEL_LABEL', 'Заголовок поля поиска', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.ini'),
(1811, 'MOD_FINDER_FIELDSET_BASIC_AUTOSUGGEST_DESCRIPTION', 'Показывать подсказки автоматического поиска?', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.ini'),
(1812, 'MOD_FINDER_FIELDSET_BASIC_AUTOSUGGEST_LABEL', 'Автоматические подсказки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.ini'),
(1813, 'MOD_FINDER_FIELDSET_BASIC_SEARCHFILTER_DESCRIPTION', 'Установка фильтра ограничит область поиска через данный модуль.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.ini'),
(1814, 'MOD_FINDER_FIELDSET_BASIC_SEARCHFILTER_LABEL', 'Фильтр поиска', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.ini'),
(1815, 'MOD_FINDER_FIELDSET_BASIC_SHOW_ADVANCED_DESCRIPTION', 'Показывать навигационные элементы расширенного поиска?<br /><br />Если выбрать значение <strong>Ссылка на компонент</strong>, будет показана ссылка на отдельную страницу компонента <em>Умный Поиск</em>.<br /><br />Если выбрать значение <strong>Показать</strong>, инструменты расширенного поиска будут отображены на этой же странице.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.ini'),
(1816, 'MOD_FINDER_FIELDSET_BASIC_SHOW_ADVANCED_LABEL', 'Расширенный поиск', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.ini'),
(1817, 'MOD_FINDER_FIELDSET_BASIC_SHOW_ADVANCED_OPTION_LINK', 'Ссылка на компонент', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.ini'),
(1818, 'MOD_FINDER_FIELD_OPENSEARCH_DESCRIPTION', 'Если эта настройка включена, некоторые браузеры смогут поддерживать поиск на вашем сайте собственными специальными инструментами.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.ini'),
(1819, 'MOD_FINDER_FIELD_OPENSEARCH_LABEL', 'OpenSearch - автопоиск', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.ini'),
(1820, 'MOD_FINDER_FIELD_OPENSEARCH_TEXT_DESCRIPTION', 'Текст, показываемый в браузерах, при добавлении вашего сайта, как провайдера поиска (не все браузеры поддерживают такую возможность).', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.ini'),
(1821, 'MOD_FINDER_FIELD_OPENSEARCH_TEXT_LABEL', 'OpenSearch - заголовок', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.ini'),
(1822, 'MOD_FINDER_SEARCHBUTTON_TEXT', 'Поиск', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.ini'),
(1823, 'MOD_FINDER_SEARCH_BUTTON', 'Искать', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.ini'),
(1824, 'MOD_FINDER_SEARCH_VALUE', 'Текст для поиска...', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.ini'),
(1825, 'MOD_FINDER_SELECT_MENU_ITEMID', 'Выберите пункт меню', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.ini'),
(1826, 'MOD_FINDER_XML_DESCRIPTION', 'Модуль - Умный Поиск.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.ini'),
(1827, 'MOD_FINDER', 'Умный поиск', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.sys.ini'),
(1828, 'MOD_FINDER_XML_DESCRIPTION', 'Это - модуль системы Умный Поиск.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.sys.ini'),
(1829, 'MOD_FINDER_LAYOUT_DEFAULT', 'По умолчанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_finder.sys.ini'),
(1830, 'MOD_FOOTER', 'Нижний колонтитул (footer)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_footer.ini'),
(1831, 'MOD_FOOTER_LINE1', '&#169; %date% %sitename%. Все права защищены.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_footer.ini'),
(1832, 'MOD_FOOTER_LINE2', '<a href="http://www.joomla.org">Joomla!</a> - бесплатное программное обеспечение, распространяемое по лицензии <a href="http://www.gnu.org/licenses/gpl-2.0.html">GNU General Public License.</a>', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_footer.ini'),
(1833, 'MOD_FOOTER_XML_DESCRIPTION', 'Этот модуль выводит информацию об авторских правах сайта и используемом программном обеспечении.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_footer.ini'),
(1834, 'MOD_FOOTER', 'Нижний колонтитул (footer)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_footer.sys.ini'),
(1835, 'MOD_FOOTER_XML_DESCRIPTION', 'Этот модуль выводит информацию об авторских правах сайта и используемом программном обеспечении.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_footer.sys.ini'),
(1836, 'MOD_FOOTER_LAYOUT_DEFAULT', 'По умолчанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_footer.sys.ini'),
(1837, 'MOD_LANGUAGES', 'Переключение языков', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_languages.ini'),
(1838, 'MOD_LANGUAGES_FIELD_ACTIVE_DESC', 'Отображать или нет активный язык. Если включено отображение активного языка, CSS-класс ''lang-active'' будет добавлен элементу, соответствующему активному языку.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_languages.ini'),
(1839, 'MOD_LANGUAGES_FIELD_ACTIVE_LABEL', 'Активный язык', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_languages.ini'),
(1840, 'MOD_LANGUAGES_FIELD_CACHING_DESC', 'Выберите следует ли кэшировать содержимое этого модуля.<br />Если вы используете Связи элементов, необходимо выбрать значение ''Не кэшировать''.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_languages.ini'),
(1841, 'MOD_LANGUAGES_FIELD_DROPDOWN_DESC', 'Если ''Да'', параметры, приведённые ниже, не будут учитываться. В списке будут выводиться названия языков контента.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_languages.ini'),
(1842, 'MOD_LANGUAGES_FIELD_DROPDOWN_LABEL', 'Включить выпадающий список', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_languages.ini'),
(1843, 'MOD_LANGUAGES_FIELD_FOOTER_DESC', 'Текст или HTML-код, который отображается в нижней части модуля переключения языка', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_languages.ini'),
(1844, 'MOD_LANGUAGES_FIELD_FOOTER_LABEL', 'Заключительный текст', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_languages.ini'),
(1845, 'MOD_LANGUAGES_FIELD_FULL_NAME_DESC', 'Если ''Да'' и параметр ''Включить изображения флагов'' выключен, будут показаны полные названия языков контента. Если ''Нет'', будут показаны аббревиатуры языков, принятые в системе коротких ссылок (SEF), в верхнем регистре. Например: EN для английского, FR для французского, RU для русского.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_languages.ini'),
(1846, 'MOD_LANGUAGES_FIELD_FULL_NAME_LABEL', 'Полные названия языков', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_languages.ini'),
(1847, 'MOD_LANGUAGES_FIELD_HEADER_DESC', 'Текст или HTML-код, который отображается в верхней части модуля переключения языка', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_languages.ini'),
(1848, 'MOD_LANGUAGES_FIELD_HEADER_LABEL', 'Начальный текст', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_languages.ini'),
(1849, 'MOD_LANGUAGES_FIELD_INLINE_DESC', 'По умолчанию список языков отображается в строку, при необходимости можно включить отображение столбцом (один под другим).', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_languages.ini'),
(1850, 'MOD_LANGUAGES_FIELD_INLINE_LABEL', 'Показывать строкой', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_languages.ini'),
(1851, 'MOD_LANGUAGES_FIELD_MODULE_LAYOUT_DESC', 'Использовать другой макет из имеющихся в комплекте модуля или переопределить в шаблоне по умолчанию. При значении ''По умолчанию'' будут показаны флаги языков.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_languages.ini'),
(1852, 'MOD_LANGUAGES_FIELD_USEIMAGE_DESC', 'Включить изображения флагов стран', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_languages.ini'),
(1853, 'MOD_LANGUAGES_FIELD_USEIMAGE_LABEL', 'Включить изображения флагов', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_languages.ini'),
(1854, 'MOD_LANGUAGES_OPTION_DEFAULT_LANGUAGE', 'По умолчанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_languages.ini'),
(1855, 'MOD_LANGUAGES_SPACERDROP_LABEL', '<u>Если включен параметр ''Включить выпадающий список'', <br />указанные ниже настройки будут игнорироваться</u>', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_languages.ini'),
(1856, 'MOD_LANGUAGES_SPACERNAME_LABEL', '<u>Если включен параметр ''Включить изображения флагов'', <br />указанные ниже настройки будут игнорироваться</u>', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_languages.ini'),
(1857, 'MOD_LANGUAGES_XML_DESCRIPTION', 'Этот модуль отображает список доступных Языков контента (их можно найти на странице ''Языки контента'' в Менеджере языков), между которыми можно переключаться при использовании на сайте системы многоязычности.<br />-- Плагин <strong>Система - Фильтр языка</strong> должен быть опубликован.<br />-- При переключении языков, если пункт меню данной страницы не привязан к другому пункту меню, модуль перенаправит посетителя на Главную страницу сайта, соответствующую данному языку.<br />В противном случае, если включен соответствующий параметр в плагине «Система - Фильтр языка», пользователь будет перенаправлен на пункт меню, соответствующий данной странице на выбранном им языке. Соответственно будут перестроены и прочие элементы навигации. <br />Обратите внимание, что если плагин <strong>Система - Фильтр языка</strong> снят с публикации, может возникнуть непредвиденная ситуация, которая приведёт к ошибке в системе.<br /><strong>Метод решения проблемы:</strong><br />1. Перейдите в Менеджер языков, на закладку Языки контента, и убедитесь, что языки, для которых вы хотите создать на сайте связанные страницы, опубликованы и каждому из них назначен Код языка для использования в URL. Так же убедитесь, что указан префикс файла изображения, которое будет выводиться в модуле выбора языка.<br />2. Укажите страницу, которая будет являться Главной для каждого опубликованного языка контента. <br />3. После этого вы можете привязать к языку любой Материал, Категорию, Ленту новостей и прочее содержимое Joomla!<br /> 4. Убедитесь, что модуль и плагин опубликованы. <br />5. При использовании привязки пунктов меню к языку проверяйте, что модуль опубликован на всех привязываемых страницах. <br />6. Порядок показа полных названий языков или изображений флагов определяется в Менеджере языков, на странице Языки контента.<br /><br />Если этот модуль опубликован, рекомендуется опубликовать модуль панели управления «Мультиязычность».', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_languages.ini'),
(1858, 'MOD_LANGUAGES', 'Переключение языков', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_languages.sys.ini'),
(1859, 'MOD_LANGUAGES_LAYOUT_DEFAULT', 'По умолчанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_languages.sys.ini'),
(1860, 'MOD_LANGUAGES_XML_DESCRIPTION', 'Этот модуль отображает список доступных Языков контента (их можно найти на странице ''Языки контента'' в Менеджере языков), между которыми можно переключаться при использовании на сайте системы многоязычности.<br />-- Плагин ''Система - Фильтр языков'' должен быть опубликован.<br />-- При переключении языков, если пункт меню данной страницы не привязан к другому пункту меню, модуль перенаправит посетителя на Главную страницу сайта, соответствующую данному языку.<br />В противном случае, если включен соответствующий параметр в плагине ‘Система - Фильтр языка’, пользователь будет перенаправлен на пункт меню, соответствующий данной странице на выбранном им языке. Соответственно будут перестроены и прочие элементы навигации. <br />Обратите внимание, что, если плагин <strong>‘Система - Фильтр языка’</strong> снят с публикации, может возникнуть непредвиденная ситуация, которая приведёт к ошибке в системе.<br /><strong>Метод решения проблемы:</strong><br />1. Перейдите в Менеджер Языков, на закладку Содержимое, и убедитесь, что языки, для которых вы хотите создать на сайте связанные страницы, опубликованы и каждому из них назначен Код Языка для использования в URL. Так же убедитесь, что указан префикс файла изображения, которое будет выводиться в модуле выбора языка.<br />2. Укажите страницу, которая будет являться Главной для каждого опубликованного языка содержимого. <br />3. После этого вы можете привязать к языку любой Материал, Категорию, Ленту новостей и прочее содержимое Joomla!<br /> 4. Убедитесь, что модуль и плагин опубликованы. <br />5. При использовании привязки пунктов меню к языку проверяйте, что модуль опубликован на всех привязываемых страницах. <br />6. Порядок показа полных названий языков или изображений флагов определяется в Менеджере Языков, на странице Содержимое.<br ><br >Если этот модуль опубликован, рекомендуется опубликовать модуль панели управления «Мультиязычность».', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_languages.sys.ini'),
(1861, 'MOD_LOGIN', 'Вход на сайт', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_login.ini'),
(1862, 'MOD_LOGIN_FIELD_GREETING_DESC', 'Показывать/Скрывать текст приветствия', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_login.ini'),
(1863, 'MOD_LOGIN_FIELD_GREETING_LABEL', 'Показывать приветствие', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_login.ini'),
(1864, 'MOD_LOGIN_FIELD_LOGIN_REDIRECTURL_DESC', 'Укажите страницу, на которую следует перенаправить пользователя после выполнения входа в систему. Выберите нужную из выпадающего списка. Выбор значения "По умолчанию" вернёт пользователя на ту же страницу.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_login.ini'),
(1865, 'MOD_LOGIN_FIELD_LOGIN_REDIRECTURL_LABEL', 'Перенаправление при входе', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_login.ini'),
(1866, 'MOD_LOGIN_FIELD_LOGOUT_REDIRECTURL_DESC', 'Укажите страницу, на которую следует перенаправить пользователя после выполнения выхода из системы. Выберите нужную из выпадающего списка. Выбор значения "По умолчанию" вернёт пользователя на ту же страницу.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_login.ini'),
(1867, 'MOD_LOGIN_FIELD_LOGOUT_REDIRECTURL_LABEL', 'Перенаправление при выходе', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_login.ini'),
(1868, 'MOD_LOGIN_FIELD_NAME_DESC', 'Показывает имя или логин пользователя после входа в систему', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_login.ini'),
(1869, 'MOD_LOGIN_FIELD_NAME_LABEL', 'Показывать имя/логин', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_login.ini'),
(1870, 'MOD_LOGIN_FIELD_POST_TEXT_DESC', 'HTML-текст, отображаемый под формой авторизации.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_login.ini'),
(1871, 'MOD_LOGIN_FIELD_POST_TEXT_LABEL', 'Заключительный текст', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_login.ini'),
(1872, 'MOD_LOGIN_FIELD_PRE_TEXT_DESC', 'HTML-текст, отображаемый над формой авторизации.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_login.ini'),
(1873, 'MOD_LOGIN_FIELD_PRE_TEXT_LABEL', 'Начальный текст', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_login.ini'),
(1874, 'MOD_LOGIN_FIELD_USESECURE_DESC', 'Отправлять учётные данные в шифрованном виде (необходима поддержка SSL). Не включайте эту опцию, если Joomla! не настроена на использование протокола https.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_login.ini'),
(1875, 'MOD_LOGIN_FIELD_USESECURE_LABEL', 'Защищённая форма регистрации', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_login.ini'),
(1876, 'MOD_LOGIN_FIELD_USETEXT_DESC', 'Определяет режим отображения наименования полей в форме &mdash; в виде иконок или в виде текста. По умолчанию &mdash; иконки.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_login.ini'),
(1877, 'MOD_LOGIN_FIELD_USETEXT_LABEL', 'Название полей формы', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_login.ini'),
(1878, 'MOD_LOGIN_FORGOT_YOUR_PASSWORD', 'Забыли пароль?', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_login.ini'),
(1879, 'MOD_LOGIN_FORGOT_YOUR_USERNAME', 'Забыли логин?', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_login.ini'),
(1880, 'MOD_LOGIN_HINAME', 'Здравствуйте, %s', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_login.ini'),
(1881, 'MOD_LOGIN_REGISTER', 'Регистрация', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_login.ini'),
(1882, 'MOD_LOGIN_REMEMBER_ME', 'Запомнить меня', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_login.ini'),
(1883, 'MOD_LOGIN_VALUE_ICONS', 'Иконки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_login.ini'),
(1884, 'MOD_LOGIN_VALUE_NAME', 'Имя', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_login.ini'),
(1885, 'MOD_LOGIN_VALUE_TEXT', 'Текст', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_login.ini'),
(1886, 'MOD_LOGIN_VALUE_USERNAME', 'Логин', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_login.ini'),
(1887, 'MOD_LOGIN_XML_DESCRIPTION', 'Этот модуль отображает форму для ввода логина и пароля пользователя при входе в систему. Также он отображает ссылку на страницу восстановления забытого пароля. Если включена система самостоятельной регистрации пользователей (на странице настроек Менеджера пользователей), будет показана так же ссылка на страницу регистрации.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_login.ini'),
(1888, 'MOD_LOGIN', 'Вход на сайт', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_login.sys.ini'),
(1889, 'MOD_LOGIN_XML_DESCRIPTION', 'Этот модуль отображает форму для ввода логина и пароля пользователя при входе в систему. Также он отображает ссылку на страницу восстановления забытого пароля. Если включена система самостоятельной регистрации пользователей (на странице настроек Менеджера пользователей), будет показана так же ссылка на страницу регистрации.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_login.sys.ini'),
(1890, 'MOD_LOGIN_LAYOUT_DEFAULT', 'По умолчанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_login.sys.ini'),
(1891, 'MOD_MENU', 'Меню', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_menu.ini'),
(1892, 'MOD_MENU_FIELD_ACTIVE_DESC', 'Выберите пункт меню, который будет использован в качестве базового для данного меню. Вы должны установить Начальный уровень таким же (или выше) как у базового пункта меню. Это позволит модулю отображаться на всех назначенных страницах. Если выбрано значение «Текущий», то активный пункт меню будет использован в качестве базового. Это позволит модулю отображаться только в том случае, если активен родительский пункт меню.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_menu.ini'),
(1893, 'MOD_MENU_FIELD_ACTIVE_LABEL', 'Базовый пункт меню', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_menu.ini'),
(1894, 'MOD_MENU_FIELD_ALLCHILDREN_DESC', 'Раскрывать меню и показывать его подменю.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_menu.ini'),
(1895, 'MOD_MENU_FIELD_ALLCHILDREN_LABEL', 'Показывать подпункты меню', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_menu.ini'),
(1896, 'MOD_MENU_FIELD_CLASS_DESC', 'Суффикс добавляется к имени CSS-класса меню по умолчанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_menu.ini'),
(1897, 'MOD_MENU_FIELD_CLASS_LABEL', 'Суффикс класса меню', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_menu.ini'),
(1898, 'MOD_MENU_FIELD_ENDLEVEL_DESC', 'Уровень, на котором будет прекращено построение дерева пунктов меню. Если выбрать ''Все'', будут показаны все уровни, в зависимости от значения параметра ''Показывать подпункты меню''.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_menu.ini'),
(1899, 'MOD_MENU_FIELD_ENDLEVEL_LABEL', 'Последний уровень', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_menu.ini'),
(1900, 'MOD_MENU_FIELD_MENUTYPE_DESC', 'Выберите меню из списка', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_menu.ini'),
(1901, 'MOD_MENU_FIELD_MENUTYPE_LABEL', 'Выбор меню', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_menu.ini'),
(1902, 'MOD_MENU_FIELD_STARTLEVEL_DESC', 'Уровень, с которого будет начато построение дерева меню. Если установить номер уровня начала и номера окончания одинаковыми, а так же выбрать значение ''Да'' для параметра ''Показывать подпункты меню'', будет отображаться только один уровень.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_menu.ini'),
(1903, 'MOD_MENU_FIELD_STARTLEVEL_LABEL', 'Начальный уровень', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_menu.ini'),
(1904, 'MOD_MENU_FIELD_TAG_ID_DESC', 'Атрибут ID, назначаемый для корневого тега UL меню (необязательно)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_menu.ini'),
(1905, 'MOD_MENU_FIELD_TAG_ID_LABEL', 'ID Меню', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_menu.ini'),
(1906, 'MOD_MENU_FIELD_TARGET_DESC', 'Параметры JavaScript для определения позиции всплывающего окна. Например: top=50,left=50,width=200,height=300', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_menu.ini'),
(1907, 'MOD_MENU_FIELD_TARGET_LABEL', 'Позиция назначения', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_menu.ini'),
(1908, 'MOD_MENU_XML_DESCRIPTION', 'Этот модуль отображает меню на страницах сайта.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_menu.ini'),
(1909, 'MOD_MENU', 'Меню', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_menu.sys.ini'),
(1910, 'MOD_MENU_XML_DESCRIPTION', 'Этот модуль отображает меню на страницах сайта.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_menu.sys.ini'),
(1911, 'MOD_MENU_LAYOUT_DEFAULT', 'По умолчанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_menu.sys.ini'),
(1912, 'MOD_RANDOM_IMAGE', 'Произвольное изображение', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_random_image.ini'),
(1913, 'MOD_RANDOM_IMAGE_FIELD_FOLDER_DESC', 'Путь к каталогу изображений, относительно URL-адреса сайта (например, images).', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_random_image.ini'),
(1914, 'MOD_RANDOM_IMAGE_FIELD_FOLDER_LABEL', 'Каталог с изображениями', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_random_image.ini'),
(1915, 'MOD_RANDOM_IMAGE_FIELD_HEIGHT_DESC', 'Показывать все изображения одинаковой высоты (высота задаётся в пикселях).', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_random_image.ini'),
(1916, 'MOD_RANDOM_IMAGE_FIELD_HEIGHT_LABEL', 'Высота', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_random_image.ini'),
(1917, 'MOD_RANDOM_IMAGE_FIELD_LINK_DESC', 'Укажите URL, на который будет перенаправлен пользователь, при нажатии на изображение.<br/>Например: http://joomlaportal.ru', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_random_image.ini'),
(1918, 'MOD_RANDOM_IMAGE_FIELD_LINK_LABEL', 'Ссылка', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_random_image.ini'),
(1919, 'MOD_RANDOM_IMAGE_FIELD_TYPE_DESC', 'Тип картинки PNG/GIF/JPG и т.д. (по умолчанию JPG)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_random_image.ini'),
(1920, 'MOD_RANDOM_IMAGE_FIELD_TYPE_LABEL', 'Тип изображения', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_random_image.ini'),
(1921, 'MOD_RANDOM_IMAGE_FIELD_WIDTH_DESC', 'Показывать все изображения одинаковой ширины (ширина задаётся в пикселях).', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_random_image.ini'),
(1922, 'MOD_RANDOM_IMAGE_FIELD_WIDTH_LABEL', 'Ширина', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_random_image.ini'),
(1923, 'MOD_RANDOM_IMAGE_NO_IMAGES', 'Нет изображений', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_random_image.ini'),
(1924, 'MOD_RANDOM_IMAGE_XML_DESCRIPTION', 'Этот модуль выводит случайно выбранное изображение из заданного каталога.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_random_image.ini'),
(1925, 'MOD_RANDOM_IMAGE', 'Случайное изображение', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_random_image.sys.ini'),
(1926, 'MOD_RANDOM_IMAGE_XML_DESCRIPTION', 'Этот модуль выводит случайно выбранное изображение из заданного каталога.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_random_image.sys.ini'),
(1927, 'MOD_RANDOM_IMAGE_LAYOUT_DEFAULT', 'По умолчанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_random_image.sys.ini'),
(1928, 'MOD_RELATED_FIELD_MAX_DESC', 'Количество отображаемых материалов (по умолчанию - 5)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_related_items.ini'),
(1929, 'MOD_RELATED_FIELD_MAX_LABEL', 'Кол-во материалов', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_related_items.ini'),
(1930, 'MOD_RELATED_FIELD_SHOWDATE_DESC', 'Показывать/Скрывать дату', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_related_items.ini'),
(1931, 'MOD_RELATED_FIELD_SHOWDATE_LABEL', 'Показывать дату', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_related_items.ini'),
(1932, 'MOD_RELATED_ITEMS', 'Материалы - Связанные материалы', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_related_items.ini'),
(1933, 'MOD_RELATED_XML_DESCRIPTION', 'Этот модуль отображает список ссылок на материалы, которые связаны с тем, что в данный момент отображается в центральной области страницы. Связи определяются по ключевым словам, введённым в параметрах материала.<br />Все ключевые слова данной статьи ищутся в списках ключевых слов других опубликованных материалов. Например, у вас есть материалы "Разведение попугаев" и другой материал "Руководство по разведению чёрных какаду". Если вы включите ключевое слово "попугай" в оба эти материала, модуль ''Связанные материалы'' будет считать их связанными и покажет ссылку на материал "Разведение попугаев" при просмотре материала "Руководство по разведению чёрных какаду" и так далее.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_related_items.ini'),
(1934, 'MOD_RELATED_ITEMS', 'Материалы - Связанные материалы', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_related_items.sys.ini'),
(1935, 'MOD_RELATED_XML_DESCRIPTION', 'Этот модуль отображает список ссылок на материалы, которые связаны с тем, что в данный момент отображается в центральной области страницы. Связи определяются по ключевым словам, введённым в параметрах материала.<br />Все ключевые слова данной статьи ищутся в списках ключевых слов других опубликованных материалов. Например, у вас есть материалы "Разведение попугаев" и другой материал "Руководство по разведению чёрных какаду". Если вы включите ключевое слово "попугай" в оба эти материала, модуль ''Связанные материалы'' будет считать их связанными и покажет ссылку на материал "Разведение попугаев" при просмотре материала "Руководство по разведению чёрных какаду" и так далее.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_related_items.sys.ini'),
(1936, 'MOD_RELATED_ITEMS_LAYOUT_DEFAULT', 'По умолчанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_related_items.sys.ini'),
(1937, 'MOD_SEARCH', 'Поиск', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_search.ini'),
(1938, 'MOD_SEARCH_FIELD_BOXWIDTH_DESC', 'Размер поля поиска (в символах)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_search.ini'),
(1939, 'MOD_SEARCH_FIELD_BOXWIDTH_LABEL', 'Ширина поля', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_search.ini'),
(1940, 'MOD_SEARCH_FIELD_BUTTON_DESC', 'Показывать кнопку поиска', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_search.ini'),
(1941, 'MOD_SEARCH_FIELD_BUTTON_LABEL', 'Кнопка поиска', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_search.ini'),
(1942, 'MOD_SEARCH_FIELD_BUTTONPOS_DESC', 'Положение кнопки по отношению к полю поиска.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_search.ini'),
(1943, 'MOD_SEARCH_FIELD_BUTTONPOS_LABEL', 'Позиция кнопки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_search.ini'),
(1944, 'MOD_SEARCH_FIELD_BUTTONTEXT_DESC', 'Текст, который будет отображён на кнопке поиска. Если оставить пустым, будет использоваться значение переменной ''searchbutton'' из языкового файла.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_search.ini'),
(1945, 'MOD_SEARCH_FIELD_BUTTONTEXT_LABEL', 'Текст на кнопке', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_search.ini'),
(1946, 'MOD_SEARCH_FIELD_IMAGEBUTTON_DESC', 'Использовать изображения в качестве кнопки. Файл изображения должен иметь имя ''searchButton.gif'' и должен быть расположен в каталоге: templates/*имя вашего шаблона*/images/', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_search.ini'),
(1947, 'MOD_SEARCH_FIELD_IMAGEBUTTON_LABEL', 'Изображение кнопки поиска', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_search.ini'),
(1948, 'MOD_SEARCH_FIELD_SETITEMID_DESC', 'Устанавливает значение параметра Itemid, который будет использован в результатах поиска, если в меню нет ссылки на компонент поиска (com_search).', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_search.ini'),
(1949, 'MOD_SEARCH_FIELD_SETITEMID_LABEL', 'Itemid', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_search.ini'),
(1950, 'MOD_SEARCH_FIELD_LABEL_TEXT_DESC', 'Текст, который будет выведен в качестве заголовка, рядом с полем поиска. Если оставить пустым, будет использоваться значение переменной ''label'' из языкового файла.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_search.ini'),
(1951, 'MOD_SEARCH_FIELD_LABEL_TEXT_LABEL', 'Заголовок поля', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_search.ini'),
(1952, 'MOD_SEARCH_FIELD_OPENSEARCH_LABEL', 'OpenSearch - автопоиск', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_search.ini'),
(1953, 'MOD_SEARCH_FIELD_OPENSEARCH_TEXT_LABEL', 'OpenSearch - заголовок', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_search.ini'),
(1954, 'MOD_SEARCH_FIELD_OPENSEARCH_TEXT_DESC', 'Текст, который будет отображаться в браузерах, при добавлении вашего сайта, как Провайдера поиска (не все браузеры поддерживают такую возможность).', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_search.ini'),
(1955, 'MOD_SEARCH_FIELD_OPENSEARCH_DESC', 'Если данный параметр включен, некоторые браузеры смогут выполнять поиск по вашему сайту.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_search.ini'),
(1956, 'MOD_SEARCH_FIELD_TEXT_DESC', 'Текст, который будет выведен в поле поиска. Если оставить пустым, будет использоваться значение переменной ''searchbox'' из языкового файла.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_search.ini'),
(1957, 'MOD_SEARCH_FIELD_TEXT_LABEL', 'Текст в поле', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_search.ini'),
(1958, 'MOD_SEARCH_FIELD_VALUE_BOTTOM', 'Снизу', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_search.ini'),
(1959, 'MOD_SEARCH_FIELD_VALUE_LEFT', 'Слева', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_search.ini'),
(1960, 'MOD_SEARCH_FIELD_VALUE_RIGHT', 'Справа', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_search.ini'),
(1961, 'MOD_SEARCH_FIELD_VALUE_TOP', 'Сверху', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_search.ini'),
(1962, 'MOD_SEARCH_LABEL_TEXT', 'Искать...', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_search.ini'),
(1963, 'MOD_SEARCH_SEARCHBOX_TEXT', 'Поиск...', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_search.ini'),
(1964, 'MOD_SEARCH_SEARCHBUTTON_TEXT', 'Искать', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_search.ini'),
(1965, 'MOD_SEARCH_SELECT_MENU_ITEMID', 'Выберите пункт меню', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_search.ini'),
(1966, 'MOD_SEARCH_XML_DESCRIPTION', 'Этот модуль отображает форму поиска.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_search.ini'),
(1967, 'MOD_SEARCH', 'Поиск', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_search.sys.ini'),
(1968, 'MOD_SEARCH_XML_DESCRIPTION', 'Этот модуль отображает форму поиска.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_search.sys.ini'),
(1969, 'MOD_SEARCH_LAYOUT_DEFAULT', 'По умолчанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_search.sys.ini'),
(1970, 'MOD_STATS', 'Статистика', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_stats.ini'),
(1971, 'MOD_STATS_ARTICLES', 'Материалы', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_stats.ini'),
(1972, 'MOD_STATS_ARTICLES_VIEW_HITS', 'Количество просмотров материалов', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_stats.ini'),
(1973, 'MOD_STATS_CACHING', 'Кэширование', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_stats.ini'),
(1974, 'MOD_STATS_FIELD_COUNTER_DESC', 'Показывать счётчик просмотров', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_stats.ini'),
(1975, 'MOD_STATS_FIELD_COUNTER_LABEL', 'Счётчик просмотров', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_stats.ini'),
(1976, 'MOD_STATS_FIELD_INCREASECOUNTER_DESC', 'Введите кол-во просмотров для увеличения показаний счётчика.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_stats.ini'),
(1977, 'MOD_STATS_FIELD_INCREASECOUNTER_LABEL', 'Увеличить показания счётчика', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_stats.ini'),
(1978, 'MOD_STATS_FIELD_SERVERINFO_DESC', 'Отображать информацию о веб-сервере', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_stats.ini'),
(1979, 'MOD_STATS_FIELD_SERVERINFO_LABEL', 'Информация о сервере', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_stats.ini'),
(1980, 'MOD_STATS_FIELD_SITEINFO_DESC', 'Отображать информацию о сайте', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_stats.ini'),
(1981, 'MOD_STATS_FIELD_SITEINFO_LABEL', 'Информация о сайте', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_stats.ini'),
(1982, 'MOD_STATS_GZIP', 'GZip', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_stats.ini'),
(1983, 'MOD_STATS_MYSQL', 'MySQL', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_stats.ini'),
(1984, 'MOD_STATS_OS', 'ОС', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_stats.ini'),
(1985, 'MOD_STATS_PHP', 'PHP', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_stats.ini'),
(1986, 'MOD_STATS_TIME', 'Время', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_stats.ini'),
(1987, 'MOD_STATS_USERS', 'Посетители', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_stats.ini'),
(1988, 'MOD_STATS_WEBLINKS', 'Ссылки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_stats.ini'),
(1989, 'MOD_STATS_XML_DESCRIPTION', 'Модуль статистики отображает информацию о сервере вместе со статистикой о пользователях сайта, количестве статей и ссылок в БД.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_stats.ini'),
(1990, 'MOD_STATS', 'Статистика', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_stats.sys.ini'),
(1991, 'MOD_STATS_XML_DESCRIPTION', 'Модуль статистики отображает информацию о сервере вместе со статистикой о пользователях сайта, количестве статей и ссылок в БД.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_stats.sys.ini');
INSERT INTO `yawnc_overrider` (`id`, `constant`, `string`, `file`) VALUES
(1992, 'MOD_STATS_LAYOUT_DEFAULT', 'По умолчанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_stats.sys.ini'),
(1993, 'MOD_SYNDICATE', 'RSS-ленты', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_syndicate.ini'),
(1994, 'MOD_SYNDICATE_DEFAULT_FEED_ENTRIES', 'RSS-лента', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_syndicate.ini'),
(1995, 'MOD_SYNDICATE_FIELD_DISPLAYTEXT_DESC', 'Если выбрано ''Да'', то рядом со значком ссылки на RSS-ленту новостей будет отображаться текст', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_syndicate.ini'),
(1996, 'MOD_SYNDICATE_FIELD_DISPLAYTEXT_LABEL', 'Показывать текст', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_syndicate.ini'),
(1997, 'MOD_SYNDICATE_FIELD_FORMAT_DESC', 'Укажите формат выводимых данных', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_syndicate.ini'),
(1998, 'MOD_SYNDICATE_FIELD_FORMAT_LABEL', 'Формат ленты', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_syndicate.ini'),
(1999, 'MOD_SYNDICATE_FIELD_TEXT_DESC', 'Если параметр &laquo;Показывать текст&raquo; имеет значение ''Да'', то введённый текст будет отображаться рядом со значком ссылки на RSS-ленту новостей. Если данное поле оставить пустым, то будет использоваться значение из языкового файла', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_syndicate.ini'),
(2000, 'MOD_SYNDICATE_FIELD_TEXT_LABEL', 'Текст', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_syndicate.ini'),
(2001, 'MOD_SYNDICATE_FIELD_VALUE_ATOM', 'Atom 1.0', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_syndicate.ini'),
(2002, 'MOD_SYNDICATE_FIELD_VALUE_RSS', 'RSS 2.0', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_syndicate.ini'),
(2003, 'MOD_SYNDICATE_XML_DESCRIPTION', 'Модуль создаёт RSS-ленту для страницы, на которой отображается.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_syndicate.ini'),
(2004, 'MOD_SYNDICATE', 'Ленты новостей', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_syndicate.sys.ini'),
(2005, 'MOD_SYNDICATE_XML_DESCRIPTION', 'Модуль создаёт RSS-ленту для страницы, на которой отображается.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_syndicate.sys.ini'),
(2006, 'MOD_SYNDICATE_LAYOUT_DEFAULT', 'По умолчанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_syndicate.sys.ini'),
(2007, 'MOD_TAGS_POPULAR', 'Популярные метки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_popular.ini'),
(2008, 'MOD_TAGS_POPULAR_FIELD_ALL_TIME', 'Всё время', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_popular.ini'),
(2009, 'MOD_TAGS_POPULAR_FIELD_DISPLAY_COUNT_DESC', 'Позволяет включить отображение количества элементов, отмеченных метками.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_popular.ini'),
(2010, 'MOD_TAGS_POPULAR_FIELD_DISPLAY_COUNT_LABEL', 'Показывать кол-во элементов', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_popular.ini'),
(2011, 'MOD_TAGS_POPULAR_FIELD_LAST_DAY', 'Последний день', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_popular.ini'),
(2012, 'MOD_TAGS_POPULAR_FIELD_LAST_HOUR', 'Последний час', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_popular.ini'),
(2013, 'MOD_TAGS_POPULAR_FIELD_LAST_MONTH', 'Последний месяц', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_popular.ini'),
(2014, 'MOD_TAGS_POPULAR_FIELD_LAST_WEEK', 'Последняя неделя', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_popular.ini'),
(2015, 'MOD_TAGS_POPULAR_FIELD_LAST_YEAR', 'Последний год', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_popular.ini'),
(2016, 'MOD_TAGS_POPULAR_FIELD_MAX_DESC', 'Устанавливает максимальное количество меток для отображения в модуле. Введите &quot;0&quot; для отображения всех имеющихся меток.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_popular.ini'),
(2017, 'MOD_TAGS_POPULAR_FIELD_MAX_LABEL', 'Кол-во меток', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_popular.ini'),
(2018, 'MOD_TAGS_POPULAR_FIELD_MAXSIZE_DESC', 'Максимальный размер шрифта, используемый для меток (пропорционально размеру шрифта сайта по умолчанию - &quot;2&quot; означает 200% от размера шрифта сайта).', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_popular.ini'),
(2019, 'MOD_TAGS_POPULAR_FIELD_MAXSIZE_LABEL', 'Максимальный шрифт', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_popular.ini'),
(2020, 'MOD_TAGS_POPULAR_FIELD_MINSIZE_DESC', 'Минимальный размер шрифта, используемый для меток (пропорционально размеру шрифта сайта по умолчанию - &quot;2&quot; означает 200% от размера шрифта сайта).', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_popular.ini'),
(2021, 'MOD_TAGS_POPULAR_FIELD_MINSIZE_LABEL', 'Минимальный шрифт', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_popular.ini'),
(2022, 'MOD_TAGS_POPULAR_FIELD_NO_RESULTS_DESC', 'Показывать сообщение если метки не найдены или скрывать модуль.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_popular.ini'),
(2023, 'MOD_TAGS_POPULAR_FIELD_NO_RESULTS_LABEL', 'Показывать текст &quot;Метки отсутствуют&quot;', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_popular.ini'),
(2024, 'MOD_TAGS_POPULAR_FIELD_ORDER_VALUE_COUNT', 'Кол-во элементов', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_popular.ini'),
(2025, 'MOD_TAGS_POPULAR_FIELD_ORDER_VALUE_DESC', 'Порядок отображения меток.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_popular.ini'),
(2026, 'MOD_TAGS_POPULAR_FIELD_ORDER_VALUE_LABEL', 'Порядок', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_popular.ini'),
(2027, 'MOD_TAGS_POPULAR_FIELD_ORDER_VALUE_RANDOM', 'Случайный', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_popular.ini'),
(2028, 'MOD_TAGS_POPULAR_FIELD_ORDER_VALUE_TITLE', 'Заголовок', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_popular.ini'),
(2029, 'MOD_TAGS_POPULAR_FIELD_TIMEFRAME_DESC', 'Позволяет задать период времени, в течение которого будет вычисляться популярность меток', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_popular.ini'),
(2030, 'MOD_TAGS_POPULAR_FIELD_TIMEFRAME_LABEL', 'Период', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_popular.ini'),
(2031, 'MOD_TAGS_POPULAR_FIELDSET_CLOUD_LABEL', 'Облако меток', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_popular.ini'),
(2032, 'MOD_TAGS_POPULAR_NO_ITEMS_FOUND', 'Метки отсутствуют.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_popular.ini'),
(2033, 'MOD_TAGS_POPULAR_MAX_DESC', 'Устанавливает максимальное количество меток для отображения в модуле', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_popular.ini'),
(2034, 'MOD_TAGS_POPULAR_MAX_LABEL', 'Кол-во меток', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_popular.ini'),
(2035, 'MOD_TAGS_POPULAR_XML_DESCRIPTION', 'Отображает список наиболее популярных меток (в параметрах модуля можно задать период времени, за который выбираются метки).', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_popular.ini'),
(2036, 'MOD_TAGS_POPULAR', 'Популярные метки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_popular.sys.ini'),
(2037, 'MOD_TAGS_POPULAR_LAYOUT_CLOUD', 'Облако меток', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_popular.sys.ini'),
(2038, 'MOD_TAGS_POPULAR_LAYOUT_DEFAULT', 'По умолчанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_popular.sys.ini'),
(2039, 'MOD_TAGS_POPULAR_XML_DESCRIPTION', 'Отображает список наиболее популярных меток (в параметрах модуля можно задать период времени, за который выбираются метки).', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_popular.sys.ini'),
(2040, 'MOD_TAGS_SIMILAR', 'Похожие метки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_similar.ini'),
(2041, 'MOD_TAGS_SIMILAR_FIELD_ALL', 'Все', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_similar.ini'),
(2042, 'MOD_TAGS_SIMILAR_FIELD_HALF', 'Половина', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_similar.ini'),
(2043, 'MOD_TAGS_SIMILAR_FIELD_MATCHTYPE_DESC', 'Определяет насколько метки элементов должны совпадать. Значение <strong>Все</strong> требует наличия у элементов всех меток, которые присутствуют у текущего элемента. Значение <strong>Любая</strong> требует наличие у элементов хотя бы одной метки, из меток текущего элемента. Значение <strong>Половина</strong> требует наличия у элементов половины меток, из меток текущего элемента.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_similar.ini'),
(2044, 'MOD_TAGS_SIMILAR_FIELD_MATCHTYPE_LABEL', 'Совпадение', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_similar.ini'),
(2045, 'MOD_TAGS_SIMILAR_FIELD_ONE', 'Любая', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_similar.ini'),
(2046, 'MOD_TAGS_SIMILAR_LAYOUT_DEFAULT', 'По умолчанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_similar.ini'),
(2047, 'MOD_TAGS_SIMILAR_MAX_DESC', 'Количество отображаемых элементов', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_similar.ini'),
(2048, 'MOD_TAGS_SIMILAR_MAX_LABEL', 'Кол-во элементов', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_similar.ini'),
(2049, 'MOD_TAGS_SIMILAR_NO_MATCHING_TAGS', 'Нет похожих меток', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_similar.ini'),
(2050, 'MOD_TAGS_SIMILAR_XML_DESCRIPTION', 'Модуль отображает ссылки на другие элементы с похожими метками. Степень совпадения меток может быть настроена в параметрах модуля', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_similar.ini'),
(2051, 'MOD_TAGS_SIMILAR_FIELD_ORDERING_LABEL', 'Порядок отображния', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_similar.ini'),
(2052, 'MOD_TAGS_SIMILAR_FIELD_ORDERING_DESC', 'Выберите порядок отображения результатов.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_similar.ini'),
(2053, 'MOD_TAGS_SIMILAR_FIELD_ORDERING_COUNT', 'Кол-во найденных меток', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_similar.ini'),
(2054, 'MOD_TAGS_SIMILAR_FIELD_ORDERING_RANDOM', 'Случайный', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_similar.ini'),
(2055, 'MOD_TAGS_SIMILAR_FIELD_ORDERING_COUNT_AND_RANDOM', 'Кол-во найденных & Случайный', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_similar.ini'),
(2056, 'MOD_TAGS_SIMILAR', 'Похожие метки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_similar.sys.ini'),
(2057, 'MOD_TAGS_SIMILAR_LAYOUT_DEFAULT', 'По умолчанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_similar.sys.ini'),
(2058, 'MOD_TAGS_SIMILAR_XML_DESCRIPTION', 'Модуль отображает ссылки на другие элементы с похожими метками. Степень совпадения меток может быть настроена в параметрах модуля', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_tags_similar.sys.ini'),
(2059, 'MOD_USERS_LATEST', 'Новые пользователи', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_users_latest.ini'),
(2060, 'MOD_USERS_LATEST_FIELD_FILTER_GROUPS_DESC', 'Выберите для фильтрации по группам пользователя', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_users_latest.ini'),
(2061, 'MOD_USERS_LATEST_FIELD_FILTER_GROUPS_LABEL', 'Фильтр групп', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_users_latest.ini'),
(2062, 'MOD_USERS_LATEST_FIELD_LINKTOWHAT_DESC', 'Выберите тип сведений для отображения', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_users_latest.ini'),
(2063, 'MOD_USERS_LATEST_FIELD_LINKTOWHAT_LABEL', 'Информация пользователя', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_users_latest.ini'),
(2064, 'MOD_USERS_LATEST_FIELD_NUMBER_DESC', 'Количество последних зарегистрированных пользователей, которое следует показывать', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_users_latest.ini'),
(2065, 'MOD_USERS_LATEST_FIELD_NUMBER_LABEL', 'Кол-во пользователей', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_users_latest.ini'),
(2066, 'MOD_USERS_LATEST_FIELD_VALUE_CONTACT', 'Контакт', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_users_latest.ini'),
(2067, 'MOD_USERS_LATEST_FIELD_VALUE_PROFILE', 'Профиль', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_users_latest.ini'),
(2068, 'MOD_USERS_LATEST_XML_DESCRIPTION', 'Этот модуль выводит список последних, зарегистрированных пользователей', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_users_latest.ini'),
(2069, 'MOD_USERS_LATEST', 'Новые пользователи', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_users_latest.sys.ini'),
(2070, 'MOD_USERS_LATEST_XML_DESCRIPTION', 'Этот модуль выводит список последних, зарегистрированных пользователей', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_users_latest.sys.ini'),
(2071, 'MOD_USERS_LATEST_LAYOUT_DEFAULT', 'По умолчанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_users_latest.sys.ini'),
(2072, 'MOD_WEBLINKS', 'Ссылки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_weblinks.ini'),
(2073, 'MOD_WEBLINKS_FIELD_CATEGORY_DESC', 'Выберите категорию ссылок, которую следует показывать', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_weblinks.ini'),
(2074, 'MOD_WEBLINKS_FIELD_COUNT_DESC', 'Кол-во ссылок, которое следует показывать', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_weblinks.ini'),
(2075, 'MOD_WEBLINKS_FIELD_COUNT_LABEL', 'Кол-во', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_weblinks.ini'),
(2076, 'MOD_WEBLINKS_FIELD_COUNTCLICKS_DESC', 'Если установлено <strong>Да</strong>, система будет регистрировать количество переходов по ссылке.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_weblinks.ini'),
(2077, 'MOD_WEBLINKS_FIELD_COUNTCLICKS_LABEL', 'Подсчёт кликов', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_weblinks.ini'),
(2078, 'MOD_WEBLINKS_FIELD_DESCRIPTION_DESC', 'Показывать описание ссылки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_weblinks.ini'),
(2079, 'MOD_WEBLINKS_FIELD_DESCRIPTION_LABEL', 'Описание', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_weblinks.ini'),
(2080, 'MOD_WEBLINKS_FIELD_FOLLOW_DESC', 'Инструкции для роботов поисковых систем. <strong>Follow</strong> - сообщает поисковым системам о необходимости проиндексировать ссылку, <strong>No follow</strong> - запрещает индексацию ссылки поисковыми системами.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_weblinks.ini'),
(2081, 'MOD_WEBLINKS_FIELD_FOLLOW_LABEL', 'Индексация ссылок', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_weblinks.ini'),
(2082, 'MOD_WEBLINKS_FIELD_HITS_DESC', 'Показывать количество просмотров ссылок', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_weblinks.ini'),
(2083, 'MOD_WEBLINKS_FIELD_HITS_LABEL', 'Кол-во просмотров', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_weblinks.ini'),
(2084, 'MOD_WEBLINKS_FIELD_ORDERDIRECTION_DESC', 'Установить направление сортировки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_weblinks.ini'),
(2085, 'MOD_WEBLINKS_FIELD_ORDERDIRECTION_LABEL', 'Направление', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_weblinks.ini'),
(2086, 'MOD_WEBLINKS_FIELD_ORDERING_DESC', 'Порядок отображения ссылок', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_weblinks.ini'),
(2087, 'MOD_WEBLINKS_FIELD_ORDERING_LABEL', 'Порядок', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_weblinks.ini'),
(2088, 'MOD_WEBLINKS_FIELD_TARGET_DESC', 'Окно, в котором откроется ссылка', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_weblinks.ini'),
(2089, 'MOD_WEBLINKS_FIELD_TARGET_LABEL', 'Целевое окно', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_weblinks.ini'),
(2090, 'MOD_WEBLINKS_FIELD_VALUE_ASCENDING', 'По возрастанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_weblinks.ini'),
(2091, 'MOD_WEBLINKS_FIELD_VALUE_DESCENDING', 'По убыванию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_weblinks.ini'),
(2092, 'MOD_WEBLINKS_FIELD_VALUE_FOLLOW', 'Follow', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_weblinks.ini'),
(2093, 'MOD_WEBLINKS_FIELD_VALUE_HITS', 'Количество просмотров', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_weblinks.ini'),
(2094, 'MOD_WEBLINKS_FIELD_VALUE_NOFOLLOW', 'No follow', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_weblinks.ini'),
(2095, 'MOD_WEBLINKS_FIELD_VALUE_ORDER', 'Порядок', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_weblinks.ini'),
(2096, 'MOD_WEBLINKS_HITS', 'Просмотры', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_weblinks.ini'),
(2097, 'MOD_WEBLINKS_XML_DESCRIPTION', 'Этот модуль отображает ссылки из указанной категории компонента Ссылки.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_weblinks.ini'),
(2098, 'MOD_WEBLINKS', 'Ссылки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_weblinks.sys.ini'),
(2099, 'MOD_WEBLINKS_XML_DESCRIPTION', 'Этот модуль отображает ссылки из указанной категории компонента Ссылки.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_weblinks.sys.ini'),
(2100, 'MOD_WEBLINKS_LAYOUT_DEFAULT', 'По умолчанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_weblinks.sys.ini'),
(2101, 'MOD_WHOSONLINE', 'Кто на сайте', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_whosonline.ini'),
(2102, 'MOD_WHOSONLINE_FIELD_FILTER_GROUPS_DESC', 'Выберите для фильтрации по группам пользователя', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_whosonline.ini'),
(2103, 'MOD_WHOSONLINE_FIELD_FILTER_GROUPS_LABEL', 'Фильтр групп', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_whosonline.ini'),
(2104, 'MOD_WHOSONLINE_FIELD_LINKTOWHAT_DESC', 'Выберите тип сведений для отображения', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_whosonline.ini'),
(2105, 'MOD_WHOSONLINE_FIELD_LINKTOWHAT_LABEL', 'Информация', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_whosonline.ini'),
(2106, 'MOD_WHOSONLINE_FIELD_VALUE_BOTH', 'И то и другое', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_whosonline.ini'),
(2107, 'MOD_WHOSONLINE_FIELD_VALUE_CONTACT', 'Контакт', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_whosonline.ini'),
(2108, 'MOD_WHOSONLINE_FIELD_VALUE_NAMES', 'Логины пользователей', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_whosonline.ini'),
(2109, 'MOD_WHOSONLINE_FIELD_VALUE_NUMBER', 'Кол-во гостей/пользователей', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_whosonline.ini'),
(2110, 'MOD_WHOSONLINE_FIELD_VALUE_PROFILE', 'Профиль', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_whosonline.ini'),
(2111, 'MOD_WHOSONLINE_GUESTS', '%s&#160;гостей', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_whosonline.ini'),
(2112, 'MOD_WHOSONLINE_GUESTS_0', 'гостей нет', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_whosonline.ini'),
(2113, 'MOD_WHOSONLINE_GUESTS_1', 'один гость', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_whosonline.ini'),
(2114, 'MOD_WHOSONLINE_MEMBERS', '%s&#160;зарегистрированных пользователей', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_whosonline.ini'),
(2115, 'MOD_WHOSONLINE_MEMBERS_0', 'ни одного зарегистрированного пользователя', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_whosonline.ini'),
(2116, 'MOD_WHOSONLINE_MEMBERS_1', 'один зарегистрированный пользователь', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_whosonline.ini'),
(2117, 'MOD_WHOSONLINE_SAME_GROUP_MESSAGE', 'Список пользователей, которые относятся к вашим группам пользователей (включая дочерние группы)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_whosonline.ini'),
(2118, 'MOD_WHOSONLINE_SHOWMODE_DESC', 'Выберите, что должно быть показано', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_whosonline.ini'),
(2119, 'MOD_WHOSONLINE_SHOWMODE_LABEL', 'Показывать', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_whosonline.ini'),
(2120, 'MOD_WHOSONLINE_XML_DESCRIPTION', 'Модуль отображает количество гостей и авторизованных пользователей (тех, что ввели логин и пароль), которые в данный момент просматривают различные страницы сайта.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_whosonline.ini'),
(2121, 'MOD_WHOSONLINE_WE_HAVE', 'Сейчас %1$s и %2$s на сайте', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_whosonline.ini'),
(2122, 'MOD_WHOSONLINE', 'Кто на сайте', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_whosonline.sys.ini'),
(2123, 'MOD_WHOSONLINE_LAYOUT_DEFAULT', 'По умолчанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_whosonline.sys.ini'),
(2124, 'MOD_WHOSONLINE_XML_DESCRIPTION', 'Модуль отображает количество гостей и авторизованных пользователей (тех, что ввели логин и пароль), которые в данный момент просматривают различные страницы сайта.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_whosonline.sys.ini'),
(2125, 'MOD_WRAPPER', 'Обёртка (Wrapper)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_wrapper.ini'),
(2126, 'MOD_WRAPPER_FIELD_ADD_DESC', 'По умолчанию, префикс http:// будет добавлен, если ни один из этих префиксов http:// или https:// не был найден в ссылке. Вы можете отключить эту функцию.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_wrapper.ini'),
(2127, 'MOD_WRAPPER_FIELD_ADD_LABEL', 'Авто доб.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_wrapper.ini'),
(2128, 'MOD_WRAPPER_FIELD_AUTOHEIGHT_DESC', 'Высота будет установлена автоматически под размер внешней страницы. Это работает только для страниц вашего домена.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_wrapper.ini'),
(2129, 'MOD_WRAPPER_FIELD_AUTOHEIGHT_LABEL', 'Высота авто', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_wrapper.ini'),
(2130, 'MOD_WRAPPER_FIELD_HEIGHT_DESC', 'Высота IFrame окна', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_wrapper.ini'),
(2131, 'MOD_WRAPPER_FIELD_HEIGHT_LABEL', 'Высота', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_wrapper.ini'),
(2132, 'MOD_WRAPPER_FIELD_SCROLL_DESC', 'Показать/Скрыть полосы прокрутки.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_wrapper.ini'),
(2133, 'MOD_WRAPPER_FIELD_SCROLL_LABEL', 'Полосы прокрутки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_wrapper.ini'),
(2134, 'MOD_WRAPPER_FIELD_TARGET_DESC', 'Название IFrame-а когда используется как Target', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_wrapper.ini'),
(2135, 'MOD_WRAPPER_FIELD_TARGET_LABEL', 'Target', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_wrapper.ini'),
(2136, 'MOD_WRAPPER_FIELD_URL_DESC', 'URL на сайт/файл, который необходимо отобразить в окне.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_wrapper.ini'),
(2137, 'MOD_WRAPPER_FIELD_URL_LABEL', 'Ссылка', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_wrapper.ini'),
(2138, 'MOD_WRAPPER_FIELD_VALUE_AUTO', 'Авто', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_wrapper.ini'),
(2139, 'MOD_WRAPPER_FIELD_WIDTH_DESC', 'Ширина IFrame окна. Вы можете ввести абсолютное значение в пикселях или относительное, добавив %.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_wrapper.ini'),
(2140, 'MOD_WRAPPER_FIELD_WIDTH_LABEL', 'Ширина', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_wrapper.ini'),
(2141, 'MOD_WRAPPER_NO_IFRAMES', 'Без IFRAME', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_wrapper.ini'),
(2142, 'MOD_WRAPPER_XML_DESCRIPTION', 'Этот модуль отображает в IFrame-окне содержимое по заданной ссылке (сайт или файл).', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_wrapper.ini'),
(2143, 'MOD_WRAPPER_FIELD_FRAME_LABEL', 'Рамка', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_wrapper.ini'),
(2144, 'MOD_WRAPPER_FIELD_FRAME_DESC', 'Показывать рамку окна IFrame', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_wrapper.ini'),
(2145, 'MOD_WRAPPER', 'Обёртка (Wrapper)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_wrapper.sys.ini'),
(2146, 'MOD_WRAPPER_NO_IFRAMES', 'Без IFRAME', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_wrapper.sys.ini'),
(2147, 'MOD_WRAPPER_XML_DESCRIPTION', 'Этот модуль отображает в IFrame-окне содержимое по заданной ссылке (сайт или файл).', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_wrapper.sys.ini'),
(2148, 'MOD_WRAPPER_LAYOUT_DEFAULT', 'По умолчанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.mod_wrapper.sys.ini'),
(2149, 'PKG_JOOMLA', 'Система управления сайтом Joomla!', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.pkg_joomla.sys.ini'),
(2150, 'PKG_JOOMLA_XML_DESCRIPTION', 'CMS Joomla! на сегодняшний день является одной из самых популярных систем управления сайтом в мире', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.pkg_joomla.sys.ini'),
(2151, 'TPL_BEEZ3_ADDITIONAL_INFORMATION', 'Дополнительная информация', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2152, 'TPL_BEEZ3_ALTCLOSE', 'закрыто', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2153, 'TPL_BEEZ3_ALTOPEN', 'открыт', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2154, 'TPL_BEEZ3_BIGGER', 'Больше', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2155, 'TPL_BEEZ3_CLICK', 'Клик', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2156, 'TPL_BEEZ3_CLOSEMENU', 'Свернуть меню', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2157, 'TPL_BEEZ3_DECREASE_SIZE', 'Уменьшить размер', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2158, 'TPL_BEEZ3_ERROR_JUMP_TO_NAV', 'Переход к навигации', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2159, 'TPL_BEEZ3_FIELD_BOOTSTRAP_DESC', 'Список расширений (используйте запятую в качестве разделителя) для работы которых требуется Bootstrap, например com_name, com_anothername.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2160, 'TPL_BEEZ3_FIELD_BOOTSTRAP_LABEL', 'Компоненты требующие<br/> Bootstrap', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2161, 'TPL_BEEZ3_FIELD_DESCRIPTION_DESC', 'Введите здесь описание вашего сайта.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2162, 'TPL_BEEZ3_FIELD_DESCRIPTION_LABEL', 'Описание сайта', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2163, 'TPL_BEEZ3_FIELD_HEADER_BACKGROUND_COLOR_DESC', 'Значение используется если в качестве цветовой схемы выбрано «Custom».', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2164, 'TPL_BEEZ3_FIELD_HEADER_BACKGROUND_COLOR_LABEL', 'Цвет фона', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2165, 'TPL_BEEZ3_FIELD_HEADER_IMAGE_DESC', 'Значение используется если в качестве цветовой схемы выбрано «Custom».', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2166, 'TPL_BEEZ3_FIELD_HEADER_IMAGE_LABEL', 'Изображение для шапки', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2167, 'TPL_BEEZ3_FIELD_LOGO_DESC', 'Пожалуйста, выберите изображение. Если вы не хотите отображать графический логотип, то нажмите на кнопку <strong>Выбрать</strong>, не выделяйте никакого изображения, а затем нажмите <strong>Вставить</strong> в модальном окне.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2168, 'TPL_BEEZ3_FIELD_LOGO_LABEL', 'Логотип', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2169, 'TPL_BEEZ3_FIELD_NAVPOSITION_DESC', 'Показывать элементы навигации до или после основного контента', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2170, 'TPL_BEEZ3_FIELD_NAVPOSITION_LABEL', 'Позиция навигации', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2171, 'TPL_BEEZ3_FIELD_SITETITLE_DESC', 'Пожалуйста, введите здесь название сайта. Оно будет отображено только если не будет использоваться графический логотип.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2172, 'TPL_BEEZ3_FIELD_SITETITLE_LABEL', 'Заголовок сайта', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2173, 'TPL_BEEZ3_FIELD_TEMPLATECOLOR_DESC', 'Цветовая схема шаблона', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2174, 'TPL_BEEZ3_FIELD_TEMPLATECOLOR_LABEL', 'Цветовая схема', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2175, 'TPL_BEEZ3_FIELD_WRAPPERLARGE_DESC', 'Ширина обёртки (Wrapper) при скрытых боковых столбцах, в процентах.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2176, 'TPL_BEEZ3_FIELD_WRAPPERLARGE_LABEL', 'Большая обёртка (%)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2177, 'TPL_BEEZ3_FIELD_WRAPPERSMALL_DESC', 'Ширина обёртки (Wrapper) при отображаемых боковых столбцах, в процентах.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2178, 'TPL_BEEZ3_FIELD_WRAPPERSMALL_LABEL', 'Малая обёртка (%)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2179, 'TPL_BEEZ3_FONTSIZE', 'Размер шрифта', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2180, 'TPL_BEEZ3_INCREASE_SIZE', 'Увеличение размера', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2181, 'TPL_BEEZ3_JUMP_TO_INFO', 'Перейти к дополнительной информации', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2182, 'TPL_BEEZ3_JUMP_TO_NAV', 'Перейти к Главной навигации и Войти', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2183, 'TPL_BEEZ3_NAVIGATION', 'Навигация', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2184, 'TPL_BEEZ3_NAV_VIEW_SEARCH', 'Nav view search', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2185, 'TPL_BEEZ3_NEXTTAB', 'Следующая вкладка', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2186, 'TPL_BEEZ3_OPENMENU', 'Раскрыть меню', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2187, 'TPL_BEEZ3_OPTION_AFTER_CONTENT', 'после контента', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2188, 'TPL_BEEZ3_OPTION_BEFORE_CONTENT', 'перед контентом', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2189, 'TPL_BEEZ3_OPTION_IMAGE', 'Custom', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2190, 'TPL_BEEZ3_OPTION_NATURE', 'Nature', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2191, 'TPL_BEEZ3_OPTION_PERSONAL', 'Personal', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2192, 'TPL_BEEZ3_OPTION_RED', 'Red', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2193, 'TPL_BEEZ3_OPTION_TURQ', 'Turquoise', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2194, 'TPL_BEEZ3_POWERED_BY', 'Работает на', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2195, 'TPL_BEEZ3_RESET', 'Сброс', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2196, 'TPL_BEEZ3_REVERT_STYLES_TO_DEFAULT', 'Вернуть стили по умолчанию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2197, 'TPL_BEEZ3_SEARCH', 'Искать', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2198, 'TPL_BEEZ3_SKIP_TO_CONTENT', 'Пропустить и перейти к материалам', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2199, 'TPL_BEEZ3_SKIP_TO_ERROR_CONTENT', 'Переход к сообщению об ошибке и поиску', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2200, 'TPL_BEEZ3_SMALLER', 'Меньше', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2201, 'TPL_BEEZ3_SYSTEM_MESSAGE', 'Информация', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2202, 'TPL_BEEZ3_TEXTRIGHTCLOSE', 'Скрыть информацию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2203, 'TPL_BEEZ3_TEXTRIGHTOPEN', 'Показать информацию', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2204, 'TPL_BEEZ3_XML_DESCRIPTION', 'Шаблон Joomla! Beez (версия с поддержкой HTML 4)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2205, 'TPL_BEEZ3_YOUR_SITE_DESCRIPTION', 'Описание вашего сайта', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2206, 'TPL_BEEZ3_LOGO', 'Логотип', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.ini'),
(2207, 'TPL_BEEZ3_POSITION_DEBUG', 'Debug', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.sys.ini'),
(2208, 'TPL_BEEZ3_POSITION_POSITION-0', 'Search', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.sys.ini'),
(2209, 'TPL_BEEZ3_POSITION_POSITION-10', 'Footer middle', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.sys.ini'),
(2210, 'TPL_BEEZ3_POSITION_POSITION-11', 'Footer bottom', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.sys.ini'),
(2211, 'TPL_BEEZ3_POSITION_POSITION-12', 'Middle top', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.sys.ini'),
(2212, 'TPL_BEEZ3_POSITION_POSITION-13', 'Unused', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.sys.ini'),
(2213, 'TPL_BEEZ3_POSITION_POSITION-14', 'Footer last', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.sys.ini'),
(2214, 'TPL_BEEZ3_POSITION_POSITION-15', 'Header', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.sys.ini'),
(2215, 'TPL_BEEZ3_POSITION_POSITION-1', 'Top', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.sys.ini'),
(2216, 'TPL_BEEZ3_POSITION_POSITION-2', 'Breadcrumbs', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.sys.ini'),
(2217, 'TPL_BEEZ3_POSITION_POSITION-3', 'Right bottom', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.sys.ini'),
(2218, 'TPL_BEEZ3_POSITION_POSITION-4', 'Left middle', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.sys.ini'),
(2219, 'TPL_BEEZ3_POSITION_POSITION-5', 'Left bottom', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.sys.ini'),
(2220, 'TPL_BEEZ3_POSITION_POSITION-6', 'Right top', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.sys.ini'),
(2221, 'TPL_BEEZ3_POSITION_POSITION-7', 'Left top', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.sys.ini'),
(2222, 'TPL_BEEZ3_POSITION_POSITION-8', 'Right middle', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.sys.ini'),
(2223, 'TPL_BEEZ3_POSITION_POSITION-9', 'Footer top', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.sys.ini'),
(2224, 'TPL_BEEZ3_XML_DESCRIPTION', 'Шаблон Joomla! Beez (версия с поддержкой HTML 4)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_beez3.sys.ini'),
(2225, 'TPL_PROTOSTAR_BACKGROUND_COLOR_DESC', 'Выберите цвет фона для фиксированных макетов. Оставьте пустым для использования значения по умолчанию (#F4F6F7)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_protostar.ini'),
(2226, 'TPL_PROTOSTAR_BACKGROUND_COLOR_LABEL', 'Цвет фона', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_protostar.ini'),
(2227, 'TPL_PROTOSTAR_BACKTOTOP', 'Наверх', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_protostar.ini'),
(2228, 'TPL_PROTOSTAR_COLOR_LABEL', 'Цвет текста', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_protostar.ini'),
(2229, 'TPL_PROTOSTAR_COLOR_DESC', 'Выберите основной цвет для шаблона. Оставьте пустым для использования значения по умолчанию (#0088CC)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_protostar.ini'),
(2230, 'TPL_PROTOSTAR_FLUID', 'Резиновый', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_protostar.ini'),
(2231, 'TPL_PROTOSTAR_FLUID_LABEL', 'Тип контейнера', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_protostar.ini'),
(2232, 'TPL_PROTOSTAR_FLUID_DESC', 'Выберите тип контейнера Bootstrap - «Резиновый» (Fluid) или «Фиксированный» (Static).', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_protostar.ini'),
(2233, 'TPL_PROTOSTAR_FONT_LABEL', 'Шрифт Google для заголовков', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_protostar.ini'),
(2234, 'TPL_PROTOSTAR_FONT_DESC', 'Загружать шрифт Google для заголовков (H1, H2, H3, etc.)', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_protostar.ini'),
(2235, 'TPL_PROTOSTAR_FONT_NAME_LABEL', 'Название шрифта Google', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_protostar.ini'),
(2236, 'TPL_PROTOSTAR_FONT_NAME_DESC', 'Например: Open+Sans или Source+Sans+Pro', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_protostar.ini'),
(2237, 'TPL_PROTOSTAR_LOGO_LABEL', 'Логотип', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_protostar.ini'),
(2238, 'TPL_PROTOSTAR_LOGO_DESC', 'Загрузить пользовательский логотип для шаблона сайта.', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_protostar.ini'),
(2239, 'TPL_PROTOSTAR_STATIC', 'Фиксированный', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_protostar.ini'),
(2240, 'TPL_PROTOSTAR_XML_DESCRIPTION', 'Protostar - шаблон для Joomla 3, продолжающий космическую тематику шаблонов Joomla (Solarflare для 1.0 и Milkyway для 1.5)) и построенный с использованием Bootstrap от Twitter и библиотеки Joomla User Interface (JUI).', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_protostar.ini'),
(2241, 'TPL_PROTOSTAR_POSITION_BANNER', 'Banner', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_protostar.sys.ini'),
(2242, 'TPL_PROTOSTAR_POSITION_DEBUG', 'Debug', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_protostar.sys.ini'),
(2243, 'TPL_PROTOSTAR_POSITION_POSITION-0', 'Search', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_protostar.sys.ini'),
(2244, 'TPL_PROTOSTAR_POSITION_POSITION-10', 'Unused', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_protostar.sys.ini'),
(2245, 'TPL_PROTOSTAR_POSITION_POSITION-11', 'Unused', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_protostar.sys.ini'),
(2246, 'TPL_PROTOSTAR_POSITION_POSITION-12', 'Unused', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_protostar.sys.ini'),
(2247, 'TPL_PROTOSTAR_POSITION_POSITION-13', 'Unused', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_protostar.sys.ini'),
(2248, 'TPL_PROTOSTAR_POSITION_POSITION-14', 'Unused', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_protostar.sys.ini'),
(2249, 'TPL_PROTOSTAR_POSITION_POSITION-15', 'Unused', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_protostar.sys.ini'),
(2250, 'TPL_PROTOSTAR_POSITION_POSITION-1', 'Navigation', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_protostar.sys.ini'),
(2251, 'TPL_PROTOSTAR_POSITION_POSITION-2', 'Breadcrumbs', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_protostar.sys.ini'),
(2252, 'TPL_PROTOSTAR_POSITION_POSITION-3', 'Top center', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_protostar.sys.ini'),
(2253, 'TPL_PROTOSTAR_POSITION_POSITION-4', 'Unused', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_protostar.sys.ini'),
(2254, 'TPL_PROTOSTAR_POSITION_POSITION-5', 'Unused', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_protostar.sys.ini'),
(2255, 'TPL_PROTOSTAR_POSITION_POSITION-6', 'Unused', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_protostar.sys.ini'),
(2256, 'TPL_PROTOSTAR_POSITION_POSITION-7', 'Right', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_protostar.sys.ini'),
(2257, 'TPL_PROTOSTAR_POSITION_POSITION-8', 'Left', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_protostar.sys.ini'),
(2258, 'TPL_PROTOSTAR_POSITION_POSITION-9', 'Unused', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_protostar.sys.ini'),
(2259, 'TPL_PROTOSTAR_POSITION_FOOTER', 'Footer', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_protostar.sys.ini'),
(2260, 'TPL_PROTOSTAR_XML_DESCRIPTION', 'Protostar - шаблон для Joomla 3, продолжающий космическую тематику шаблонов Joomla (Solarflare для 1.0 и Milkyway для 1.5)) и построенный с использованием Bootstrap от Twitter и библиотеки Joomla User Interface (JUI).', 'Z:\\home\\localhost\\www\\lavka\\language\\ru-RU\\ru-RU.tpl_protostar.sys.ini');

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_postinstall_messages`
--

CREATE TABLE IF NOT EXISTS `yawnc_postinstall_messages` (
  `postinstall_message_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `extension_id` bigint(20) NOT NULL DEFAULT '700' COMMENT 'FK to #__extensions',
  `title_key` varchar(255) NOT NULL DEFAULT '' COMMENT 'Lang key for the title',
  `description_key` varchar(255) NOT NULL DEFAULT '' COMMENT 'Lang key for description',
  `action_key` varchar(255) NOT NULL DEFAULT '',
  `language_extension` varchar(255) NOT NULL DEFAULT 'com_postinstall' COMMENT 'Extension holding lang keys',
  `language_client_id` tinyint(3) NOT NULL DEFAULT '1',
  `type` varchar(10) NOT NULL DEFAULT 'link' COMMENT 'Message type - message, link, action',
  `action_file` varchar(255) DEFAULT '' COMMENT 'RAD URI to the PHP file containing action method',
  `action` varchar(255) DEFAULT '' COMMENT 'Action method name or URL',
  `condition_file` varchar(255) DEFAULT NULL COMMENT 'RAD URI to file holding display condition method',
  `condition_method` varchar(255) DEFAULT NULL COMMENT 'Display condition method, must return boolean',
  `version_introduced` varchar(50) NOT NULL DEFAULT '3.2.0' COMMENT 'Version when this message was introduced',
  `enabled` tinyint(3) NOT NULL DEFAULT '1',
  PRIMARY KEY (`postinstall_message_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `yawnc_postinstall_messages`
--

INSERT INTO `yawnc_postinstall_messages` (`postinstall_message_id`, `extension_id`, `title_key`, `description_key`, `action_key`, `language_extension`, `language_client_id`, `type`, `action_file`, `action`, `condition_file`, `condition_method`, `version_introduced`, `enabled`) VALUES
(1, 700, 'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_TITLE', 'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_BODY', 'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_ACTION', 'plg_twofactorauth_totp', 1, 'action', 'site://plugins/twofactorauth/totp/postinstall/actions.php', 'twofactorauth_postinstall_action', 'site://plugins/twofactorauth/totp/postinstall/actions.php', 'twofactorauth_postinstall_condition', '3.2.0', 1),
(2, 700, 'COM_CPANEL_WELCOME_BEGINNERS_TITLE', 'COM_CPANEL_WELCOME_BEGINNERS_MESSAGE', '', 'com_cpanel', 1, 'message', '', '', '', '', '3.2.0', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_redirect_links`
--

CREATE TABLE IF NOT EXISTS `yawnc_redirect_links` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `old_url` varchar(255) NOT NULL,
  `new_url` varchar(255) DEFAULT NULL,
  `referer` varchar(150) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `published` tinyint(4) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `header` smallint(3) NOT NULL DEFAULT '301',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_link_old` (`old_url`),
  KEY `idx_link_modifed` (`modified_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_schemas`
--

CREATE TABLE IF NOT EXISTS `yawnc_schemas` (
  `extension_id` int(11) NOT NULL,
  `version_id` varchar(20) NOT NULL,
  PRIMARY KEY (`extension_id`,`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yawnc_schemas`
--

INSERT INTO `yawnc_schemas` (`extension_id`, `version_id`) VALUES
(700, '3.4.0-2015-02-26');

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_session`
--

CREATE TABLE IF NOT EXISTS `yawnc_session` (
  `session_id` varchar(200) NOT NULL DEFAULT '',
  `client_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `guest` tinyint(4) unsigned DEFAULT '1',
  `time` varchar(14) DEFAULT '',
  `data` mediumtext,
  `userid` int(11) DEFAULT '0',
  `username` varchar(150) DEFAULT '',
  PRIMARY KEY (`session_id`),
  KEY `userid` (`userid`),
  KEY `time` (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yawnc_session`
--

INSERT INTO `yawnc_session` (`session_id`, `client_id`, `guest`, `time`, `data`, `userid`, `username`) VALUES
('6rf5q72edj16tq3jnugiunbhf3', 0, 1, '1443188094', '__default|a:7:{s:15:"session.counter";i:80;s:19:"session.timer.start";i:1443180541;s:18:"session.timer.last";i:1443188017;s:17:"session.timer.now";i:1443188093;s:22:"session.client.browser";s:108:"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.99 Safari/537.36";s:8:"registry";O:24:"Joomla\\Registry\\Registry":2:{s:7:"\\0\\0\\0data";O:8:"stdClass":0:{}s:9:"separator";s:1:".";}s:4:"user";O:5:"JUser":26:{s:9:"\\0\\0\\0isRoot";b:0;s:2:"id";i:0;s:4:"name";N;s:8:"username";N;s:5:"email";N;s:8:"password";N;s:14:"password_clear";s:0:"";s:5:"block";N;s:9:"sendEmail";i:0;s:12:"registerDate";N;s:13:"lastvisitDate";N;s:10:"activation";N;s:6:"params";N;s:6:"groups";a:1:{i:0;s:1:"9";}s:5:"guest";i:1;s:13:"lastResetTime";N;s:10:"resetCount";N;s:12:"requireReset";N;s:10:"\\0\\0\\0_params";O:24:"Joomla\\Registry\\Registry":2:{s:7:"\\0\\0\\0data";O:8:"stdClass":0:{}s:9:"separator";s:1:".";}s:14:"\\0\\0\\0_authGroups";a:2:{i:0;i:1;i:1;i:9;}s:14:"\\0\\0\\0_authLevels";a:3:{i:0;i:1;i:1;i:1;i:2;i:5;}s:15:"\\0\\0\\0_authActions";N;s:12:"\\0\\0\\0_errorMsg";N;s:13:"\\0\\0\\0userHelper";O:18:"JUserWrapperHelper":0:{}s:10:"\\0\\0\\0_errors";a:0:{}s:3:"aid";i:0;}}', 0, ''),
('iaq7mir6liptsspvnpvgk9q337', 1, 0, '1443189213', '__default|a:9:{s:15:"session.counter";i:121;s:19:"session.timer.start";i:1443180503;s:18:"session.timer.last";i:1443189206;s:17:"session.timer.now";i:1443189212;s:22:"session.client.browser";s:108:"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.99 Safari/537.36";s:8:"registry";O:24:"Joomla\\Registry\\Registry":2:{s:7:"\\0\\0\\0data";O:8:"stdClass":2:{s:11:"application";O:8:"stdClass":1:{s:4:"lang";s:0:"";}s:11:"com_modules";O:8:"stdClass":3:{s:7:"modules";O:8:"stdClass":1:{s:6:"filter";O:8:"stdClass":1:{s:18:"client_id_previous";i:0;}}s:4:"edit";O:8:"stdClass":1:{s:6:"module";O:8:"stdClass":2:{s:2:"id";a:0:{}s:4:"data";N;}}s:3:"add";O:8:"stdClass":1:{s:6:"module";O:8:"stdClass":2:{s:12:"extension_id";N;s:6:"params";N;}}}}s:9:"separator";s:1:".";}s:4:"user";O:5:"JUser":28:{s:9:"\\0\\0\\0isRoot";b:1;s:2:"id";s:3:"595";s:4:"name";s:10:"Super User";s:8:"username";s:5:"admin";s:5:"email";s:20:"info@malevich.com.ua";s:8:"password";s:60:"$2y$10$otnXE12FZOUU8.R4CvOeCOLpyaALSf1BjCCdF6EbeNo2nL58vbAJu";s:14:"password_clear";s:0:"";s:5:"block";s:1:"0";s:9:"sendEmail";s:1:"1";s:12:"registerDate";s:19:"2015-09-10 10:40:56";s:13:"lastvisitDate";s:19:"2015-09-24 14:16:36";s:10:"activation";s:1:"0";s:6:"params";s:0:"";s:6:"groups";a:1:{i:8;s:1:"8";}s:5:"guest";i:0;s:13:"lastResetTime";s:19:"0000-00-00 00:00:00";s:10:"resetCount";s:1:"0";s:12:"requireReset";s:1:"0";s:10:"\\0\\0\\0_params";O:24:"Joomla\\Registry\\Registry":2:{s:7:"\\0\\0\\0data";O:8:"stdClass":0:{}s:9:"separator";s:1:".";}s:14:"\\0\\0\\0_authGroups";a:2:{i:0;i:1;i:1;i:8;}s:14:"\\0\\0\\0_authLevels";a:5:{i:0;i:1;i:1;i:1;i:2;i:2;i:3;i:3;i:4;i:6;}s:15:"\\0\\0\\0_authActions";N;s:12:"\\0\\0\\0_errorMsg";N;s:13:"\\0\\0\\0userHelper";O:18:"JUserWrapperHelper":0:{}s:10:"\\0\\0\\0_errors";a:0:{}s:3:"aid";i:0;s:6:"otpKey";s:0:"";s:4:"otep";s:0:"";}s:13:"session.token";s:32:"5d745d9d95ae7af859016f6490becc23";s:20:"com_media.return_url";s:26:"index.php?option=com_media";}', 595, 'admin');

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_tags`
--

CREATE TABLE IF NOT EXISTS `yawnc_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `level` int(10) unsigned NOT NULL DEFAULT '0',
  `path` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `metadesc` varchar(1024) NOT NULL COMMENT 'The meta description for the page.',
  `metakey` varchar(1024) NOT NULL COMMENT 'The meta keywords for the page.',
  `metadata` varchar(2048) NOT NULL COMMENT 'JSON encoded metadata properties.',
  `created_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` text NOT NULL,
  `urls` text NOT NULL,
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL,
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `tag_idx` (`published`,`access`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_path` (`path`),
  KEY `idx_left_right` (`lft`,`rgt`),
  KEY `idx_alias` (`alias`),
  KEY `idx_language` (`language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `yawnc_tags`
--

INSERT INTO `yawnc_tags` (`id`, `parent_id`, `lft`, `rgt`, `level`, `path`, `title`, `alias`, `note`, `description`, `published`, `checked_out`, `checked_out_time`, `access`, `params`, `metadesc`, `metakey`, `metadata`, `created_user_id`, `created_time`, `created_by_alias`, `modified_user_id`, `modified_time`, `images`, `urls`, `hits`, `language`, `version`, `publish_up`, `publish_down`) VALUES
(1, 0, 0, 1, 0, '', 'ROOT', 'root', '', '', 1, 0, '0000-00-00 00:00:00', 1, '', '', '', '', 42, '2011-01-01 00:00:01', '', 0, '0000-00-00 00:00:00', '', '', 0, '*', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_template_styles`
--

CREATE TABLE IF NOT EXISTS `yawnc_template_styles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `template` varchar(50) NOT NULL DEFAULT '',
  `client_id` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `home` char(7) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_template` (`template`),
  KEY `idx_home` (`home`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `yawnc_template_styles`
--

INSERT INTO `yawnc_template_styles` (`id`, `template`, `client_id`, `home`, `title`, `params`) VALUES
(4, 'beez3', 0, '0', 'Beez3 - Default', '{"wrapperSmall":"53","wrapperLarge":"72","logo":"images\\/joomla_black.png","sitetitle":"Joomla!","sitedescription":"Open Source Content Management","navposition":"left","templatecolor":"personal","html5":"0"}'),
(5, 'hathor', 1, '0', 'Hathor - Default', '{"showSiteName":"0","colourChoice":"","boldText":"0"}'),
(7, 'protostar', 0, '0', 'protostar - Default', '{"templateColor":"","logoFile":"","googleFont":"1","googleFontName":"Open+Sans","fluidContainer":"0"}'),
(8, 'isis', 1, '1', 'isis - Default', '{"templateColor":"","logoFile":""}'),
(9, 'lavka', 0, '1', 'lavka', '{"config":""}');

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_ucm_base`
--

CREATE TABLE IF NOT EXISTS `yawnc_ucm_base` (
  `ucm_id` int(10) unsigned NOT NULL,
  `ucm_item_id` int(10) NOT NULL,
  `ucm_type_id` int(11) NOT NULL,
  `ucm_language_id` int(11) NOT NULL,
  PRIMARY KEY (`ucm_id`),
  KEY `idx_ucm_item_id` (`ucm_item_id`),
  KEY `idx_ucm_type_id` (`ucm_type_id`),
  KEY `idx_ucm_language_id` (`ucm_language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_ucm_content`
--

CREATE TABLE IF NOT EXISTS `yawnc_ucm_content` (
  `core_content_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `core_type_alias` varchar(255) NOT NULL DEFAULT '' COMMENT 'FK to the content types table',
  `core_title` varchar(255) NOT NULL,
  `core_alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `core_body` mediumtext NOT NULL,
  `core_state` tinyint(1) NOT NULL DEFAULT '0',
  `core_checked_out_time` varchar(255) NOT NULL DEFAULT '',
  `core_checked_out_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `core_access` int(10) unsigned NOT NULL DEFAULT '0',
  `core_params` text NOT NULL,
  `core_featured` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `core_metadata` varchar(2048) NOT NULL COMMENT 'JSON encoded metadata properties.',
  `core_created_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `core_created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `core_created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_modified_user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Most recent user that modified',
  `core_modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_language` char(7) NOT NULL,
  `core_publish_up` datetime NOT NULL,
  `core_publish_down` datetime NOT NULL,
  `core_content_item_id` int(10) unsigned DEFAULT NULL COMMENT 'ID from the individual type table',
  `asset_id` int(10) unsigned DEFAULT NULL COMMENT 'FK to the #__assets table.',
  `core_images` text NOT NULL,
  `core_urls` text NOT NULL,
  `core_hits` int(10) unsigned NOT NULL DEFAULT '0',
  `core_version` int(10) unsigned NOT NULL DEFAULT '1',
  `core_ordering` int(11) NOT NULL DEFAULT '0',
  `core_metakey` text NOT NULL,
  `core_metadesc` text NOT NULL,
  `core_catid` int(10) unsigned NOT NULL DEFAULT '0',
  `core_xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `core_type_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`core_content_id`),
  KEY `tag_idx` (`core_state`,`core_access`),
  KEY `idx_access` (`core_access`),
  KEY `idx_alias` (`core_alias`),
  KEY `idx_language` (`core_language`),
  KEY `idx_title` (`core_title`),
  KEY `idx_modified_time` (`core_modified_time`),
  KEY `idx_created_time` (`core_created_time`),
  KEY `idx_content_type` (`core_type_alias`),
  KEY `idx_core_modified_user_id` (`core_modified_user_id`),
  KEY `idx_core_checked_out_user_id` (`core_checked_out_user_id`),
  KEY `idx_core_created_user_id` (`core_created_user_id`),
  KEY `idx_core_type_id` (`core_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Contains core content data in name spaced fields' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_ucm_history`
--

CREATE TABLE IF NOT EXISTS `yawnc_ucm_history` (
  `version_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ucm_item_id` int(10) unsigned NOT NULL,
  `ucm_type_id` int(10) unsigned NOT NULL,
  `version_note` varchar(255) NOT NULL DEFAULT '' COMMENT 'Optional version name',
  `save_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `character_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of characters in this version.',
  `sha1_hash` varchar(50) NOT NULL DEFAULT '' COMMENT 'SHA1 hash of the version_data column.',
  `version_data` mediumtext NOT NULL COMMENT 'json-encoded string of version data',
  `keep_forever` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=auto delete; 1=keep',
  PRIMARY KEY (`version_id`),
  KEY `idx_ucm_item_id` (`ucm_type_id`,`ucm_item_id`),
  KEY `idx_save_date` (`save_date`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=49 ;

--
-- Дамп данных таблицы `yawnc_ucm_history`
--

INSERT INTO `yawnc_ucm_history` (`version_id`, `ucm_item_id`, `ucm_type_id`, `version_note`, `save_date`, `editor_user_id`, `character_count`, `sha1_hash`, `version_data`, `keep_forever`) VALUES
(1, 1, 1, '', '2015-09-10 16:18:34', 595, 2438, 'ccc54fd8adb8640aba9cccc533ac9901729f2e0a', '{"id":1,"asset_id":54,"title":"\\u0418\\u043a\\u043e\\u043d\\u044b \\u0425\\u0440\\u0430\\u043c\\u043e\\u0432\\u044b\\u0435","alias":"ikony-khramovye","introtext":"<p>\\u0432\\u0430\\u0440\\u043f\\u0446\\u0440\\u0430\\u043f\\u0443\\u043e\\u043b\\u0430\\u0444\\u0446\\u0440\\u0430\\u043b\\u0444\\u0443\\u043e\\u0430\\u0434\\u043b\\u0444\\u0446\\u043b\\u0430\\u0434\\u0436\\u0444\\u0446\\u043b\\u0430\\u0434\\u0436\\u0444\\u0446\\u043b\\u0434\\u0436\\u0430\\u0444\\u0446\\u043b\\u0430\\u0434\\u0436\\u0444\\u0446\\u043b\\u0430\\u0434\\u0436\\u0439\\u0444\\u0446\\u0430<\\/p>\\r\\n<p>\\u0430\\u0440\\u0432\\u0444\\u0446\\u0433\\u0448\\u0430\\u0444\\u0446\\u0433\\u0448\\u0444\\u0446\\u0448\\u0430\\u0440\\u0444\\u0446\\u0433\\u0448\\u0430\\u0440\\u0444\\u0446\\u0433\\u0448\\u0430\\u0440\\u0432\\u0444\\u0446\\u0433\\u0448\\u0430\\u0440\\u0444\\u0446\\u0433\\u0448\\u0430\\u0440\\u0444\\u0446\\u0433\\u0448\\u0430\\u0440\\u0444\\u0446\\u0433\\u0448\\u0430\\u0440\\u0444\\u0446\\u0433<\\/p>","fulltext":"","state":1,"catid":"2","created":"2015-09-10 16:18:34","created_by":"595","created_by_alias":"","modified":"2015-09-10 16:18:34","modified_by":null,"checked_out":null,"checked_out_time":null,"publish_up":"2015-09-10 16:18:34","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":1,"ordering":null,"metakey":"","metadesc":"","access":"1","hits":null,"metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(2, 1, 1, '', '2015-09-16 13:50:33', 595, 2457, '48a64d68d35336f822185d6129067eb66593f5ba', '{"id":1,"asset_id":"54","title":"\\u0418\\u043a\\u043e\\u043d\\u044b \\u0425\\u0440\\u0430\\u043c\\u043e\\u0432\\u044b\\u0435","alias":"ikony-khramovye","introtext":"<p>\\u0432\\u0430\\u0440\\u043f\\u0446\\u0440\\u0430\\u043f\\u0443\\u043e\\u043b\\u0430\\u0444\\u0446\\u0440\\u0430\\u043b\\u0444\\u0443\\u043e\\u0430\\u0434\\u043b\\u0444\\u0446\\u043b\\u0430\\u0434\\u0436\\u0444\\u0446\\u043b\\u0430\\u0434\\u0436\\u0444\\u0446\\u043b\\u0434\\u0436\\u0430\\u0444\\u0446\\u043b\\u0430\\u0434\\u0436\\u0444\\u0446\\u043b\\u0430\\u0434\\u0436\\u0439\\u0444\\u0446\\u0430<\\/p>\\r\\n<p>\\u0430\\u0440\\u0432\\u0444\\u0446\\u0433\\u0448\\u0430\\u0444\\u0446\\u0433\\u0448\\u0444\\u0446\\u0448\\u0430\\u0440\\u0444\\u0446\\u0433\\u0448\\u0430\\u0440\\u0444\\u0446\\u0433\\u0448\\u0430\\u0440\\u0432\\u0444\\u0446\\u0433\\u0448\\u0430\\u0440\\u0444\\u0446\\u0433\\u0448\\u0430\\u0440\\u0444\\u0446\\u0433\\u0448\\u0430\\u0440\\u0444\\u0446\\u0433\\u0448\\u0430\\u0440\\u0444\\u0446\\u0433<\\/p>","fulltext":"","state":1,"catid":"2","created":"2015-09-10 16:18:34","created_by":"595","created_by_alias":"","modified":"2015-09-16 13:50:33","modified_by":"595","checked_out":"595","checked_out_time":"2015-09-16 13:38:33","publish_up":"2015-09-10 16:18:34","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":2,"ordering":"0","metakey":"","metadesc":"","access":"1","hits":"0","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"1","language":"*","xreference":""}', 0),
(3, 2, 1, '', '2015-09-16 13:53:12', 595, 1830, 'a23b66a4e2a91a946d273c0d539dd07545bb1768', '{"id":2,"asset_id":61,"title":"\\u0421\\u0432\\u0435\\u0447\\u0438 \\u0432\\u043e\\u0441\\u043a\\u043e\\u0432\\u044b\\u0435","alias":"svechi-voskovye","introtext":"\\u043f\\u0443\\u044b\\u0432\\u043f\\u043c\\u0444\\u0443\\u0430\\u043f\\u0444\\u0446\\u0430\\u0444\\u0446\\u0430","fulltext":"","state":1,"catid":"2","created":"2015-09-16 13:53:12","created_by":"595","created_by_alias":"","modified":"2015-09-16 13:53:12","modified_by":null,"checked_out":null,"checked_out_time":null,"publish_up":"2015-09-16 13:53:12","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":1,"ordering":null,"metakey":"","metadesc":"","access":"1","hits":null,"metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"1","language":"*","xreference":""}', 0),
(4, 3, 1, '', '2015-09-16 13:54:09', 595, 2000, '57d8620b237fa9529f86232144dfe02462a891e7', '{"id":3,"asset_id":62,"title":"\\u041a\\u043d\\u0438\\u0433\\u0438, \\u043f\\u043e\\u043b\\u0438\\u0433\\u0440\\u0430\\u0444\\u0438\\u044f, \\u043c\\u043e\\u043b\\u0438\\u0442\\u0432\\u044b \\u043d\\u0430 \\u0431\\u0435\\u0440\\u0435\\u0441\\u0442\\u0435","alias":"knigi-poligrafiya-molitvy-na-bereste","introtext":"\\u043f\\u044b\\u043a\\u043f\\u0444\\u044b\\u0446\\u043f\\u044f\\u043f\\u044f\\u044f\\u043f\\u044f\\u043f\\u044f\\u043a\\u043f\\u043a\\u043f\\u043a\\u043f","fulltext":"","state":1,"catid":"2","created":"2015-09-16 13:54:09","created_by":"595","created_by_alias":"","modified":"2015-09-16 13:54:09","modified_by":null,"checked_out":null,"checked_out_time":null,"publish_up":"2015-09-16 13:54:09","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":1,"ordering":null,"metakey":"","metadesc":"","access":"1","hits":null,"metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"1","language":"*","xreference":""}', 0),
(5, 8, 5, '', '2015-09-16 14:40:19', 595, 572, '996c705237a0493147617a0543e3342de5ede390', '{"id":8,"asset_id":63,"parent_id":"1","lft":"11","rgt":12,"level":1,"path":null,"extension":"com_content","title":"\\u041b\\u0430\\u0432\\u043a\\u0430","alias":"lavka","note":"","description":"","published":"1","checked_out":null,"checked_out_time":null,"access":"1","params":"{\\"category_layout\\":\\"\\",\\"image\\":\\"\\",\\"image_alt\\":\\"\\"}","metadesc":"","metakey":"","metadata":"{\\"author\\":\\"\\",\\"robots\\":\\"\\"}","created_user_id":"595","created_time":"2015-09-16 14:40:19","modified_user_id":null,"modified_time":"2015-09-16 14:40:19","hits":"0","language":"*","version":null}', 0),
(6, 3, 1, '', '2015-09-16 14:40:40', 595, 2019, 'aa9c5ccaa8c266ce314b172ed1a1a5ec3938a3f1', '{"id":3,"asset_id":"62","title":"\\u041a\\u043d\\u0438\\u0433\\u0438, \\u043f\\u043e\\u043b\\u0438\\u0433\\u0440\\u0430\\u0444\\u0438\\u044f, \\u043c\\u043e\\u043b\\u0438\\u0442\\u0432\\u044b \\u043d\\u0430 \\u0431\\u0435\\u0440\\u0435\\u0441\\u0442\\u0435","alias":"knigi-poligrafiya-molitvy-na-bereste","introtext":"\\u043f\\u044b\\u043a\\u043f\\u0444\\u044b\\u0446\\u043f\\u044f\\u043f\\u044f\\u044f\\u043f\\u044f\\u043f\\u044f\\u043a\\u043f\\u043a\\u043f\\u043a\\u043f","fulltext":"","state":1,"catid":"8","created":"2015-09-16 13:54:09","created_by":"595","created_by_alias":"","modified":"2015-09-16 14:40:40","modified_by":"595","checked_out":"595","checked_out_time":"2015-09-16 14:40:31","publish_up":"2015-09-16 13:54:09","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":3,"ordering":"0","metakey":"","metadesc":"","access":"1","hits":"0","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"1","language":"*","xreference":""}', 0),
(7, 2, 1, '', '2015-09-16 14:41:02', 595, 1851, 'b5a630d443ed6d82877077194b4bf740479b8f70', '{"id":"2","asset_id":"61","title":"\\u0421\\u0432\\u0435\\u0447\\u0438 \\u0432\\u043e\\u0441\\u043a\\u043e\\u0432\\u044b\\u0435","alias":"svechi-voskovye","introtext":"\\u043f\\u0443\\u044b\\u0432\\u043f\\u043c\\u0444\\u0443\\u0430\\u043f\\u0444\\u0446\\u0430\\u0444\\u0446\\u0430","fulltext":"","state":"1","catid":8,"created":"2015-09-16 13:53:12","created_by":"595","created_by_alias":"","modified":"2015-09-16 14:41:02","modified_by":"595","checked_out":"0","checked_out_time":"0000-00-00 00:00:00","publish_up":"2015-09-16 13:53:12","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":"2","ordering":"1","metakey":"","metadesc":"","access":"1","hits":"0","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"1","language":"*","xreference":""}', 0),
(8, 1, 1, '', '2015-09-16 14:41:02', 595, 2459, 'bcc2c39bd02fd10351c9656a6189688aafc2b49f', '{"id":"1","asset_id":"54","title":"\\u0418\\u043a\\u043e\\u043d\\u044b \\u0425\\u0440\\u0430\\u043c\\u043e\\u0432\\u044b\\u0435","alias":"ikony-khramovye","introtext":"<p>\\u0432\\u0430\\u0440\\u043f\\u0446\\u0440\\u0430\\u043f\\u0443\\u043e\\u043b\\u0430\\u0444\\u0446\\u0440\\u0430\\u043b\\u0444\\u0443\\u043e\\u0430\\u0434\\u043b\\u0444\\u0446\\u043b\\u0430\\u0434\\u0436\\u0444\\u0446\\u043b\\u0430\\u0434\\u0436\\u0444\\u0446\\u043b\\u0434\\u0436\\u0430\\u0444\\u0446\\u043b\\u0430\\u0434\\u0436\\u0444\\u0446\\u043b\\u0430\\u0434\\u0436\\u0439\\u0444\\u0446\\u0430<\\/p>\\r\\n<p>\\u0430\\u0440\\u0432\\u0444\\u0446\\u0433\\u0448\\u0430\\u0444\\u0446\\u0433\\u0448\\u0444\\u0446\\u0448\\u0430\\u0440\\u0444\\u0446\\u0433\\u0448\\u0430\\u0440\\u0444\\u0446\\u0433\\u0448\\u0430\\u0440\\u0432\\u0444\\u0446\\u0433\\u0448\\u0430\\u0440\\u0444\\u0446\\u0433\\u0448\\u0430\\u0440\\u0444\\u0446\\u0433\\u0448\\u0430\\u0440\\u0444\\u0446\\u0433\\u0448\\u0430\\u0440\\u0444\\u0446\\u0433<\\/p>","fulltext":"","state":"1","catid":8,"created":"2015-09-10 16:18:34","created_by":"595","created_by_alias":"","modified":"2015-09-16 14:41:02","modified_by":"595","checked_out":"0","checked_out_time":"0000-00-00 00:00:00","publish_up":"2015-09-10 16:18:34","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":"2","ordering":"2","metakey":"","metadesc":"","access":"1","hits":"0","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"1","language":"*","xreference":""}', 0),
(9, 2, 1, '', '2015-09-16 14:41:48', 595, 1851, '6c9e66abf66ac7380e1208b9c64e883dd21b07e0', '{"id":"2","asset_id":"61","title":"\\u0421\\u0432\\u0435\\u0447\\u0438 \\u0432\\u043e\\u0441\\u043a\\u043e\\u0432\\u044b\\u0435","alias":"svechi-voskovye","introtext":"\\u043f\\u0443\\u044b\\u0432\\u043f\\u043c\\u0444\\u0443\\u0430\\u043f\\u0444\\u0446\\u0430\\u0444\\u0446\\u0430","fulltext":"","state":"1","catid":2,"created":"2015-09-16 13:53:12","created_by":"595","created_by_alias":"","modified":"2015-09-16 14:41:48","modified_by":"595","checked_out":"0","checked_out_time":"0000-00-00 00:00:00","publish_up":"2015-09-16 13:53:12","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":"2","ordering":"1","metakey":"","metadesc":"","access":"1","hits":"0","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"1","language":"*","xreference":""}', 0),
(10, 1, 1, '', '2015-09-16 14:41:48', 595, 2459, '4e24a307960fa3d21099e0306f054ef8a1cdbca7', '{"id":"1","asset_id":"54","title":"\\u0418\\u043a\\u043e\\u043d\\u044b \\u0425\\u0440\\u0430\\u043c\\u043e\\u0432\\u044b\\u0435","alias":"ikony-khramovye","introtext":"<p>\\u0432\\u0430\\u0440\\u043f\\u0446\\u0440\\u0430\\u043f\\u0443\\u043e\\u043b\\u0430\\u0444\\u0446\\u0440\\u0430\\u043b\\u0444\\u0443\\u043e\\u0430\\u0434\\u043b\\u0444\\u0446\\u043b\\u0430\\u0434\\u0436\\u0444\\u0446\\u043b\\u0430\\u0434\\u0436\\u0444\\u0446\\u043b\\u0434\\u0436\\u0430\\u0444\\u0446\\u043b\\u0430\\u0434\\u0436\\u0444\\u0446\\u043b\\u0430\\u0434\\u0436\\u0439\\u0444\\u0446\\u0430<\\/p>\\r\\n<p>\\u0430\\u0440\\u0432\\u0444\\u0446\\u0433\\u0448\\u0430\\u0444\\u0446\\u0433\\u0448\\u0444\\u0446\\u0448\\u0430\\u0440\\u0444\\u0446\\u0433\\u0448\\u0430\\u0440\\u0444\\u0446\\u0433\\u0448\\u0430\\u0440\\u0432\\u0444\\u0446\\u0433\\u0448\\u0430\\u0440\\u0444\\u0446\\u0433\\u0448\\u0430\\u0440\\u0444\\u0446\\u0433\\u0448\\u0430\\u0440\\u0444\\u0446\\u0433\\u0448\\u0430\\u0440\\u0444\\u0446\\u0433<\\/p>","fulltext":"","state":"1","catid":2,"created":"2015-09-10 16:18:34","created_by":"595","created_by_alias":"","modified":"2015-09-16 14:41:48","modified_by":"595","checked_out":"0","checked_out_time":"0000-00-00 00:00:00","publish_up":"2015-09-10 16:18:34","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":"2","ordering":"2","metakey":"","metadesc":"","access":"1","hits":"0","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"1","language":"*","xreference":""}', 0),
(11, 2, 1, '', '2015-09-16 14:43:22', 595, 1849, '09a69803516da2e8f88958b921c0a374fa1f658d', '{"id":2,"asset_id":"61","title":"\\u0421\\u0432\\u0435\\u0447\\u0438 \\u0432\\u043e\\u0441\\u043a\\u043e\\u0432\\u044b\\u0435","alias":"svechi-voskovye","introtext":"\\u043f\\u0443\\u044b\\u0432\\u043f\\u043c\\u0444\\u0443\\u0430\\u043f\\u0444\\u0446\\u0430\\u0444\\u0446\\u0430","fulltext":"","state":1,"catid":"2","created":"2015-09-16 13:53:12","created_by":"595","created_by_alias":"","modified":"2015-09-16 14:43:22","modified_by":"595","checked_out":"595","checked_out_time":"2015-09-16 14:43:16","publish_up":"2015-09-16 13:53:12","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":3,"ordering":"1","metakey":"","metadesc":"","access":"1","hits":"0","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(12, 9, 5, '', '2015-09-18 12:15:24', 595, 586, '4438cc4373b7eee9d5793e7a443b45a1aa6c90e5', '{"id":9,"asset_id":65,"parent_id":"1","lft":"13","rgt":14,"level":1,"path":null,"extension":"com_content","title":"\\u041d\\u043e\\u0432\\u043e\\u0441\\u0442\\u0438","alias":"novosti","note":"","description":"","published":"1","checked_out":null,"checked_out_time":null,"access":"1","params":"{\\"category_layout\\":\\"\\",\\"image\\":\\"\\",\\"image_alt\\":\\"\\"}","metadesc":"","metakey":"","metadata":"{\\"author\\":\\"\\",\\"robots\\":\\"\\"}","created_user_id":"595","created_time":"2015-09-18 12:15:24","modified_user_id":null,"modified_time":"2015-09-18 12:15:24","hits":"0","language":"*","version":null}', 0),
(13, 4, 1, '', '2015-09-18 12:16:59', 595, 1819, 'd802ad0c4992b02dc199a032333417b6cdc46111', '{"id":4,"asset_id":66,"title":"\\"\\u0412\\u043e\\u0434\\u044b, \\u0442\\u0435\\u043a\\u0443\\u0449\\u0438\\u0435 \\u0432 \\u0436\\u0438\\u0437\\u043d\\u044c \\u0432\\u0435\\u0447\\u043d\\u0443\\u044e\\"","alias":"vody-tekushchie-v-zhizn-vechnuyu","introtext":"","fulltext":"","state":1,"catid":"9","created":"2015-09-18 12:16:59","created_by":"595","created_by_alias":"","modified":"2015-09-18 12:16:59","modified_by":null,"checked_out":null,"checked_out_time":null,"publish_up":"2015-09-18 12:16:59","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":1,"ordering":null,"metakey":"","metadesc":"","access":"1","hits":null,"metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(14, 5, 1, '', '2015-09-18 12:17:42', 595, 1930, '3d57ccc9f4ce67e3ac8bc3a511fd131034e51344', '{"id":5,"asset_id":67,"title":"\\u041f\\u0440\\u0430\\u0437\\u0434\\u043d\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u041f\\u0430\\u0441\\u0445\\u0438 \\u0425\\u0440\\u0438\\u0441\\u0442\\u043e\\u0432\\u043e\\u0439 \\u0432 \\u0410\\u043b\\u0435\\u043a\\u0441\\u0430\\u043d\\u0434\\u0440\\u043e\\u0432\\u043a\\u0435","alias":"prazdnovanie-paskhi-khristovoj-v-aleksandrovke","introtext":"","fulltext":"","state":1,"catid":"9","created":"2015-09-18 12:17:42","created_by":"595","created_by_alias":"","modified":"2015-09-18 12:17:42","modified_by":null,"checked_out":null,"checked_out_time":null,"publish_up":"2015-09-18 12:17:42","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":1,"ordering":null,"metakey":"","metadesc":"","access":"1","hits":null,"metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(15, 6, 1, '', '2015-09-18 12:18:05', 595, 1806, 'c4e1339e2d3b5b74430dc43405cf347cc7d5dff1', '{"id":6,"asset_id":68,"title":"\\u041c\\u0430\\u0441\\u043b\\u0435\\u043d\\u0438\\u0446\\u0430 \\u0432 \\u0410\\u043b\\u0435\\u043a\\u0441\\u0430\\u043d\\u0434\\u0440\\u043e\\u0432\\u043a\\u0435","alias":"maslenitsa-v-aleksandrovke","introtext":"","fulltext":"","state":1,"catid":"9","created":"2015-09-18 12:18:05","created_by":"595","created_by_alias":"","modified":"2015-09-18 12:18:05","modified_by":null,"checked_out":null,"checked_out_time":null,"publish_up":"2015-09-18 12:18:05","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":1,"ordering":null,"metakey":"","metadesc":"","access":"1","hits":null,"metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(16, 7, 1, '', '2015-09-18 12:19:02', 595, 1954, 'f38938a14938f1eb7eae9ac347c4513ac8b24b3c', '{"id":7,"asset_id":69,"title":"XIII \\u0421\\u0440\\u0435\\u0442\\u0435\\u043d\\u0441\\u043a\\u0438\\u0439 \\u0444\\u0435\\u0441\\u0442\\u0438\\u0432\\u0430\\u043b\\u044c \\u0434\\u0443\\u0445\\u043e\\u0432\\u043d\\u043e\\u0439 \\u0438 \\u043d\\u0430\\u0440\\u043e\\u0434\\u043d\\u043e\\u0439 \\u043c\\u0443\\u0437\\u044b\\u043a\\u0438","alias":"xiii-sretenskij-festival-dukhovnoj-i-narodnoj-muzyki","introtext":"","fulltext":"","state":1,"catid":"9","created":"2015-09-18 12:19:02","created_by":"595","created_by_alias":"","modified":"2015-09-18 12:19:02","modified_by":null,"checked_out":null,"checked_out_time":null,"publish_up":"2015-09-18 12:19:02","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":1,"ordering":null,"metakey":"","metadesc":"","access":"1","hits":null,"metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(18, 4, 1, '', '2015-09-18 12:37:35', 595, 1985, '6b8d808cf75705edc71485f400096f3b75d8549f', '{"id":4,"asset_id":"66","title":"\\"\\u0412\\u043e\\u0434\\u044b, \\u0442\\u0435\\u043a\\u0443\\u0449\\u0438\\u0435 \\u0432 \\u0436\\u0438\\u0437\\u043d\\u044c \\u0432\\u0435\\u0447\\u043d\\u0443\\u044e\\"","alias":"vody-tekushchie-v-zhizn-vechnuyu","introtext":"\\"\\u0412\\u043e\\u0434\\u044b, \\u0442\\u0435\\u043a\\u0443\\u0449\\u0438\\u0435 \\u0432 \\u0436\\u0438\\u0437\\u043d\\u044c \\u0432\\u0435\\u0447\\u043d\\u0443\\u044e\\"","fulltext":"","state":1,"catid":"9","created":"2015-09-18 12:16:59","created_by":"595","created_by_alias":"","modified":"2015-09-18 12:37:35","modified_by":"595","checked_out":"595","checked_out_time":"2015-09-18 12:37:23","publish_up":"2015-09-18 12:16:59","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":2,"ordering":"4","metakey":"","metadesc":"","access":"1","hits":"0","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(19, 4, 1, '', '2015-09-18 12:49:00', 595, 1838, 'c2e5fc9794ebb59ec5b6debbf6c1400f5a904542', '{"id":4,"asset_id":"66","title":"\\"\\u0412\\u043e\\u0434\\u044b, \\u0442\\u0435\\u043a\\u0443\\u0449\\u0438\\u0435 \\u0432 \\u0436\\u0438\\u0437\\u043d\\u044c \\u0432\\u0435\\u0447\\u043d\\u0443\\u044e\\"","alias":"vody-tekushchie-v-zhizn-vechnuyu","introtext":"","fulltext":"","state":1,"catid":"9","created":"2015-09-18 12:16:59","created_by":"595","created_by_alias":"","modified":"2015-09-18 12:49:00","modified_by":"595","checked_out":"595","checked_out_time":"2015-09-18 12:48:52","publish_up":"2015-09-18 12:16:59","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":3,"ordering":"4","metakey":"","metadesc":"","access":"1","hits":"0","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(20, 4, 1, '', '2015-09-18 12:50:41', 595, 1846, '8e227d6554376760e480ef8b6d3e5c082221a96c', '{"id":4,"asset_id":"66","title":"\\"\\u0412\\u043e\\u0434\\u044b, \\u0442\\u0435\\u043a\\u0443\\u0449\\u0438\\u0435 \\u0432 \\u0436\\u0438\\u0437\\u043d\\u044c \\u0432\\u0435\\u0447\\u043d\\u0443\\u044e\\"","alias":"vody-tekushchie-v-zhizn-vechnuyu","introtext":"\\r\\n\\r\\n","fulltext":"","state":1,"catid":"9","created":"2015-09-18 12:16:59","created_by":"595","created_by_alias":"","modified":"2015-09-18 12:50:41","modified_by":"595","checked_out":"595","checked_out_time":"2015-09-18 12:50:17","publish_up":"2015-09-18 12:16:59","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":6,"ordering":"4","metakey":"","metadesc":"","access":"1","hits":"0","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(21, 4, 1, '', '2015-09-18 12:56:11', 595, 1842, '0745fac3d9167d85c22c9b3ed1f6fe4836715f2c', '{"id":4,"asset_id":"66","title":"\\"\\u0412\\u043e\\u0434\\u044b, \\u0442\\u0435\\u043a\\u0443\\u0449\\u0438\\u0435 \\u0432 \\u0436\\u0438\\u0437\\u043d\\u044c \\u0432\\u0435\\u0447\\u043d\\u0443\\u044e\\"","alias":"vody-tekushchie-v-zhizn-vechnuyu","introtext":"\\r\\n","fulltext":"","state":1,"catid":"9","created":"2015-09-18 12:16:59","created_by":"595","created_by_alias":"","modified":"2015-09-18 12:56:11","modified_by":"595","checked_out":"595","checked_out_time":"2015-09-18 12:50:41","publish_up":"2015-09-18 12:16:59","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":7,"ordering":"4","metakey":"","metadesc":"","access":"1","hits":"0","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(25, 7, 1, '', '2015-09-19 10:00:04', 595, 2235, '60cb86d94e6575a75c6dc2dc073d894d960f5858', '{"id":7,"asset_id":"69","title":"XIII \\u0421\\u0440\\u0435\\u0442\\u0435\\u043d\\u0441\\u043a\\u0438\\u0439 \\u0444\\u0435\\u0441\\u0442\\u0438\\u0432\\u0430\\u043b\\u044c \\u0434\\u0443\\u0445\\u043e\\u0432\\u043d\\u043e\\u0439 \\u0438 \\u043d\\u0430\\u0440\\u043e\\u0434\\u043d\\u043e\\u0439 \\u043c\\u0443\\u0437\\u044b\\u043a\\u0438","alias":"xiii-sretenskij-festival-dukhovnoj-i-narodnoj-muzyki","introtext":"XIII \\u0421\\u0440\\u0435\\u0442\\u0435\\u043d\\u0441\\u043a\\u0438\\u0439 \\u0444\\u0435\\u0441\\u0442\\u0438\\u0432\\u0430\\u043b\\u044c \\u0434\\u0443\\u0445\\u043e\\u0432\\u043d\\u043e\\u0439 \\u0438 \\u043d\\u0430\\u0440\\u043e\\u0434\\u043d\\u043e\\u0439 \\u043c\\u0443\\u0437\\u044b\\u043a\\u0438","fulltext":"","state":1,"catid":"9","created":"2015-09-18 12:19:02","created_by":"595","created_by_alias":"","modified":"2015-09-19 10:00:04","modified_by":"595","checked_out":"595","checked_out_time":"2015-09-19 09:59:46","publish_up":"2015-09-18 12:19:02","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":3,"ordering":"1","metakey":"","metadesc":"","access":"1","hits":"0","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(26, 7, 1, '', '2015-09-19 10:00:23', 595, 1973, 'b3a0350f68ee0ef3c968123f0620a24caa64c922', '{"id":7,"asset_id":"69","title":"XIII \\u0421\\u0440\\u0435\\u0442\\u0435\\u043d\\u0441\\u043a\\u0438\\u0439 \\u0444\\u0435\\u0441\\u0442\\u0438\\u0432\\u0430\\u043b\\u044c \\u0434\\u0443\\u0445\\u043e\\u0432\\u043d\\u043e\\u0439 \\u0438 \\u043d\\u0430\\u0440\\u043e\\u0434\\u043d\\u043e\\u0439 \\u043c\\u0443\\u0437\\u044b\\u043a\\u0438","alias":"xiii-sretenskij-festival-dukhovnoj-i-narodnoj-muzyki","introtext":"","fulltext":"","state":1,"catid":"9","created":"2015-09-18 12:19:02","created_by":"595","created_by_alias":"","modified":"2015-09-19 10:00:23","modified_by":"595","checked_out":"595","checked_out_time":"2015-09-19 10:00:04","publish_up":"2015-09-18 12:19:02","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":4,"ordering":"1","metakey":"","metadesc":"","access":"1","hits":"0","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0);
INSERT INTO `yawnc_ucm_history` (`version_id`, `ucm_item_id`, `ucm_type_id`, `version_note`, `save_date`, `editor_user_id`, `character_count`, `sha1_hash`, `version_data`, `keep_forever`) VALUES
(35, 8, 1, '', '2015-09-19 12:21:52', 595, 2302, 'd8c5018207ab6b746ce6332cfbcafe248d09a7f2', '{"id":8,"asset_id":"70","title":"XIII \\u0421\\u0440\\u0435\\u0442\\u0435\\u043d\\u0441\\u043a\\u0438\\u0439 \\u0444\\u0435\\u0441\\u0442\\u0438\\u0432\\u0430\\u043b\\u044c \\u0434\\u0443\\u0445\\u043e\\u0432\\u043d\\u043e\\u0439 \\u0438 \\u043d\\u0430\\u0440\\u043e\\u0434\\u043d\\u043e\\u0439 \\u043c\\u0443\\u0437\\u044b\\u043a\\u0438","alias":"xiii-sretenskij-festival-dukhovnoj-i-narodnoj-muzyki-2","introtext":"XIII \\u0421\\u0440\\u0435\\u0442\\u0435\\u043d\\u0441\\u043a\\u0438\\u0439 \\u0444\\u0435\\u0441\\u0442\\u0438\\u0432\\u0430\\u043b\\u044c \\u0434\\u0443\\u0445\\u043e\\u0432\\u043d\\u043e\\u0439 \\u0438 \\u043d\\u0430\\u0440\\u043e\\u0434\\u043d\\u043e\\u0439 \\u043c\\u0443\\u0437\\u044b\\u043a\\u0438\\r\\n\\r\\n","fulltext":"","state":1,"catid":"9","created":"2015-09-18 12:19:39","created_by":"595","created_by_alias":"","modified":"2015-09-19 12:21:52","modified_by":"595","checked_out":"595","checked_out_time":"2015-09-19 12:20:48","publish_up":"2015-09-18 12:19:39","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"0\\",\\"link_titles\\":\\"0\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\u043f\\u043e\\u0434\\u0440\\u043e\\u0431\\u043d\\u0435\\u0435\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":19,"ordering":"0","metakey":"","metadesc":"","access":"1","hits":"0","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(36, 8, 1, '', '2015-09-19 12:42:04', 595, 2306, 'f987b7b6018aeaa592dfd54783b80f685cfadca6', '{"id":8,"asset_id":"70","title":"XIII \\u0421\\u0440\\u0435\\u0442\\u0435\\u043d\\u0441\\u043a\\u0438\\u0439 \\u0444\\u0435\\u0441\\u0442\\u0438\\u0432\\u0430\\u043b\\u044c \\u0434\\u0443\\u0445\\u043e\\u0432\\u043d\\u043e\\u0439 \\u0438 \\u043d\\u0430\\u0440\\u043e\\u0434\\u043d\\u043e\\u0439 \\u043c\\u0443\\u0437\\u044b\\u043a\\u0438","alias":"xiii-sretenskij-festival-dukhovnoj-i-narodnoj-muzyki-2","introtext":"<p>XIII \\u0421\\u0440\\u0435\\u0442\\u0435\\u043d\\u0441\\u043a\\u0438\\u0439 \\u0444\\u0435\\u0441\\u0442\\u0438\\u0432\\u0430\\u043b\\u044c \\u0434\\u0443\\u0445\\u043e\\u0432\\u043d\\u043e\\u0439 \\u0438 \\u043d\\u0430\\u0440\\u043e\\u0434\\u043d\\u043e\\u0439 \\u043c\\u0443\\u0437\\u044b\\u043a\\u0438<\\/p>\\r\\n","fulltext":"","state":1,"catid":"9","created":"2015-09-18 12:19:39","created_by":"595","created_by_alias":"","modified":"2015-09-19 12:42:04","modified_by":"595","checked_out":"595","checked_out_time":"2015-09-19 12:41:07","publish_up":"2015-09-18 12:19:39","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"0\\",\\"link_titles\\":\\"0\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\u043f\\u043e\\u0434\\u0440\\u043e\\u0431\\u043d\\u0435\\u0435\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":22,"ordering":"0","metakey":"","metadesc":"","access":"1","hits":"0","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(37, 8, 1, '', '2015-09-19 12:42:24', 595, 2323, 'c4fcf64ba7985a035cc090791a7a4cb9f6103304', '{"id":8,"asset_id":"70","title":"XIII \\u0421\\u0440\\u0435\\u0442\\u0435\\u043d\\u0441\\u043a\\u0438\\u0439 \\u0444\\u0435\\u0441\\u0442\\u0438\\u0432\\u0430\\u043b\\u044c \\u0434\\u0443\\u0445\\u043e\\u0432\\u043d\\u043e\\u0439 \\u0438 \\u043d\\u0430\\u0440\\u043e\\u0434\\u043d\\u043e\\u0439 \\u043c\\u0443\\u0437\\u044b\\u043a\\u0438","alias":"xiii-sretenskij-festival-dukhovnoj-i-narodnoj-muzyki-2","introtext":"<p>XIII \\u0421\\u0440\\u0435\\u0442\\u0435\\u043d\\u0441\\u043a\\u0438\\u0439 \\u0444\\u0435\\u0441\\u0442\\u0438\\u0432\\u0430\\u043b\\u044c \\u0434\\u0443\\u0445\\u043e\\u0432\\u043d\\u043e\\u0439 \\u0438 \\u043d\\u0430\\u0440\\u043e\\u0434\\u043d\\u043e\\u0439 \\u043c\\u0443\\u0437\\u044b\\u043a\\u0438<\\/p>\\r\\n","fulltext":"\\r\\nghbdnsjhkjlkj","state":1,"catid":"9","created":"2015-09-18 12:19:39","created_by":"595","created_by_alias":"","modified":"2015-09-19 12:42:24","modified_by":"595","checked_out":"595","checked_out_time":"2015-09-19 12:42:04","publish_up":"2015-09-18 12:19:39","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"0\\",\\"link_titles\\":\\"0\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\u043f\\u043e\\u0434\\u0440\\u043e\\u0431\\u043d\\u0435\\u0435\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":23,"ordering":"0","metakey":"","metadesc":"","access":"1","hits":"0","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(38, 8, 1, '', '2015-09-19 12:43:35', 595, 2321, '61f6b28ca034add14ce0da03939109b26c4ccd1a', '{"id":8,"asset_id":"70","title":"XIII \\u0421\\u0440\\u0435\\u0442\\u0435\\u043d\\u0441\\u043a\\u0438\\u0439 \\u0444\\u0435\\u0441\\u0442\\u0438\\u0432\\u0430\\u043b\\u044c \\u0434\\u0443\\u0445\\u043e\\u0432\\u043d\\u043e\\u0439 \\u0438 \\u043d\\u0430\\u0440\\u043e\\u0434\\u043d\\u043e\\u0439 \\u043c\\u0443\\u0437\\u044b\\u043a\\u0438","alias":"xiii-sretenskij-festival-dukhovnoj-i-narodnoj-muzyki-2","introtext":"<p>XIII \\u0421\\u0440\\u0435\\u0442\\u0435\\u043d\\u0441\\u043a\\u0438\\u0439 \\u0444\\u0435\\u0441\\u0442\\u0438\\u0432\\u0430\\u043b\\u044c \\u0434\\u0443\\u0445\\u043e\\u0432\\u043d\\u043e\\u0439 \\u0438 \\u043d\\u0430\\u0440\\u043e\\u0434\\u043d\\u043e\\u0439 \\u043c\\u0443\\u0437\\u044b\\u043a\\u0438<\\/p>\\r\\n","fulltext":"\\r\\nghbdnsjhkjlkj","state":1,"catid":"9","created":"2015-09-18 12:19:39","created_by":"595","created_by_alias":"","modified":"2015-09-19 12:43:35","modified_by":"595","checked_out":"595","checked_out_time":"2015-09-19 12:42:24","publish_up":"2015-09-18 12:19:39","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\u043f\\u043e\\u0434\\u0440\\u043e\\u0431\\u043d\\u0435\\u0435\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":24,"ordering":"0","metakey":"","metadesc":"","access":"1","hits":"0","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(39, 8, 1, '', '2015-09-19 12:43:58', 595, 2322, '1e898e0128c9cd2227a7eea53701486d51659d73', '{"id":8,"asset_id":"70","title":"XIII \\u0421\\u0440\\u0435\\u0442\\u0435\\u043d\\u0441\\u043a\\u0438\\u0439 \\u0444\\u0435\\u0441\\u0442\\u0438\\u0432\\u0430\\u043b\\u044c \\u0434\\u0443\\u0445\\u043e\\u0432\\u043d\\u043e\\u0439 \\u0438 \\u043d\\u0430\\u0440\\u043e\\u0434\\u043d\\u043e\\u0439 \\u043c\\u0443\\u0437\\u044b\\u043a\\u0438","alias":"xiii-sretenskij-festival-dukhovnoj-i-narodnoj-muzyki-2","introtext":"<p>XIII \\u0421\\u0440\\u0435\\u0442\\u0435\\u043d\\u0441\\u043a\\u0438\\u0439 \\u0444\\u0435\\u0441\\u0442\\u0438\\u0432\\u0430\\u043b\\u044c \\u0434\\u0443\\u0445\\u043e\\u0432\\u043d\\u043e\\u0439 \\u0438 \\u043d\\u0430\\u0440\\u043e\\u0434\\u043d\\u043e\\u0439 \\u043c\\u0443\\u0437\\u044b\\u043a\\u0438<\\/p>\\r\\n","fulltext":"\\r\\nghbdnsjhkjlkj","state":1,"catid":"9","created":"2015-09-18 12:19:39","created_by":"595","created_by_alias":"","modified":"2015-09-19 12:43:58","modified_by":"595","checked_out":"595","checked_out_time":"2015-09-19 12:43:35","publish_up":"2015-09-18 12:19:39","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"0\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\u043f\\u043e\\u0434\\u0440\\u043e\\u0431\\u043d\\u0435\\u0435\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":25,"ordering":"0","metakey":"","metadesc":"","access":"1","hits":"0","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(40, 8, 1, '', '2015-09-19 13:22:58', 595, 2292, 'a4235029f3a95f0796291742ee62c23177c840ed', '{"id":8,"asset_id":"70","title":"XIII \\u0421\\u0440\\u0435\\u0442\\u0435\\u043d\\u0441\\u043a\\u0438\\u0439 \\u0444\\u0435\\u0441\\u0442\\u0438\\u0432\\u0430\\u043b\\u044c \\u0434\\u0443\\u0445\\u043e\\u0432\\u043d\\u043e\\u0439 \\u0438 \\u043d\\u0430\\u0440\\u043e\\u0434\\u043d\\u043e\\u0439 \\u043c\\u0443\\u0437\\u044b\\u043a\\u0438","alias":"xiii-sretenskij-festival-dukhovnoj-i-narodnoj-muzyki-2","introtext":"<p>XIII \\u0421\\u0440\\u0435\\u0442\\u0435\\u043d\\u0441\\u043a\\u0438\\u0439 \\u0444\\u0435\\u0441\\u0442\\u0438\\u0432\\u0430\\u043b\\u044c \\u0434\\u0443\\u0445\\u043e\\u0432\\u043d\\u043e\\u0439 \\u0438 \\u043d\\u0430\\u0440\\u043e\\u0434\\u043d\\u043e\\u0439 \\u043c\\u0443\\u0437\\u044b\\u043a\\u0438<\\/p>\\r\\n","fulltext":"\\r\\nghbdnsjhkjlkj","state":1,"catid":"9","created":"2015-09-18 12:19:39","created_by":"595","created_by_alias":"","modified":"2015-09-19 13:22:58","modified_by":"595","checked_out":"595","checked_out_time":"2015-09-19 13:22:38","publish_up":"2015-09-18 12:19:39","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"0\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"khjgfhfjkhlknlk;lkjkoiub\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":28,"ordering":"0","metakey":"","metadesc":"","access":"1","hits":"0","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(41, 8, 1, '', '2015-09-19 13:23:26', 595, 2264, '56ee564179c738439d6b0895d0bc1b94f733c76e', '{"id":8,"asset_id":"70","title":"XIII \\u0421\\u0440\\u0435\\u0442\\u0435\\u043d\\u0441\\u043a\\u0438\\u0439 \\u0444\\u0435\\u0441\\u0442\\u0438\\u0432\\u0430\\u043b\\u044c \\u0434\\u0443\\u0445\\u043e\\u0432\\u043d\\u043e\\u0439 \\u0438 \\u043d\\u0430\\u0440\\u043e\\u0434\\u043d\\u043e\\u0439 \\u043c\\u0443\\u0437\\u044b\\u043a\\u0438","alias":"xiii-sretenskij-festival-dukhovnoj-i-narodnoj-muzyki-2","introtext":"<p>XIII \\u0421\\u0440\\u0435\\u0442\\u0435\\u043d\\u0441\\u043a\\u0438\\u0439 \\u0444\\u0435\\u0441\\u0442\\u0438\\u0432\\u0430\\u043b\\u044c \\u0434\\u0443\\u0445\\u043e\\u0432\\u043d\\u043e\\u0439 \\u0438 \\u043d\\u0430\\u0440\\u043e\\u0434\\u043d\\u043e\\u0439 \\u043c\\u0443\\u0437\\u044b\\u043a\\u0438<\\/p>","fulltext":"\\r\\nghbdnsjhkjlkj","state":1,"catid":"9","created":"2015-09-18 12:19:39","created_by":"595","created_by_alias":"","modified":"2015-09-19 13:23:26","modified_by":"595","checked_out":"595","checked_out_time":"2015-09-19 13:22:58","publish_up":"2015-09-18 12:19:39","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"0\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":29,"ordering":"0","metakey":"","metadesc":"","access":"1","hits":"0","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(42, 8, 1, '', '2015-09-19 13:24:05', 595, 2256, '1f9619e7b2256d72b161c866b46f314c995bc444', '{"id":8,"asset_id":"70","title":"XIII \\u0421\\u0440\\u0435\\u0442\\u0435\\u043d\\u0441\\u043a\\u0438\\u0439 \\u0444\\u0435\\u0441\\u0442\\u0438\\u0432\\u0430\\u043b\\u044c \\u0434\\u0443\\u0445\\u043e\\u0432\\u043d\\u043e\\u0439 \\u0438 \\u043d\\u0430\\u0440\\u043e\\u0434\\u043d\\u043e\\u0439 \\u043c\\u0443\\u0437\\u044b\\u043a\\u0438","alias":"xiii-sretenskij-festival-dukhovnoj-i-narodnoj-muzyki-2","introtext":"XIII \\u0421\\u0440\\u0435\\u0442\\u0435\\u043d\\u0441\\u043a\\u0438\\u0439 \\u0444\\u0435\\u0441\\u0442\\u0438\\u0432\\u0430\\u043b\\u044c \\u0434\\u0443\\u0445\\u043e\\u0432\\u043d\\u043e\\u0439 \\u0438 \\u043d\\u0430\\u0440\\u043e\\u0434\\u043d\\u043e\\u0439 \\u043c\\u0443\\u0437\\u044b\\u043a\\u0438","fulltext":"\\r\\nghbdnsjhkjlkj","state":1,"catid":"9","created":"2015-09-18 12:19:39","created_by":"595","created_by_alias":"","modified":"2015-09-19 13:24:05","modified_by":"595","checked_out":"595","checked_out_time":"2015-09-19 13:23:26","publish_up":"2015-09-18 12:19:39","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"0\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":30,"ordering":"0","metakey":"","metadesc":"","access":"1","hits":"0","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(43, 8, 1, '', '2015-09-19 13:24:48', 595, 1994, 'a9a210ff8b7328adaad35ffa702eb545d81e7fdb', '{"id":8,"asset_id":"70","title":"XIII \\u0421\\u0440\\u0435\\u0442\\u0435\\u043d\\u0441\\u043a\\u0438\\u0439 \\u0444\\u0435\\u0441\\u0442\\u0438\\u0432\\u0430\\u043b\\u044c \\u0434\\u0443\\u0445\\u043e\\u0432\\u043d\\u043e\\u0439 \\u0438 \\u043d\\u0430\\u0440\\u043e\\u0434\\u043d\\u043e\\u0439 \\u043c\\u0443\\u0437\\u044b\\u043a\\u0438","alias":"xiii-sretenskij-festival-dukhovnoj-i-narodnoj-muzyki-2","introtext":"","fulltext":"\\r\\nghbdnsjhkjlkj","state":1,"catid":"9","created":"2015-09-18 12:19:39","created_by":"595","created_by_alias":"","modified":"2015-09-19 13:24:48","modified_by":"595","checked_out":"595","checked_out_time":"2015-09-19 13:24:05","publish_up":"2015-09-18 12:19:39","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"0\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":31,"ordering":"0","metakey":"","metadesc":"","access":"1","hits":"0","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(44, 8, 1, '', '2015-09-19 13:28:29', 595, 2021, 'be74e4efb8a1e8143073ea3d23193cf8d3acbc2a', '{"id":8,"asset_id":"70","title":"XIII \\u0421\\u0440\\u0435\\u0442\\u0435\\u043d\\u0441\\u043a\\u0438\\u0439 \\u0444\\u0435\\u0441\\u0442\\u0438\\u0432\\u0430\\u043b\\u044c \\u0434\\u0443\\u0445\\u043e\\u0432\\u043d\\u043e\\u0439 \\u0438 \\u043d\\u0430\\u0440\\u043e\\u0434\\u043d\\u043e\\u0439 \\u043c\\u0443\\u0437\\u044b\\u043a\\u0438","alias":"xiii-sretenskij-festival-dukhovnoj-i-narodnoj-muzyki-2","introtext":"<a href=\\"#\\">hgjhhjhj<\\/a>\\r\\nghbdnsjhkjlkj","fulltext":"","state":1,"catid":"9","created":"2015-09-18 12:19:39","created_by":"595","created_by_alias":"","modified":"2015-09-19 13:28:29","modified_by":"595","checked_out":"595","checked_out_time":"2015-09-19 13:24:48","publish_up":"2015-09-18 12:19:39","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"0\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":32,"ordering":"0","metakey":"","metadesc":"","access":"1","hits":"0","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(45, 7, 1, '', '2015-09-19 13:55:20', 595, 2031, '198819901078f88dfdb75a2cdb69bac360304ea1', '{"id":7,"asset_id":"69","title":"XIII \\u0421\\u0440\\u0435\\u0442\\u0435\\u043d\\u0441\\u043a\\u0438\\u0439 \\u0444\\u0435\\u0441\\u0442\\u0438\\u0432\\u0430\\u043b\\u044c \\u0434\\u0443\\u0445\\u043e\\u0432\\u043d\\u043e\\u0439 \\u0438 \\u043d\\u0430\\u0440\\u043e\\u0434\\u043d\\u043e\\u0439 \\u043c\\u0443\\u0437\\u044b\\u043a\\u0438","alias":"xiii-sretenskij-festival-dukhovnoj-i-narodnoj-muzyki","introtext":"","fulltext":"\\r\\n\\u0440\\u043f\\u0440\\u0430\\u043f\\u0440\\u0430\\u0440\\u0438","state":1,"catid":"9","created":"2015-09-18 12:19:02","created_by":"595","created_by_alias":"","modified":"2015-09-19 13:55:20","modified_by":"595","checked_out":"595","checked_out_time":"2015-09-19 13:55:06","publish_up":"2015-09-18 12:19:02","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":6,"ordering":"1","metakey":"","metadesc":"","access":"1","hits":"0","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(46, 6, 1, '', '2015-09-19 13:55:42', 595, 1889, '8283165b0bab452ed5fae7e969a3303958411e0a', '{"id":6,"asset_id":"68","title":"\\u041c\\u0430\\u0441\\u043b\\u0435\\u043d\\u0438\\u0446\\u0430 \\u0432 \\u0410\\u043b\\u0435\\u043a\\u0441\\u0430\\u043d\\u0434\\u0440\\u043e\\u0432\\u043a\\u0435","alias":"maslenitsa-v-aleksandrovke","introtext":"","fulltext":"\\r\\n\\u043e\\u0442\\u043e\\u0440\\u043c\\u0440\\u043f\\u0430\\u0440\\u043f","state":1,"catid":"9","created":"2015-09-18 12:18:05","created_by":"595","created_by_alias":"","modified":"2015-09-19 13:55:42","modified_by":"595","checked_out":"595","checked_out_time":"2015-09-19 13:55:32","publish_up":"2015-09-18 12:18:05","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":2,"ordering":"2","metakey":"","metadesc":"","access":"1","hits":"0","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(47, 5, 1, '', '2015-09-19 13:56:02', 595, 2014, 'bb84a3b98d44dd843105d88af057bc49734cc0ee', '{"id":5,"asset_id":"67","title":"\\u041f\\u0440\\u0430\\u0437\\u0434\\u043d\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u041f\\u0430\\u0441\\u0445\\u0438 \\u0425\\u0440\\u0438\\u0441\\u0442\\u043e\\u0432\\u043e\\u0439 \\u0432 \\u0410\\u043b\\u0435\\u043a\\u0441\\u0430\\u043d\\u0434\\u0440\\u043e\\u0432\\u043a\\u0435","alias":"prazdnovanie-paskhi-khristovoj-v-aleksandrovke","introtext":"","fulltext":"\\r\\n\\u043c\\u043f\\u0430\\u043f\\u0440\\u0430\\u043f\\u0440\\u0438 \\u0440","state":1,"catid":"9","created":"2015-09-18 12:17:42","created_by":"595","created_by_alias":"","modified":"2015-09-19 13:56:02","modified_by":"595","checked_out":"595","checked_out_time":"2015-09-19 13:55:51","publish_up":"2015-09-18 12:17:42","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":2,"ordering":"3","metakey":"","metadesc":"","access":"1","hits":"0","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(48, 4, 1, '', '2015-09-19 13:56:18', 595, 1884, '4b4f1faca71ccd7cbf35ce57f845971f6bc406f1', '{"id":4,"asset_id":"66","title":"\\"\\u0412\\u043e\\u0434\\u044b, \\u0442\\u0435\\u043a\\u0443\\u0449\\u0438\\u0435 \\u0432 \\u0436\\u0438\\u0437\\u043d\\u044c \\u0432\\u0435\\u0447\\u043d\\u0443\\u044e\\"","alias":"vody-tekushchie-v-zhizn-vechnuyu","introtext":"","fulltext":"\\r\\n\\u043b\\u043c\\u043f\\u0430\\u043f\\u0440\\u0438","state":1,"catid":"9","created":"2015-09-18 12:16:59","created_by":"595","created_by_alias":"","modified":"2015-09-19 13:56:18","modified_by":"595","checked_out":"595","checked_out_time":"2015-09-19 13:56:07","publish_up":"2015-09-18 12:16:59","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":8,"ordering":"4","metakey":"","metadesc":"","access":"1","hits":"0","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_updates`
--

CREATE TABLE IF NOT EXISTS `yawnc_updates` (
  `update_id` int(11) NOT NULL AUTO_INCREMENT,
  `update_site_id` int(11) DEFAULT '0',
  `extension_id` int(11) DEFAULT '0',
  `name` varchar(100) DEFAULT '',
  `description` text NOT NULL,
  `element` varchar(100) DEFAULT '',
  `type` varchar(20) DEFAULT '',
  `folder` varchar(20) DEFAULT '',
  `client_id` tinyint(3) DEFAULT '0',
  `version` varchar(32) DEFAULT '',
  `data` text NOT NULL,
  `detailsurl` text NOT NULL,
  `infourl` text NOT NULL,
  `extra_query` varchar(1000) DEFAULT '',
  PRIMARY KEY (`update_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Available Updates' AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `yawnc_updates`
--

INSERT INTO `yawnc_updates` (`update_id`, `update_site_id`, `extension_id`, `name`, `description`, `element`, `type`, `folder`, `client_id`, `version`, `data`, `detailsurl`, `infourl`, `extra_query`) VALUES
(1, 3, 0, 'Bahasa Indonesia', '', 'pkg_id-ID', 'package', '', 0, '3.3.0.2', '', 'http://update.joomla.org/language/details3/id-ID_details.xml', '', ''),
(2, 3, 0, 'Finnish', '', 'pkg_fi-FI', 'package', '', 0, '3.4.2.1', '', 'http://update.joomla.org/language/details3/fi-FI_details.xml', '', ''),
(3, 3, 0, 'Swahili', '', 'pkg_sw-KE', 'package', '', 0, '3.4.4.1', '', 'http://update.joomla.org/language/details3/sw-KE_details.xml', '', ''),
(4, 3, 0, 'Montenegrin', '', 'pkg_srp-ME', 'package', '', 0, '3.3.1.1', '', 'http://update.joomla.org/language/details3/srp-ME_details.xml', '', ''),
(5, 3, 0, 'EnglishCA', '', 'pkg_en-CA', 'package', '', 0, '3.3.6.1', '', 'http://update.joomla.org/language/details3/en-CA_details.xml', '', ''),
(6, 3, 0, 'FrenchCA', '', 'pkg_fr-CA', 'package', '', 0, '3.4.4.2', '', 'http://update.joomla.org/language/details3/fr-CA_details.xml', '', ''),
(7, 3, 0, 'Welsh', '', 'pkg_cy-GB', 'package', '', 0, '3.3.0.2', '', 'http://update.joomla.org/language/details3/cy-GB_details.xml', '', ''),
(8, 3, 0, 'Sinhala', '', 'pkg_si-LK', 'package', '', 0, '3.3.1.1', '', 'http://update.joomla.org/language/details3/si-LK_details.xml', '', ''),
(9, 3, 0, 'Dari Persian', '', 'pkg_prs-AF', 'package', '', 0, '3.4.3.1', '', 'http://update.joomla.org/language/details3/prs-AF_details.xml', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_update_sites`
--

CREATE TABLE IF NOT EXISTS `yawnc_update_sites` (
  `update_site_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT '',
  `type` varchar(20) DEFAULT '',
  `location` text NOT NULL,
  `enabled` int(11) DEFAULT '0',
  `last_check_timestamp` bigint(20) DEFAULT '0',
  `extra_query` varchar(1000) DEFAULT '',
  PRIMARY KEY (`update_site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Update Sites' AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `yawnc_update_sites`
--

INSERT INTO `yawnc_update_sites` (`update_site_id`, `name`, `type`, `location`, `enabled`, `last_check_timestamp`, `extra_query`) VALUES
(1, 'Joomla! Core', 'collection', 'http://update.joomla.org/core/list.xml', 1, 1442319905, ''),
(2, 'Joomla! Extension Directory', 'collection', 'http://update.joomla.org/jed/list.xml', 1, 1442319905, ''),
(3, 'Accredited Joomla! Translations', 'collection', 'http://update.joomla.org/language/translationlist_3.xml', 1, 1442319903, ''),
(4, 'Joomla! Update Component Update Site', 'extension', 'http://update.joomla.org/core/extensions/com_joomlaupdate.xml', 1, 1442319903, '');

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_update_sites_extensions`
--

CREATE TABLE IF NOT EXISTS `yawnc_update_sites_extensions` (
  `update_site_id` int(11) NOT NULL DEFAULT '0',
  `extension_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`update_site_id`,`extension_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Links extensions to update sites';

--
-- Дамп данных таблицы `yawnc_update_sites_extensions`
--

INSERT INTO `yawnc_update_sites_extensions` (`update_site_id`, `extension_id`) VALUES
(1, 700),
(2, 700),
(3, 600),
(3, 10002),
(4, 28);

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_usergroups`
--

CREATE TABLE IF NOT EXISTS `yawnc_usergroups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Adjacency List Reference Id',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `title` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_usergroup_parent_title_lookup` (`parent_id`,`title`),
  KEY `idx_usergroup_title_lookup` (`title`),
  KEY `idx_usergroup_adjacency_lookup` (`parent_id`),
  KEY `idx_usergroup_nested_set_lookup` (`lft`,`rgt`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `yawnc_usergroups`
--

INSERT INTO `yawnc_usergroups` (`id`, `parent_id`, `lft`, `rgt`, `title`) VALUES
(1, 0, 1, 18, 'Public'),
(2, 1, 8, 15, 'Registered'),
(3, 2, 9, 14, 'Author'),
(4, 3, 10, 13, 'Editor'),
(5, 4, 11, 12, 'Publisher'),
(6, 1, 4, 7, 'Manager'),
(7, 6, 5, 6, 'Administrator'),
(8, 1, 16, 17, 'Super Users'),
(9, 1, 2, 3, 'Guest');

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_users`
--

CREATE TABLE IF NOT EXISTS `yawnc_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `username` varchar(150) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `block` tinyint(4) NOT NULL DEFAULT '0',
  `sendEmail` tinyint(4) DEFAULT '0',
  `registerDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastvisitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activation` varchar(100) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  `lastResetTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Date of last password reset',
  `resetCount` int(11) NOT NULL DEFAULT '0' COMMENT 'Count of password resets since lastResetTime',
  `otpKey` varchar(1000) NOT NULL DEFAULT '' COMMENT 'Two factor authentication encrypted keys',
  `otep` varchar(1000) NOT NULL DEFAULT '' COMMENT 'One time emergency passwords',
  `requireReset` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Require user to reset password on next login',
  PRIMARY KEY (`id`),
  KEY `idx_name` (`name`),
  KEY `idx_block` (`block`),
  KEY `username` (`username`),
  KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=596 ;

--
-- Дамп данных таблицы `yawnc_users`
--

INSERT INTO `yawnc_users` (`id`, `name`, `username`, `email`, `password`, `block`, `sendEmail`, `registerDate`, `lastvisitDate`, `activation`, `params`, `lastResetTime`, `resetCount`, `otpKey`, `otep`, `requireReset`) VALUES
(595, 'Super User', 'admin', 'info@malevich.com.ua', '$2y$10$otnXE12FZOUU8.R4CvOeCOLpyaALSf1BjCCdF6EbeNo2nL58vbAJu', 0, 1, '2015-09-10 10:40:56', '2015-09-25 11:28:36', '0', '', '0000-00-00 00:00:00', 0, '', '', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_user_keys`
--

CREATE TABLE IF NOT EXISTS `yawnc_user_keys` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `series` varchar(255) NOT NULL,
  `invalid` tinyint(4) NOT NULL,
  `time` varchar(200) NOT NULL,
  `uastring` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `series` (`series`),
  UNIQUE KEY `series_2` (`series`),
  UNIQUE KEY `series_3` (`series`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_user_notes`
--

CREATE TABLE IF NOT EXISTS `yawnc_user_notes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `subject` varchar(100) NOT NULL DEFAULT '',
  `body` text NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_user_id` int(10) unsigned NOT NULL,
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `review_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_category_id` (`catid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_user_profiles`
--

CREATE TABLE IF NOT EXISTS `yawnc_user_profiles` (
  `user_id` int(11) NOT NULL,
  `profile_key` varchar(100) NOT NULL,
  `profile_value` text NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `idx_user_id_profile_key` (`user_id`,`profile_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Simple user profile storage table';

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_user_usergroup_map`
--

CREATE TABLE IF NOT EXISTS `yawnc_user_usergroup_map` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Foreign Key to #__users.id',
  `group_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Foreign Key to #__usergroups.id',
  PRIMARY KEY (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yawnc_user_usergroup_map`
--

INSERT INTO `yawnc_user_usergroup_map` (`user_id`, `group_id`) VALUES
(595, 8);

-- --------------------------------------------------------

--
-- Структура таблицы `yawnc_viewlevels`
--

CREATE TABLE IF NOT EXISTS `yawnc_viewlevels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `title` varchar(100) NOT NULL DEFAULT '',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rules` varchar(5120) NOT NULL COMMENT 'JSON encoded access control.',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_assetgroup_title_lookup` (`title`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `yawnc_viewlevels`
--

INSERT INTO `yawnc_viewlevels` (`id`, `title`, `ordering`, `rules`) VALUES
(1, 'Public', 0, '[1]'),
(2, 'Registered', 2, '[6,2,8]'),
(3, 'Special', 3, '[6,3,8]'),
(5, 'Guest', 1, '[9]'),
(6, 'Super Users', 4, '[8]');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
